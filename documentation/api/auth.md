---
title: DataWarehouse API Authentication
linkTitle: Authentication
weight: 10
aliases: [/l/datawarehouse/api/auth]
---

DataWarehouse REST API supports two authentication methods:

* [OIDC Authentication]: currently supported only for Red Hat associates,
  authenticated using a cached Kerberos ticket-granting ticket, obtained via `kinit`
* [TokenAuthentication]: a simple token-based HTTP Authentication without expiration,
  handled manually by admins, used by service-accounts

Either authentication method grants the same privileges over records the user
would get while using the website in a browser, which are explained in
[DataWarehouse Authorization and Permission].

In general, it's easier to authenticate to DataWarehouse API using [datawarehouse-api-lib],
a Python client that also facilitates consuming the DataWarehouse API.

## [OIDC Authentication]

1. First, obtain a Kerberos ticket-granting ticket: `kinit user@REALM`

2. Start an authenticated session with DataWarehouse.

If you are not using [datawarehouse-api-lib], you can start the session directly,
with `requests` and `requests-gssapi` installed:

```python
import requests, requests_gssapi
DW_HOST = "https://datawarehouse.cki-project.org/"
session = requests.Session()  # or cki_lib.session.get_session()
session.get(
    f"{DW_HOST}/oidc/authenticate/",
    auth=requests_gssapi.HTTPSPNEGOAuth(mutual_authentication=requests_gssapi.OPTIONAL),
    allow_redirects=True,
).raise_for_status()
# Use this `session` e.g. `session.get(f"{DW_HOST}/api/1/kcidb/checkouts")`
```

Otherwise, with [datawarehouse-api-lib]:

```python
import datawarehouse
DW_HOST = "https://datawarehouse.cki-project.org/"
DW_CLIENT = datawarehouse.Datawarehouse(DW_HOST, token="oidc")
# Use it, e.g. `DW_CLIENT.kcidb.checkouts.list()`
```

![datawarehouse-api-lib doing the OIDC authentication sequence](oidc-api-sequence.png)

## [TokenAuthentication]

DataWarehouse token authentication is intended for service-accounts and implemented via the Django Rest Framework,
as a way to provide one token that never expires for a user.

To use a token, add an `Authorization` header in requests, like so:

```yaml
Authorization: Token 0123456789abcdef
```

If using [datawarehouse-api-lib]:

```python
datawarehouse.Datawarehouse(DW_HOST, token="0123456789abcdef")
```

In order to manage tokens, you need to be an `admin` with `superuser` privileges.

To create a new token in DataWarehouse:

1. Go to the **"Auth token/Tokens"** page in the [admin interface];
2. Click on **"Add token"** on the top right.

To revoke a token:

1. Click on one of the tokens listed on **"Auth token/Tokens"**;
2. Click **"Delete"** on the bottom right.

[DataWarehouse Authorization and Permission]: ../datawarehouse-auth-and-perms.md
[TokenAuthentication]: https://www.django-rest-framework.org/api-guide/authentication/#tokenauthentication
[OIDC Authentication]: https://mozilla-django-oidc.readthedocs.io/en/stable/index.html
[datawarehouse-api-lib]: https://gitlab.com/cki-project/datawarehouse-api-lib.git
[admin interface]: https://datawarehouse.cki-project.org/admin/
