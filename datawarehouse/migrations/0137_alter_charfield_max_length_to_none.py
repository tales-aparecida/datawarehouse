# Generated by Django 5.0.6 on 2024-06-24 19:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('datawarehouse', '0136_alter_kcidbbuild_id_alter_kcidbcheckout_id_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='artifact',
            name='url',
            field=models.URLField(max_length=2048),
        ),
        migrations.AlterField(
            model_name='beakerresource',
            name='fqdn',
            field=models.CharField(blank=True, unique=True),
        ),
        migrations.AlterField(
            model_name='gittree',
            name='name',
            field=models.CharField(),
        ),
        migrations.AlterField(
            model_name='issuekind',
            name='description',
            field=models.CharField(),
        ),
        migrations.AlterField(
            model_name='issueregex',
            name='architecture_match',
            field=models.CharField(default=None, null=True),
        ),
        migrations.AlterField(
            model_name='issueregex',
            name='file_name_match',
            field=models.CharField(default=None, null=True),
        ),
        migrations.AlterField(
            model_name='issueregex',
            name='kpet_tree_name_match',
            field=models.CharField(default=None, null=True),
        ),
        migrations.AlterField(
            model_name='issueregex',
            name='test_name_match',
            field=models.CharField(default=None, null=True),
        ),
        migrations.AlterField(
            model_name='issueregex',
            name='testresult_name_match',
            field=models.CharField(default=None, null=True),
        ),
        migrations.AlterField(
            model_name='issueregex',
            name='tree_match',
            field=models.CharField(default=None, null=True),
        ),
        migrations.AlterField(
            model_name='kcidbbuild',
            name='config_name',
            field=models.CharField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='kcidbbuild',
            name='config_url',
            field=models.URLField(blank=True, max_length=2048, null=True),
        ),
        migrations.AlterField(
            model_name='kcidbbuild',
            name='id',
            field=models.CharField(unique=True),
        ),
        migrations.AlterField(
            model_name='kcidbbuild',
            name='kpet_tree_name',
            field=models.CharField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='kcidbbuild',
            name='package_name',
            field=models.CharField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='kcidbbuild',
            name='package_release',
            field=models.CharField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='kcidbbuild',
            name='package_version',
            field=models.CharField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='kcidbbuild',
            name='testing_skipped_reason',
            field=models.CharField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='kcidbcheckout',
            name='git_commit_hash',
            field=models.CharField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='kcidbcheckout',
            name='git_commit_name',
            field=models.CharField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='kcidbcheckout',
            name='git_repository_branch',
            field=models.CharField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='kcidbcheckout',
            name='git_repository_url',
            field=models.URLField(blank=True, max_length=2048, null=True),
        ),
        migrations.AlterField(
            model_name='kcidbcheckout',
            name='id',
            field=models.CharField(unique=True),
        ),
        migrations.AlterField(
            model_name='kcidbcheckout',
            name='kernel_version',
            field=models.CharField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='kcidbcheckout',
            name='message_id',
            field=models.CharField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='kcidbcheckout',
            name='patchset_hash',
            field=models.CharField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='kcidbcheckout',
            name='source_package_name',
            field=models.CharField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='kcidbcheckout',
            name='source_package_release',
            field=models.CharField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='kcidbcheckout',
            name='source_package_version',
            field=models.CharField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='kcidborigin',
            name='name',
            field=models.CharField(unique=True),
        ),
        migrations.AlterField(
            model_name='kcidbtest',
            name='id',
            field=models.CharField(unique=True),
        ),
        migrations.AlterField(
            model_name='kcidbtestresult',
            name='id',
            field=models.CharField(unique=True),
        ),
        migrations.AlterField(
            model_name='maintainer',
            name='name',
            field=models.CharField(),
        ),
        migrations.AlterField(
            model_name='patch',
            name='subject',
            field=models.CharField(),
        ),
        migrations.AlterField(
            model_name='patch',
            name='url',
            field=models.URLField(max_length=2048),
        ),
        migrations.AlterField(
            model_name='provenancecomponent',
            name='service_name',
            field=models.CharField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='queuedtask',
            name='call_id',
            field=models.CharField(),
        ),
        migrations.AlterField(
            model_name='queuedtask',
            name='name',
            field=models.CharField(),
        ),
        migrations.AlterField(
            model_name='report',
            name='msgid',
            field=models.CharField(unique=True),
        ),
        migrations.AlterField(
            model_name='report',
            name='subject',
            field=models.CharField(),
        ),
        migrations.AlterField(
            model_name='test',
            name='name',
            field=models.CharField(),
        ),
        migrations.AlterField(
            model_name='test',
            name='universal_id',
            field=models.CharField(null=True),
        ),
        migrations.AlterField(
            model_name='testmaintainer',
            name='gitlab_username',
            field=models.CharField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='testmaintainer',
            name='name',
            field=models.CharField(),
        ),
    ]
