"""Cron utils."""
from cki_lib.cronjob import CronJob
from django.db import connection


class CronJobWithDBConnection(CronJob):
    """CronJob with proper connection handling."""

    def run(self, *args, **kwargs):
        """Run job."""
        try:
            self.entrypoint(*args, **kwargs)
        finally:
            connection.close()

    def entrypoint(self, *, last_run_datetime=None):
        """
        Entrypoint for the task.

        This method is called every time the job is run and should
        perform all the work.
        """
        raise NotImplementedError
