"""Pagination."""
import collections
import math

from rest_framework.pagination import LimitOffsetPagination


class DatawarehousePagination(LimitOffsetPagination):
    """DatawarehousePagination class."""

    max_limit = 100


class EndlessPage(collections.abc.Sequence):
    """Page returned by the endless paginator."""

    def __init__(self, object_list, number, paginator):
        """Instantiate a page."""
        self.object_list = object_list
        self.number = number
        self.paginator = paginator

    def __len__(self):
        """Return the number of objects."""
        return len(self.object_list)

    def __getitem__(self, index):
        """Get an object."""
        if not isinstance(index, (slice, int)):
            raise TypeError
        # The object_list is converted to a list so that if it was a QuerySet
        # it won't be a database hit per __getitem__.
        if not isinstance(self.object_list, list):
            self.object_list = list(self.object_list)
        return self.object_list[index]

    def has_next(self):  # pylint: disable=no-self-use
        """Return whether there might be a next page."""
        return len(self.object_list) == self.paginator.per_page

    def has_previous(self):
        """Return whether there is a previous page."""
        return self.number > 1

    def next_page_number(self):
        """Return the number of the next page."""
        return self.number + 1

    def previous_page_number(self):
        """Return the number of the previous page."""
        return self.number - 1

    def start_index(self):
        """Return the 1-based index of the first object on this page."""
        return (self.paginator.per_page * (self.number - 1)) + 1

    def end_index(self):
        """Return the 1-based index of the last object on this page."""
        return self.number * self.paginator.per_page

    def page_range(self):
        """Return information for the pagination view."""
        pages = []
        if self.number > 2:
            pages.append({'label': 1})
        if self.number > 3:
            pages.append({'label': '\u2026', 'class': 'disabled',
                          'style': 'border: none; background: none'})
        if self.has_previous():
            pages.append({'label': self.number - 1})
        pages.append({'label': self.number, 'class': 'active'})
        if self.has_next():
            pages.append({'label': self.number + 1})
            pages.append({'label': '\u2026', 'class': 'disabled',
                          'style': 'border: none; background: none'})
        return pages


class EndlessPaginator:  # pylint: disable=too-few-public-methods
    """Paginator that does not count the query set."""

    def __init__(self, object_list, per_page):
        """Instantiate a paginator."""
        self.object_list = object_list
        self.per_page = int(per_page)

    def get_page(self, number):
        """Return a Page object for the given 1-based page number."""
        # Translate string 'last' into the last page number
        if number == 'last':
            object_count = len(self.object_list) if isinstance(self.object_list, list) else self.object_list.count()
            number = math.ceil(object_count / self.per_page)
        try:
            number = int(number)
        except (TypeError, ValueError):
            number = 1

        if number <= 0:
            number = 1

        bottom = (number - 1) * self.per_page
        top = bottom + self.per_page
        return EndlessPage(self.object_list[bottom:top], number, self)
