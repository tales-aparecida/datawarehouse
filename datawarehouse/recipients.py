"""Logic for recipient rule parsing."""
from datawarehouse import models


def when_always(*_):
    """Evaluate condition for when: always."""
    return True


def when_failed(checkout: models.KCIDBCheckout):
    """Evaluate condition for when: failed."""
    return not checkout.succeeded


def when_failed_tests(checkout: models.KCIDBCheckout):
    """Evaluate condition for when: failed_tests."""
    return checkout.has_untriaged_tests_or_regression_blocking


def failed_tests_maintainers(checkout: models.KCIDBCheckout):
    """Return list of maintainers for failed tests."""
    test_cases = checkout.untriaged_tests_or_regression_blocking.values_list("test_id", flat=True)

    return list(
        models.TestMaintainer.objects.filter(test__in=test_cases).exclude(email=None).values_list("email", flat=True)
    )


def submitter(checkout: models.KCIDBCheckout):
    """
    Return submitter email.

    Returned value is a list to match other parsers output.
    """
    if not checkout.submitter:
        return []
    return [checkout.submitter.email]


class Recipients:
    # pylint: disable=too-few-public-methods
    """Email recipients information."""

    evaluation_rules = {
        "always": when_always,
        "failed": when_failed,
        "failed_tests": when_failed_tests,
    }

    recipients_keywords = {
        'failed_tests_maintainers': failed_tests_maintainers,
        'submitter': submitter,
    }

    def __init__(self, checkout):
        """Init."""
        self.checkout = checkout

    def _parse_recipients(self, recipients):
        """Parse list of recipients."""
        recipients = set(recipients) if isinstance(recipients, list) else {recipients}

        for key, key_parser in self.recipients_keywords.items():
            if key in recipients:
                recipients.remove(key)
                recipients.update(key_parser(self.checkout))

        return list(recipients)

    def _conditions_match(self, rule):
        """Evaluate rule conditions."""
        return self.evaluation_rules[rule["when"]](self.checkout)

    def render(self):
        """Parse reporting rules and return recipients."""
        targets = ['To', 'BCC']
        recipients = {}

        for rule in self.checkout.report_rules:
            if not self._conditions_match(rule):
                continue
            for target in targets:
                if field := rule.get(f'send_{target.lower()}'):
                    for recipient in self._parse_recipients(field):
                        recipients[recipient] = (target, rule.get("report_template", "default"))

        grouped_recipients = {}
        for recipient, (target, template) in recipients.items():
            if template not in grouped_recipients:
                grouped_recipients[template] = {}
            if target not in grouped_recipients[template]:
                grouped_recipients[template][target] = set()
            grouped_recipients[template][target].add(recipient)

        return grouped_recipients
