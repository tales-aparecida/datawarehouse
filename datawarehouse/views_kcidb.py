"""KCIDB Views."""
import datetime

from cki_lib.logger import get_logger
from cki_lib.misc import strtobool
from django.contrib.auth import get_user_model
from django.core.exceptions import PermissionDenied
from django.db.models import Exists
from django.db.models import OuterRef
from django.db.models import Prefetch
from django.db.models import Q
from django.http import Http404
from django.http import HttpRequest
from django.http import HttpResponse
from django.http import HttpResponseBadRequest
from django.http import HttpResponseRedirect
from django.http import QueryDict
from django.shortcuts import get_object_or_404
from django.template import loader
from django.urls import reverse
from django.utils import timezone
from django.views.decorators.http import require_POST

from . import authorization
from . import models
from . import pagination
from . import utils

User = get_user_model()
LOGGER = get_logger(__name__)


def checkouts_list(request):
    """Get list of checkouts."""
    template = loader.get_template('web/kcidb/checkouts.html')
    page = request.GET.get('page')

    checkouts = models.KCIDBCheckout.objects.filter_authorized(request)
    checkouts, filters = utils.filter_checkouts_view(request, checkouts)
    git_branches = (
        checkouts
        .order_by('git_repository_branch')
        .distinct('git_repository_branch')
        .values_list('git_repository_branch', flat=True)
    )

    git_trees = (
        models.GitTree.objects
        .filter(kcidbcheckout__in=checkouts)
        .order_by('name')
        .distinct()
    )

    paginator = pagination.EndlessPaginator(
        checkouts.values_list('iid', flat=True),
        30
    )
    checkout_iids = paginator.get_page(page)

    checkouts = (
        models.KCIDBCheckout.objects
        .aggregated()
        .filter(iid__in=checkout_iids)
        .prefetch_related(
            'tree',
        )
    )

    context = {
        'checkouts': checkouts,
        'paginator': checkout_iids,
        # Filter parameters
        'gittrees': git_trees,
        'git_branches': git_branches,
        'filters': filters,
    }

    return HttpResponse(template.render(context, request))


def checkouts_list_by_failure(request, stage):
    """Show failed checkouts classified by stage."""
    template = loader.get_template('web/kcidb/checkouts_failures.html')
    page = request.GET.get('page')

    checkouts = models.KCIDBCheckout.objects.filter_authorized(request)

    objects = {
        'checkout': checkouts.filter(valid=False),
        'build': checkouts.filter(kcidbbuild__valid=False),
        'test': checkouts.filter(
            # Filter failed tests and exclude the waived ones.
            # .exclude() generated a *really* slow query. See cki-project/datawarehouse!258
            # waived__in=(False, None) doesn't handle NULL. See https://code.djangoproject.com/ticket/13579
            #
            # status in UNSUCCESSFUL_STATUSES & (waived==False | waived is NULL)
            Q(kcidbbuild__kcidbtest__status__in=models.KCIDBTest.UNSUCCESSFUL_STATUSES) &
            (
                Q(kcidbbuild__kcidbtest__waived=False) |
                Q(kcidbbuild__kcidbtest__waived__isnull=True)
            )
        )
    }

    if stage not in ['all'] + list(objects):
        return HttpResponseBadRequest(f'Not sure what {stage} is.')

    stages = list(objects) if stage == 'all' else [stage]

    # Get a list of iids of the checkouts with failures
    checkout_iids = []
    for stage_name in stages:
        checkouts, filters = utils.filter_checkouts_view(request, objects[stage_name])
        checkout_iids.extend(
            checkouts.values_list('iid', flat=True)
        )

    # Sort ids descendingly and remove duplicates
    checkout_iids = sorted(list(set(checkout_iids)), reverse=True)

    paginator = pagination.EndlessPaginator(checkout_iids, 30)
    checkout_iids_page = paginator.get_page(page)

    checkouts = (
        models.KCIDBCheckout.objects
        .aggregated()
        .filter(iid__in=checkout_iids_page)
        .prefetch_related(
            'tree',
        )
    )

    context = {
        'paginator': checkout_iids_page,
        'checkouts': checkouts,
        'stage_filter': stage,
        'stages': list(objects),
        # Filter parameters
        'gittrees': models.GitTree.objects.filter_authorized(request).order_by('name').distinct(),
        'filters': filters,
    }

    return HttpResponse(template.render(context, request))


def checkouts_baselines_get(request):
    """Get list of baselines."""
    template = loader.get_template('web/kcidb/checkouts_baselines_history.html')
    page = request.GET.get('page')

    git_repository_url = request.GET.get('git_repository_url')
    git_repository_branch = request.GET.get('git_repository_branch')
    tree_name = request.GET.get('tree_name')
    kpet_tree_name = request.GET.get('kpet_tree_name')
    package_name = request.GET.get('package_name')

    required_params = (
        (git_repository_url and git_repository_branch and tree_name) or
        (kpet_tree_name and package_name)
    )
    if not required_params:
        return HttpResponseRedirect(reverse('views.kcidb.baselines'))

    checkouts_iids = (
        models.KCIDBCheckout.objects
        .filter_authorized(request)
        .filter(
            **utils.clean_dict(
                {
                    'related_merge_request__isnull': True,
                    'git_repository_url': git_repository_url,
                    'git_repository_branch': git_repository_branch,
                    'tree__name': tree_name,
                    'kcidbbuild__kpet_tree_name': kpet_tree_name,
                    'kcidbbuild__package_name': package_name,
                }
            )
        )
        .values_list('iid', flat=True)
    )

    paginator = pagination.EndlessPaginator(checkouts_iids, 30)
    checkouts_iids_page = paginator.get_page(page)

    checkouts = (
        models.KCIDBCheckout.objects
        .annotated_by_architecture()
        .filter(iid__in=checkouts_iids_page)
        .order_by('-iid')
        .select_related(
            'tree',
        )
    )

    context = {
        'checkouts': checkouts,
        'architectures': models.ArchitectureEnum,
        'paginator': checkouts_iids_page,
        'git_repository_url': git_repository_url,
        'git_repository_branch': git_repository_branch,
        'tree_name': tree_name,
        'kpet_tree_name': kpet_tree_name,
        'package_name': package_name,
    }

    return HttpResponse(template.render(context, request))


def checkouts_baselines_list(request):
    """Get list of baselines."""
    template = loader.get_template('web/kcidb/checkouts_baselines.html')

    last_2_months = timezone.now() - datetime.timedelta(days=60)
    checkouts_iids = (
        models.KCIDBCheckout.objects
        .filter_authorized(request)
        .filter(
            start_time__gte=last_2_months,
            scratch=False,
        )
        .exclude(git_repository_url=None)  # Remove checkouts without necessary data
        .order_by('git_repository_url', 'git_repository_branch', 'tree__name', '-iid')
        .distinct('git_repository_url', 'git_repository_branch', 'tree__name')
        .values_list('iid', flat=True)
    )

    # Include Checkouts that have no git_repository_url grouping them by kpet_tree_name
    # and package_name. This is intended to include Brew official builds.
    no_git_url_checkouts = (
        models.KCIDBCheckout.objects
        .filter_authorized(request)
        .filter(
            start_time__gte=last_2_months,
            scratch=False,
        )
        .order_by('kcidbbuild__kpet_tree_name', 'kcidbbuild__package_name', '-iid')
        .distinct('kcidbbuild__kpet_tree_name', 'kcidbbuild__package_name')
        .values_list('iid', flat=True)
    )

    checkouts = (
        models.KCIDBCheckout.objects
        .annotated_by_architecture()
        .filter(iid__in=list(checkouts_iids) + list(no_git_url_checkouts))
        .order_by('git_repository_url', 'git_repository_branch', 'kcidbbuild__kpet_tree_name', 'tree__name')
        .prefetch_related('kcidbbuild_set')
        .distinct()
        .select_related('tree')
    )

    context = {
        'checkouts': checkouts,
        'architectures': models.ArchitectureEnum,
    }

    return HttpResponse(template.render(context, request))


def checkouts_get(request, checkout_id):
    """Get a single checkout."""
    template = loader.get_template('web/kcidb/checkout.html')

    authorized_checkouts = models.KCIDBCheckout.objects.filter_authorized(request)
    checkouts = (
        authorized_checkouts.aggregated()
        .annotation_for_filter()
        .select_related("log", "tree")
        .prefetch_related("provenance", "reports", "patches")
    )
    checkout, redirect_request = utils.get_object_or_404_or_redirect_url(request, checkouts, checkout_id)
    if redirect_request is not None:
        return redirect_request

    builds = (
        models.KCIDBBuild.objects.aggregated()
        .filter(checkout=checkout)
        .select_related("compiler")
    )

    grouped_issues = utils.group_issue_occurrences(
        models.IssueOccurrence.objects.filter_authorized(request)
        .filter(related_checkout=checkout)
        .select_related("issue", "issue__kind")
        .prefetch_related(
            Prefetch("created_by", queryset=User.objects.only("username", "email")),
            Prefetch("kcidb_checkout", queryset=models.KCIDBCheckout.objects.only("id", "iid", "valid")),
            Prefetch(
                "kcidb_build", queryset=models.KCIDBBuild.objects.only("id", "iid", "valid", "debug", "architecture")
            ),
            Prefetch(
                "kcidb_test",
                queryset=models.KCIDBTest.objects.annotate_is_untriaged(waived_means_triaged=True)
                .only("id", "iid", "status", "waived", "test_id")
                .select_related("test"),
            ),
            Prefetch(
                "kcidb_testresult",
                queryset=models.KCIDBTestResult.objects.select_related("test", "test__test"),
            ),
        )
    )

    related_merge_request = checkout.related_merge_request or {}
    # NOTE: this should include only the latest checkouts in the MR, right now it's all checkouts
    sibling_checkouts = (
        authorized_checkouts.filter(related_merge_request__url=related_merge_request.get("url"))
        .aggregated()
        .prefetch_related("tree")
    )

    context = {
        # Detailed object
        "checkout": checkout,
        # List of child objects
        "builds": builds,
        # "Known Issue" cards
        "grouped_issues": grouped_issues,
        # "Show Checkouts from the same MR" button
        "sibling_checkouts": sibling_checkouts,
        # "Associate Issue" modal
        "issues": models.Issue.objects.filter(resolved_at__isnull=True).select_related("kind"),
        "checkouts_failed": [checkout] if not checkout.valid else [],
        "builds_failed": (
            models.KCIDBBuild.objects.filter(checkout=checkout, valid=False)
            .annotate_is_untriaged()
            .only("id", "architecture", "valid")
        ),
        "tests_failed": (
            models.KCIDBTest.objects.filter(build__checkout=checkout)
            .select_related("build", "test")
            .filter(status__in=models.KCIDBTest.UNSUCCESSFUL_STATUSES)
            .annotate_is_untriaged(waived_means_triaged=True)
            .prefetch_related(
                Prefetch("kcidbtestresult_set", queryset=models.KCIDBTestResult.objects.all()),
                Prefetch("kcidbtestresult_set__issues", queryset=models.Issue.objects.only("id")),
                Prefetch("issues", queryset=models.Issue.objects.only("id")),
            )
        ),
    }

    return HttpResponse(template.render(context, request))


def builds_list(request):
    """Get list of builds."""
    template = loader.get_template('web/kcidb/builds.html')
    page = request.GET.get('page')

    builds = models.KCIDBBuild.objects.filter_authorized(request)
    builds, filters = utils.filter_checkouts_view(request, builds, path_to_checkout='checkout__')
    builds, filters_build = utils.filter_builds_view(request, builds)

    filters.update(filters_build)

    paginator = pagination.EndlessPaginator(
        builds.values_list('iid', flat=True),
        30
    )
    build_iids = paginator.get_page(page)

    builds = (
        models.KCIDBBuild.objects
        .aggregated()
        .filter(iid__in=build_iids)
        .prefetch_related(
            'checkout',
            'checkout__tree',
        )
    )

    package_names = (
        models.KCIDBBuild.objects
        .filter_authorized(request)
        .order_by('package_name')
        .distinct()
        .values_list('package_name', flat=True)
    )

    context = {
        'builds': builds,
        'paginator': build_iids,
        # Filter parameters
        'architectures': models.ArchitectureEnum,
        'package_names': package_names,
        'gittrees': models.GitTree.objects.filter_authorized(request).order_by('name').distinct(),
        'filters': filters,
    }

    return HttpResponse(template.render(context, request))


def builds_get(request, build_id):
    """Get a single build."""
    template = loader.get_template('web/kcidb/build.html')

    builds = (
        models.KCIDBBuild.objects.filter_authorized(request)
        .aggregated()
        .annotation_for_filter()
        .annotate_has_broken_boot_tests()
        .select_related("compiler", "log", "origin", "checkout")
        .prefetch_related("input_files", "output_files", "provenance")
    )
    build, redirect_request = utils.get_object_or_404_or_redirect_url(request, builds, build_id)
    if redirect_request is not None:
        return redirect_request

    tests = (
        models.KCIDBTest.objects.filter(build=build)
        .select_related("build", "build__checkout", "build__checkout__tree", "test", "environment")
    )

    # NOTE: it's faster to combine 3 indexes than to join tables multiple times
    occurs = (
        models.IssueOccurrence.objects.filter(kcidb_build=build)
        .union(
            models.IssueOccurrence.objects.filter(kcidb_test__build=build),
            models.IssueOccurrence.objects.filter(kcidb_testresult__test__build=build),
        )
        .values_list("id", flat=True)
    )

    grouped_issues = utils.group_issue_occurrences(
        models.IssueOccurrence.objects.filter_authorized(request)
        .filter(id__in=occurs)
        .select_related("issue", "issue__kind")
        .prefetch_related(
            Prefetch("created_by", queryset=User.objects.only("username", "email")),
            Prefetch(
                "kcidb_build", queryset=models.KCIDBBuild.objects.only("id", "iid", "valid", "debug", "architecture")
            ),
            Prefetch(
                "kcidb_test",
                queryset=models.KCIDBTest.objects.only("id", "iid", "status", "waived", "test_id").select_related(
                    "test"
                ),
            ),
            Prefetch(
                "kcidb_testresult",
                queryset=models.KCIDBTestResult.objects.select_related("test", "test__test"),
            ),
        )
    )

    context = {
        # Detailed object
        "build": build,
        # List of child objects
        "tests": tests,
        # "Known Issue" cards
        "grouped_issues": grouped_issues,
        # "Associate Issue" modal
        "issues": models.Issue.objects.filter(resolved_at__isnull=True).select_related("kind"),
        "builds_failed": [build] if build.valid is False else [],
        "tests_failed": (
            models.KCIDBTest.objects.filter(build=build)
            .select_related("build", "test")
            .filter(status__in=models.KCIDBTest.UNSUCCESSFUL_STATUSES)
            .annotate_is_untriaged(waived_means_triaged=True)
            .prefetch_related(
                Prefetch("kcidbtestresult_set", queryset=models.KCIDBTestResult.objects.all()),
                Prefetch("kcidbtestresult_set__issues", queryset=models.Issue.objects.only("id")),
                Prefetch("issues", queryset=models.Issue.objects.only("id")),
            )
        ),
    }

    return HttpResponse(template.render(context, request))


def tests_list(request: HttpRequest):  # pylint: disable=too-many-locals  # noqa: PLR0912, PLR0914
    """Get list of tests."""
    template = loader.get_template('web/kcidb/tests.html')
    page = request.GET.get('page')

    cleaned_params = QueryDict(mutable=True)
    cleaned_params.update({k: v for k, v in request.GET.items() if v})
    if request.GET.dict() != cleaned_params.dict():
        return HttpResponseRedirect(reverse("views.kcidb.tests") + "?" + cleaned_params.urlencode())

    result_filter = request.GET.get('result_filter')
    arch_filter = request.GET.get('arch_filter')
    package_name_filter = request.GET.get('package_name_filter')
    issues_tagged_filter = request.GET.get('issues_tagged_filter')
    waived_filter = request.GET.get("waived_filter")
    test_filter = request.GET.get('test_filter')
    testresult_filter = request.GET.get('testresult_filter')
    testresult_status_filter = request.GET.get('testresult_status_filter')
    host_filter = request.GET.get('host_filter')
    tree_filter = request.GET.get('tree_filter')
    kernel_version_filter = request.GET.get('kernel_version_filter')
    sort_by_start_time = request.GET.get('sort_by_start_time')

    tests = models.KCIDBTest.objects.filter_authorized(request).annotation_for_filter()

    try:
        architecture = models.ArchitectureEnum[arch_filter] if arch_filter else None
    except KeyError:
        raise Http404(f'Tried to filter tests using and invalid Architecture={arch_filter!r}') from None

    try:
        status = models.ResultEnum[result_filter] if result_filter else None
    except KeyError:
        raise Http404(f'Tried to filter tests using and invalid Test status={result_filter!r}') from None

    try:
        testresult_status = models.ResultEnum[testresult_status_filter] if testresult_status_filter else None
    except KeyError:
        raise Http404(f'Tried to filter tests using and invalid Subtest status={testresult_status_filter!r}') from None

    filters = utils.clean_dict({
        'status': status,
        'build__architecture': architecture,
        'build__package_name': package_name_filter if package_name_filter else None,
        'test__name__regex': test_filter or None,
        'environment__fqdn__regex': host_filter or None,
        'build__checkout__tree__name__regex': tree_filter or None,
        'build__checkout__kernel_version__regex': kernel_version_filter or None,
        'inherited_start_time__isnull': False if sort_by_start_time else None,
    })

    tests = tests.filter(**filters).order_by(
                '-inherited_start_time' if sort_by_start_time else '-iid'
    )

    if issues_tagged_filter:
        try:
            tests = (
                tests.filter_triaged(waived_means_triaged=False)
                if strtobool(issues_tagged_filter)
                else tests.filter_untriaged(waived_means_triaged=False)
            )
        except ValueError:
            LOGGER.debug("Invalid issues_tagged_filter=%r", issues_tagged_filter)

    if waived_filter:
        try:
            tests = tests.filter(waived=True) if strtobool(waived_filter) else tests.exclude(waived=True)
        except ValueError:
            LOGGER.debug("Invalid waived_filter=%r", waived_filter)

    if testresult_filters := utils.clean_dict({
        'name__regex': testresult_filter or None,
        'status': testresult_status,
    }):
        tests = tests.filter(Exists(
            models.KCIDBTestResult.objects.filter(test=OuterRef('iid'), **testresult_filters).values('id')
        ))

    paginator = pagination.EndlessPaginator(
        tests.values_list('iid', flat=True),
        30
    )

    test_iids = paginator.get_page(page)

    tests = (
        models.KCIDBTest.objects
        .filter(iid__in=test_iids)
        .prefetch_related(
            'build',
            'build__checkout__tree',
        )
        .annotation_for_filter()
        .order_by(
            '-inherited_start_time' if sort_by_start_time else '-iid'
        )
    )

    package_names = (
        models.KCIDBBuild.objects
        .filter_authorized(request)
        .order_by('package_name')
        .values_list('package_name', flat=True)
        .exclude(package_name__isnull=True)
        .distinct()
    )

    context = {
        "tests": tests,
        "paginator": test_iids,
        # Filter parameters
        "result_filter": result_filter,
        "arch_filter": arch_filter,
        "package_name_filter": package_name_filter,
        "issues_tagged_filter": issues_tagged_filter,
        "waived_filter": waived_filter,
        "test_filter": test_filter,
        "testresult_filter": testresult_filter,
        "testresult_status_filter": testresult_status_filter,
        "host_filter": host_filter,
        "tree_filter": tree_filter,
        "kernel_version_filter": kernel_version_filter,
        "sort_by_start_time": sort_by_start_time,
        "package_names": package_names,
    }

    return HttpResponse(template.render(context, request))


def tests_get(request, test_id):
    """Get a single test."""
    template = loader.get_template('web/kcidb/test.html')

    tests = (
        models.KCIDBTest.objects.filter_authorized(request)
        .select_related(
            "build",
            "build__checkout",
            "environment",
            "origin",
            "test",
            "log",
        )
        .prefetch_related(
            "output_files",
            "provenance",
            "test__maintainers",
        )
    )
    test, redirect_request = utils.get_object_or_404_or_redirect_url(request, tests, test_id)
    if redirect_request is not None:
        return redirect_request

    # NOTE: it's faster to combine 2 indexes than to join tables multiple times
    occurs = (
        models.IssueOccurrence.objects.filter(kcidb_test=test)
        .union(
            models.IssueOccurrence.objects.filter(kcidb_testresult__test=test),
        )
        .values_list("id", flat=True)
    )

    grouped_issues = utils.group_issue_occurrences(
        models.IssueOccurrence.objects.filter_authorized(request)
        .filter(id__in=occurs)
        .select_related("issue", "issue__kind")
        .prefetch_related(
            Prefetch("created_by", queryset=User.objects.only("username", "email")),
            Prefetch(
                "kcidb_test",
                queryset=models.KCIDBTest.objects.only("id", "iid", "status", "waived", "test_id").select_related(
                    "test"
                ),
            ),
            Prefetch(
                "kcidb_testresult",
                queryset=models.KCIDBTestResult.objects.select_related("test", "test__test"),
            ),
        )
    )

    context = {
        # Detailed object
        "test": test,
        # List of child objects
        "results": test.kcidbtestresult_set.all().prefetch_related("output_files", "issues"),
        # "Known Issue" cards
        "grouped_issues": grouped_issues,
        # "Associate Issue" modal
        "issues": models.Issue.objects.filter(resolved_at__isnull=True).select_related("kind"),
        "tests_failed": [test] if test.status in models.KCIDBTest.UNSUCCESSFUL_STATUSES else [],
    }

    return HttpResponse(template.render(context, request))


@require_POST
def kcidb_issue(request):
    """Link/Unlink checkouts, builds or tests to a given issue."""
    action = request.POST.get('action', 'add')
    if action == 'add':
        if not request.user.has_perm('datawarehouse.add_issueoccurrence'):
            raise PermissionDenied()
    elif action == 'remove':
        if not request.user.has_perm('datawarehouse.delete_issueoccurrence'):
            raise PermissionDenied()
    else:
        return HttpResponseBadRequest(f'Action {action} unknown.')

    # Check if Issue exists and is authorized
    issue_id = request.POST.get('issue_id')
    issue = get_object_or_404(models.Issue.objects.filter_read_authorized(request), id=issue_id)

    objects = {
        'checkout': {
            'permission': 'datawarehouse.change_kcidbcheckout',
            'elements': models.KCIDBCheckout.objects.filter(
                iid__in=request.POST.getlist('checkout_iids')
            ),
        },
        'build': {
            'permission': 'datawarehouse.change_kcidbbuild',
            'elements': models.KCIDBBuild.objects.filter(
                iid__in=request.POST.getlist('build_iids')
            ),
        },
        'test': {
            'permission': 'datawarehouse.change_kcidbtest',
            'elements': models.KCIDBTest.objects.filter(
                iid__in=request.POST.getlist('test_iids')
            ),
        },
        'testresult': {
            'permission': 'datawarehouse.change_kcidbtestresult',
            'elements': models.KCIDBTestResult.objects.filter(
                iid__in=request.POST.getlist('testresult_iids')
            ),
        },
    }

    # Check all permissions before performing any change.
    for obj in objects.values():
        if obj['elements'] and not request.user.has_perm(obj['permission']):
            raise PermissionDenied()

        all_objects_authorized = authorization.PolicyAuthorizationBackend.all_objects_authorized(
            request,
            obj['elements'],
        )
        if not all_objects_authorized:
            raise Http404()

    for obj in objects.values():
        for element in obj['elements']:
            if action == 'add':
                utils.create_issue_occurrence(issue, element, request)
            else:  # action == 'remove':
                element.issues.remove(issue)

            LOGGER.info('action="%s issue on %s" user="%s" issue_id="%s" iid="%s"',
                        action, element.__class__.__name__, request.user.username, issue.id, element.iid)

    return HttpResponseRedirect(request.POST.get('redirect_to'))


def search(request):
    """Search for checkouts by id or by pipeline_id."""
    template = loader.get_template('web/search.html')
    page = request.GET.get('page')
    query = request.GET.get('q', '').strip()

    if not query:
        return HttpResponse(template.render({}, request))

    query_filter = (
        Q(id__icontains=query) |
        Q(nvr__icontains=query) |
        Q(nvr_old__icontains=query)
    )

    if query.isdigit():
        query_filter |= Q(iid=query)

    checkouts = (
        models.KCIDBCheckout.objects
        .filter_authorized(request)
        .annotation_for_filter()
        .filter(query_filter)
    )

    paginator = pagination.EndlessPaginator(
        checkouts.values_list('iid', flat=True),
        30
    )
    checkout_iids = paginator.get_page(page)

    checkouts = (
        models.KCIDBCheckout.objects
        .aggregated()
        .filter(iid__in=checkout_iids)
        .prefetch_related(
            'tree',
        )
    )

    context = {
        'checkouts': checkouts,
        'paginator': checkout_iids,
        'query': query,
    }

    return HttpResponse(template.render(context, request))


def revision_redirect(request, revision_iid):
    """
    Resolve revision urls into checkouts.

    Keep /kcidb/revisions/{iid} compatibility for old links.
    """
    return HttpResponseRedirect(
        reverse('views.kcidb.checkouts', args=[revision_iid])
    )
