"""Custom QuerySets for KCIDB models."""
from cki_lib.logger import get_logger
from django.db import models
from django.db.models.functions import Coalesce
from django.db.models.functions import Concat

from datawarehouse.models import issue_models
from datawarehouse.models import kcidb_models
from datawarehouse.models import pipeline_models
from datawarehouse.models import test_models
from datawarehouse.models.utils import AuthorizedQuerySet

LOGGER = get_logger(__name__)


class KCIDBCheckoutQuerySet(AuthorizedQuerySet):
    """QuerySet for KCIDBCheckout."""

    @classmethod
    def _subquery_checkouts(cls):
        """Return a SubQuery with the checkouts that have the same pk as the current queryset."""
        return kcidb_models.KCIDBCheckout.objects.filter(pk=models.OuterRef('pk'))

    @classmethod
    def _subquery_builds(cls):
        """Return a SubQuery with the builds linked to the current queryset pks."""
        return kcidb_models.KCIDBBuild.objects.filter(checkout=models.OuterRef('pk'))

    @classmethod
    def _subquery_tests(cls):
        """Return a SubQuery with the tests linked to the current queryset pks through their builds."""
        return kcidb_models.KCIDBTest.objects.filter(build__checkout=models.OuterRef('pk'))

    def annotation_for_filter(self):
        """Add annotated fields to help with filtering."""
        annotations = {
            'nvr_old': Concat('source_package_name', models.Value('-'), 'kernel_version'),
            'nvr': Concat(
                'source_package_name',
                models.Value('-'),
                'source_package_version',
                models.Value('-'),
                'source_package_release'
            )
        }
        return self.annotate(**annotations)

    def aggregated(self):
        # pylint: disable=too-many-locals
        """Add aggregated information."""
        checkout = self._subquery_checkouts()
        checkout_untriaged = checkout.filter(
            valid=False,
            issues=None
        )

        builds_run = self._subquery_builds().annotate_has_broken_boot_tests()
        builds_running = builds_run.filter(valid=None)
        builds_pass = builds_run.filter(valid=True)
        builds_fail = builds_run.filter(valid=False)
        builds_untriaged = builds_fail.filter_untriaged()
        builds_broken_boot_tests = builds_run.filter(has_broken_boot_tests=True)

        tests_run = self._subquery_tests()
        tests_running = tests_run.filter(status=None)
        tests_pass = tests_run.filter(status=test_models.ResultEnum.PASS)
        tests_skip = tests_run.filter(status=test_models.ResultEnum.SKIP)
        tests_fail = tests_run.filter(status=test_models.ResultEnum.FAIL)
        tests_miss = tests_run.filter(status=test_models.ResultEnum.MISS)
        tests_error = tests_run.filter(status=test_models.ResultEnum.ERROR)
        tests_untriaged = tests_run.filter_untriaged(waived_means_triaged=False)

        tests_actionable = tests_untriaged.exclude(waived=True)
        tests_triaged = tests_run.filter_triaged()
        tests_act_fail = tests_actionable.filter(status=test_models.ResultEnum.FAIL)
        tests_act_error = tests_actionable.filter(status=test_models.ResultEnum.ERROR)

        counts = {
            'builds_run': builds_run,
            'builds_running': builds_running,
            'builds_pass': builds_pass,
            'builds_fail': builds_fail,
            'builds_broken_boot_tests': builds_broken_boot_tests,
            'tests_run': tests_run,
            'tests_running': tests_running,
            'tests_pass': tests_pass,
            'tests_skip': tests_skip,
            'tests_fail': tests_fail,
            'tests_miss': tests_miss,
            'tests_error': tests_error,
            'tests_act_fail': tests_act_fail,
            'tests_act_error': tests_act_error,
            'tests_triaged': tests_triaged,
        }

        # Look for issues to determine if there are objects triaged.
        checkout_issues = issue_models.Issue.objects.filter(
            kcidbcheckout__iid=models.OuterRef('iid')
        )
        build_issues = issue_models.Issue.objects.filter(
            kcidbbuild__checkout__iid=models.OuterRef('iid')
        )
        test_issues = issue_models.Issue.objects.filter(
            kcidbtest__build__checkout__iid=models.OuterRef('iid')
        )

        annotations = {
            # Child objects passed
            'stats_builds_passed': ~models.Exists(builds_fail),

            # Objects have issues
            'stats_checkout_triaged': models.Exists(checkout_issues),
            'stats_builds_triaged': models.Exists(build_issues),
            'stats_tests_triaged': models.Exists(test_issues),

            # Objects have failures without issues
            'stats_checkout_untriaged': models.Exists(checkout_untriaged),
            'stats_builds_untriaged': models.Exists(builds_untriaged),
            'stats_tests_untriaged': models.Exists(tests_untriaged),

            # Overall checkout tests status
            'stats_tests_actionable': models.Exists(tests_actionable),
            'stats_tests_status': models.Case(
                models.When(models.Exists(tests_running), then=models.Value(None)),
                models.When(models.Exists(builds_broken_boot_tests), then=models.Value(test_models.ResultEnum.FAIL)),
                models.When(models.Exists(tests_act_fail), then=models.Value(test_models.ResultEnum.FAIL)),
                models.When(models.Exists(tests_act_error), then=models.Value(test_models.ResultEnum.ERROR)),
                models.When(models.Exists(tests_fail), then=models.Value(test_models.ResultEnum.FAIL)),
                models.When(models.Exists(tests_error), then=models.Value(test_models.ResultEnum.ERROR)),
                models.When(models.Exists(tests_miss), then=models.Value(test_models.ResultEnum.MISS)),
                models.When(models.Exists(tests_pass), then=models.Value(test_models.ResultEnum.PASS)),
                models.When(models.Exists(tests_skip), then=models.Value(test_models.ResultEnum.SKIP)),
                default=None,
                output_field=models.CharField(max_length=1, choices=test_models.ResultEnum.choices)
            ),

            # Overall checkout build status
            'stats_builds_actionable': models.Exists(builds_untriaged),
            'stats_builds_status': models.Case(
                models.When(models.Exists(builds_running), then=models.Value(None)),
                models.When(models.Exists(builds_fail), then=models.Value(test_models.ResultEnum.FAIL)),
                models.When(models.Exists(builds_pass), then=models.Value(test_models.ResultEnum.PASS)),
                default=None,
                output_field=models.CharField(max_length=1, choices=test_models.ResultEnum.choices)
            )
        }

        # Add count element for all the queries listed on $counts.
        for name, query in counts.items():
            count = query.values('iid').order_by().annotate(c=models.Count('*')).values('c')
            count.query.set_group_by()
            annotations[f'stats_{name}_count'] = models.Subquery(count, output_field=models.IntegerField())

        return self.annotate(**annotations)

    def annotated_by_architecture(self):
        # pylint: disable=too-many-locals
        """Add build and test information by architecture."""
        counts = {}
        for arch in pipeline_models.ArchitectureEnum:
            builds_run = (
                self._subquery_builds()
                .filter(architecture=arch)
                .exclude(valid=None)
            )
            builds_fail = builds_run.filter(valid=False)
            tests_run = (
                self._subquery_tests()
                .filter(build__architecture=arch)
                .exclude(status=None)
            )
            tests_fail = tests_run.filter(status=test_models.ResultEnum.FAIL)

            counts.update({
                f'{arch.name}_builds_ran': builds_run,
                f'{arch.name}_builds_failed': builds_fail,
                f'{arch.name}_builds_failed_untriaged': builds_fail.filter_untriaged(),
                f'{arch.name}_builds_with_issues': builds_fail.exclude(issues=None),
                f'{arch.name}_tests_ran': tests_run,
                f'{arch.name}_tests_failed': tests_fail.exclude(waived=True),
                f'{arch.name}_tests_failed_waived': tests_fail.filter(waived=True),
                f'{arch.name}_tests_failed_untriaged': tests_fail.filter_untriaged(waived_means_triaged=True),
                f'{arch.name}_tests_with_issues': tests_run.exclude(issues=None),
            })

        annotations = {}
        for name, query in counts.items():
            count = query.values('iid').order_by().annotate(c=models.Count('*')).values('c')
            count.query.set_group_by()
            annotations[f'stats_{name}_count'] = models.Subquery(count, output_field=models.IntegerField())

        return self.annotate(**annotations)

    def filter_ready_to_report(self):
        """
        Get checkouts ready to report.

        To match the 'ready_to_report' condition, a checkout must match
        all the following conditions:

        - Not be already tagged ready_to_report (ready_to_report == False).
        - Checkout valid != None (finished) and last_triaged_at != None (triaged).
        - If Builds exist, all Builds valid != None (finished) and last_triaged_at != None (triaged),
          additionally, if there are no failed builds, there's also no builds missing a test_plan.
        - If Test exist, all Tests status != None (finished) and last_triaged_at != None (triaged).
        """
        unfinished = models.Q(valid=None) | models.Q(last_triaged_at=None)
        unfinished_builds = models.Exists(self._subquery_builds().filter(unfinished))
        unfinished_tests = models.Exists(self._subquery_tests().filter(
            models.Q(status=None) | models.Q(last_triaged_at=None)
        ))

        # There are not KCIDBBuild with test_plan_missing, as long as there are no failed builds.
        missing_testplan = (
            models.Exists(self._subquery_builds().filter(test_plan_missing=True)) &
            ~models.Exists(self._subquery_builds().filter(valid=False))
        )

        return (
            self
            .filter(ready_to_report=False)
            .exclude(unfinished | unfinished_builds | unfinished_tests | missing_testplan)
        )

    def filter_build_setups_finished(self):
        """Get checkouts with all the build setups finished.

        To achieve this match, we're checking the following conditions:
        - notification_sent_build_setups_finished must be False
        - Checkout must have any Build, and all Builds must have kpet_tree_name != None
        """
        has_builds = models.Exists(self._subquery_builds())
        has_unfinished_build_setups = models.Exists(self._subquery_builds().filter(kpet_tree_name=None))

        return (
            self
            .filter(notification_sent_build_setups_finished=False)
            .exclude(~has_builds)
            .exclude(has_unfinished_build_setups)
        )

    def filter_tests_finished(self):
        """Get checkout with all tests finished.

        To achieve this match, we're checking the following conditions:
        - notification_sent_build_setups_finished must be False
        - Checkout must have any Test, and all Tests status != None (finished).
        - Checkout must not have Builds with test_plan_missing.
        """
        has_tests = models.Exists(self._subquery_tests())
        has_unfinished_tests = models.Exists(self._subquery_tests().filter(status=None))

        has_builds_with_test_plan_missing = models.Exists(self._subquery_builds().filter(test_plan_missing=True))
        return (
            self
            .filter(notification_sent_tests_finished=False)
            .exclude(~has_tests)
            .exclude(has_unfinished_tests)
            .exclude(has_builds_with_test_plan_missing)
        )

    def filter_has_builds(self):
        """Get checkout with any build."""
        return self.filter(models.Exists(self._subquery_builds()))

    def filter_has_tests(self):
        """Get checkout with any test."""
        return self.filter(models.Exists(self._subquery_tests()))

    def filter_untriaged(self):
        """Filter untriaged checkouts."""
        issueoccurrences = issue_models.IssueOccurrence.objects.filter(kcidb_checkout_id=models.OuterRef("iid"))
        untriaged = ~models.Exists(issueoccurrences)

        return self.filter(
            untriaged,
            valid=False,
        )

    def get_by_natural_key(self, obj_id):
        """Lookup the object by the natural key."""
        return self.get(id=obj_id)


class KCIDBBuildQuerySet(AuthorizedQuerySet):
    """QuerySet for KCIDBBuild."""

    @classmethod
    def _subquery_builds(cls):
        """Return a SubQuery with the builds linked to the current queryset pks."""
        return kcidb_models.KCIDBBuild.objects.filter(pk=models.OuterRef('pk'))

    @classmethod
    def _subquery_tests(cls):
        """Return a SubQuery with the tests linked to the current queryset pks."""
        return kcidb_models.KCIDBTest.objects.filter(build=models.OuterRef('pk'))

    def annotation_for_filter(self):
        """Add annotated fields to help with filtering."""
        annotations = {
            'nvr': Concat(
                'package_name',
                models.Value('-'),
                'package_version',
                models.Value('-'),
                'package_release'
            )
        }
        return self.annotate(**annotations)

    def aggregated(self):
        # pylint: disable=too-many-locals
        """Add aggregated information."""
        builds_run = self._subquery_builds().annotate_has_broken_boot_tests()
        builds_broken_boot_tests = builds_run.filter(has_broken_boot_tests=True)

        tests_run = self._subquery_tests()
        tests_running = tests_run.filter(status=None)
        tests_pass = tests_run.filter(status=test_models.ResultEnum.PASS)
        tests_skip = tests_run.filter(status=test_models.ResultEnum.SKIP)
        tests_miss = tests_run.filter(status=test_models.ResultEnum.MISS)
        tests_fail = tests_run.filter(status=test_models.ResultEnum.FAIL)
        tests_error = tests_run.filter(status=test_models.ResultEnum.ERROR)

        tests_untriaged = tests_run.filter_untriaged(waived_means_triaged=False)

        tests_actionable = tests_untriaged.exclude(waived=True)
        tests_triaged = tests_run.filter_triaged()
        tests_act_fail = tests_actionable.filter(status=test_models.ResultEnum.FAIL)
        tests_act_error = tests_actionable.filter(status=test_models.ResultEnum.ERROR)

        counts = {
            'tests_run': tests_run,
            'tests_running': tests_running,
            'tests_pass': tests_pass,
            'tests_skip': tests_skip,
            'tests_miss': tests_miss,
            'tests_fail': tests_fail,
            'tests_error': tests_error,
            'tests_act_fail': tests_act_fail,
            'tests_act_error': tests_act_error,
            'tests_triaged': tests_triaged,
        }

        annotations = {
            'stats_tests_untriaged': models.Exists(tests_untriaged),

            # Overall checkout tests status
            'stats_tests_actionable': models.Exists(tests_actionable),
            'stats_tests_status': models.Case(
                models.When(models.Exists(tests_running), then=models.Value(None)),
                models.When(models.Exists(builds_broken_boot_tests), then=models.Value(test_models.ResultEnum.FAIL)),
                models.When(models.Exists(tests_act_fail), then=models.Value(test_models.ResultEnum.FAIL)),
                models.When(models.Exists(tests_act_error), then=models.Value(test_models.ResultEnum.ERROR)),
                models.When(models.Exists(tests_fail), then=models.Value(test_models.ResultEnum.FAIL)),
                models.When(models.Exists(tests_error), then=models.Value(test_models.ResultEnum.ERROR)),
                models.When(models.Exists(tests_miss), then=models.Value(test_models.ResultEnum.MISS)),
                models.When(models.Exists(tests_pass), then=models.Value(test_models.ResultEnum.PASS)),
                models.When(models.Exists(tests_skip), then=models.Value(test_models.ResultEnum.SKIP)),
                default=None,
                output_field=models.CharField(max_length=1, choices=test_models.ResultEnum.choices)
            )
        }

        # Add count element for all the queries listed on $counts.
        for name, query in counts.items():
            count = query.values('id').order_by().annotate(c=models.Count('*')).values('c')
            count.query.set_group_by()
            annotations[f'stats_{name}_count'] = models.Subquery(count, output_field=models.IntegerField())

        return self.annotate(**annotations)

    def annotate_is_untriaged(self):
        """Annotate queryset with the boolean `is_untriaged` field.

        `is_untriaged` is True if the build failed and there's no issue linked to it.
        """
        # Only unsuccessful things can be triaged
        unsuccessful_status = models.Q(valid=False)

        issueoccurrences = issue_models.IssueOccurrence.objects.filter(kcidb_build_id=models.OuterRef("iid"))
        is_untriaged = ~models.Exists(issueoccurrences)

        return self.annotate(is_untriaged=unsuccessful_status & is_untriaged)

    def filter_triaged(self):
        """Filter triaged builds."""
        return self.annotate_is_untriaged().filter(
            is_untriaged=False,
            valid=False,
        )

    def filter_untriaged(self):
        """Filter untriaged builds."""
        return self.annotate_is_untriaged().filter(
            is_untriaged=True,
            valid=False,
        )

    def annotate_has_broken_boot_tests(self):
        """Filter unstable builds."""
        boot_tests = self._subquery_tests().filter(test__name='Boot test')
        tests_me = boot_tests.filter(status__in=[test_models.ResultEnum.MISS, test_models.ResultEnum.ERROR])
        tests_pf = boot_tests.filter(status__in=[test_models.ResultEnum.PASS, test_models.ResultEnum.FAIL])
        return self.annotate(has_broken_boot_tests=models.Exists(tests_me) & ~models.Exists(tests_pf))

    def get_by_natural_key(self, obj_id):
        """Lookup the object by the natural key."""
        return self.get(id=obj_id)


class KCIDBTestQuerySet(AuthorizedQuerySet):
    """QuerySet for KCIDBTestQuerySet."""

    def annotation_for_filter(self):
        """Add annotated fields to help with filtering."""
        annotations = {
            'inherited_start_time': Coalesce(
                'start_time',
                'build__start_time',
                'build__checkout__start_time'
            )
        }
        return self.annotate(**annotations)

    def annotate_is_untriaged(self, issue_occurrences=None, *, waived_means_triaged: bool = True):
        """
        Annotate queryset with the boolean `is_untriaged` field.

        If the test has no subtests, it will be True when there're issues linked to the test,
        otherwise it will be True if all subtests are linked to a test.

        Args:
            issue_occurrences: Overrides the related manager for IssueOccurrence. Uses the std queryset if omitted.
            waived_means_triaged: If True (default), waived tests don't need triaging i.e `is_untriaged=False`
        """
        if issue_occurrences is None:
            issue_occurrences = issue_models.IssueOccurrence.objects.all()

        # Only unsuccessful tests can be triaged
        needs_triaging = models.Q(status__in=kcidb_models.KCIDBTest.UNSUCCESSFUL_STATUSES)
        # Sometimes QE wants to triage waived tests, e.g. when introducing a new test
        if waived_means_triaged:
            needs_triaging &= ~models.Q(waived=True)

        is_untriaged = ~models.Exists(issue_occurrences.filter(kcidb_test_id=models.OuterRef("iid")))

        kcidb_testresults = kcidb_models.KCIDBTestResult.objects.filter(test=models.OuterRef("iid"))
        has_testresults = models.Exists(kcidb_testresults)
        has_untriaged_testresults = kcidb_testresults.filter_untriaged(
            issue_occurrences=issue_occurrences, waived_means_triaged=waived_means_triaged
        )

        is_missing_triaging = models.Case(
            models.When(has_testresults, then=models.Exists(has_untriaged_testresults)),
            default=is_untriaged,
        )

        return self.annotate(is_untriaged=needs_triaging & is_missing_triaging)

    def filter_triaged(self, *, waived_means_triaged: bool = True):
        """Filter triaged tests."""
        return self.annotate_is_untriaged(waived_means_triaged=waived_means_triaged).filter(
            is_untriaged=False,
            status__in=kcidb_models.KCIDBTest.UNSUCCESSFUL_STATUSES,
        )

    def filter_untriaged(self, issue_occurrences=None, *, waived_means_triaged: bool = True):
        """Filter untriaged tests."""
        return (
            self.annotate_is_untriaged(issue_occurrences=issue_occurrences, waived_means_triaged=waived_means_triaged)
            .filter(
                is_untriaged=True,
                status__in=kcidb_models.KCIDBTest.UNSUCCESSFUL_STATUSES,
            )
            .select_related("test")
        )

    def filter_untriaged_blocking(self, issue_occurrences=None, *, waived_means_triaged: bool = True):
        """Filter untriaged blocking tests."""
        return self.filter_untriaged(issue_occurrences, waived_means_triaged=waived_means_triaged).filter(
            status=test_models.ResultEnum.FAIL,
        )

    def filter_untriaged_non_blocking(self, *, waived_means_triaged: bool = True):
        """Filter untriaged non_blocking tests."""
        # exclude blocking
        return self.filter_untriaged(waived_means_triaged=waived_means_triaged).exclude(
            status=test_models.ResultEnum.FAIL,
        )

    def get_by_natural_key(self, obj_id):
        """Lookup the object by the natural key."""
        return self.get(id=obj_id)


class KCIDBTestResultQuerySet(AuthorizedQuerySet):
    """QuerySet for KCIDBTestResult."""

    def filter_untriaged(self, issue_occurrences=None, *, waived_means_triaged: bool = True):
        """
        Filter untriaged test results.

        Args:
            issue_occurrences: Overrides the related manager for IssueOccurrence. Uses the std queryset if omitted.
        """
        if issue_occurrences is None:
            issue_occurrences = issue_models.IssueOccurrence.objects.all()

        # Only unsuccessful tests can be triaged
        needs_triaging = models.Q(status__in=kcidb_models.KCIDBTest.UNSUCCESSFUL_STATUSES)
        # Sometimes QE wants to triage waived tests, e.g. when introducing a new test
        if waived_means_triaged:
            needs_triaging &= ~models.Q(test__waived=True)

        is_untriaged = ~models.Exists(issue_occurrences.filter(kcidb_testresult_id=models.OuterRef("iid")))

        return self.filter(
            needs_triaging & is_untriaged,
            status__in=kcidb_models.KCIDBTest.UNSUCCESSFUL_STATUSES,
        )

    def get_by_natural_key(self, obj_id):
        """Lookup the object by the natural key."""
        return self.get(id=obj_id)
