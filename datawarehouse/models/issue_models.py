# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (c) 2018-2019 Red Hat, Inc.
"""Issue models file."""

from django.conf import settings
from django.db import models
from django.db.models.functions import Coalesce
from django.db.models.functions import Concat
from django.db.models.functions import Extract
from django.db.models.functions import JSONObject
from django.urls import reverse
from django_prometheus.models import ExportModelOperationsMixin as EMOM

from datawarehouse import models as dw_models

from .utils import AuthorizedQuerySet
from .utils import CreatedStampedModel
from .utils import GenericDescriptionQuerySet
from .utils import Model
from .utils import StampedModel


class IssueQuerySet(GenericDescriptionQuerySet):
    """QuerySet for Issue."""

    def kcidb_values(self):
        """Query and format values from issues into KCIDB v04.02."""
        return (
            self.annotate(dummy=models.Value("dummy"))  # Workaround to overwrite the ID field,
            .values("dummy")  # this value is not actually returned, just used to bypass the ORM
            .annotate(
                id=Concat(models.Value("redhat:issue_"), "id", output_field=models.CharField()),
                origin=models.Value("redhat"),
                comment=models.F("description"),
                report_url=models.F("ticket_url"),
                version=Extract(models.F("last_edited_at"), "epoch"),
                culprit=models.Case(
                    # Omit nested fields if issue kind is "Unidentified"
                    models.When(kind__tag="Unidentified", then=models.Value(None)),
                    default=JSONObject(
                        code=models.F("kind__kernel_code_related"),
                        tool=models.Case(
                            models.When(
                                kind__tag__in=["Unstable Test", "Non-Kernel Bug"],
                                then=models.Value(True),
                            ),
                            default=False,
                        ),
                        harness=models.Case(
                            models.When(
                                kind__tag__in=["Infra", "Pipeline Bug", "Workflow Problem"],
                                then=models.Value(True),
                            ),
                            default=False,
                        ),
                    ),
                ),
                misc__is_public=models.Case(
                    models.When(policy__name=dw_models.Policy.PUBLIC, then=models.Value(True)),
                    default=models.Value(False),
                    output_field=models.BooleanField(),
                ),
                # DW_KCIDB_SCHEMA_VERSION
                misc__kcidb__version__major=models.Value(settings.PRODUCER_KCIDB_SCHEMA.major),
                misc__kcidb__version__minor=models.Value(settings.PRODUCER_KCIDB_SCHEMA.minor),
            )
            .values(
                "id",
                "origin",
                "comment",
                "report_url",
                "version",
                "culprit",
                "misc__is_public",
                "misc__kcidb__version__major",
                "misc__kcidb__version__minor",
            )
        )


class IssueOccurrenceQuerySet(AuthorizedQuerySet):
    """QuerySet for IssueOccurrence."""

    def kcidb_values(self):
        """Query and format values from issues into KCIDB v04.02."""
        return (
            self.filter(kcidb_checkout=None)  # Don't want issues about checkouts because KCIDB doesn't support it
            .annotate(dummy=models.Value("dummy"))
            .values("dummy")
            .annotate(
                id=Concat(models.Value("redhat:incident_"), "id", output_field=models.CharField()),
                origin=models.Value("redhat"),
                issue_id=Concat(models.Value("redhat:issue_"), models.F("issue_id"), output_field=models.CharField()),
                issue_version=Extract(models.F("issue__last_edited_at"), "epoch"),
                build_id=models.F("kcidb_build__id"),
                test_id=Coalesce(models.F("kcidb_test__id"), models.F("kcidb_testresult__test__id")),
                present=models.Value(True),
                misc__is_public=models.Case(
                    models.When(
                        models.Q(issue__policy__name=dw_models.Policy.PUBLIC)
                        & models.Q(related_checkout__public=True),
                        then=models.Value(True),
                    ),
                    default=models.Value(False),
                    output_field=models.BooleanField(),
                ),
                # DW_KCIDB_SCHEMA_VERSION
                misc__kcidb__version__major=models.Value(settings.PRODUCER_KCIDB_SCHEMA.major),
                misc__kcidb__version__minor=models.Value(settings.PRODUCER_KCIDB_SCHEMA.minor),
            )
            .values(
                "id",
                "origin",
                "issue_id",
                "issue_version",
                "build_id",
                "test_id",
                "present",
                "misc__is_public",
                "misc__kcidb__version__major",
                "misc__kcidb__version__minor",
            )
        )


class IssueKind(EMOM('issue_kind'), models.Model):
    """Model for IssueKind."""

    description = models.CharField(max_length=None)
    tag = models.CharField(max_length=20)
    color = models.CharField(max_length=7, default='#dc3545')
    kernel_code_related = models.BooleanField(default=False)

    objects = GenericDescriptionQuerySet.as_manager()

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.description}'


class Issue(EMOM('issue'), Model, StampedModel):
    """Model for Issue."""

    kind = models.ForeignKey(IssueKind, on_delete=models.CASCADE, null=True)
    description = models.TextField()
    ticket_url = models.URLField(max_length=2048, unique=True)
    resolved_at = models.DateTimeField(blank=True, null=True)

    policy = models.ForeignKey(dw_models.Policy, on_delete=models.PROTECT,
                               null=True, blank=True)
    # Automatically set the policy as 'public' if there is a 'public' object associated
    policy_auto_public = models.BooleanField(default=False)

    objects = IssueQuerySet.as_manager()

    class Meta:
        """Meta."""

        ordering = ('-id', )

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.description}'

    @property
    def resolved(self):
        """Return True if the Issue is resolved."""
        return bool(self.resolved_at)

    @property
    def web_url(self):
        """Return the URL to this object in the web interface."""
        return settings.DATAWAREHOUSE_URL + reverse('views.issue.get', args=[self.id])

    def get_checkouts(self, request):
        """
        Get KCIDBCheckout for this issue.

        Only return objects authorized for the user doing the request.
        """
        return (
            dw_models.KCIDBCheckout.objects
            .filter_authorized(request)
            .filter(
                iid__in=(
                    self.issueoccurrence_set.values_list('related_checkout__iid', flat=True)
                )
            )
            .select_related('tree')
            .order_by(
                models.F('start_time').desc(nulls_last=True)
            )
        )


class IssueRegex(EMOM('issue_regex'), Model, StampedModel):
    """Model for IssueRegex."""

    issue = models.ForeignKey(Issue, on_delete=models.CASCADE, related_name='issue_regexes')
    text_match = models.TextField()
    file_name_match = models.CharField(max_length=None, null=True, default=None)
    test_name_match = models.CharField(max_length=None, null=True, default=None)
    testresult_name_match = models.CharField(max_length=None, null=True, default=None)
    architecture_match = models.CharField(max_length=None, null=True, default=None)
    tree_match = models.CharField(max_length=None, null=True, default=None)
    kpet_tree_name_match = models.CharField(max_length=None, null=True, default=None)
    package_name_match = models.TextField(null=True, default=None)

    path_to_policy = 'issue__policy'

    objects = AuthorizedQuerySet.as_manager()

    class Meta:
        """Meta."""

        ordering = ('-id', )

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.kpet_tree_name_match} - {self.tree_match} - {self.architecture_match} \
        - {self.package_name_match} - {self.test_name_match} - {self.testresult_name_match} \
        - {self.file_name_match} - {self.text_match}'


class IssueOccurrence(EMOM('issue_occurrence'), Model, CreatedStampedModel):
    """Model for IssueOccurrence."""

    issue = models.ForeignKey('Issue', on_delete=models.CASCADE)
    kcidb_checkout = models.ForeignKey('KCIDBCheckout', null=True, on_delete=models.CASCADE)
    kcidb_build = models.ForeignKey('KCIDBBuild', null=True, on_delete=models.CASCADE)
    kcidb_test = models.ForeignKey('KCIDBTest', null=True, on_delete=models.CASCADE)
    kcidb_testresult = models.ForeignKey('KCIDBTestResult', null=True, on_delete=models.CASCADE)
    is_regression = models.BooleanField(default=False)

    # Store the affected checkout when the occurrence happened on a child object (build or test).
    # This attribute is populated by signal_receivers.issue_occurrence_assigned post_add signal.
    related_checkout = models.ForeignKey('KCIDBCheckout', null=True, on_delete=models.CASCADE,
                                         related_name='related_issue_occurrences')

    path_to_policy = 'issue__policy'
    objects = IssueOccurrenceQuerySet.as_manager()

    class Meta:
        """Meta."""

        ordering = ('-id', )

        constraints = (
            models.UniqueConstraint(
                fields=["issue_id", "kcidb_checkout_id", "kcidb_build_id", "kcidb_test_id", "kcidb_testresult_id"],
                name="unique_issue_kcidb_object",
            ),
            models.CheckConstraint(
                check=(
                    models.Q(kcidb_checkout__isnull=False)
                    ^ models.Q(kcidb_build__isnull=False)
                    ^ models.Q(kcidb_test__isnull=False)
                    ^ models.Q(kcidb_testresult__isnull=False)
                ),
                name="required_kcidb_object",
            ),
        )

    def __str__(self):
        """Return __str__ formatted."""
        return f"{self.id} - {self.issue!r} - {self.kcidb_object!r}"

    @property
    def kcidb_object(self):
        """Returns the kcidb object who owns this occurrence."""
        return self.kcidb_testresult or self.kcidb_test or self.kcidb_build or self.kcidb_checkout

    @property
    def related_test(self):
        """Returns the kcidb test related to this occurrence."""
        return self.kcidb_test or self.kcidb_testresult.test  # pylint: disable=no-member
