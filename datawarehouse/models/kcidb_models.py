# pylint: disable=too-many-lines
"""KCIDB schema models file."""
from collections.abc import Callable
from email.utils import parseaddr
import json
import typing

from cki_lib.logger import get_logger
from cki_lib.misc import datetime_fromisoformat_tz_utc
from cki_lib.misc import get_nested_key
from django.conf import settings
from django.db import models
from django.db import transaction
from django.forms.models import model_to_dict
from django.urls import reverse
from django.utils.functional import cached_property
from django_prometheus.models import ExportModelOperationsMixin as EMOM

from datawarehouse import metrics
from datawarehouse import utils
from datawarehouse.models import authorization_models
from datawarehouse.models import file_models
from datawarehouse.models import patch_models
from datawarehouse.models import pipeline_models
from datawarehouse.models import test_models
from datawarehouse.models.utils import GenericNameQuerySet
from datawarehouse.models.utils import Model

from .kcidb_querysets import KCIDBBuildQuerySet
from .kcidb_querysets import KCIDBCheckoutQuerySet
from .kcidb_querysets import KCIDBTestQuerySet
from .kcidb_querysets import KCIDBTestResultQuerySet

LOGGER = get_logger(__name__)

T = typing.TypeVar("T")
R = typing.TypeVar("R")


def apply_if(func: Callable[[T], R], value: T | None) -> R | None:
    """Apply a function to a value if it's not None, otherwise returns None.

    Args:
        func: The function to apply.
        value: The value to be processed.

    Returns:
        The result of applying the function to the value, or None if the value was already None.
    """
    return func(value) if value else None


class ObjectStatusEnum(models.TextChoices):
    """Status of objects in messages."""

    NEW = 'new'
    NEEDS_TRIAGE = 'needs_triage'
    READY_TO_REPORT = 'ready_to_report'
    UPDATED = 'updated'
    BUILD_SETUPS_FINISHED = 'build_setups_finished'
    TESTS_FINISHED = 'tests_finished'
    CHECKOUT_ISSUEOCCURRENCES_CHANGED = 'checkout_issueoccurrences_changed'


class ProvenanceComponentFunctionEnum(models.TextChoices):
    """Provenance component function."""

    COORDINATOR = 'c', 'coordinator'
    PROVISIONER = 'p', 'provisioner'
    EXECUTOR = 'e', 'executor'


class MissingParent(Exception):
    """Object's parent is missing."""

    def __init__(self, obj_cls, obj_id):
        """Craft exception message."""
        super().__init__(f'{obj_cls.__name__} id={obj_id} is not present in the DB')


class Maintainer(EMOM('maintainer'), models.Model):
    """Model for Maintainer."""

    name = models.CharField(max_length=None)
    email = models.EmailField()

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.name} <{self.email}>'

    @classmethod
    def create_from_address(cls, address):
        """Create Maintainer from address."""
        name, email = parseaddr(address)
        maintainer = cls.objects.only('pk').update_or_create(
            email=email,
            defaults={'name': name},
        )[0]
        return maintainer


class ProvenanceComponent(EMOM('provenance_component'), models.Model):
    """Model for a provenance component."""

    url = models.URLField(unique=True)
    function = models.CharField(max_length=1, choices=ProvenanceComponentFunctionEnum.choices)
    service_name = models.CharField(max_length=None, blank=True, null=True)
    misc = models.JSONField(blank=True, null=True)

    def __str__(self):
        """Return __str__ formatted."""
        if self.service_name == 'gitlab':
            if '/jobs/' in self.url:
                return 'Gitlab Job'
            if '/pipelines/' in self.url:
                return 'Gitlab Pipeline'

        elif self.service_name == 'beaker':
            if '/recipes/' in self.url:
                return 'Beaker Recipe'

        return self.get_function_display()

    @classmethod
    def create_from_dict(cls, data):
        """Create ProvenanceComponent from dict."""
        provenance_obj, _ = cls.objects.only('pk').get_or_create(
            url=data['url'],
            defaults={
                'function': ProvenanceComponentFunctionEnum[data['function'].upper()],
                'service_name': data.get('service_name'),
                'misc': data.get('misc'),
            }
        )
        return provenance_obj


class KCIDBOrigin(EMOM('kcidb_origin'), models.Model):
    """Model for KCIDBOrigin."""

    name = models.CharField(max_length=None, unique=True)

    objects = GenericNameQuerySet.as_manager()

    def __str__(self):
        """Return __str__ formatted."""
        return self.name

    @classmethod
    def create_from_string(cls, name):
        """Create KCIDBOrigin from string."""
        return cls.objects.only('pk').get_or_create(name=name)[0]


class KCIDBCheckout(EMOM('kcidb_checkout'), Model):
    # pylint: disable=too-many-public-methods
    """Model for KCIDBCheckout."""

    iid = models.AutoField(primary_key=True)

    id = models.CharField(max_length=None, unique=True)
    origin = models.ForeignKey(KCIDBOrigin, on_delete=models.PROTECT)
    tree = models.ForeignKey('GitTree', on_delete=models.PROTECT, null=True, blank=True)
    git_repository_url = models.URLField(max_length=2048, null=True, blank=True)
    git_repository_branch = models.CharField(max_length=None, null=True, blank=True)
    git_commit_hash = models.CharField(max_length=None, null=True, blank=True)
    git_commit_name = models.CharField(max_length=None, null=True, blank=True)
    patches = models.ManyToManyField('Patch', related_name='checkouts', blank=True)
    patchset_hash = models.CharField(max_length=None, null=True, blank=True)
    message_id = models.CharField(null=True, blank=True, max_length=None)
    comment = models.TextField(null=True, blank=True)
    start_time = models.DateTimeField(null=True, blank=True)
    contacts = models.ManyToManyField('Maintainer', related_name='checkouts', blank=True)
    log = models.ForeignKey('Artifact', on_delete=models.SET_NULL, null=True, blank=True)
    log_excerpt = models.TextField(null=True, blank=True)
    valid = models.BooleanField(null=True)

    # Fields not belonging to KCIDB schema
    public = models.BooleanField(null=True, blank=True)
    kernel_version = models.CharField(max_length=None, blank=True, null=True)
    source_package_name = models.CharField(max_length=None, null=True, blank=True)
    source_package_version = models.CharField(max_length=None, null=True, blank=True)
    source_package_release = models.CharField(max_length=None, null=True, blank=True)
    ready_to_report = models.BooleanField(default=False)

    brew_task_id = models.IntegerField(null=True, blank=True)
    submitter = models.ForeignKey('Maintainer', related_name='submitted_checkouts',
                                  on_delete=models.SET_NULL, blank=True, null=True)
    scratch = models.BooleanField(blank=True, null=True)
    retrigger = models.BooleanField(blank=True, null=True)
    all_sources_targeted = models.BooleanField(blank=True, null=True)

    patchset_modified_files = models.JSONField(blank=True, null=True)
    related_merge_request = models.JSONField(blank=True, null=True)

    notification_sent_build_setups_finished = models.BooleanField(default=False)
    notification_sent_tests_finished = models.BooleanField(default=False)

    issues = models.ManyToManyField(to='Issue', through='IssueOccurrence', through_fields=('kcidb_checkout', 'issue'))
    last_triaged_at = models.DateTimeField(blank=True, null=True)

    report_rules = models.JSONField(null=True, blank=True, default=list)

    policy = models.ForeignKey(authorization_models.Policy, on_delete=models.PROTECT,
                               null=True, blank=True)
    provenance = models.ManyToManyField(ProvenanceComponent, blank=True, related_name='checkouts')
    objects = KCIDBCheckoutQuerySet.as_manager()

    # Link this object to the related one defining the authorization
    path_to_policy = 'policy'

    class Meta:
        """Metadata."""

        ordering = ('-iid',)

    def __str__(self):
        """Return __str__ formatted."""
        return self.id

    @property
    def checkout(self):
        """
        Return the checkout for this object.

        Provide a transparent way of doing obj.checkout no matter which KCIDB object it is.
        """
        return self

    def symmetric_difference(self, other):
        """Return the symmetric difference between this KCIDBCheckout instance and another KCIDBCheckout instance.

        Args:
            other: The other KCIDBCheckout instance to compare against.

        Returns:
            A set of (key, value) tuples representing the symmetric difference between the two instances on key fields,
            or a set containing type names if 'other' is not a KCIDBCheckout instance.
        """
        fields = ['id', 'git_repository_url', 'git_repository_branch', 'git_commit_hash', 'git_commit_name',
                  'patchset_hash', 'message_id', 'comment', 'start_time', 'log_excerpt', 'valid']
        if isinstance(other, KCIDBCheckout):
            return set(utils.clean_dict(model_to_dict(self, fields=fields)).items()).symmetric_difference(
                utils.clean_dict(model_to_dict(other, fields=fields)).items()
            )
        return {("type", type(self).__name__), ("type", type(other).__name__)}

    @property
    def web_url(self):
        """Return the URL to this object in the web interface."""
        return settings.DATAWAREHOUSE_URL + reverse('views.kcidb.checkouts', args=[self.iid])

    @property
    def is_public(self):
        """Return True if this KCIDBCheckout is public."""
        return self.public

    @property
    def is_missing_triage(self):
        """Return True if this checkout is missing triage."""
        # Seize annotations from KCIDBCheckoutQuerySet.aggregated, if they exist
        if (stats_checkout_untriaged := getattr(self, 'stats_checkout_untriaged', None)) is not None:
            return stats_checkout_untriaged

        return KCIDBCheckout.objects.filter(pk=self.pk).filter_untriaged().exists()

    @property
    def builds_triaged(self):
        """Return list of triaged builds."""
        # Seize annotations from KCIDBCheckoutQuerySet.aggregated, if they exist
        if getattr(self, 'stats_builds_triaged', None) is False:
            return KCIDBBuild.objects.none()

        return self.kcidbbuild_set.exclude(issues=None)

    @property
    def builds_untriaged(self):
        """Return list of untriaged builds."""
        # Seize annotations from KCIDBCheckoutQuerySet.aggregated, if they exist
        if getattr(self, 'stats_builds_untriaged', None) is False:
            return KCIDBBuild.objects.none()

        return self.kcidbbuild_set.filter_untriaged()

    @property
    def tests(self):
        """Return queryset with all KCIDBTest related to this checkout."""
        return KCIDBTest.objects.filter(build__checkout=self)

    @property
    def untriaged_tests_blocking(self):
        """Return list of untriaged blocking tests."""
        # Seize annotations from KCIDBCheckoutQuerySet.aggregated, if they exist
        if getattr(self, 'stats_tests_untriaged', None) is False:
            return KCIDBTest.objects.none()

        return self.tests.filter_untriaged_blocking()

    @property
    def untriaged_tests_non_blocking(self):
        """Return list of untriaged non blocking tests."""
        # Seize annotations from KCIDBCheckoutQuerySet.aggregated, if they exist
        if getattr(self, 'stats_tests_untriaged', None) is False:
            return KCIDBTest.objects.none()

        return self.tests.filter_untriaged_non_blocking()

    @property
    def untriaged_tests_or_regression_blocking(self):
        """Returns list of untriaged blocking tests or tests linked to a regression."""
        return (
            self.tests.annotate_is_untriaged(waived_means_triaged=True)
            .filter(status=test_models.ResultEnum.FAIL)
            .exclude(waived=True)  # Block-able
            .filter(
                models.Q(is_untriaged=True)  # Untriaged
                | models.Exists(  # Has regression
                    self.related_issue_occurrences.filter(is_regression=True).filter(
                        models.Q(kcidb_test_id=models.OuterRef("pk"))
                        | models.Q(kcidb_testresult__test_id=models.OuterRef("pk"))
                    )
                )
            )
        )

    @property
    def has_objects_missing_triage(self):
        """Return if there are failures missing triage."""
        try:
            return (
                self.stats_checkout_untriaged or
                self.stats_builds_untriaged or
                self.stats_tests_untriaged
            )
        except AttributeError:
            return None

    @property
    def has_objects_with_issues(self):
        """Return if the checkout or one of it's builds and tests has any issues."""
        try:
            return (
                self.stats_checkout_triaged or
                self.stats_builds_triaged or
                self.stats_tests_triaged
            )
        except AttributeError:
            return None

    @property
    def is_test_plan(self):
        """Return True if is a test plan."""
        return self.valid is None

    @property
    def succeeded(self):
        """Assess whether the checkout was a success."""
        return bool(self.valid and self.all_builds_passed and not self.has_untriaged_tests_or_regression_blocking)

    @property
    def all_builds_passed(self) -> bool:
        """Return False if the checkout has any builds with valid=False, otherwise True."""
        # Seize annotations from KCIDBCheckoutQuerySet.aggregated, if they exist
        if (stats_builds_passed := getattr(self, 'stats_builds_passed', None)) is not None:
            return stats_builds_passed

        return not self.kcidbbuild_set.filter(valid=False).exists()

    @property
    def has_untriaged_tests_or_regression_blocking(self):
        """Returns list of untriaged blocking tests or tests linked to a regression."""
        return self.untriaged_tests_or_regression_blocking.exists()

    @classmethod
    def create_from_json(cls, data):
        # pylint: disable=too-many-locals
        """Create KCIDBCheckout from kcidb json."""
        with transaction.atomic():
            try:
                existing_checkout = KCIDBCheckout.objects.get(id=data['id'], origin__name=data['origin'])
            except KCIDBCheckout.DoesNotExist:
                existing_checkout = None

            misc = data.get('misc', {})

            origin = KCIDBOrigin.create_from_string(data["origin"])
            log = apply_if(file_models.Artifact.create_from_url, data.get("log_url"))
            tree = apply_if(pipeline_models.GitTree.create_from_string, data.get("tree_name"))
            report_rules = apply_if(json.loads, misc.get("report_rules"))
            submitter = apply_if(Maintainer.create_from_address, misc.get("submitter"))

            public = bool(misc.get("is_public", False))
            if retrigger := bool(misc.get("retrigger", False)):
                policy_name = authorization_models.Policy.RETRIGGER
            elif public:
                policy_name = authorization_models.Policy.PUBLIC
            else:
                policy_name = authorization_models.Policy.INTERNAL
            policy_lazy = authorization_models.Policy.objects.only("pk").filter(name=policy_name)

            defaults_dict = utils.clean_dict({
                "comment": data.get("comment"),
                "git_commit_hash": data.get("git_commit_hash"),
                "git_commit_name": data.get("git_commit_name"),
                "git_repository_branch": data.get("git_repository_branch"),
                "git_repository_url": data.get("git_repository_url"),
                "log": log,
                "log_excerpt": data.get("log_excerpt"),
                "message_id": data.get("message_id"),
                "patchset_hash": data.get("patchset_hash"),
                "retrigger": retrigger,
                "start_time": apply_if(datetime_fromisoformat_tz_utc, data.get("start_time")),
                "tree": tree,
                "valid": data.get("valid"),
                # Misc fields
                "all_sources_targeted": misc.get("all_sources_targeted"),
                "brew_task_id": misc.get("brew_task_id"),
                "kernel_version": misc.get("kernel_version"),
                "patchset_modified_files": misc.get("patchset_modified_files"),
                "related_merge_request": misc.get("mr"),
                "report_rules": report_rules,
                "scratch": misc.get("scratch"),
                "source_package_name": misc.get("source_package_name"),
                "source_package_release": misc.get("source_package_release"),
                "source_package_version": misc.get("source_package_version"),
                "submitter": submitter,
            })

            checkout, created = cls.objects.defer("patchset_modified_files").update_or_create(
                id=data["id"],
                origin=origin,
                defaults=defaults_dict,
                create_defaults={
                    **defaults_dict,
                    "public": public,
                    "policy": policy_lazy.get,  # use callable to evaluate query only on creation
                },
            )

            if created:
                LOGGER.info("Creating KCIDBCheckout: %r", checkout)

            for contact in data.get('contacts', []):
                maintainer = Maintainer.create_from_address(contact)
                checkout.contacts.add(maintainer)

            if patches_data := data.get('patchset_files', []):
                patches = patch_models.Patch.create_from_patchset_files(patches_data)
                checkout.patches.set(patches)

            if provenance_components := misc.get('provenance'):
                # NOTE: Checkout extends the list of provenance on submission to support multiple brew pipelines
                checkout.provenance.add(*[
                    ProvenanceComponent.create_from_dict(data) for data in provenance_components
                ])

        utils.created_object_send_message(existing_checkout, checkout, 'checkout')

        return checkout

    @cached_property
    def annotated_by_architecture(self):
        """Return annotated fields in a dict structure for easier access in templates."""
        data = {}
        for arch in pipeline_models.ArchitectureEnum:
            arch_data = {
                'builds': {
                    'ran': getattr(self, f'stats_{arch.name}_builds_ran_count'),
                    'failed': getattr(self, f'stats_{arch.name}_builds_failed_count'),
                    'failed_untriaged': getattr(self, f'stats_{arch.name}_builds_failed_untriaged_count'),
                },
                'tests': {
                    'ran': getattr(self, f'stats_{arch.name}_tests_ran_count'),
                    'failed': getattr(self, f'stats_{arch.name}_tests_failed_count'),
                    'failed_untriaged': getattr(self, f'stats_{arch.name}_tests_failed_untriaged_count'),
                    'failed_waived': getattr(self, f'stats_{arch.name}_tests_failed_waived_count'),
                },
                'known_issues': (
                    getattr(self, f'stats_{arch.name}_builds_with_issues_count') +
                    getattr(self, f'stats_{arch.name}_tests_with_issues_count')
                )
            }
            data[arch.name] = arch_data

        return data


class KCIDBBuild(EMOM('kcidb_build'), Model):
    """Model for KCIDBBuild."""

    iid = models.AutoField(primary_key=True)

    checkout = models.ForeignKey('KCIDBCheckout', on_delete=models.CASCADE)
    id = models.CharField(max_length=None, unique=True)
    origin = models.ForeignKey(KCIDBOrigin, on_delete=models.PROTECT)
    comment = models.TextField(null=True, blank=True)
    start_time = models.DateTimeField(null=True, blank=True)
    duration = models.IntegerField(null=True, blank=True)
    command = models.TextField(null=True, blank=True)
    compiler = models.ForeignKey('Compiler', on_delete=models.PROTECT, null=True, blank=True)
    input_files = models.ManyToManyField('Artifact', related_name='build_input', blank=True)
    output_files = models.ManyToManyField('Artifact', related_name='build_output', blank=True)
    config_name = models.CharField(max_length=None, null=True, blank=True)
    config_url = models.URLField(max_length=2048, null=True, blank=True)
    log = models.ForeignKey('Artifact', on_delete=models.SET_NULL, null=True, blank=True)
    log_excerpt = models.TextField(null=True, blank=True)
    valid = models.BooleanField(null=True)

    architecture = models.IntegerField(choices=pipeline_models.ArchitectureEnum.choices,
                                       null=True, blank=True)

    # Fields not belonging to KCIDB schema
    debug = models.BooleanField(null=True, blank=True)
    # Flag to prevent a KCIDBCheckout from being ready if a build is missing tests.
    test_plan_missing = models.BooleanField(default=False)

    kpet_tree_name = models.CharField(max_length=None, null=True, blank=True)
    package_name = models.CharField(max_length=None, null=True, blank=True)
    package_version = models.CharField(max_length=None, null=True, blank=True)
    package_release = models.CharField(max_length=None, null=True, blank=True)
    provenance = models.ManyToManyField(ProvenanceComponent, blank=True, related_name='builds')
    testing_skipped_reason = models.CharField(max_length=None, null=True, blank=True)
    issues = models.ManyToManyField(to='Issue', through='IssueOccurrence', through_fields=('kcidb_build', 'issue'))
    last_triaged_at = models.DateTimeField(blank=True, null=True)

    objects = KCIDBBuildQuerySet.as_manager()

    # Link this object to the related one defining the authorization
    path_to_policy = 'checkout__policy'

    class Meta:
        """Metadata."""

        ordering = ('-iid',)

    def __str__(self):
        """Return __str__ formatted."""
        return self.id

    def symmetric_difference(self, other):
        """Return the symmetric difference between this KCIDBBuild instance and another KCIDBBuild instance.

        Args:
            other: The other KCIDBBuild instance to compare against.

        Returns:
            A set of (key, value) tuples representing the symmetric difference between the two instances on key fields,
            or a set containing type names if 'other' is not a KCIDBBuild instance.
        """
        fields = ['id', 'comment', 'start_time', 'duration', 'architecture', 'command', 'config_name',
                  'config_url', 'log_excerpt', 'valid']
        if isinstance(other, KCIDBBuild):
            return set(utils.clean_dict(model_to_dict(self, fields=fields)).items()).symmetric_difference(
                utils.clean_dict(model_to_dict(other, fields=fields)).items()
            )
        return {("type", type(self).__name__), ("type", type(other).__name__)}

    @property
    def web_url(self):
        """Return the URL to this object in the web interface."""
        return settings.DATAWAREHOUSE_URL + reverse('views.kcidb.builds', args=[self.iid])

    @property
    def is_public(self) -> bool | None:
        """Return True if this KCIDBBuild is public."""
        return self.checkout.is_public

    @property
    def retrigger(self) -> bool | None:
        """Return True if this KCIDBBuild originated from a retriggered pipeline."""
        return self.checkout.retrigger

    @property
    def untriaged_tests_blocking(self):
        """Return list of untriaged blocking tests."""
        # Seize annotations from KCIDBCheckoutQuerySet.aggregated, if they exist
        if getattr(self, 'stats_tests_untriaged', None) is False:
            return KCIDBTest.objects.none()

        return KCIDBTest.objects.filter_untriaged_blocking().filter(build=self)

    @property
    def untriaged_tests_non_blocking(self):
        """Return list of untriaged non blocking tests."""
        # Seize annotations from KCIDBCheckoutQuerySet.aggregated, if they exist
        if getattr(self, 'stats_tests_untriaged', None) is False:
            return KCIDBTest.objects.none()

        return KCIDBTest.objects.filter_untriaged_non_blocking().filter(build=self)

    @property
    def is_test_plan(self):
        """Return True if is a test plan."""
        return self.valid is None

    @property
    def is_missing_triage(self):
        """Return True if object failed and has no issues linked."""
        return KCIDBBuild.objects.filter(pk=self.pk).filter_untriaged().exists()

    @classmethod
    def create_from_json(cls, data):
        # pylint: disable=too-many-locals
        """Create KCIDBBuild from kcidb json."""
        with transaction.atomic():
            misc = data.get('misc', {})

            try:
                checkout = KCIDBCheckout.objects.only('pk').get(id=data['checkout_id'])
            except KCIDBCheckout.DoesNotExist as exc:
                raise MissingParent(KCIDBCheckout, data['checkout_id']) from exc

            try:
                existing_build = KCIDBBuild.objects.get(id=data['id'])
            except KCIDBBuild.DoesNotExist:
                existing_build = None

            arch = pipeline_models.ArchitectureEnum[arch_name] if (arch_name := data.get("architecture")) else None
            compiler = apply_if(pipeline_models.Compiler.create_from_string, data.get("compiler"))
            log = apply_if(file_models.Artifact.create_from_url, data.get("log_url"))
            origin = KCIDBOrigin.create_from_string(data['origin'])

            defaults_dict = utils.clean_dict({
                "command": data.get("command"),
                "comment": data.get("comment"),
                "compiler": compiler,
                "config_name": data.get("config_name"),
                "config_url": data.get("config_url"),
                "duration": data.get("duration"),
                "log": log,
                "log_excerpt": data.get("log_excerpt"),
                "start_time": apply_if(datetime_fromisoformat_tz_utc, data.get("start_time")),
                "valid": data.get("valid"),
                # Misc fields
                "kpet_tree_name": misc.get("kpet_tree_name"),
                "package_name": misc.get("package_name"),
                "package_release": misc.get("package_release"),
                "package_version": misc.get("package_version"),
                "retrigger": misc.get("retrigger"),
                "test_plan_missing": misc.get("test_plan_missing"),
                "testing_skipped_reason": misc.get("testing_skipped_reason"),
            })

            build, created = cls.objects.update_or_create(
                checkout=checkout,
                id=data["id"],
                origin=origin,
                defaults=defaults_dict,
                create_defaults={
                    **defaults_dict,
                    "architecture": arch,
                    "debug": misc.get("debug"),
                },
            )

            if created:
                LOGGER.info("Creating KCIDBBuild: %r", build)

            if input_files := data.get('input_files'):
                build.input_files.set(
                    file_models.Artifact.create_from_json(input_file)
                    for input_file in input_files
                )

            if output_files := data.get('output_files'):
                build.output_files.set(
                    file_models.Artifact.create_from_json(output_file)
                    for output_file in output_files
                )

            if provenance_components := misc.get('provenance'):
                # NOTE: Overwrite list of provenance on submission to support retried jobs
                build.provenance.set([
                    ProvenanceComponent.create_from_dict(data)
                    for data in provenance_components
                ])

        utils.created_object_send_message(existing_build, build, 'build')

        metrics.update_time_to_build.delay(build.iid)

        return build


class KCIDBTest(EMOM('kcidb_test'), Model):
    """Model for KCIDBTest."""

    iid = models.AutoField(primary_key=True)

    build = models.ForeignKey('KCIDBBuild', on_delete=models.CASCADE)
    id = models.CharField(max_length=None, unique=True)
    origin = models.ForeignKey(KCIDBOrigin, on_delete=models.PROTECT)
    environment = models.ForeignKey('BeakerResource', on_delete=models.PROTECT, null=True, blank=True)
    test = models.ForeignKey('Test', on_delete=models.PROTECT, null=True, blank=True)
    waived = models.BooleanField(null=True)  # Waived on KPET-db
    start_time = models.DateTimeField(null=True, blank=True)
    duration = models.IntegerField(null=True, blank=True)
    output_files = models.ManyToManyField('Artifact', related_name='test', blank=True)

    status = models.CharField(max_length=1, choices=test_models.ResultEnum.choices,
                              null=True, blank=True)

    log = models.ForeignKey('Artifact', on_delete=models.SET_NULL, null=True, blank=True)
    log_excerpt = models.TextField(null=True, blank=True)

    # Fields not belonging to KCIDB schema
    provenance = models.ManyToManyField(ProvenanceComponent, blank=True, related_name='tests')
    targeted = models.BooleanField(null=True, blank=True)
    polarion_id = models.TextField(blank=True, null=True)
    issues = models.ManyToManyField(to='Issue', through='IssueOccurrence', through_fields=('kcidb_test', 'issue'))
    last_triaged_at = models.DateTimeField(blank=True, null=True)

    objects = KCIDBTestQuerySet.as_manager()

    # Link this object to the related one defining the authorization
    path_to_policy = 'build__checkout__policy'

    UNSUCCESSFUL_STATUSES = (
        test_models.ResultEnum.ERROR,
        test_models.ResultEnum.FAIL,
    )

    class Meta:
        """Metadata."""

        ordering = ('iid',)

    def __str__(self):
        """Return __str__ formatted."""
        return self.id

    @property
    def checkout(self):
        """
        Return the checkout for this object.

        Provide a transparent way of doing obj.checkout no matter which KCIDB object it is.
        """
        kcidb_test = KCIDBTest.objects.select_related('build__checkout').only('build__checkout').get(pk=self.iid)
        return kcidb_test.build.checkout

    def symmetric_difference(self, other):
        """Return the symmetric difference between this KCIDBTest instance and another KCIDBTest instance.

        Args:
            other: The other KCIDBTest instance to compare against.

        Returns:
            A set of (key, value) tuples representing the symmetric difference between the two instances on key fields,
            or a set containing type names if 'other' is not a KCIDBTest instance.
        """
        fields = ['id', 'status', 'waived', 'start_time', 'duration', 'log_excerpt', 'targeted']
        if isinstance(other, KCIDBTest):
            return set(utils.clean_dict(model_to_dict(self, fields=fields)).items()).symmetric_difference(
                utils.clean_dict(model_to_dict(other, fields=fields)).items()
            )
        return {("type", type(self).__name__), ("type", type(other).__name__)}

    @property
    def web_url(self):
        """Return the URL to this object in the web interface."""
        return settings.DATAWAREHOUSE_URL + reverse('views.kcidb.tests', args=[self.iid])

    @property
    def is_public(self) -> bool | None:
        """Return True if this KCIDBTest is public."""
        return self.build.is_public

    @property
    def retrigger(self) -> bool | None:
        """Return True if this KCIDBTest originated from a retriggered pipeline."""
        return self.build.retrigger

    @property
    def is_test_plan(self):
        """Return True if is a test plan."""
        return self.status is None

    @property
    def is_triaged(self):
        """Return True if object failed is fully triaged."""
        return KCIDBTest.objects.filter(pk=self.pk).filter_triaged(waived_means_triaged=False).exists()

    @property
    def is_missing_triage(self):
        """Return True if object failed and has no issues linked."""
        return KCIDBTest.objects.filter(pk=self.pk).filter_untriaged(waived_means_triaged=True).exists()

    @property
    def testresults_untriaged(self):
        """Return queryset of unsuccessful KCIDBTestResults which have no known issues."""
        return self.kcidbtestresult_set.filter_untriaged()

    @property
    def testresults_untriaged_failures(self):
        """Return queryset of failed KCIDBTestResults which have no known issues."""
        return self.testresults_untriaged.filter(
            status=test_models.ResultEnum.FAIL,
        )

    @property
    def testresults_untriaged_nonfailures(self):
        """Return queryset of nonfailures KCIDBTestResults which have no known issues."""
        return self.testresults_untriaged.exclude(
            status=test_models.ResultEnum.FAIL,
        )

    @classmethod
    def create_from_json(cls, data):
        # pylint: disable=too-many-locals
        """Create KCIDBTest from kcidb json."""
        with transaction.atomic():
            misc = data.get('misc', {})

            try:
                build = KCIDBBuild.objects.only('pk').get(id=data['build_id'])
            except KCIDBBuild.DoesNotExist as exc:
                raise MissingParent(KCIDBBuild, data['build_id']) from exc

            try:
                existing_test = KCIDBTest.objects.get(id=data['id'])
            except KCIDBTest.DoesNotExist:
                existing_test = None

            environment = apply_if(
                test_models.BeakerResource.create_from_string, get_nested_key(data, "environment/comment")
            )
            log = apply_if(file_models.Artifact.create_from_url, data.get("log_url"))
            origin = KCIDBOrigin.create_from_string(data["origin"])

            if test_comment := data.get('comment'):
                test_case = test_models.Test.get_and_update(
                    name=test_comment,
                    universal_id=data.get('path'),
                    maintainers=misc.get('maintainers'),
                    fetch_url=misc.get('fetch_url'),
                )
            else:
                test_case = None

            if status := data.get('status', None):
                status = test_models.ResultEnum[status]

            defaults_dict = utils.clean_dict({
                "duration": data.get("duration"),
                "environment": environment,
                "log": log,
                "log_excerpt": data.get("log_excerpt"),
                "start_time": apply_if(datetime_fromisoformat_tz_utc, data.get("start_time")),
                "status": status,
                "test": test_case,
                "waived": data.get("waived"),
                # Misc fields
                "polarion_id": misc.get("polarion_id"),
                "retrigger": misc.get("retrigger"),
            })

            test, created = cls.objects.update_or_create(
                build=build,
                id=data["id"],
                origin=origin,
                defaults=defaults_dict,
                create_defaults={
                    **defaults_dict,
                    "targeted": misc.get("targeted", False),
                },
            )

            if created:
                LOGGER.info("Creating KCIDBTest: %r", test)

            if output_files := data.get('output_files'):
                test.output_files.set(
                    file_models.Artifact.create_from_json(output_file)
                    for output_file in output_files
                )

            if (results := misc.get('results')) is not None:
                fresh_results = [
                    KCIDBTestResult.create_from_json(test, result) for result in results
                ]

                test.kcidbtestresult_set.exclude(id__in=fresh_results).delete()

            if provenance_components := misc.get('provenance'):
                # NOTE: Overwrite list of provenance on submission to support retried jobs
                test.provenance.set([
                    ProvenanceComponent.create_from_dict(data)
                    for data in provenance_components
                ])

        utils.created_object_send_message(existing_test, test, 'test')

        return test


class KCIDBTestResult(EMOM('kcidb_test_result'), Model):
    """Model for KCIDBTestResult."""

    iid = models.AutoField(primary_key=True)

    test = models.ForeignKey('KCIDBTest', on_delete=models.CASCADE)
    id = models.CharField(max_length=None, unique=True)
    name = models.TextField(null=True, blank=True)
    status = models.CharField(max_length=1, choices=test_models.ResultEnum.choices,
                              null=True, blank=True)
    output_files = models.ManyToManyField('Artifact', related_name='test_result', blank=True)
    issues = models.ManyToManyField(to='Issue', through='IssueOccurrence')

    objects = KCIDBTestResultQuerySet.as_manager()

    # Link this object to the related one defining the authorization
    path_to_policy = 'test__build__checkout__policy'

    class Meta:
        """Metadata."""

        ordering = ('iid',)

    def __str__(self):
        """Return __str__ formatted."""
        return self.id

    @property
    def checkout(self):
        """
        Return the checkout for this object.

        Provide a transparent way of doing obj.checkout no matter which KCIDB object it is.
        """
        kcidb_testresult = (
            KCIDBTestResult.objects.select_related('test__build__checkout')
            .only('test__build__checkout').get(pk=self.iid)
        )
        return kcidb_testresult.test.build.checkout

    @classmethod
    @transaction.atomic
    def create_from_json(cls, test, data):
        """Create KCIDBTestResult from kcidb test result json."""
        if status := data.get('status', None):
            status = test_models.ResultEnum[status]

        result, _ = cls.objects.only('pk').update_or_create(
            test=test,
            id=data['id'],
            defaults=utils.clean_dict({
                'status': status,
                'name': data.get('comment', data.get('name')),
            })
        )

        if output_files := data.get('output_files'):
            result.output_files.set(
                file_models.Artifact.create_from_json(output_file)
                for output_file in output_files
            )

        return result

    @property
    def is_missing_triage(self):
        """Return True if object failed and has no issues linked."""
        return KCIDBTestResult.objects.filter_untriaged().filter(pk=self.pk).exists()
