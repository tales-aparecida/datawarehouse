"""Account related views."""
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib.auth.password_validation import validate_password
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.shortcuts import get_current_site
from django.core.exceptions import ValidationError
from django.http import HttpResponse
from django.http import HttpResponseNotAllowed
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.template import loader
from django.urls import reverse
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_decode
from django.utils.http import urlsafe_base64_encode

from datawarehouse import models
from datawarehouse import utils

User = get_user_model()


@login_required
def user_settings(request):
    """User account settings."""
    template = loader.get_template('registration/user_settings.html')

    if request.method == "GET":
        return HttpResponse(template.render({}, request))

    if request.method == "POST":
        name = request.POST.get('name')
        username = request.POST.get('username')

        if not (name and username):
            return HttpResponse(template.render({'error': 'Missing fields'}, request))
        if request.user.username != username and User.objects.filter(username=username).exists():
            return HttpResponse(template.render({'error': 'Username already taken'}, request))

        request.user.username = username
        request.user.first_name = name
        request.user.save(update_fields=['username', 'first_name'])

        return HttpResponse(template.render({}, request))

    return HttpResponseNotAllowed(['GET', 'POST'])


def user_get(request, username):
    """Display user details."""
    template = loader.get_template('registration/user_details.html')

    if request.method == "GET":
        user = get_object_or_404(User, username=username)
        return HttpResponse(template.render({'queried_user': user}, request))

    return HttpResponseNotAllowed(['GET'])


@login_required
def user_delete(request):
    """Delete an user."""
    template = loader.get_template('registration/user_delete.html')

    if request.method == "GET":
        return HttpResponse(template.render({}, request))

    if request.method == "POST":
        current_site = get_current_site(request)

        email_template = loader.get_template('registration/user_delete_email.html')
        email_context = {
            'user': request.user,
            'domain': current_site.domain,
            'protocol': 'https' if request.is_secure() else 'http',
        }

        utils.notify_user(
            subject='Your account was successfully deleted',
            message=email_template.render(email_context, request),
            user=request.user,
        )

        request.user.delete()
        return HttpResponseRedirect(reverse('login'))

    return HttpResponseNotAllowed(['GET', 'POST'])


def user_signup(request):
    """Create a new account."""
    template = loader.get_template('registration/user_signup.html')

    if request.method == "GET":
        return HttpResponse(template.render({'captcha': utils.get_captcha()}, request))

    if request.method == "POST":
        username = request.POST.get('username')
        email = request.POST.get('email')
        name = request.POST.get('name')
        hashkey = request.POST.get('captcha_key')
        captcha_answer = request.POST.get('captcha_answer')

        error = None
        if not (username and email and name and captcha_answer):
            error = 'Missing fields'
            error_input = None
        elif not utils.verify_captcha(hashkey, captcha_answer):
            error = 'Incorrect Captcha solution'
            error_input = 'captcha'
        elif User.objects.filter(username=username).exists():
            error = 'Username already taken'
            error_input = 'username'
        elif User.objects.filter(email=email).exists():
            error = 'Email address already in use'
            error_input = 'email'

        if error:
            context = {
                'username': username, 'email': email, 'name': name,
                'error': error, 'error_input': error_input,
                'captcha': utils.get_captcha()
            }
            return HttpResponse(template.render(context, request))

        user = User.objects.create_user(username, email, password=None)
        user.first_name = name
        user.save(update_fields=['first_name'])

        current_site = get_current_site(request)
        email_template = loader.get_template('registration/user_signup_email.html')
        email_context = {
            'token': default_token_generator.make_token(user),
            'uid': urlsafe_base64_encode(force_bytes(user.pk)),
            'user': user,
            'domain': current_site.domain,
            'protocol': 'https' if request.is_secure() else 'http',
        }

        utils.notify_user(
            subject='Confirm your account',
            message=email_template.render(email_context, request),
            user=user,
        )

        return HttpResponseRedirect(reverse('account.signup.done'))

    return HttpResponseNotAllowed(['GET', 'POST'])


def user_signup_done(request):
    """Display a message telling the user to check their email account."""
    if request.method == "GET":
        template = loader.get_template('registration/user_signup_done.html')
        return HttpResponse(template.render({}, request))

    return HttpResponseNotAllowed(['GET'])


def user_signup_complete(request, uidb64, token):
    # pylint: disable=too-many-return-statements
    """Validate account creation and generate a password."""
    template = loader.get_template('registration/user_signup_complete.html')
    context = {'uidb64': uidb64, 'token': token}

    if request.method == "GET":
        # Display the 'create new password' form.
        uid = urlsafe_base64_decode(uidb64)
        user = get_object_or_404(User, pk=uid)

        if not default_token_generator.check_token(user, token):
            error_template = loader.get_template('registration/user_signup_error.html')
            context = {
                'error': 'Token is not valid. If the problem persists contact an administrator.'
            }
            return HttpResponse(error_template.render(context, request))

        return HttpResponse(template.render(context, request))

    if request.method == "POST":
        # Save the new password.
        uid = urlsafe_base64_decode(uidb64)
        user = get_object_or_404(User, pk=uid)

        if not default_token_generator.check_token(user, token):
            error_template = loader.get_template('registration/user_signup_error.html')
            context = {
                'error': 'Token is not valid. If the problem persists contact an administrator.'
            }
            return HttpResponse(error_template.render(context, request))

        password1 = request.POST.get('password1')
        password2 = request.POST.get('password2')

        error = None
        if not (password1 and password2):
            error = 'Missing fields'
        elif password1 != password2:
            error = 'Passwords do not match'
        else:
            try:
                validate_password(password1, user)
            except ValidationError as validators:
                error = ' '.join(validators)

        if error:
            context['error'] = error
            return HttpResponse(template.render(context, request))

        user.set_password(password1)
        user.save(update_fields=['password'])

        return HttpResponseRedirect(reverse('login'))

    return HttpResponseNotAllowed(['GET', 'POST'])


@login_required
def user_subscriptions(request):
    """Manage user subscriptions."""
    if request.method == "POST":
        enabled_subscriptions = request.POST.getlist('subscriptions')

        subscriptions, _ = models.UserSubscriptions.objects.get_or_create(user=request.user)
        subscriptions.issue_regression_subscribed_at = utils.datetime_bool('issue_regression' in enabled_subscriptions)
        subscriptions.issue_regression_visibility = request.POST.get('issue_regression_visibility', 'bcc')
        subscriptions.save()

        return HttpResponseRedirect(reverse('account.user.settings'))

    return HttpResponseNotAllowed(['POST'])
