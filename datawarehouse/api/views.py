"""API Views."""
from datetime import datetime

from cki_lib.logger import get_logger
from django.db.models import Count
from django.db.models import Q
from rest_framework import generics
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from datawarehouse import models
from datawarehouse import serializers
from datawarehouse import utils

from .permissions import DjangoModelPermissionOrReadOnly
from .permissions import PolicyAuthorizationPermission
from .permissions import PolicyFilterBackend

LOGGER = get_logger(__name__)


class BasePolicyAuthViewSet(viewsets.ModelViewSet):
    """Base class for policy authorized viewset."""

    filter_backends = [PolicyFilterBackend]

    def get_permissions(self):
        """Set permissions based on the view action."""
        if self.action in ('list', 'create'):
            return [DjangoModelPermissionOrReadOnly()]

        return [PolicyAuthorizationPermission()]


class IssueViewSet(utils.MultipleFieldLookupMixin, BasePolicyAuthViewSet):
    """ViewSet for Issues."""

    queryset = models.Issue.objects.all()
    serializer_class = serializers.IssueSerializer
    lookup_fields = (
        ('pk', 'id'),
        ('resolved', 'resolved_at__isnotnull'),
        ('kind', 'kind')
    )

    @action(detail=False, methods=['get'], name="Top Issues")
    def top(self, request):
        """Get top issues hit in a datetime range."""
        date_range = [
            utils.timestamp_to_datetime(request.GET.get('date_from', datetime.min)),
            utils.timestamp_to_datetime(request.GET.get('date_to', datetime.max))
        ]

        recent_issues = self.get_queryset().annotate(hit_count=Count(
            'issueoccurrence__related_checkout__iid',
            distinct=True,
            filter=Q(issueoccurrence__related_checkout__start_time__range=date_range)
        )).order_by('-hit_count')

        page = self.paginate_queryset(recent_issues)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(recent_issues, many=True)
        response_data = {
            "count": recent_issues.count(),
            "results": serializer.data
        }
        return Response(response_data)


class IssueKindViewSet(viewsets.ModelViewSet):
    """ViewSet for Issue Kinds."""

    queryset = models.IssueKind.objects.all()
    serializer_class = serializers.IssueKindSerializer
    lookup_field = 'id'


class PolicyViewSet(viewsets.ModelViewSet):
    """ViewSet for Issue Kinds."""

    queryset = models.Policy.objects.all()
    serializer_class = serializers.PolicySerializer
    lookup_field = 'id'


class TestSingle(utils.MultipleFieldLookupMixin, generics.RetrieveAPIView):
    """Endpoint for handling single test."""

    serializer_class = serializers.TestSerializer
    queryset = models.Test.objects.all()
    lookup_fields = (
        ('test_id', 'id'),
    )


class TestList(utils.MultipleFieldLookupMixin, generics.ListAPIView):
    """Endpoint for handling many tests."""

    serializer_class = serializers.TestSerializer
    queryset = models.Test.objects.all().order_by('id')
    filter_backends = [PolicyFilterBackend]


class IssueRegexGet(utils.MultipleFieldLookupMixin, generics.RetrieveAPIView):
    """Get a single IssueRegex."""

    serializer_class = serializers.IssueRegexSerializer
    queryset = models.IssueRegex.objects.all()
    lookup_fields = (
        ('issue_regex_id', 'id'),
    )


class IssueRegexList(utils.MultipleFieldLookupMixin, generics.ListAPIView):
    """Get a list of IssueRegex."""

    serializer_class = serializers.IssueRegexSerializer
    queryset = models.IssueRegex.objects.all()
