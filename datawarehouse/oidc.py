"""OIDC Modules."""
from django.db import IntegrityError
from mozilla_django_oidc.auth import OIDCAuthenticationBackend

from datawarehouse.models import OIDCUser


class OIDCAuthBackend(OIDCAuthenticationBackend):
    """OIDCAuthenticationBackend override."""

    def create_user(self, claims):
        """Override create_user to use more values from claims."""
        email = claims.get('email', '')
        username = claims.get('preferred_username', '')
        first_name = claims.get('name', '')

        user = self.UserModel.objects.create_user(username, email=email, first_name=first_name)

        try:
            OIDCUser.objects.create(user=user)
        except IntegrityError:
            # User already exists, no problem.
            pass

        return user

    def update_user(self, user, claims):
        """Override update_user to use more values from claims."""
        user.email = claims.get('email', '')
        user.first_name = claims.get('name', '')
        user.username = claims.get('preferred_username', '')
        user.save()

        try:
            OIDCUser.objects.create(user=user)
        except IntegrityError:
            # User already exists, no problem.
            pass

        return user
