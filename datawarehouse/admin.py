# -*- coding: utf-8 -*-
# Copyright (c) 2018 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Admin file."""

from django import forms
from django.contrib import admin
from django.contrib import messages
from django.db import transaction
from django.db.models import Count
from django.db.models import Exists
from django.db.models import ExpressionWrapper
from django.db.models import F
from django.db.models import IntegerField
from django.db.models import Max
from django.db.models import OuterRef
from django.db.models import Q
from django.db.models.functions import Coalesce
from django.template import defaultfilters
from django.utils.html import format_html

from . import models


class AuthorizedModelAdmin(admin.ModelAdmin):
    """Overrides the base queryset using object-level permission."""

    def get_queryset(self, request):
        """Filter queryset using object-level permission."""
        return super().get_queryset(request).filter_authorized(request)


@admin.register(models.GitTree)
class GitTreeAdmin(AuthorizedModelAdmin):
    """Basic admin page for GitTree."""

    list_display = ("id", "name", "get_checkout_count", "get_checkout_latest_start_time")
    search_fields = ("name",)

    def get_queryset(self, request):
        """Annotate how many checkouts each GitTree has."""
        queryset = super().get_queryset(request)
        return queryset.annotate(checkout_count=Count("kcidbcheckout"), latest_checkout=Max("kcidbcheckout__iid"))

    @classmethod
    @admin.display(description="Checkouts Count", ordering="checkout_count")
    def get_checkout_count(cls, git_tree: models.GitTree):
        """Render how many checkouts are linked to the given GitTree."""
        return git_tree.checkout_count

    @classmethod
    @admin.display(description="Latest Checkout.iid", ordering=F("latest_checkout").desc(nulls_last=True))
    def get_checkout_latest_start_time(cls, git_tree: models.GitTree):
        """Render the timestamp of the latest checkout.start_time linked to the given GitTree."""
        if not (latest_checkout := git_tree.kcidbcheckout_set.order_by("-iid").first()):
            return ""

        latest_start_time = (
            f" ({defaultfilters.date(latest_checkout.start_time, 'Y-m-d H:i')})" if latest_checkout.start_time else ""
        )
        return format_html(
            '<a href="{}">{}{}</span>',
            latest_checkout.web_url,
            latest_checkout.id,
            latest_start_time,
        )


@admin.register(models.Issue)
class IssueAdmin(AuthorizedModelAdmin):
    """Basic admin page for Issue."""

    list_display = ("id", "kind", "description", "ticket_url", "policy", "resolved_at")
    search_fields = ("=ticket_url", "description")
    list_filter = ("kind", "policy", "resolved_at")

    def get_search_results(self, request, queryset, search_term) -> tuple:
        """Search with integers matching the id."""
        try:
            search_term_as_int = int(search_term)
        except ValueError:
            return super().get_search_results(request, queryset, search_term)

        return self.model.objects.filter(id=search_term_as_int), False  # False means "does not contain duplicates"


@admin.register(models.IssueRegex)
class IssueRegexAdmin(AuthorizedModelAdmin):
    """Basic admin page for IssueRegex."""

    @classmethod
    @admin.display(description="Issue", ordering="issue_id")
    def get_issue(cls, issue_regex: models.IssueRegex):
        """Render the related Issue as a hyperlink to its DW's page."""
        issue: models.Issue = issue_regex.issue
        return format_html(
            '<a href="{}">#{} {}</span>',
            issue.web_url,
            issue.id,
            issue,
        )

    list_display = (
        "id",
        "get_issue",
        "file_name_match",
        "text_match",
        "test_name_match",
        "testresult_name_match",
        "architecture_match",
        "package_name_match",
        "kpet_tree_name_match",
        "tree_match",
    )
    search_fields = ("issue__description", "file_name_match", "text_match")
    list_filter = ("issue__kind", "issue__resolved_at")


class SelectIssueForm(admin.helpers.ActionForm):
    """A form with a dropdown to select an Issue."""

    target_issue = forms.ModelChoiceField(
        queryset=models.Issue.objects.all(),
        widget=admin.widgets.AutocompleteSelect(
            models.IssueOccurrence.issue.field,  # pylint: disable=no-member
            admin.site,
        ),
        required=False,
    )


@admin.register(models.IssueOccurrence)
class IssueOccurrenceAdmin(AuthorizedModelAdmin):
    """Basic admin page for IssueOccurrence."""

    list_select_related = (
        "issue",
        "related_checkout__tree",
        "related_checkout",
        "kcidb_checkout",
        "kcidb_build",
        "kcidb_test",
        "kcidb_testresult",
    )

    search_fields = (
        "=issue__ticket_url",
        "=issue__id",
        "=related_checkout__id",
        "related_checkout__tree__name",
    )
    list_display = ("id", "get_related_checkout_tree", "related_checkout", "get_kcidb_object", "get_issue")
    list_filter = ("issue__kind", "issue__policy", "created_at", "issue")

    autocomplete_fields = ("issue", "kcidb_checkout", "kcidb_build", "kcidb_test", "kcidb_testresult")
    fields = ("issue", "kcidb_checkout", "kcidb_build", "kcidb_test", "kcidb_testresult")
    action_form = SelectIssueForm

    actions = ("move_to_another_issue",)

    @classmethod
    @admin.display(
        description="Git Tree",
        ordering=("related_checkout__tree__name"),
    )
    def get_related_checkout_tree(cls, issue_occurrence: models.IssueOccurrence):
        """Render the KCIDB object targeted by the issue occurrence."""
        return issue_occurrence.related_checkout.tree

    @classmethod
    @admin.display(
        description="KCIDB Object",
        ordering=Coalesce("kcidb_testresult", "kcidb_test", "kcidb_build", "kcidb_checkout"),
    )
    def get_kcidb_object(cls, issue_occurrence: models.IssueOccurrence):
        """Render the KCIDB object targeted by the issue occurrence."""
        return repr(issue_occurrence.kcidb_object)

    @classmethod
    @admin.display(description="Issue", ordering="issue_id")
    def get_issue(cls, issue_regex: models.IssueRegex):
        """Render the related Issue as a hyperlink to its DW's page."""
        issue: models.Issue = issue_regex.issue
        return format_html(
            '<a href="{}">#{} {}</span>',
            issue.web_url,
            issue.id,
            issue,
        )

    @admin.action(
        permissions=["change"],
        description="Move selected occurrences to another issue",
    )
    def move_to_another_issue(self, request, queryset):
        """Recreate the selected issue occurrences in the targeted issue."""
        if not (target_issue_id := request.POST.get("target_issue")):
            return self.message_user(request, "Missing target issue", messages.ERROR)

        # Make sure they're not trying to use an unauthorized issue
        if not models.Issue.objects.filter_authorized(request).filter(id=target_issue_id).exists():
            return self.message_user(request, f"Target issue {target_issue_id} does not exist", messages.ERROR)

        with transaction.atomic():
            # Need to evaluate and recreate the queryset without the filter_authorized because
            # it has a ".distinct()" call which can't be used when deleting objects.
            selected_ids = list(queryset.values_list("id", flat=True))
            selected_occurrences = models.IssueOccurrence.objects.filter(id__in=selected_ids)
            to_move = selected_occurrences.exclude(issue=target_issue_id)
            ignored = selected_occurrences.filter(issue=target_issue_id).count()
            occurrences_already_there = (
                models.IssueOccurrence.objects.annotate(
                    outer_testresult=ExpressionWrapper(OuterRef("kcidb_testresult"), output_field=IntegerField())
                )
                .filter(
                    # Need to compare using OR because (None == None) is False in SQL
                    Q(kcidb_testresult=OuterRef("kcidb_testresult"))
                    | Q(kcidb_test=OuterRef("kcidb_test"))
                    | Q(kcidb_build=OuterRef("kcidb_build"))
                    | Q(kcidb_checkout=OuterRef("kcidb_checkout")),
                    issue=target_issue_id,
                )
                .exclude(id=OuterRef("id"))
            )
            # Delete occurrences that are already in the target issue
            deleted_count, deleted_detail = to_move.filter(Exists(occurrences_already_there)).delete()
            # Move remaining occurrences to the target issue
            updated = to_move.update(issue=target_issue_id)

        msg = f"{updated} occurrences were moved to the targeted issue."
        if ignored:
            msg += f" {ignored} were already pointing to the targeted issue."
        if deleted_count:
            msg += f" {deleted_count} had an equivalent occurrence in the target and were deleted ({deleted_detail})."

        return self.message_user(request, msg, messages.SUCCESS)


@admin.register(models.KCIDBCheckout)
class KCIDBCheckoutAdmin(AuthorizedModelAdmin):
    """Basic admin page for KCIDBCheckout."""

    search_fields = ("=id", "=iid")


@admin.register(models.KCIDBBuild)
class KCIDBBuildAdmin(AuthorizedModelAdmin):
    """Basic admin page for KCIDBBuild."""

    search_fields = ("=id", "=iid")


@admin.register(models.KCIDBTest)
class KCIDBTestAdmin(AuthorizedModelAdmin):
    """Basic admin page for KCIDBTest."""

    search_fields = ("=id", "=iid")


@admin.register(models.KCIDBTestResult)
class KCIDBTestResultAdmin(AuthorizedModelAdmin):
    """Basic admin page for KCIDBTestResult."""

    search_fields = ("=id", "=iid")


@admin.register(models.LDAPGroupLink)
class LDAPGroupLinkAdmin(admin.ModelAdmin):
    """Basic admin page for LDAPGroupLink."""

    fields = ("description", "filter_query", "groups", "extra_users")
    filter_horizontal = ("groups", "extra_users")

    list_display = ("id", "description", "filter_query", "list_groups", "list_extra_users")
    list_filter = ("groups",)
    search_fields = ("description", "filter_query")

    def has_module_permission(self, request):
        """Make sure only superusers are allowed to mess with permissions."""
        return request.user.is_superuser

    @classmethod
    def list_groups(cls, obj: models.LDAPGroupLink) -> str:
        """Show all groups linked to the object."""
        return repr([str(group) for group in obj.groups.all()])

    @classmethod
    def list_extra_users(cls, obj: models.LDAPGroupLink) -> str:
        """Show all extra_users linked to the object."""
        return repr([str(user) for user in obj.extra_users.all()])


admin.site.register(models.Policy)
admin.site.register(models.Patch)
admin.site.register(models.TestMaintainer)
admin.site.register(models.Test)
admin.site.register(models.BeakerResource)
admin.site.register(models.Artifact)
admin.site.register(models.IssueKind)
admin.site.register(models.Report)
admin.site.register(models.Recipient)
admin.site.register(models.QueuedTask)
