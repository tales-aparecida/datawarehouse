# -*- coding: utf-8 -*-
# Copyright (c) 2018 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Utils file."""
import datetime
import email
from functools import wraps
import re
import typing

from captcha.helpers import captcha_image_url
from captcha.models import CaptchaStore
from cki_lib.logger import get_logger
import dateutil
from django.conf import settings
from django.contrib.auth.views import redirect_to_login
from django.core.exceptions import BadRequest
from django.core.handlers.asgi import ASGIRequest
from django.db.models import Q
from django.db.models.base import Model
from django.db.models.manager import Manager
from django.db.models.query import QuerySet
from django.http import Http404
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.utils import timezone
from django.views.decorators.cache import cache_page
from kombu.exceptions import OperationalError
from requests_futures.sessions import FuturesSession
from rest_framework import generics

from datawarehouse import authorization
from datawarehouse import message_email
from datawarehouse import message_rabbitmq
from datawarehouse import models
from datawarehouse import scripts
from datawarehouse import signals

LOGGER = get_logger(__name__)
MSG_QUEUE = message_rabbitmq.MessageQueue()

EMAIL_QUEUE = message_email.EmailQueue()


def clean_string(value):
    """Clean the input string by replacing any invalid characters with a replacement character.

    Args:
        value (str or any): The input string to be cleaned.

    Returns:
        str or any: The cleaned string, or the original value if it's not a string.
    """
    return value.encode(errors="ignore").decode() if isinstance(value, str) else value


def parse_patches_from_urls(patch_urls):
    """
    Get patch subject from list of patches' urls.

    The result respects the order of the input.
    """
    session = FuturesSession(max_workers=settings.REQUESTS_MAX_WORKERS)
    return [
        email.message_from_bytes(s.result().content)['Subject']
        for p, s in [(p, session.get(p)) for p in patch_urls]
    ]


def timestamp_to_datetime(timestamp):
    """Convert a timestamp string to a _timezone-aware_ datetime, while keeping None values."""
    if isinstance(timestamp, datetime.datetime):
        time = timestamp
    else:
        try:
            time = datetime.datetime.fromisoformat(timestamp)
        except ValueError:
            time = dateutil.parser.parse(timestamp)
        except TypeError:
            return None

    try:
        return timezone.make_aware(time)
    except ValueError:  # tz info already set
        return time


def clean_dict(data):
    """Remove keys with None value from dict."""
    return {
        key: value
        for key, value in data.items()
        if value is not None
    }


def nested_dict_from_double_underscore(data: dict) -> dict:
    """
    Convert a dictionary with double underscore notation keys into a nested dictionary.

    Args:
        data: The input dictionary with keys in double underscore notation.

    Returns:
        dict: A nested dictionary where keys are split at double underscores to form nested keys.

    Examples:
        >>> input_dict = {
        ...     'type': 'user',
        ...     'user__name': 'John Doe',
        ...     'user__age': 30,
        ...     'user__address__city': 'New York',
        ...     'user__address__zip': '10001'
        ... }
        >>> nested_dict_from_double_underscore(input_dict)
        {'type': 'user', 'user': {'name': 'John Doe', 'age': 30, 'address': {'city': 'New York', 'zip': '10001'}}}
    """

    def insert(d, keys, value):
        for key in keys[:-1]:
            d = d.setdefault(key, {})
        d[keys[-1]] = value

    nested_dict = {}
    for double_underscore_key, value in data.items():
        keys = double_underscore_key.split('__')
        insert(nested_dict, keys, value)

    return nested_dict


def created_object_send_message(existing_object, new_object, object_type):
    """Send UMB Messages.

    This function compare to kcidb object and send message when it's necessary.

    NOTE: It can't be a pre_save signal-receiver because there's some M2M fields
    that need to be populated after the kcidb object is created.

    Here is the logic to send messages

    |   Exists  |   We get  |         Send Message       | Message type |
    | --------  | - ------- | -------------------------- | ------------ |
    | Nothing   | Anything  | Always                     | NEW          |
    | Test Plan | Result    | Always                     | UPDATED      |
    | Anything  | Anything  | When they have differences | UPDATED      |
    """
    status = None
    if not existing_object:
        # First time the object is seen
        status = models.ObjectStatusEnum.NEW
    elif existing_object.is_test_plan and not new_object.is_test_plan:
        # We had result for this test plan
        status = models.ObjectStatusEnum.UPDATED
    elif diff := existing_object.symmetric_difference(new_object):
        LOGGER.info("Updating %r: %r", new_object, diff)

        # We had results here but new (different) for this object
        status = models.ObjectStatusEnum.UPDATED

    if status:
        common_args = {
            "sender": f'kcidb.{object_type}.created_object_send_message',
            "status": status,
            "object_type": object_type,
        }
        try:
            scripts.misc.signalize_kcidb_object.delay(**common_args, pks=[new_object.iid])
        except OperationalError:
            LOGGER.exception("Failed to enqueue celery message. Using direct signal as fallback.")
            signals.kcidb_object.send(**common_args, objects=[new_object])


class MultipleFieldLookupMixin:
    """
    GET with multiple filters.

    Allows setting parameters as name:field to use a different value as the parameter
    key than the representation on the database.

    For example, checkout_id:checkout__origin_id takes a `checkout_id` keyword parameter but
    filters the database with `checkout__origin_id`.
    """

    lookup_fields = set()

    @staticmethod
    def parse_field(field, value):
        """Replace %%id wildcard with id or iid depending on the value."""
        if field.endswith('%%id'):
            if value.isdigit() and int(value) < 2e9:
                field = field.replace('%%id', 'iid')
            else:
                field = field.replace('%%id', 'id')

        return field

    def get_queryset(self):
        """Override get_queryset."""
        filter_params = {}
        for param, field in self.lookup_fields:
            # If it's part of the url parameters or part of the GET parameters
            if value := self.kwargs.get(param, self.request.GET.get(param)):
                field_name = self.parse_field(field, value)
                filter_params[field_name] = value

        queryset = self.filter_queryset(self.queryset)
        queryset = authorization.PolicyAuthorizationBackend.filter_authorized(self.request, queryset)
        try:
            return queryset.filter(**filter_params)
        except ValueError:
            raise Http404() from None

    def get_object(self):
        """Override get_object."""
        queryset = self.get_queryset()
        obj = generics.get_object_or_404(queryset)

        # May raise a permission denied
        self.check_object_permissions(self.request, obj)
        return obj


def filter_view(request, queryset, filters_list):
    """
    Filter queryset according to GET parameters.

    From a list of pre defined filter parameters, filter the queryset using
    the fields set on filters_list.

    Returns the filtered queryset and the list of filters applied.
    """
    filters = {}

    for param in request.GET:
        filter_def = filters_list.get(param)
        if not filter_def:
            continue

        filter_is_list = filter_def.get('is_list', False)

        if filter_is_list:
            value = request.GET.getlist(param)
        else:
            value = request.GET.get(param)

        if not value:
            continue

        param_filter = Q(**{filter_def['field']: value})

        if filter_is_list and 'None' in value:
            # It's not possible to use 'None' as value for __in queries, so
            # we need to split it into two queries: one with the values and
            # another one looking for __isnull.
            # https://code.djangoproject.com/ticket/20024
            in_query = filter_def['field']
            isnull_query = re.sub(r'__in$', '__isnull', in_query)
            # NOTE: we don't want to modify value, so we can't use list.remove()
            value_without_none = [v for v in value if v != 'None']
            param_filter = Q(**{isnull_query: True})
            if value_without_none:
                param_filter |= Q(**{in_query: value_without_none})

        try:
            queryset = queryset.filter(param_filter)
        except ValueError as e:
            # Invalid param_filter isn't necessarily a critical failure, might be
            # caused by just wrong params filled in by the user.
            error_message = f'Invalid request, malformed/incorrect filter parameters. {e!r}'
            LOGGER.warning(error_message)
            raise BadRequest(error_message) from e

        filters[param] = value

    return queryset, filters


def filter_checkouts_view(request, queryset, path_to_checkout=''):
    """Filter checkouts depending on request's GET parameters."""
    filters_list = {
        'filter_email': {'field': f'{path_to_checkout}contacts__email__icontains'},
        'filter_gittrees': {'field': f'{path_to_checkout}tree__name__in', 'is_list': True},
        'filter_git_branches': {'field': f'{path_to_checkout}git_repository_branch__in', 'is_list': True},
        'filter_scratch': {'field': f'{path_to_checkout}scratch__in', 'is_list': True},
    }

    return filter_view(request, queryset, filters_list)


def filter_builds_view(request, queryset, path_to_build=''):
    """Filter builds depending on request's GET parameters."""
    filters_list = {
        'filter_architectures': {'field': f'{path_to_build}architecture__in', 'is_list': True},
        'filter_package_names': {'field': f'{path_to_build}package_name__in', 'is_list': True},
    }

    return filter_view(request, queryset, filters_list)


def notify_user(subject, message, user):
    """Send notification to user."""
    EMAIL_QUEUE.add(subject, message, user.email)


def datetime_bool(bool_value):
    """
    Return a timestamp depending if bool_value is True.

    This is useful for DateTimeFields used as boolean flags.
    """
    return timezone.now() if bool_value else None


def get_captcha():
    """Return new Captcha key with img url for challenge."""
    captcha = {
        'key': CaptchaStore.generate_key()
    }
    captcha['image'] = captcha_image_url(captcha['key'])
    return captcha


def verify_captcha(hashkey, solution):
    """Check captcha solution."""
    if settings.CAPTCHA_TEST_MODE:
        return solution.lower() == "passed"

    try:
        store = CaptchaStore.objects.get(hashkey=hashkey)
    except CaptchaStore.DoesNotExist:
        return False

    store.delete()
    return solution == store.challenge


def query_id_or_iid(value):
    """Return query content for id or iid depending on value."""
    if value.isdigit() and int(value) < 2e9:
        return {'iid': value}

    return {'id': value}


def cache_anonymous(ttl=settings.DEFAULT_CACHE_TTL_S):
    """Cache anonymous requests."""

    def _decorator(view_func):
        @wraps(view_func)
        def _wrapped_view(request, *args, **kwargs):
            # When FF_CACHE_ANONYMOUS is disabled, ignore conditions.
            if not settings.FF_CACHE_ANONYMOUS:
                return view_func(request, *args, **kwargs)

            # For authenticated users, do not cache.
            if request.user.is_authenticated:
                return view_func(request, *args, **kwargs)

            # If not authenticated, return cached page.
            return cache_page(ttl)(view_func)(request, *args, **kwargs)
        return _wrapped_view

    return _decorator


def group_issue_occurrences(issue_occurrences):
    """
    Group issue_occurrences by issue.

    Given a list of issue occurrences, group them by issue to make
    them easier to group into cards on the UI.
    """
    result = {
        io.issue: {'issue': io.issue, 'checkouts': [], 'builds': [], 'tests': []}
        for io in issue_occurrences
    }

    for occurrence in issue_occurrences:
        result[occurrence.issue]['is_regression'] = occurrence.is_regression
        if occurrence.kcidb_checkout:
            result[occurrence.issue]['checkouts'].append(occurrence)
        if occurrence.kcidb_build:
            result[occurrence.issue]['builds'].append(occurrence)
        if occurrence.kcidb_test or occurrence.kcidb_testresult:
            result[occurrence.issue]['tests'].append(occurrence)

    return list(sorted(result.values(), key=lambda x: (x['is_regression'], x['issue'].id), reverse=True))


def create_issue_occurrence(issue, kcidb_object, request):
    """Create IssueOccurrence linking issue and kcidb_object."""
    through_defaults = {"created_by": request.user}

    kcidb_object.issues.add(issue, through_defaults=through_defaults)


def get_object_or_404_or_redirect_url(request: ASGIRequest,
                                      klass: typing.Union[Model, Manager, QuerySet],
                                      obj_id: str
                                      ) -> typing.Tuple[typing.Optional[Model], typing.Optional[HttpResponseRedirect]]:
    """
    Get an object, OBJ, of type build, checkout or test and return it as (OBJ, REDIRECT).

    For authenticated users, REDIRECT is None.
    For unauthenticated users, OBJ is None and REDIRECT is an HTTP
    redirection request to the login page.

    If the page is not found and the user is authenticated, it raises an Http404 exception.

    This is a wrapper around django.shortcuts.get_object_or_404.
    """
    obj_filter = query_id_or_iid(obj_id)
    try:
        obj = get_object_or_404(klass, **obj_filter)
    except Http404:
        if not request.user.is_authenticated:
            LOGGER.info("Unauthenticated user: redirect to the login page")
            return None, redirect_to_login(request.get_full_path())
        raise
    return obj, None
