function filterDropdown(originalFilter, selectElementId) {
    const filter = originalFilter.toLowerCase();
    const dropdown = document.getElementById(selectElementId);

    Array.from(dropdown.children).forEach(option => {
        if (option.innerText.toLowerCase().indexOf(filter) !== -1) {
            option.style.display = 'block';
        } else {
            option.style.display = 'none';
            // If the selected option is now hidden, unselect it
            if (option.selected) {
                dropdown.value = '';
            }
        }
    });
}

function confirmRemove(what){
    return confirm('Do you really want to remove this ' + what + '? This action can not be undone');
}

function confirmRemoveIssue(elem) {
    // Confirmation dialog to delete an Issue
    return confirmRemove('issue');
}

function confirmRemoveBuild(elem) {
    // Confirmation dialog to delete a Build from an Issue Occurrence.
    return confirmRemove('build from the issue');
}

function confirmRemoveTest(elem) {
    // Confirmation dialog to delete a Test from an Issue Occurrence.
    return confirmRemove('test from the issue');
}

function findGetParameter(parameterName) {
    // Return value of option in GET parameters
    let params = (new URL(document.location)).searchParams;
    return params.get(parameterName);
}

function setSelectedValue(selectElementId, valueToSet) {
    // Set the value of a dropdown select element
    var selectObj = document.getElementById(selectElementId);
    for (var i = 0; i < selectObj.options.length; i++) {
        if (selectObj.options[i].value == valueToSet) {
            selectObj.options[i].selected = true;
            return;
        }
    }
}
