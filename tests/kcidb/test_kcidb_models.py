# pylint: disable=too-many-lines
"""Test model creation from KCIDB data."""
import copy
import datetime
import json
import os
import pathlib
import unittest
from unittest import mock

from django.test.utils import override_settings
from kombu.exceptions import OperationalError
import responses

from datawarehouse import models
from datawarehouse import scripts
from datawarehouse import signals
from datawarehouse.models import kcidb_models
from datawarehouse.utils import LOGGER as utils_logger
from datawarehouse.utils import clean_dict
from tests import utils


def load_json(name):
    """Load json file from assets."""
    file_name = os.path.join(utils.ASSETS_DIR, name)
    file_content = pathlib.Path(file_name).read_text(encoding='utf-8')

    return json.loads(file_content)


def mock_patch():
    """Mock patch requests."""
    patch_body = """MIME-Version: 1.0
Subject: [RHEL PATCH 206/206] fix some stuff, and break some other
commit 56887cffe946bb0a90c74429fa94d6110a73119d
Author: Patch Author <patch@author.com>
Date:   Mon Feb 22 10:48:09 2021 +0100

    fix some stuff, and break some other
    """
    responses.add(responses.GET, 'http://patchwork.server/patch/2322797/mbox/', body=patch_body)


class TestApplyIfNotNone(unittest.TestCase):
    """Test the helper kcidb_models.apply_if()."""

    def test_apply_if_called(self):
        """Test if the function is called with a non-None value, and the outcome is its result."""
        mock_func = mock.Mock(return_value=mock.sentinel.result)
        result = kcidb_models.apply_if(mock_func, mock.sentinel.value)
        mock_func.assert_called_once_with(mock.sentinel.value)
        self.assertEqual(result, mock.sentinel.result)

    def test_apply_if_not_called(self):
        """Test if the function is not called with a None value, and the outcome is None."""
        mock_func = mock.Mock()
        result = kcidb_models.apply_if(mock_func, None)
        mock_func.assert_not_called()
        self.assertIsNone(result)


class TestMaintainer(utils.TestCase):
    """Tests for the kcidb_models.Maintainer."""

    def test__str__(self):
        """Test Maintainer.__str__."""
        maintainer = models.Maintainer(name='Someone', email='someone@mail.com')

        self.assertEqual(str(maintainer), 'Someone <someone@mail.com>')

    def test_create_from_address(self):
        """Test Maintainer.create_from_address works as expected."""
        maintainer = models.Maintainer.create_from_address('Someone <someone@mail.com>')

        self.assertEqual(maintainer.email, 'someone@mail.com')
        self.assertEqual(maintainer.name, 'Someone')


class TestKCIDBOrigin(utils.TestCase):
    """Tests for the kcidb_models.KCIDBOrigin."""

    def test__str__(self):
        """Test KCIDBOrigin.__str__."""
        origin = models.KCIDBOrigin(name='original')

        self.assertEqual(str(origin), 'original')

    def test_create_from_string(self):
        """Test KCIDBOrigin.create_from_string works as expected."""
        origin = models.KCIDBOrigin.create_from_string('original')

        self.assertEqual(origin.name, 'original')


class TestKCIDBCheckout(utils.TestCase):
    """Tests for KCIDBCheckout."""

    fixtures = [
        'tests/fixtures/policies_for_group_abc.yaml',
        'tests/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
        'tests/fixtures/issues.yaml',
    ]

    def test_checkout(self):
        """Test if the property KCIDBCheckout.checkout works as expected."""
        kcidb_checkout = models.KCIDBCheckout.objects.get(id="redhat:public_checkout")
        expected = kcidb_checkout

        with self.assertNumQueries(0):
            result = kcidb_checkout.checkout

        self.assertEqual(expected, result)

    def test_checkout_web_url(self):
        """Test KCIDBCheckout's web_url property."""
        checkout = models.KCIDBCheckout.objects.first()
        with mock.patch('datawarehouse.models.kcidb_models.settings.DATAWAREHOUSE_URL', 'http://dw'):
            self.assertEqual(f'http://dw/kcidb/checkouts/{checkout.iid}', checkout.web_url)

    def test_is_public(self):
        """Test KCIDBCheckout's is_public property."""
        checkout_qs = models.KCIDBCheckout.objects.filter(id="redhat:public_checkout")

        for public in [None, False, True]:
            checkout_qs.update(public=public)
            checkout = checkout_qs.get()
            with self.subTest(public=public), self.assertNumQueries(0):
                match public:
                    case True:
                        self.assertTrue(checkout.is_public)
                    case _:
                        self.assertFalse(checkout.is_public)

    def test_checkout_is_missing_triage(self):
        """Test KCIDBCheckout's is_missing_triage property."""
        checkout = models.KCIDBCheckout.objects.first()

        # Valid, triage not needed.
        checkout.valid = True
        checkout.save()
        self.assertFalse(checkout.is_missing_triage)

        # Invalid, missing triage.
        checkout.valid = False
        checkout.save()
        self.assertTrue(checkout.is_missing_triage)

        # Issue tagged, triage done.
        models.IssueOccurrence.objects.create(
            issue=models.Issue.objects.first(),
            kcidb_checkout=checkout
        )
        self.assertFalse(checkout.is_missing_triage)

    def test_tests(self):
        """Test KCIDBCheckout.tests filters tests connected to a checkout through its builds."""
        kcidb_checkout = models.KCIDBCheckout.objects.get(id="redhat:public_checkout")
        expected = models.KCIDBTest.objects.filter(build__checkout=kcidb_checkout).values_list("id", flat=True)

        with self.assertNumQueries(1):
            result = list(kcidb_checkout.tests.values_list("id", flat=True))

        self.assertCountEqual(result, expected)

    def test_all_builds_passed(self):
        """Test KCIDBCheckout.all_builds_passed."""
        kcidb_checkouts = models.KCIDBCheckout.objects.filter(id="redhat:public_checkout")
        kcidb_checkout = kcidb_checkouts.get()

        for valid, expected in {None: True, False: False, True: True}.items():
            kcidb_checkout.kcidbbuild_set.update(valid=valid)

            with self.subTest("Not annotated", valid=valid):
                self.assertEqual(kcidb_checkout.all_builds_passed, expected)

            with self.subTest("Annotated with 'aggregated'", valid=valid):
                self.assertEqual(kcidb_checkouts.aggregated().get().all_builds_passed, expected)

    def test_untriaged_tests_or_regression_blocking(self):
        """Test KCIDBCheckout.untriaged_tests_or_regression_blocking()."""
        checkout = models.KCIDBCheckout.objects.get(id="redhat:public_checkout_valid")
        build = models.KCIDBBuild.objects.get(id="redhat:public_build_3")
        kcidb_test = models.KCIDBTest.objects.get(id="redhat:public_test_S")
        kcidb_testresult = kcidb_test.kcidbtestresult_set.get()
        issue_1 = models.Issue.objects.first()

        # Prepare fixture so that the checkout has exactly one test and one testresult to start
        kcidb_test.build = build
        kcidb_test.save()
        build.checkout = checkout
        build.save()
        self.assertEqual(models.KCIDBTest.objects.filter(build__checkout=checkout).count(), 1)
        self.assertEqual(models.KCIDBTestResult.objects.filter(test__build__checkout=checkout).count(), 1)

        input_domain = utils.product_dict(
            waived=[None, False, True],
            status=[
                None,
                models.ResultEnum.FAIL,
                models.ResultEnum.ERROR,
                models.ResultEnum.MISS,
                models.ResultEnum.PASS,
                models.ResultEnum.DONE,
                models.ResultEnum.SKIP,
            ],
            triaged=[False, True, "regression"],
        )
        for triage_subtest in [True, False]:
            if not triage_subtest:
                kcidb_testresult.delete()
                kcidb_testresult = None

            for case_kwargs in input_domain:
                with self.subTest(triage_subtest=triage_subtest, **case_kwargs):
                    kcidb_test.waived = case_kwargs["waived"]
                    kcidb_test.status = case_kwargs["status"]
                    kcidb_test.save(update_fields=["waived", "status"])
                    kcidb_test.issues.set([])  # reset occurrences

                    if triage_subtest:
                        kcidb_testresult.status = case_kwargs["status"]
                        kcidb_testresult.save(update_fields=["status"])
                        kcidb_testresult.issues.set([])  # reset occurrences

                    if triaged := case_kwargs["triaged"]:
                        models.IssueOccurrence.objects.create(
                            issue=issue_1,
                            related_checkout=checkout,
                            kcidb_checkout=None,
                            kcidb_build=None,
                            kcidb_test=kcidb_test if not triage_subtest else None,
                            kcidb_testresult=kcidb_testresult if triage_subtest else None,
                            is_regression=triaged == "regression",
                        )

                    # Calls!
                    with self.assertNumQueries(1):
                        result = list(checkout.untriaged_tests_or_regression_blocking.values_list("id", flat=True))

                    with self.assertNumQueries(1):
                        result_bool = checkout.has_untriaged_tests_or_regression_blocking

                    match case_kwargs:
                        # Waived or Triaged on any status will always result in no blockers
                        case {"waived": True} | {"triaged": True}:
                            expected = []
                            expected_bool = False
                        # Otherwise, FAILs should be blockers, both untriaged and on regression
                        case {"status": "F"}:
                            expected = ["redhat:public_test_S"]
                            expected_bool = True
                        # Finally, the remaining status should never miss triage
                        case _:
                            expected = []
                            expected_bool = False

                    self.assertEqual(result, expected)
                    self.assertEqual(result_bool, expected_bool)

    def test_succeeded(self):
        """White-box test of KCIDBCheckout.succeeded, relying on the behavior of its components."""
        input_domain = utils.product_dict(
            valid=[None, False, True],
            all_builds_passed=[False, True],
            has_untriaged_tests_or_regression_blocking=[False, True],
        )
        checkout_queryset = models.KCIDBCheckout.objects.filter(id="redhat:public_checkout")

        for case_kwargs in input_domain:
            with self.subTest(**case_kwargs), mock.patch.multiple(models.KCIDBCheckout, **case_kwargs):
                checkout_queryset.update(valid=case_kwargs["valid"])
                checkout = checkout_queryset.get()

                match case_kwargs:
                    case {
                        "valid": True,
                        "all_builds_passed": True,
                        "has_untriaged_tests_or_regression_blocking": False,
                    }:
                        self.assertTrue(checkout.succeeded)
                    case _:
                        self.assertFalse(checkout.succeeded)


class TestKCIDBBuild(utils.TestCase):
    """Tests for KCIDBBuild."""

    fixtures = [
        'tests/fixtures/policies_for_group_abc.yaml',
        'tests/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
        'tests/fixtures/issues.yaml',
    ]

    def test_checkout(self):
        """Test if the property KCIDBBuild.checkout works as expected."""
        kcidb_checkout = models.KCIDBCheckout.objects.get(id="redhat:public_checkout")
        kcidb_build = models.KCIDBBuild.objects.filter(checkout=kcidb_checkout).first()
        expected = kcidb_checkout

        with self.assertNumQueries(1):
            result = kcidb_build.checkout

        self.assertEqual(expected, result)

    def test_build_web_url(self):
        """Test KCIDBBuild's web_url property."""
        build = models.KCIDBBuild.objects.first()
        with mock.patch('datawarehouse.models.kcidb_models.settings.DATAWAREHOUSE_URL', 'http://dw'):
            self.assertEqual(f'http://dw/kcidb/builds/{build.iid}', build.web_url)

    def test_is_public(self):
        """Test KCIDBBuild's is_public property."""
        checkout_qs = models.KCIDBCheckout.objects.filter(id="redhat:public_checkout")

        for public in [None, False, True]:
            checkout_qs.update(public=public)
            build = models.KCIDBBuild.objects.select_related("checkout").get(id="redhat:public_build_1")
            with self.subTest(public=public), self.assertNumQueries(0):
                match public:
                    case True:
                        self.assertTrue(build.is_public)
                    case _:
                        self.assertFalse(build.is_public)

    def test_retrigger(self):
        """Test KCIDBBuild's retrigger property."""
        checkout_qs = models.KCIDBCheckout.objects.filter(id="redhat:public_checkout")

        for retrigger in [None, False, True]:
            checkout_qs.update(retrigger=retrigger)
            build = models.KCIDBBuild.objects.select_related("checkout").get(id="redhat:public_build_1")
            with self.subTest(retrigger=retrigger), self.assertNumQueries(0):
                match retrigger:
                    case True:
                        self.assertTrue(build.retrigger)
                    case _:
                        self.assertFalse(build.retrigger)

    def test_build_is_missing_triage(self):
        """Test KCIDBBuild's is_missing_triage property."""
        build = models.KCIDBBuild.objects.first()

        # Valid, triage not needed.
        build.valid = True
        build.save()
        self.assertFalse(build.is_missing_triage)

        # Invalid, missing triage.
        build.valid = False
        build.save()
        self.assertTrue(build.is_missing_triage)

        # Issue tagged, triage done.
        models.IssueOccurrence.objects.create(
            issue=models.Issue.objects.first(),
            kcidb_build=build
        )
        self.assertFalse(build.is_missing_triage)


class TestKCIDBTest(utils.TestCase):
    """Tests for KCIDBTest."""

    fixtures = [
        'tests/fixtures/policies_for_group_abc.yaml',
        'tests/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
        'tests/fixtures/issues.yaml',
    ]

    def test_checkout(self):
        """Test if the property KCIDBTest.checkout works as expected."""
        kcidb_checkout = models.KCIDBCheckout.objects.get(id="redhat:public_checkout")
        kcidb_test = models.KCIDBTest.objects.filter(build__checkout=kcidb_checkout).first()
        expected = kcidb_checkout

        with self.assertNumQueries(1):
            result = kcidb_test.checkout

        self.assertEqual(expected, result)

    def test_test_web_url(self):
        """Test KCIDBTest's web_url property."""
        test = models.KCIDBTest.objects.first()
        with mock.patch('datawarehouse.models.kcidb_models.settings.DATAWAREHOUSE_URL', 'http://dw'):
            self.assertEqual(f'http://dw/kcidb/tests/{test.iid}', test.web_url)

    def test_is_public(self):
        """Test KCIDBTest's is_public property."""
        checkout_qs = models.KCIDBCheckout.objects.filter(id="redhat:public_checkout")

        for public in [None, False, True]:
            checkout_qs.update(public=public)
            test = models.KCIDBTest.objects.select_related("build__checkout").get(id="redhat:public_test_R")
            with self.subTest(public=public), self.assertNumQueries(0):
                match public:
                    case True:
                        self.assertTrue(test.is_public)
                    case _:
                        self.assertFalse(test.is_public)

    def test_retrigger(self):
        """Test KCIDBTest's retrigger property."""
        checkout_qs = models.KCIDBCheckout.objects.filter(id="redhat:public_checkout")

        for retrigger in [None, False, True]:
            checkout_qs.update(retrigger=retrigger)
            test = models.KCIDBTest.objects.select_related("build__checkout").get(id="redhat:public_test_R")
            with self.subTest(retrigger=retrigger), self.assertNumQueries(0):
                match retrigger:
                    case True:
                        self.assertTrue(test.retrigger)
                    case _:
                        self.assertFalse(test.retrigger)

    def _test_triaging_helpers(self, *, triage_subtest: bool):
        """Test KCIDBTest.is_triaged and KCIDBTest.is_missing_triage."""
        test_queryset = models.KCIDBTest.objects.all()
        issue_1 = models.Issue.objects.get(description="Issue Public")

        input_domain = utils.product_dict(
            waived=[None, False, True],
            status=[
                None,
                models.ResultEnum.FAIL,
                models.ResultEnum.ERROR,
                models.ResultEnum.MISS,
                models.ResultEnum.PASS,
                models.ResultEnum.DONE,
                models.ResultEnum.SKIP,
            ],
            triaged=[False, True],
        )
        for case_kwargs in input_domain:
            with self.subTest(triage_subtest=triage_subtest, **case_kwargs):
                kcidb_test = test_queryset.get(status=case_kwargs["status"])

                kcidb_test.waived = case_kwargs["waived"]
                kcidb_test.save(update_fields=["waived"])

                issues = [issue_1] if case_kwargs["triaged"] else []
                if triage_subtest:
                    # All unsuccessful results must be triaged
                    # FIXME: Maybe it should be enough to triage the worst result
                    # NOTE: each test fixture has the same status as its worst result
                    kcidb_test.issues.set([])
                    unsuccessful_results = kcidb_test.kcidbtestresult_set.filter(
                        status__in=models.KCIDBTest.UNSUCCESSFUL_STATUSES
                    )
                    for kcidb_testresult in unsuccessful_results:
                        kcidb_testresult.issues.set(issues)
                else:
                    kcidb_test.issues.set(issues)

                with self.assertNumQueries(1):
                    result = kcidb_test.is_triaged

                self.assertEqual(
                    result,
                    case_kwargs["status"] in models.KCIDBTest.UNSUCCESSFUL_STATUSES and case_kwargs["triaged"],
                    "only True if an unsuccessful test has an issueoccurrence (or all its subtests have one)",
                )
                match case_kwargs:
                    # Waived or Triaged on any status will result in False
                    case {"waived": True} | {"triaged": True}:
                        self.assertFalse(kcidb_test.is_missing_triage)
                    # Otherwise, if neither are defined, only unsuccessful status should miss triage
                    case {"status": status} if status in models.KCIDBTest.UNSUCCESSFUL_STATUSES:
                        self.assertTrue(kcidb_test.is_missing_triage)
                    # Finally, the remaining status should never miss triage
                    case _:
                        self.assertFalse(kcidb_test.is_missing_triage)

    def test_triaging_helpers_with_subtests(self):
        """Test KCIDBTest.is_triaged and KCIDBTest.is_missing_triage with subtests."""
        self._test_triaging_helpers(triage_subtest=True)

    def test_triaging_helpers_without_subtests(self):
        """Test KCIDBTest.is_triaged and KCIDBTest.is_missing_triage without subtests."""
        models.KCIDBTestResult.objects.all().delete()
        self._test_triaging_helpers(triage_subtest=False)


class TestKCIDBTestResult(utils.TestCase):
    """Tests for KCIDBTestResult."""

    fixtures = [
        'tests/fixtures/policies_for_group_abc.yaml',
        'tests/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
        'tests/fixtures/issues.yaml',
    ]

    def test_checkout(self):
        """Test if the property KCIDBTestResult.checkout works as expected."""
        kcidb_checkout = models.KCIDBCheckout.objects.get(id="redhat:public_checkout")
        kcidb_testresult = models.KCIDBTestResult.objects.filter(test__build__checkout=kcidb_checkout).first()
        expected = kcidb_checkout

        with self.assertNumQueries(1):
            result = kcidb_testresult.checkout

        self.assertEqual(expected, result)


@override_settings(
    CELERY_TASK_ALWAYS_EAGER=True,
    # Disable behavior from kcidb objects' pre_save signal
    CLEAR_ISSUEOCCURRENCES_ON_SAVE=False,
)
@mock.patch("datawarehouse.signals.kcidb_object.send", wraps=signals.kcidb_object.send)
class TestKCIDBCheckoutFromJson(utils.TestCase):
    """Test creation of KCIDBCheckout model."""

    fixtures = (
        "tests/fixtures/basic_policies.yaml",
        "tests/fixtures/basic.yaml",
    )

    def test_basic(self, mocked_send_signal):
        """Submit only id."""
        data = {
            'id': 'redhat:1',
            'origin': 'redhat',
        }
        checkout = models.KCIDBCheckout.create_from_json(data)

        mocked_send_signal.assert_called_once_with(
            sender="kcidb.checkout.created_object_send_message",
            status=models.ObjectStatusEnum.NEW,
            object_type="checkout",
            objects=[checkout],
            misc=None,
        )

        self.assertIsInstance(checkout, models.KCIDBCheckout)

    def test_policies(self, mocked_send_signal):
        """Ensure policy is set as expected, and never updated."""
        policy_retrigger = models.Policy.objects.get(name=models.Policy.RETRIGGER)
        policy_public = models.Policy.objects.get(name=models.Policy.PUBLIC)
        policy_internal = models.Policy.objects.get(name=models.Policy.INTERNAL)

        input_domain = utils.product_dict(
            is_public=[None, False, True],
            retrigger=[None, False, True],
            update_is_public=[None, False, True],
            update_retrigger=[None, False, True],
        )
        for i, case_kwargs in enumerate(input_domain):
            with self.subTest("Create", **case_kwargs):
                data = {
                    "id": f"redhat:{i}",
                    "origin": "redhat",
                    "misc": clean_dict({
                        "is_public": case_kwargs["is_public"],
                        "retrigger": case_kwargs["retrigger"],
                    }),
                }

                checkout = models.KCIDBCheckout.create_from_json(data)

                mocked_send_signal.assert_called_once_with(
                    sender="kcidb.checkout.created_object_send_message",
                    status=models.ObjectStatusEnum.NEW,
                    object_type="checkout",
                    objects=[checkout],
                    misc=None,
                )
                mocked_send_signal.reset_mock()

                match case_kwargs:
                    case {"retrigger": True, "is_public": is_public}:
                        expected_policy = policy_retrigger
                        expected_retrigger = True
                        expected_is_public = bool(is_public)
                    case {"is_public": True}:
                        expected_policy = policy_public
                        expected_retrigger = False
                        expected_is_public = True
                    case _:
                        expected_policy = policy_internal
                        expected_retrigger = False
                        expected_is_public = False

                self.assertEqual(checkout.policy, expected_policy)
                self.assertEqual(checkout.retrigger, expected_retrigger)
                self.assertEqual(checkout.is_public, expected_is_public)

            with self.subTest("Update", **case_kwargs):
                data = {
                    "id": f"redhat:{i}",
                    "origin": "redhat",
                    "misc": clean_dict({
                        "is_public": case_kwargs["update_is_public"],
                        "retrigger": case_kwargs["update_retrigger"],
                    }),
                }

                expected_updated_retrigger = bool(case_kwargs["update_retrigger"])

                updated_checkout = models.KCIDBCheckout.create_from_json(data)

                mocked_send_signal.assert_not_called()

                self.assertEqual(updated_checkout.pk, checkout.pk, "Sanity check: same instance")
                self.assertEqual(updated_checkout.policy, expected_policy, "Policy shouldn't update")
                self.assertEqual(updated_checkout.is_public, expected_is_public, "Visibility shouldn't update")
                self.assertEqual(updated_checkout.retrigger, expected_updated_retrigger, "Retrigger always updates")

    def test_re_submit(self, mocked_send_signal):
        """Submit twice. Updates current data."""
        data = {
            'id': 'redhat:1',
            'origin': 'redhat',
            'misc': {
                'scratch': True,
                'is_public': True,
                'retrigger': True,
            }
        }
        checkout = models.KCIDBCheckout.create_from_json(data)
        self.assertIsNone(checkout.message_id)
        self.assertTrue(checkout.scratch)
        self.assertTrue(checkout.is_public)
        self.assertEqual(models.Policy.RETRIGGER, checkout.policy.name)

        mocked_send_signal.assert_called_once_with(
            sender="kcidb.checkout.created_object_send_message",
            status=models.ObjectStatusEnum.NEW,
            object_type="checkout",
            objects=[checkout],
            misc=None,
        )
        mocked_send_signal.reset_mock()

        # Re submit with new data.
        data.update({
            'message_id': '<foo@bar.com>',
            'misc': {
                'scratch': False,
                'is_public': False,
                'retrigger': False,
            }
        })
        checkout = models.KCIDBCheckout.create_from_json(data)
        self.assertEqual(checkout.message_id, data['message_id'])
        self.assertFalse(checkout.scratch, "Expected to have been updated")
        # Some properties shouldn't update
        self.assertTrue(checkout.is_public, "Expected to keep the initial value")
        self.assertEqual(models.Policy.RETRIGGER, checkout.policy.name, "Expected to keep the initial value")

        mocked_send_signal.assert_called_once_with(
            sender="kcidb.checkout.created_object_send_message",
            status=models.ObjectStatusEnum.UPDATED,
            object_type="checkout",
            objects=[checkout],
            misc=None,
        )
        mocked_send_signal.reset_mock()

        # Re submit with new data, without the previous value.
        del data['message_id']
        data['valid'] = True
        checkout = models.KCIDBCheckout.create_from_json(data)
        self.assertIsNotNone(checkout.message_id)
        self.assertEqual(checkout.valid, data['valid'])

        mocked_send_signal.assert_called_once_with(
            sender="kcidb.checkout.created_object_send_message",
            status=models.ObjectStatusEnum.UPDATED,
            object_type="checkout",
            objects=[checkout],
            misc=None,
        )

        # Check there's only one instance
        self.assertEqual(
            1,
            models.KCIDBCheckout.objects.filter(id=data['id']).count()
        )

    @responses.activate
    def test_patch(self, mocked_send_signal):
        """Submit email patches."""
        body = (
            b'From foo@baz Mon 25 Nov 2019 02:27:19 PM CET\n'
            b'From: Some One <some-one@redhat.com>\n'
            b'Date: Tue, 19 Nov 2019 23:47:33 +0100\n'
            b'Subject: fix something somewhere\n'
            b'..')
        responses.add(responses.GET, 'http://some-patch.server/patch/1234', body=body)
        responses.add(responses.GET, 'http://some-patch.server/patch/1235', body=body)
        data = {
            'id': 'redhat:1',
            'origin': 'redhat',
            'patchset_files': [
                {'url': 'http://some-patch.server/patch/1234', 'name': '1234'},
                {'url': 'http://some-patch.server/patch/1235'}
            ],
        }

        checkout = models.KCIDBCheckout.create_from_json(data)

        mocked_send_signal.assert_called_once_with(
            sender="kcidb.checkout.created_object_send_message",
            status=models.ObjectStatusEnum.NEW,
            object_type="checkout",
            objects=[checkout],
            misc=None,
        )

        self.assertEqual(2, checkout.patches.count())
        self.assertFalse(hasattr(checkout.patches.first(), 'patchworkpatch'))

        expected_keys = [
            # If "name" is provided, use it as a subject
            ('http://some-patch.server/patch/1234', '1234'),
            # Otherwise, use the subject from the patch body
            ('http://some-patch.server/patch/1235', 'fix something somewhere'),
        ]
        patch_keys = [patch.natural_key() for patch in checkout.patches.all()]
        self.assertEqual(expected_keys, patch_keys)

    def test_log_url(self, mocked_send_signal):
        """Submit log_url."""
        data = {
            'id': 'redhat:1',
            'origin': 'redhat',
            'log_url': 'http://log.server/log.name',
        }

        checkout = models.KCIDBCheckout.create_from_json(data)

        mocked_send_signal.assert_called_once_with(
            sender="kcidb.checkout.created_object_send_message",
            status=models.ObjectStatusEnum.NEW,
            object_type="checkout",
            objects=[checkout],
            misc=None,
        )

        self.assertIsInstance(checkout.log, models.Artifact)
        self.assertEqual('http://log.server/log.name', checkout.log.url)
        self.assertEqual('log.name', checkout.log.name)

    def test_contacts(self, mocked_send_signal):
        """Test contact submission."""
        data = {
            'id': 'redhat:1',
            'origin': 'redhat',
            'contacts': ['someone@email.com', 'Some Other <some-other@mail.com>']
        }

        checkout = models.KCIDBCheckout.create_from_json(data)

        mocked_send_signal.assert_called_once_with(
            sender="kcidb.checkout.created_object_send_message",
            status=models.ObjectStatusEnum.NEW,
            object_type="checkout",
            objects=[checkout],
            misc=None,
        )

        self.assertEqual(2, checkout.contacts.count())
        contact_1 = checkout.contacts.get(email='someone@email.com')
        self.assertEqual('', contact_1.name)
        contact_2 = checkout.contacts.get(email='some-other@mail.com')
        self.assertEqual('Some Other', contact_2.name)

    def test_create_from_json_with_empty_strings(self, mocked_send_signal):
        """Test object submission with empty strings on fields that get parsed into FKs."""
        data = {
            'origin': 'redhat',
            'id': 'redhat:887318',
            'log_url': '',
            'tree_name': '',
            'submitter': '',
        }

        checkout = models.KCIDBCheckout.create_from_json(data)
        mocked_send_signal.assert_called_once_with(
            sender="kcidb.checkout.created_object_send_message",
            status=models.ObjectStatusEnum.NEW,
            object_type="checkout",
            objects=[checkout],
            misc=None,
        )

        self.assertEqual('redhat:887318', str(checkout), 'Assert behavior of __str__()')

        self.assertIsNone(checkout.log)
        self.assertIsNone(checkout.tree)
        self.assertIsNone(checkout.submitter)

    @responses.activate
    def test_all_data(self, mocked_send_signal):
        """Check it creates all the objects."""
        mock_patch()
        data = {
            'id': 'redhat:1',
            'origin': 'redhat',
            'tree_name': 'arm',
            'git_repository_url': 'https://repository.com/repo/kernel-rhel',
            'git_repository_branch': 'rhel-1.2.3',
            'git_commit_hash': '403cbf29a4e277ad4872515ec3854b175960bbdf',
            'git_commit_name': 'commit name',
            'patchset_files': [{'url': 'http://patchwork.server/patch/2322797/mbox/',
                                'name': 'mbox'}],
            'patchset_hash': '21c9a43d22cd02babb34b45a9defb881ae8228f0d034a0779b1321e851cad6a4',
            'message_id': '<e41888bcd8ecf2e9bc8cc37c56386e01a5b43c56.some-one@redhat.com>',
            'comment': 'this is the comment',
            'start_time': '2020-06-01T06:47:41.108Z',
            'valid': True,
            'contacts': ['someone@email.com', 'Some Other <some-other@mail.com>'],
            'log_url': 'http://log.server/log.name',
            'log_excerpt': 'Some\nLog\nLines',
            'misc': {
                'brew_task_id': 123,
                'submitter': 'someone@email.com',
                'scratch': True,
                'is_public': True,
                'source_package_name': 'kernel',
                'all_sources_targeted': True,
                'patchset_modified_files': [
                    {'path': 'file/1'},
                    {'path': 'file/2'}
                ],
                'mr': {
                    'id': 9,
                    'url': 'https://url/to/merge/request',
                    'diff_url': 'https://url/to/merge/request',
                },
            },
        }
        # If the reader is interested in reading all queries, simply modify the assertion number
        # Summary: 11 SAVEPOINT + 11 RELEASE; 21 SELECT; 9 INSERT; 1 UPDATE
        with self.assertNumQueries(53):
            checkout = models.KCIDBCheckout.create_from_json(data)

        mocked_send_signal.assert_called_once_with(
            sender="kcidb.checkout.created_object_send_message",
            status=models.ObjectStatusEnum.NEW,
            object_type="checkout",
            objects=[checkout],
            misc=None,
        )
        mocked_send_signal.reset_mock()

        self.assertEqual('Some\nLog\nLines', checkout.log_excerpt)

        with self.subTest("Resubmit should be almost a noop"):
            # Summary: 5 SAVEPOINT + 5 RELEASE; 10 SELECT; 2 INSERT; 4 UPDATE
            with self.assertNumQueries(26):
                models.KCIDBCheckout.create_from_json(data)

            mocked_send_signal.assert_not_called()

        # Submit a base version without data to make sure resubmitting empty does not override missing fields
        checkout = models.KCIDBCheckout.create_from_json({
            'id': data['id'],
            'origin': data['origin']
        })

        mocked_send_signal.assert_not_called()

        self.assertEqual('redhat:1', checkout.id)
        self.assertIsInstance(checkout.origin, models.KCIDBOrigin)
        self.assertEqual('redhat', checkout.origin.name)

        self.assertIsInstance(checkout.tree, models.GitTree)
        self.assertEqual('arm', checkout.tree.name)

        self.assertEqual('https://repository.com/repo/kernel-rhel', checkout.git_repository_url)
        self.assertEqual('rhel-1.2.3', checkout.git_repository_branch)
        self.assertEqual('403cbf29a4e277ad4872515ec3854b175960bbdf', checkout.git_commit_hash)
        self.assertEqual('commit name', checkout.git_commit_name)
        self.assertEqual('<e41888bcd8ecf2e9bc8cc37c56386e01a5b43c56.some-one@redhat.com>', checkout.message_id)
        self.assertEqual('this is the comment', checkout.comment)
        self.assertEqual(True, checkout.valid)
        self.assertEqual('21c9a43d22cd02babb34b45a9defb881ae8228f0d034a0779b1321e851cad6a4', checkout.patchset_hash)
        self.assertEqual("Some\nLog\nLines", checkout.log_excerpt)
        self.assertEqual(True, checkout.public)
        self.assertEqual(models.Policy.PUBLIC, checkout.policy.name)
        self.assertEqual(
            datetime.datetime(2020, 6, 1, 6, 47, 41, 108000, tzinfo=datetime.timezone.utc),
            checkout.start_time
        )

        self.assertEqual(123, checkout.brew_task_id)
        self.assertIsInstance(checkout.submitter, models.Maintainer)
        self.assertEqual('someone@email.com', checkout.submitter.email)
        self.assertEqual(True, checkout.scratch)
        self.assertEqual('kernel', checkout.source_package_name)
        self.assertEqual(True, checkout.all_sources_targeted)
        self.assertEqual([{'path': 'file/1'}, {'path': 'file/2'}], checkout.patchset_modified_files)
        self.assertEqual(
            {'id': 9, 'url': 'https://url/to/merge/request', 'diff_url': 'https://url/to/merge/request'},
            checkout.related_merge_request
        )
        self.assertFalse(checkout.is_test_plan)

    def test_report_rules(self, mocked_send_signal):
        """Submit report rules."""
        data = {
            'id': 'redhat:1',
            'origin': 'redhat',
            'misc': {
                'report_rules': '[{"when": "always", "send_to": "foo@bar.com"}]'
            }
        }

        checkout = models.KCIDBCheckout.create_from_json(data)
        mocked_send_signal.assert_called_once_with(
            sender="kcidb.checkout.created_object_send_message",
            status=models.ObjectStatusEnum.NEW,
            object_type="checkout",
            objects=[checkout],
            misc=None,
        )

        self.assertEqual(
            [{'when': 'always', 'send_to': 'foo@bar.com'}],
            checkout.report_rules
        )

    def test_provenance(self, mocked_send_signal):
        """Check provenance fields are correctly set."""
        data = {
            'id': 'redhat:1',
            'origin': 'redhat',
            'misc': {
                'provenance': [
                    {'url': 'http://1', 'function': 'coordinator'},
                    {'url': 'http://2', 'function': 'executor', 'misc': {'some': 'data'}},
                ]
            },
        }

        with self.subTest("Fills provenance on creation"):
            checkout = models.KCIDBCheckout.create_from_json(data)

            mocked_send_signal.assert_called_once_with(
                sender="kcidb.checkout.created_object_send_message",
                status=models.ObjectStatusEnum.NEW,
                object_type="checkout",
                objects=[checkout],
                misc=None,
            )
            mocked_send_signal.reset_mock()

        with self.subTest("Resubmit without provenance key, should not remove it"):
            del data["misc"]["provenance"]
            updated_checkout = models.KCIDBCheckout.create_from_json(data)

            mocked_send_signal.assert_not_called()

            self.assertEqual(checkout.pk, updated_checkout.pk)

            self.assertQuerySetEqual(
                updated_checkout.provenance.order_by("url").values_list("url", "function", "misc"),
                [("http://1", "c", None), ("http://2", "e", {"some": "data"})],
            )
            mocked_send_signal.reset_mock()

        with self.subTest("Resubmit with different provenances, extend them"):
            data["misc"]["provenance"] = [{"url": "http://3", "function": "coordinator"}]
            updated_checkout = models.KCIDBCheckout.create_from_json(data)

            mocked_send_signal.assert_not_called()

            self.assertEqual(checkout.pk, updated_checkout.pk)

            self.assertQuerySetEqual(
                updated_checkout.provenance.order_by("url").values_list("url", "function", "misc"),
                [("http://1", "c", None), ("http://2", "e", {"some": "data"}), ("http://3", "c", None)],
            )


@override_settings(
    CELERY_TASK_ALWAYS_EAGER=True,
    # Disable behavior from kcidb objects' pre_save signal
    CLEAR_ISSUEOCCURRENCES_ON_SAVE=False,
)
@mock.patch("datawarehouse.signals.kcidb_object.send", wraps=signals.kcidb_object.send)
class TestKCIDBBuildFromJson(utils.TestCase):
    """Test creation of KCIDBBuild model."""

    fixtures = (
        "tests/fixtures/basic_policies.yaml",
        "tests/fixtures/basic.yaml",
    )

    def setUp(self):
        origin = models.KCIDBOrigin.create_from_string("redhat")
        self.checkout = models.KCIDBCheckout.objects.create(id="redhat:1", origin=origin)

    def test_missing_parent(self, mocked_send_signal):
        """Assert raises MissingParent as expected."""
        with self.assertRaises(models.MissingParent) as exception_ctx:
            data = {
                'checkout_id': 'nothing-like-this',
                'origin': 'redhat',
                'id': 'redhat:887318',
            }
            models.KCIDBBuild.create_from_json(data)
        self.assertEqual(str(exception_ctx.exception), 'KCIDBCheckout id=nothing-like-this is not present in the DB')

        mocked_send_signal.assert_not_called()

    @mock.patch("datawarehouse.metrics.update_time_to_build")
    def test_basic(self, mock_update_time_to_build, mocked_send_signal):
        """Submit only id."""
        data = {
            "checkout_id": self.checkout.id,
            "origin": "redhat",
            "id": "redhat:887318",
        }
        build = models.KCIDBBuild.create_from_json(data)

        mocked_send_signal.assert_called_once_with(
            sender="kcidb.build.created_object_send_message",
            status=models.ObjectStatusEnum.NEW,
            object_type="build",
            objects=[build],
            misc=None,
        )
        mocked_send_signal.reset_mock()

        self.assertEqual("redhat:887318", str(build), "Assert behavior of __str__()")

        self.assertIsInstance(build, models.KCIDBBuild)
        self.assertIsInstance(build.checkout, models.KCIDBCheckout)
        self.assertEqual('redhat:1', build.checkout.id)
        self.assertEqual('redhat:887318', build.id)
        self.assertIsInstance(build.origin, models.KCIDBOrigin)
        self.assertEqual('redhat', build.origin.name)

        mock_update_time_to_build.delay.assert_called_once_with(build.iid)

    def test_re_submit(self, mocked_send_signal):
        """Submit submit multiple times. Updates current data."""
        data = {
            "checkout_id": self.checkout.id,
            "origin": "redhat",
            "id": "redhat:887318",
            "architecture": "x86_64",
            "misc": {
                "debug": True,
            },
        }
        build = models.KCIDBBuild.create_from_json(data)

        mocked_send_signal.assert_called_once_with(
            sender="kcidb.build.created_object_send_message",
            status=models.ObjectStatusEnum.NEW,
            object_type="build",
            objects=[build],
            misc=None,
        )
        mocked_send_signal.reset_mock()

        self.assertIsNone(build.valid)

        # Re submit with new data.
        resubmit_data = copy.deepcopy(data)
        resubmit_data["valid"] = False
        resubmit_data["architecture"] = "aarch64"
        resubmit_data["misc"]["debug"] = False
        build = models.KCIDBBuild.create_from_json(resubmit_data)

        mocked_send_signal.assert_called_once_with(
            sender="kcidb.build.created_object_send_message",
            status=models.ObjectStatusEnum.UPDATED,
            object_type="build",
            objects=[build],
            misc=None,
        )
        mocked_send_signal.reset_mock()

        self.assertEqual(build.valid, resubmit_data["valid"], "Expected `valid` to be defined")
        self.assertEqual(
            build.architecture, models.ArchitectureEnum[data["architecture"]], "Expected `architecture` to be remain"
        )
        self.assertEqual(build.debug, data["misc"]["debug"], "Expected `debug` to be remain")

        # Re submit with new data, without the previous value.
        resubmit_again = copy.deepcopy(data)
        resubmit_again["comment"] = "foobar"
        build = models.KCIDBBuild.create_from_json(resubmit_again)

        mocked_send_signal.assert_called_once_with(
            sender="kcidb.build.created_object_send_message",
            status=models.ObjectStatusEnum.UPDATED,
            object_type="build",
            objects=[build],
            misc=None,
        )

        self.assertIsNotNone(build.valid)
        self.assertEqual(build.comment, resubmit_again["comment"])

        # Check there's only one instance
        self.assertEqual(
            1,
            models.KCIDBBuild.objects.filter(id=data['id']).count()
        )

    def test_submit_files(self, mocked_send_signal):
        """Test files submission. log_url, input_files and output_files."""
        data = {
            'checkout_id': 'redhat:1',
            'origin': 'redhat',
            'id': 'redhat:887318',
            'log_url': 'http://log.server/log.name',
            'input_files': [
                {'url': 'http://log.server/input.file', 'name': 'input.file'},
                {'url': 'http://log.server/input.file.2', 'name': 'input.file.2'},
            ],
            'output_files': [
                {'url': 'http://log.server/output.file', 'name': 'output.file'},
                {'url': 'http://log.server/output.file.2', 'name': 'output.file.2'},
            ],
        }
        build = models.KCIDBBuild.create_from_json(data)

        mocked_send_signal.assert_called_once_with(
            sender="kcidb.build.created_object_send_message",
            status=models.ObjectStatusEnum.NEW,
            object_type="build",
            objects=[build],
            misc=None,
        )

        # log_url
        self.assertIsInstance(build.log, models.Artifact)
        self.assertEqual('http://log.server/log.name', build.log.url)
        self.assertEqual('log.name', build.log.name)

        # input_files
        self.assertListEqual(
            [{'url': 'http://log.server/input.file', 'name': 'input.file'},
             {'url': 'http://log.server/input.file.2', 'name': 'input.file.2'}],
            [{'url': file.url, 'name': file.name} for file in build.input_files.order_by('url').all()]
        )

        # output_files
        self.assertListEqual(
            [{'url': 'http://log.server/output.file', 'name': 'output.file'},
             {'url': 'http://log.server/output.file.2', 'name': 'output.file.2'}],
            [{'url': file.url, 'name': file.name} for file in build.output_files.order_by('url').all()]
        )

    def test_re_submit_files(self, mocked_send_signal):
        """Submit files multiple times."""
        data = {
            'checkout_id': 'redhat:1',
            'origin': 'redhat',
            'id': 'redhat:887318',
            'input_files': [
                {'url': 'http://log.server/input.file.1', 'name': 'input.file.1'},
                {'url': 'http://log.server/input.file.2', 'name': 'input.file.2'},
            ],
            'output_files': [
                {'url': 'http://log.server/output.file.1', 'name': 'output.file.1'},
                {'url': 'http://log.server/output.file.2', 'name': 'output.file.2'},
            ],
        }

        build = models.KCIDBBuild.create_from_json(data)

        mocked_send_signal.assert_called_once_with(
            sender="kcidb.build.created_object_send_message",
            status=models.ObjectStatusEnum.NEW,
            object_type="build",
            objects=[build],
            misc=None,
        )
        mocked_send_signal.reset_mock()

        self.assertEqual(
            set(build.input_files.all()),
            set(models.Artifact.objects.filter(name__in=['input.file.1', 'input.file.2']))
        )
        self.assertEqual(
            set(build.output_files.all()),
            set(models.Artifact.objects.filter(name__in=['output.file.1', 'output.file.2']))
        )

        # With new files, the current ones are replaced
        data.update({
            'input_files': [
                {'url': 'http://log.server/input.file.3', 'name': 'input.file.3'},
            ],
            'output_files': [
                {'url': 'http://log.server/output.file.3', 'name': 'output.file.3'},
            ],
        })

        build = models.KCIDBBuild.create_from_json(data)

        mocked_send_signal.assert_not_called()

        self.assertEqual(
            set(build.input_files.all()),
            set(models.Artifact.objects.filter(name='input.file.3'))
        )
        self.assertEqual(
            set(build.output_files.all()),
            set(models.Artifact.objects.filter(name='output.file.3'))
        )

        # If no files are provided, the files are not removed
        del data['input_files']
        del data['output_files']

        build = models.KCIDBBuild.create_from_json(data)

        mocked_send_signal.assert_not_called()

        self.assertEqual(build.input_files.count(), 1)
        self.assertEqual(build.output_files.count(), 1)

    def test_create_from_json_with_empty_strings(self, mocked_send_signal):
        """Test object submission with empty strings on fields that get parsed into FKs."""
        data = {
            'checkout_id': 'redhat:1',
            'origin': 'redhat',
            'id': 'redhat:887318',
            'architecture': '',
            'compiler': '',
            'log_url': '',
        }

        build = models.KCIDBBuild.create_from_json(data)

        mocked_send_signal.assert_called_once_with(
            sender="kcidb.build.created_object_send_message",
            status=models.ObjectStatusEnum.NEW,
            object_type="build",
            objects=[build],
            misc=None,
        )
        mocked_send_signal.reset_mock()

        self.assertIsNone(build.architecture)
        self.assertIsNone(build.compiler)
        self.assertIsNone(build.log)

    def test_all_data(self, mocked_send_signal):
        """Test complete object submission."""
        data = {
            'checkout_id': 'redhat:1',
            'origin': 'redhat',
            'id': 'redhat:887318',
            'log_url': 'http://log.server/log.name',
            'log_excerpt': 'Some\nLog\nLines',
            'input_files': [
                {'url': 'http://log.server/input.file', 'name': 'input.file'},
                {'url': 'http://log.server/input.file.2', 'name': 'input.file.2'},
            ],
            'output_files': [
                {'url': 'http://log.server/output.file', 'name': 'output.file'},
                {'url': 'http://log.server/output.file.2', 'name': 'output.file.2'},
            ],
            'start_time': '2020-06-03T15:14:57.215Z',
            'duration': 632,
            'architecture': 'aarch64',
            'command': 'make rpmbuild ...',
            'compiler': 'aarch64-linux-gnu-gcc (GCC) 8.2.1 20181105 (Red Hat Cross 8.2.1-1)',
            'config_name': 'fedora',
            'valid': True,
            'misc': {
                'test_plan_missing': True,
                'debug': True,
                'kpet_tree_name': 'kpet_tree_name',
                'package_name': 'package_name',
                'testing_skipped_reason': 'unsupported',
            }
        }
        # If the reader is interested in reading all queries, simply modify the assertion number
        # Summary: 9 SAVEPOINT + 9 RELEASE; 23 SELECT; 9 INSERT
        with self.assertNumQueries(50):
            build = models.KCIDBBuild.create_from_json(data)

        mocked_send_signal.assert_called_once_with(
            sender="kcidb.build.created_object_send_message",
            status=models.ObjectStatusEnum.NEW,
            object_type="build",
            objects=[build],
            misc=None,
        )
        mocked_send_signal.reset_mock()

        self.assertEqual('Some\nLog\nLines', build.log_excerpt)

        with self.subTest("Resubmit should be almost a noop"):
            # Summary: 2 SAVEPOINT + 2 RELEASE; 14 SELECT; 1 UPDATE
            with self.assertNumQueries(19):
                models.KCIDBBuild.create_from_json(data)

            mocked_send_signal.assert_not_called()

        # Submit a base version without data to make sure resubmitting empty does not override missing fields
        build = models.KCIDBBuild.create_from_json({
            'checkout_id': data['checkout_id'],
            'id': data['id'],
            'origin': data['origin']
        })

        mocked_send_signal.assert_not_called()

        self.assertEqual(
            datetime.datetime(2020, 6, 3, 15, 14, 57, 215000, tzinfo=datetime.timezone.utc),
            build.start_time
        )
        self.assertEqual(632, build.duration)
        self.assertEqual('make rpmbuild ...', build.command)
        self.assertEqual('fedora', build.config_name)
        self.assertEqual(True, build.valid)

        self.assertEqual(models.ArchitectureEnum['aarch64'], build.architecture)
        self.assertIsInstance(build.compiler, models.Compiler)
        self.assertEqual('aarch64-linux-gnu-gcc (GCC) 8.2.1 20181105 (Red Hat Cross 8.2.1-1)', build.compiler.name)
        self.assertEqual("Some\nLog\nLines", build.log_excerpt)
        self.assertIsInstance(build.log, models.Artifact)
        self.assertEqual(build.log.url, 'http://log.server/log.name')
        self.assertTrue(build.test_plan_missing)
        self.assertTrue(build.debug)

        self.assertEqual('kpet_tree_name', build.kpet_tree_name)
        self.assertEqual('package_name', build.package_name)
        self.assertEqual('unsupported', build.testing_skipped_reason)
        self.assertFalse(build.is_test_plan)

    def test_test_plan_missing(self, mocked_send_signal):
        """Test misc/test_plan_missing flag submission and update."""
        data = {
            "checkout_id": "redhat:1",
            "origin": "redhat",
            "id": "redhat:887318",
            "misc": {},
        }
        build = models.KCIDBBuild.create_from_json(data)

        mocked_send_signal.assert_called_once_with(
            sender="kcidb.build.created_object_send_message",
            status=models.ObjectStatusEnum.NEW,
            object_type="build",
            objects=[build],
            misc=None,
        )
        mocked_send_signal.reset_mock()

        cases = [
            # misc, expected value
            ({"test_plan_missing": True}, True),
            ({"test_plan_missing": False}, False),
        ]

        for misc, expected in cases:
            data["misc"] = misc
            build = models.KCIDBBuild.create_from_json(data)

            mocked_send_signal.assert_not_called()

            self.assertEqual(expected, build.test_plan_missing)

    def test_provenance(self, mocked_send_signal):
        """Check provenance fields are correctly set."""
        data = {
            'checkout_id': 'redhat:1',
            'origin': 'redhat',
            'id': 'redhat:2',
            'misc': {
                'provenance': [
                    {'url': 'http://1', 'function': 'coordinator'},
                    {'url': 'http://2', 'function': 'executor', 'misc': {'some': 'data'}},
                ]
            },
        }

        with self.subTest("Fills provenance on creation"):
            build = models.KCIDBBuild.create_from_json(data)

            mocked_send_signal.assert_called_once_with(
                sender="kcidb.build.created_object_send_message",
                status=models.ObjectStatusEnum.NEW,
                object_type="build",
                objects=[build],
                misc=None,
            )
            mocked_send_signal.reset_mock()

        with self.subTest("Resubmit without provenance key, should not remove it"):
            del data["misc"]["provenance"]
            updated_build = models.KCIDBBuild.create_from_json(data)

            self.assertEqual(build.pk, updated_build.pk)

            mocked_send_signal.assert_not_called()

            self.assertQuerySetEqual(
                updated_build.provenance.order_by("url").values_list("url", "function", "misc"),
                [("http://1", "c", None), ("http://2", "e", {"some": "data"})],
            )

        with self.subTest("Resubmit with different provenances, extend them"):
            data["misc"]["provenance"] = [{"url": "http://3", "function": "coordinator"}]
            actually_updated_build = models.KCIDBBuild.create_from_json(data)

            mocked_send_signal.assert_not_called()

            self.assertEqual(build.pk, actually_updated_build.pk)

            self.assertQuerySetEqual(
                actually_updated_build.provenance.order_by("url").values_list("url", "function", "misc"),
                [("http://3", "c", None)],
            )


@override_settings(
    CELERY_TASK_ALWAYS_EAGER=True,
    # Disable behavior from kcidb objects' pre_save signal
    CLEAR_ISSUEOCCURRENCES_ON_SAVE=False,
)
@mock.patch("datawarehouse.signals.kcidb_object.send", wraps=signals.kcidb_object.send)
class TestKCIDBTestFromJson(utils.TestCase):
    """Test creation of KCIDBTest model."""

    fixtures = (
        "tests/fixtures/basic_policies.yaml",
        "tests/fixtures/basic.yaml",
    )

    def setUp(self):
        origin = models.KCIDBOrigin.create_from_string("redhat")
        checkout = models.KCIDBCheckout.objects.create(id="redhat:1", origin=origin)
        self.build = models.KCIDBBuild.objects.create(
            checkout=checkout, id="redhat:887318", origin=origin, architecture=models.ArchitectureEnum.aarch64
        )

    def test_missing_parent(self, mocked_send_signal):
        """Assert raises MissingParent as expected."""
        with self.assertRaises(models.MissingParent) as exception_ctx:
            data = {
                'build_id': 'nothing-like-this',
                'id': 'redhat:111218982',
                'origin': 'redhat',
            }
            models.KCIDBTest.create_from_json(data)
        self.assertEqual(str(exception_ctx.exception), 'KCIDBBuild id=nothing-like-this is not present in the DB')

        mocked_send_signal.assert_not_called()

    def test_basic(self, mocked_send_signal):
        """Submit only id."""
        data = {
            "build_id": self.build.id,
            "id": "redhat:111218982",
            "origin": "redhat",
        }
        test = models.KCIDBTest.create_from_json(data)

        mocked_send_signal.assert_called_once_with(
            sender="kcidb.test.created_object_send_message",
            status=models.ObjectStatusEnum.NEW,
            object_type="test",
            objects=[test],
            misc=None,
        )

        self.assertEqual('redhat:111218982', str(test), 'Assert behavior of __str__()')

        self.assertIsInstance(test, models.KCIDBTest)
        self.assertIsInstance(test.build, models.KCIDBBuild)
        self.assertEqual(self.build.id, test.build.id)

        # These are False by default
        self.assertFalse(test.targeted)

    def test_re_submit(self, mocked_send_signal):
        """Submit multiple times. Updates current data."""
        data = {
            "build_id": self.build.id,
            "id": "redhat:111218982",
            "origin": "redhat",
            "misc": {
                "targeted": False,
            },
        }
        test = models.KCIDBTest.create_from_json(data)

        mocked_send_signal.assert_called_once_with(
            sender="kcidb.test.created_object_send_message",
            status=models.ObjectStatusEnum.NEW,
            object_type="test",
            objects=[test],
            misc=None,
        )
        mocked_send_signal.reset_mock()

        self.assertIsNone(test.waived)

        # Re submit with new data.
        data['waived'] = True
        data['misc']['targeted'] = True
        test = models.KCIDBTest.create_from_json(data)

        mocked_send_signal.assert_called_once_with(
            sender="kcidb.test.created_object_send_message",
            status=models.ObjectStatusEnum.UPDATED,
            object_type="test",
            objects=[test],
            misc=None,
        )
        mocked_send_signal.reset_mock()

        self.assertTrue(test.waived, 'Expected to have been updated')
        # Some properties should not be updated
        self.assertFalse(test.targeted, 'Expected to keep the initial value')

        # Re submit with new data, without the previous value.
        del data['waived']
        data['duration'] = 123
        test = models.KCIDBTest.create_from_json(data)

        mocked_send_signal.assert_called_once_with(
            sender="kcidb.test.created_object_send_message",
            status=models.ObjectStatusEnum.UPDATED,
            object_type="test",
            objects=[test],
            misc=None,
        )
        mocked_send_signal.reset_mock()

        mocked_send_signal.assert_not_called()

        self.assertTrue(test.waived, 'Expected to keep the last value')
        self.assertEqual(test.duration, data['duration'])

        # Check there's only one instance
        self.assertEqual(
            1,
            models.KCIDBTest.objects.filter(id=data['id']).count()
        )

    def test_output_files(self, mocked_send_signal):
        """Check created artifacts for output_files."""
        data = {
            "build_id": self.build.id,
            "id": "redhat:111218982",
            "origin": "redhat",
            "output_files": [
                {"url": "http://log.server/output.file", "name": "output.file"},
                {"url": "http://log.server/output.file.2", "name": "output.file.2"},
            ],
        }
        test = models.KCIDBTest.create_from_json(data)

        mocked_send_signal.assert_called_once_with(
            sender="kcidb.test.created_object_send_message",
            status=models.ObjectStatusEnum.NEW,
            object_type="test",
            objects=[test],
            misc=None,
        )

        self.assertListEqual(
            [
                {'url': 'http://log.server/output.file', 'name': 'output.file'},
                {'url': 'http://log.server/output.file.2', 'name': 'output.file.2'},
            ],
            [
                {'url': file.url, 'name': file.name}
                for file in test.output_files.order_by('name').all()
            ]
        )

    def test_maintainers_empty(self, mocked_send_signal):
        """Test maintainers field as comma separated string."""
        data = {
            "build_id": self.build.id,
            "id": "redhat:111218982",
            "origin": "redhat",
            "comment": "Boot test",
            "misc": {"maintainers": None},
        }
        kcidb_test = models.KCIDBTest.create_from_json(data)

        mocked_send_signal.assert_called_once_with(
            sender="kcidb.test.created_object_send_message",
            status=models.ObjectStatusEnum.NEW,
            object_type="test",
            objects=[kcidb_test],
            misc=None,
        )

    def test_maintainers_not_empty(self, mocked_send_signal):
        """Test maintainers field as dict."""
        data = {
            "build_id": self.build.id,
            "id": "redhat:111218982",
            "origin": "redhat",
            "comment": "Boot test",
            "misc": {
                "maintainers": [
                    {"name": "Maintainer One", "email": "one@maintainer.com"},
                    {"email": "two@maintainer.com", "gitlab": "two"},
                    {"email": "three@maintainer.com"},
                ]
            },
        }
        test = models.KCIDBTest.create_from_json(data)

        mocked_send_signal.assert_called_once_with(
            sender="kcidb.test.created_object_send_message",
            status=models.ObjectStatusEnum.NEW,
            object_type="test",
            objects=[test],
            misc=None,
        )
        mocked_send_signal.reset_mock()

        cases = [
            ('Maintainer One', 'one@maintainer.com', None),
            ('', 'two@maintainer.com', 'two'),
            ('', 'three@maintainer.com', None),
        ]

        for name, email, gitlab_username in cases:
            self.assertTrue(
                test.test.maintainers.filter(
                    name=name, email=email, gitlab_username=gitlab_username).exists(),
                (name, email)
            )

    def test_maintainers_overwrite(self, mocked_send_signal):
        """Test behaviour with empty maintainers list. Should not overwrite."""
        data = {
            "build_id": self.build.id,
            "id": "redhat:111218982",
            "origin": "redhat",
            "comment": "Boot test",
            "misc": {
                "maintainers": [
                    {"name": "Maintainer One", "email": "one@maintainer.com"},
                ]
            },
        }
        kcidb_test = models.KCIDBTest.create_from_json(data)

        mocked_send_signal.assert_called_once_with(
            sender="kcidb.test.created_object_send_message",
            status=models.ObjectStatusEnum.NEW,
            object_type="test",
            objects=[kcidb_test],
            misc=None,
        )
        mocked_send_signal.reset_mock()

        # Resubmit test without maintainers
        data = {
            "build_id": self.build.id,
            "id": "redhat:111218982",
            "origin": "redhat",
            "comment": "Boot test",
        }
        kcidb_test = models.KCIDBTest.create_from_json(data)

        mocked_send_signal.assert_not_called()

        self.assertEqual(
            'Maintainer One',
            kcidb_test.test.maintainers.get().name
        )

    def test_create_from_json_with_empty_strings(self, mocked_send_signal):
        """Test object submission with empty strings on fields that get parsed into FKs."""
        data = {
            "build_id": self.build.id,
            "origin": "redhat",
            "id": "redhat:111218982",
            "environment": "",
            "comment": "",
            "status": "",
            "log_url": "",
        }

        test = models.KCIDBTest.create_from_json(data)

        mocked_send_signal.assert_called_once_with(
            sender="kcidb.test.created_object_send_message",
            status=models.ObjectStatusEnum.NEW,
            object_type="test",
            objects=[test],
            misc=None,
        )

        self.assertIsNone(test.environment)
        self.assertIsNone(test.test)
        self.assertIsNone(test.log)
        self.assertEqual(test.status, '')

    def test_all_data(self, mocked_send_signal):
        """Test complete object submission."""
        data = {
            "build_id": self.build.id,
            "id": "redhat:111218982",
            "origin": "redhat",
            "environment": {"comment": "hostname.redhat.com"},
            "path": "boot",
            "comment": "Boot test",
            "waived": False,
            "start_time": "2020-06-03T15:52:25Z",
            "duration": 158,
            "status": "PASS",
            "log_url": "http://log.server/log.name",
            "log_excerpt": "Some\nLog\nLines",
            "output_files": [
                {"url": "http://log.server/output.file", "name": "output.file"},
            ],
            "misc": {
                "maintainers": [
                    {"name": "Cosme Fulanito", "email": "cosme@fulanito.com"},
                    {"name": "Pepe", "email": "pe@pe.com"},
                ],
                "targeted": True,
                "fetch_url": "http://test.url",
                "polarion_id": "2111a0fa-1c27-4663-94d5-a951c509413e",
                "results": [
                    {
                        "id": "redhat:1.1",
                        "status": "FAIL",
                        "output_files": [
                            {"url": "http://log.server/result-1.file", "name": "result-1.file"},
                        ],
                    },
                    {
                        "id": "redhat:1.2",
                        "status": "SKIP",
                        "output_files": [
                            {"url": "http://log.server/result-2.file", "name": "result-2.file"},
                        ],
                    },
                ],
            },
        }
        # If the reader is interested in reading all queries, simply modify the assertion number
        # Summary: 17 SAVEPOINT + 17 RELEASE; 33 SELECT; 15 INSERT
        with self.assertNumQueries(82):
            test = models.KCIDBTest.create_from_json(data)

        mocked_send_signal.assert_called_once_with(
            sender="kcidb.test.created_object_send_message",
            status=models.ObjectStatusEnum.NEW,
            object_type="test",
            objects=[test],
            misc=None,
        )
        mocked_send_signal.reset_mock()

        self.assertEqual('Some\nLog\nLines', test.log_excerpt)

        with self.subTest("Resubmit should be almost a noop"):
            # Summary: 6 SAVEPOINT + 6 RELEASE; 27 SELECT; 3 UPDATE
            with self.assertNumQueries(42):
                models.KCIDBTest.create_from_json(data)

            mocked_send_signal.assert_not_called()

        # Submit a base version without data to make sure resubmitting empty does not override missing fields
        test = models.KCIDBTest.create_from_json({
            'build_id': data['build_id'],
            'id': data['id'],
            'origin': data['origin'],
        })

        mocked_send_signal.assert_not_called()

        self.assertIsInstance(test.environment, models.BeakerResource)
        self.assertEqual('hostname.redhat.com', test.environment.fqdn)

        self.assertEqual('P', test.status)

        self.assertIsInstance(test.test, models.Test)
        self.assertEqual('boot', test.test.universal_id)
        self.assertEqual('Boot test', test.test.name)
        self.assertEqual(2, test.test.maintainers.count())
        self.assertTrue(
            test.test.maintainers.filter(name='Cosme Fulanito', email='cosme@fulanito.com').exists()
        )
        self.assertTrue(
            test.test.maintainers.filter(name='Pepe', email='pe@pe.com').exists()
        )
        self.assertEqual('http://test.url', test.test.fetch_url)
        self.assertEqual('2111a0fa-1c27-4663-94d5-a951c509413e', test.polarion_id)

        self.assertEqual(False, test.waived)
        self.assertEqual(
            datetime.datetime(2020, 6, 3, 15, 52, 25, 0, tzinfo=datetime.timezone.utc),
            test.start_time
        )
        self.assertEqual(158, test.duration)

        self.assertEqual(1, test.output_files.count())
        self.assertTrue(test.targeted)

        self.assertEqual('http://log.server/log.name', test.log.url)
        self.assertEqual('log.name', test.log.name)
        self.assertEqual("Some\nLog\nLines", test.log_excerpt)
        self.assertFalse(test.is_test_plan)

    def test_re_submit_files(self, mocked_send_signal):
        """Submit files multiple times."""
        data = {
            "build_id": self.build.id,
            "id": "redhat:111218982",
            "origin": "redhat",
            "output_files": [
                {"url": "http://log.server/output.file", "name": "output.file"},
                {"url": "http://log.server/output.file.2", "name": "output.file.2"},
            ],
        }
        test = models.KCIDBTest.create_from_json(data)

        mocked_send_signal.assert_called_once_with(
            sender="kcidb.test.created_object_send_message",
            status=models.ObjectStatusEnum.NEW,
            object_type="test",
            objects=[test],
            misc=None,
        )
        mocked_send_signal.reset_mock()

        self.assertEqual(
            set(test.output_files.all()),
            set(models.Artifact.objects.filter(name__in=['output.file', 'output.file.2']))
        )

        # With new files, the current ones are replaced
        data.update({
            'output_files': [
                {'url': 'http://log.server/output.file.3', 'name': 'output.file.3'},
            ],
        })

        test = models.KCIDBTest.create_from_json(data)

        mocked_send_signal.assert_not_called()

        self.assertEqual(
            set(test.output_files.all()),
            set(models.Artifact.objects.filter(name='output.file.3'))
        )

        # If no files are provided, the files are not removed
        del data['output_files']

        test = models.KCIDBTest.create_from_json(data)

        mocked_send_signal.assert_not_called()

        self.assertEqual(test.output_files.count(), 1)

    def test_provenance(self, mocked_send_signal):
        """Check provenance fields are correctly set."""
        data = {
            "build_id": self.build.id,
            "origin": "redhat",
            "id": "redhat:111218982",
            "misc": {
                "provenance": [
                    {"url": "http://1", "function": "coordinator"},
                    {"url": "http://2", "function": "executor", "misc": {"some": "data"}},
                ]
            },
        }

        with self.subTest("Fills provenance on creation"):
            kcidb_test = models.KCIDBTest.create_from_json(data)

            mocked_send_signal.assert_called_once_with(
                sender="kcidb.test.created_object_send_message",
                status=models.ObjectStatusEnum.NEW,
                object_type="test",
                objects=[kcidb_test],
                misc=None,
            )
            mocked_send_signal.reset_mock()

        with self.subTest("Resubmit without provenance key, should not remove it"):
            del data["misc"]["provenance"]
            updated_kcidb_test = models.KCIDBTest.create_from_json(data)

            mocked_send_signal.assert_not_called()

            self.assertEqual(kcidb_test.pk, updated_kcidb_test.pk)

            self.assertQuerySetEqual(
                updated_kcidb_test.provenance.order_by("url").values_list("url", "function", "misc"),
                [("http://1", "c", None), ("http://2", "e", {"some": "data"})],
            )

        with self.subTest("Resubmit with different provenances, overwrites them"):
            data["misc"]["provenance"] = [{"url": "http://3", "function": "coordinator"}]
            actually_updated_kcidb_test = models.KCIDBTest.create_from_json(data)

            mocked_send_signal.assert_not_called()

            self.assertEqual(kcidb_test.pk, actually_updated_kcidb_test.pk)

            self.assertQuerySetEqual(
                actually_updated_kcidb_test.provenance.order_by("url").values_list("url", "function", "misc"),
                [("http://3", "c", None)],
            )

    def test_results(self, mocked_send_signal):
        """Assert TestResults are created and recreated as expected.

        KCIDBTestResult itself can be updated selecting by ID, but due to how
        those IDs are created by Beaker, it's unlikely they would be the same
        between runs. Therefore, the expected behavior is recreating them every time.
        """
        data = {
            "build_id": self.build.id,
            "id": "redhat:1",
            "origin": "redhat",
            "misc": {"results": []},
        }
        results_1_a = [{'id': 'redhat:1.1', 'status': 'FAIL'}, {'id': 'redhat:1.2', 'status': 'SKIP'}]
        expected_1_a = [('redhat:1.1', 'F'), ('redhat:1.2', 'S')]
        results_1_b = [{'id': 'redhat:1.1', 'status': 'FAIL'}, {'id': 'redhat:1.2', 'status': 'PASS'}]
        expected_1_b = [('redhat:1.1', 'F'), ('redhat:1.2', 'P')]
        results_2 = [{'id': 'redhat:1.3', 'status': 'ERROR'}, {'id': 'redhat:1.4', 'status': 'PASS'}]
        expected_2 = [('redhat:1.3', 'E'), ('redhat:1.4', 'P')]

        iids_before = []

        with self.subTest('Create TestResults'):
            data['misc']['results'] = results_1_a

            test = models.KCIDBTest.create_from_json(data)

            mocked_send_signal.assert_called_once_with(
                sender="kcidb.test.created_object_send_message",
                status=models.ObjectStatusEnum.NEW,
                object_type="test",
                objects=[test],
                misc=None,
            )
            mocked_send_signal.reset_mock()

            results = test.kcidbtestresult_set.all()
            self.assertQuerySetEqual(results.values_list('id', 'status'), expected_1_a)

            iids_after = list(results.values_list('iid', flat=True))
            self.assertNotEqual(iids_before, iids_after, "Expected KCIDBTestResult to be created")

            iids_before = iids_after

        with self.subTest('Update TestResults with the same IDs'):
            # NOTE: Updating a TestResult attribute is unexpected, but it's allowed.
            # The actual behavior we're seeking is a no-op when given the same KCIDB file.

            data['misc']['results'] = results_1_b

            test = models.KCIDBTest.create_from_json(data)

            mocked_send_signal.assert_not_called()

            results = test.kcidbtestresult_set.all()
            self.assertQuerySetEqual(results.values_list('id', 'status'), expected_1_b)

            iids_after = list(results.values_list('iid', flat=True))
            self.assertEqual(iids_before, iids_after, "Expected KCIDBTestResult to remain")

            iids_before = iids_after

        with self.subTest('Recreate TestResults with new IDs'):
            data['misc']['results'] = results_2

            test = models.KCIDBTest.create_from_json(data)

            mocked_send_signal.assert_not_called()

            results = test.kcidbtestresult_set.all()
            iids_after = list(results.values_list('iid', flat=True))

            self.assertQuerySetEqual(results.values_list('id', 'status'), expected_2)
            self.assertNotEqual(iids_before, iids_after, "Expected KCIDBTestResult to be recreated")

            iids_before = iids_after

        with self.subTest('Keep TestResults if missing key'):
            del data['misc']['results']

            test = models.KCIDBTest.create_from_json(data)

            mocked_send_signal.assert_not_called()

            results = test.kcidbtestresult_set.all()
            iids_after = list(results.values_list('iid', flat=True))

            self.assertQuerySetEqual(results.values_list('id', 'status'), expected_2)
            self.assertEqual(iids_before, iids_after, "Expected KCIDBTestResult to remain")

        with self.subTest('TestResults cleared if given empty list'):
            data['misc']['results'] = []

            test = models.KCIDBTest.create_from_json(data)

            mocked_send_signal.assert_not_called()

            results = test.kcidbtestresult_set.all()

            self.assertQuerySetEqual(results.values_list('id', 'status'), [],
                                     "Expected KCIDBTestResult to be removed")


@override_settings(
    CELERY_TASK_ALWAYS_EAGER=True,
    # Disable behavior from kcidb objects' pre_save signal
    CLEAR_ISSUEOCCURRENCES_ON_SAVE=False,
)
@mock.patch('datawarehouse.scripts.misc.signalize_kcidb_object', wraps=scripts.misc.signalize_kcidb_object)
@mock.patch('datawarehouse.signals.kcidb_object')
class TestKCIDBNotifications(utils.TestCase):
    """Test kcidb notifications are sent only when the object is created or updated."""

    fixtures = [
        'tests/fixtures/basic_policies.yaml',
        'tests/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
    ]

    def test_created_object_send_message_reliability(self, signal, async_signal):
        """Test whether the notification falls back to direct signals when there's no RabbitMQ."""
        async_signal.delay.side_effect = OperationalError()

        basic_data = {
            'id': 'redhat:1',
            'origin': 'redhat',
        }

        # Create call
        with self.assertLogs(logger=utils_logger, level="ERROR") as log_ctx:
            new = models.KCIDBCheckout.create_from_json(basic_data)

        expected_log = f"ERROR:{utils_logger.name}:Failed to enqueue celery message. Using direct signal as fallback."
        self.assertTrue(log_ctx.output[0].startswith(expected_log), "Expect log with an error message and stack trace")

        new_args = dict(
            sender='kcidb.checkout.created_object_send_message',
            status=models.ObjectStatusEnum.NEW,
            object_type='checkout',
        )
        async_signal.delay.assert_called_once_with(**new_args, pks=[new.iid])
        signal.send.assert_called_once_with(**new_args, objects=[new])

    def test_create_and_update_checkout(self, signal, async_signal):
        """Test signal to send_kcidb_object_message when handling checkout."""
        basic_data = {
            'id': 'redhat:1',
            'origin': 'redhat',
        }

        with self.subTest("Create Checkout triggers signal"):
            # Create call
            new = models.KCIDBCheckout.create_from_json(basic_data)

            new_args = {
                "sender": "kcidb.checkout.created_object_send_message",
                "status": models.ObjectStatusEnum.NEW,
                "object_type": "checkout",
            }
            async_signal.delay.assert_called_once_with(**new_args, pks=[new.iid])
            signal.send.assert_called_once_with(**new_args, misc=None, objects=[new])

        async_signal.reset_mock()
        signal.reset_mock()

        with self.subTest("Update Checkout is_test_plan property data triggers signal"):
            # Update call affecting is_test_plan property
            updated_data = {**basic_data, "valid": False}
            updated_is_test_plan = models.KCIDBCheckout.create_from_json(updated_data)

            self.assertEqual(updated_is_test_plan.iid, new.iid)
            self.assertNotEqual(updated_is_test_plan.is_test_plan, new.is_test_plan)

            updated_args = {
                "sender": "kcidb.checkout.created_object_send_message",
                "status": models.ObjectStatusEnum.UPDATED,
                "object_type": "checkout",
            }
            async_signal.delay.assert_called_once_with(**updated_args, pks=[updated_is_test_plan.iid])
            signal.send.assert_called_once_with(**updated_args, misc=None, objects=[updated_is_test_plan])

        async_signal.reset_mock()
        signal.reset_mock()

        with self.subTest("Update Checkout with not-equivalent (but same is_test_plan) data triggers signal"):
            # Update call
            updated_data = {**basic_data, "valid": True}
            updated = models.KCIDBCheckout.create_from_json(updated_data)

            self.assertEqual(updated.iid, new.iid)
            self.assertEqual(updated_is_test_plan.is_test_plan, updated.is_test_plan)

            updated_args = {
                "sender": "kcidb.checkout.created_object_send_message",
                "status": models.ObjectStatusEnum.UPDATED,
                "object_type": "checkout",
            }
            async_signal.delay.assert_called_once_with(**updated_args, pks=[updated.iid])
            signal.send.assert_called_once_with(**updated_args, misc=None, objects=[updated])

        async_signal.reset_mock()
        signal.reset_mock()

        with self.subTest("Update Checkout with equivalent data DOES NOT trigger signal"):
            # Update call with equivalent data
            equivalent_data = {**updated_data, "last_triaged_at": '2024-02-15T12:34:56+00:00'}
            equivalent = models.KCIDBCheckout.create_from_json(equivalent_data)

            self.assertEqual(equivalent.iid, new.iid)
            self.assertEqual(updated.is_test_plan, updated_is_test_plan.is_test_plan)

            async_signal.delay.assert_not_called()
            signal.send.assert_not_called()

    def test_create_and_update_build(self, signal, async_signal):
        """Test signal to send_kcidb_object_message when handling build."""
        basic_data = {
            "checkout_id": "redhat:public_checkout",
            "id": "redhat:1",
            "origin": "redhat",
        }

        with self.subTest("Create Build triggers signal"):
            # Create call
            new = models.KCIDBBuild.create_from_json(basic_data)

            new_args = {
                "sender": "kcidb.build.created_object_send_message",
                "status": models.ObjectStatusEnum.NEW,
                "object_type": "build",
            }
            async_signal.delay.assert_called_once_with(**new_args, pks=[new.iid])
            signal.send.assert_called_once_with(**new_args, misc=None, objects=[new])

        async_signal.reset_mock()
        signal.reset_mock()

        with self.subTest("Update Build is_test_plan property data triggers signal"):
            # Update call affecting is_test_plan property
            updated_data = {**basic_data, "valid": False}
            updated_is_test_plan = models.KCIDBBuild.create_from_json(updated_data)

            self.assertEqual(updated_is_test_plan.iid, new.iid)
            self.assertNotEqual(updated_is_test_plan.is_test_plan, new.is_test_plan)

            updated_args = {
                "sender": "kcidb.build.created_object_send_message",
                "status": models.ObjectStatusEnum.UPDATED,
                "object_type": "build",
            }
            async_signal.delay.assert_called_once_with(**updated_args, pks=[updated_is_test_plan.iid])
            signal.send.assert_called_once_with(**updated_args, misc=None, objects=[updated_is_test_plan])

        async_signal.reset_mock()
        signal.reset_mock()

        with self.subTest("Update Build with not-equivalent (but same is_test_plan) data triggers signal"):
            # Update call
            updated_data = {**basic_data, "valid": True}
            updated = models.KCIDBBuild.create_from_json(updated_data)

            self.assertEqual(updated.iid, new.iid)
            self.assertEqual(updated_is_test_plan.is_test_plan, updated.is_test_plan)

            updated_args = {
                "sender": "kcidb.build.created_object_send_message",
                "status": models.ObjectStatusEnum.UPDATED,
                "object_type": "build",
            }
            async_signal.delay.assert_called_once_with(**updated_args, pks=[updated.iid])
            signal.send.assert_called_once_with(**updated_args, misc=None, objects=[updated])

        async_signal.reset_mock()
        signal.reset_mock()

        with self.subTest("Update Build with equivalent data DOES NOT trigger signal"):
            # Update call with equivalent data
            equivalent_data = {**updated_data, "last_triaged_at": '2024-02-15T12:34:56+00:00'}
            equivalent = models.KCIDBBuild.create_from_json(equivalent_data)

            self.assertEqual(equivalent.iid, new.iid)

            async_signal.delay.assert_not_called()
            signal.send.assert_not_called()

    def test_create_and_update_test(self, signal, async_signal):
        """Test signal to send_kcidb_object_message when handling test."""
        basic_data = {
            "build_id": "redhat:public_build_1",
            "id": "redhat:1",
            "origin": "redhat",
        }

        with self.subTest("Create Test triggers signal"):
            # Create call
            new = models.KCIDBTest.create_from_json(basic_data)

            new_args = {
                "sender": "kcidb.test.created_object_send_message",
                "status": models.ObjectStatusEnum.NEW,
                "object_type": "test",
            }
            async_signal.delay.assert_called_once_with(**new_args, pks=[new.iid])
            signal.send.assert_called_once_with(**new_args, misc=None, objects=[new])

        async_signal.reset_mock()
        signal.reset_mock()

        with self.subTest("Update Test is_test_plan property data triggers signal"):
            # Update call affecting is_test_plan property
            updated_data = {**basic_data, "status": "FAIL"}
            updated_is_test_plan = models.KCIDBTest.create_from_json(updated_data)

            self.assertEqual(updated_is_test_plan.iid, new.iid)
            self.assertNotEqual(updated_is_test_plan.is_test_plan, new.is_test_plan)

            updated_args = {
                "sender": "kcidb.test.created_object_send_message",
                "status": models.ObjectStatusEnum.UPDATED,
                "object_type": "test",
            }
            async_signal.delay.assert_called_once_with(**updated_args, pks=[updated_is_test_plan.iid])
            signal.send.assert_called_once_with(**updated_args, misc=None, objects=[updated_is_test_plan])

        async_signal.reset_mock()
        signal.reset_mock()

        with self.subTest("Update Test with not-equivalent (but same is_test_plan) data triggers signal"):
            # Update call
            updated_data = {**basic_data, "status": "PASS"}
            updated = models.KCIDBTest.create_from_json(updated_data)

            self.assertEqual(updated.iid, new.iid)
            self.assertEqual(updated.is_test_plan, updated_is_test_plan.is_test_plan)

            updated_args = {
                "sender": "kcidb.test.created_object_send_message",
                "status": models.ObjectStatusEnum.UPDATED,
                "object_type": "test",
            }
            async_signal.delay.assert_called_once_with(**updated_args, pks=[updated.iid])
            signal.send.assert_called_once_with(**updated_args, misc=None, objects=[updated])

        async_signal.reset_mock()
        signal.reset_mock()

        with self.subTest("Update Test with equivalent data DOES NOT trigger signal"):
            # Update call with equivalent data
            equivalent_data = {**updated_data, "last_triaged_at": '2024-02-15T12:34:56+00:00'}
            equivalent = models.KCIDBTest.create_from_json(equivalent_data)

            self.assertEqual(equivalent.iid, new.iid)

            async_signal.delay.assert_not_called()
            signal.send.assert_not_called()


class TestKCIDBObjectCompare(utils.TestCase):
    """Test the equivalence of KCIDB Objects."""

    def test_checkout(self):
        """Test checkout."""
        origin = models.KCIDBOrigin.objects.get_or_create(name="redhat")[0]
        checkout1 = models.KCIDBCheckout.objects.create(id="redhat:1", origin=origin)
        checkout2 = models.KCIDBCheckout.objects.create(id="redhat:2", origin=origin)

        self.assertEqual(checkout1.symmetric_difference(checkout1), set())
        self.assertCountEqual(checkout1.symmetric_difference(checkout2), {("id", "redhat:1"), ("id", "redhat:2")})
        self.assertCountEqual(checkout1.symmetric_difference("string"), {("type", "str"), ("type", "KCIDBCheckout")})

    def test_build(self):
        """Test build."""
        origin = models.KCIDBOrigin.objects.get_or_create(name="redhat")[0]
        checkout = models.KCIDBCheckout.objects.create(id="redhat:1", origin=origin)
        build1 = models.KCIDBBuild.objects.create(checkout=checkout, id="redhat:887318", origin=origin)
        build2 = models.KCIDBBuild.objects.create(checkout=checkout, id="redhat:887319", origin=origin)

        self.assertEqual(build1.symmetric_difference(build1), set())
        self.assertCountEqual(build1.symmetric_difference(build2), {("id", "redhat:887318"), ("id", "redhat:887319")})
        self.assertCountEqual(build1.symmetric_difference("string"), {("type", "str"), ("type", "KCIDBBuild")})

    def test_test(self):
        """Test test."""
        origin = models.KCIDBOrigin.objects.get_or_create(name="redhat")[0]
        checkout = models.KCIDBCheckout.objects.create(id="redhat:1", origin=origin)
        build = models.KCIDBBuild.objects.create(checkout=checkout, id="redhat:887318", origin=origin)
        test1 = models.KCIDBTest.objects.create(build=build, id="redhat:1121892", origin=origin)
        test2 = models.KCIDBTest.objects.create(build=build, id="redhat:1121893", origin=origin)

        self.assertEqual(test1.symmetric_difference(test1), set())
        self.assertCountEqual(test1.symmetric_difference(test2), {("id", "redhat:1121892"), ("id", "redhat:1121893")})
        self.assertCountEqual(test1.symmetric_difference("string"), {("type", "str"), ("type", "KCIDBTest")})


@override_settings(
    CELERY_TASK_ALWAYS_EAGER=True,
    # Disable behavior from kcidb objects' pre_save signal
    CLEAR_ISSUEOCCURRENCES_ON_SAVE=False,
)
@mock.patch("datawarehouse.signals.kcidb_object.send", wraps=signals.kcidb_object.send)
class TestKCIDBTestResultFromJson(utils.TestCase):
    """Test creation of KCIDBTestResult model."""

    fixtures = (
        "tests/fixtures/basic_policies.yaml",
        "tests/fixtures/basic.yaml",
    )

    def setUp(self):
        origin = models.KCIDBOrigin.create_from_string("redhat")
        checkout = models.KCIDBCheckout.objects.create(id="redhat:1", origin=origin)
        build = models.KCIDBBuild.objects.create(
            checkout=checkout,
            id="redhat:887318",
            origin=origin,
            architecture=models.ArchitectureEnum.aarch64,
        )
        test_case = models.Test.objects.create(name="redhat three")
        self.kcidb_test = models.KCIDBTest.objects.create(origin=origin, build=build, id="redhat:3", test=test_case)

    def test_basic(self, mocked_send_signal):
        """Submit only id."""
        data = {
            'id': 'redhat:2.1',
        }
        result = models.KCIDBTestResult.create_from_json(self.kcidb_test, data)

        mocked_send_signal.assert_not_called()

        self.assertIsInstance(result, models.KCIDBTestResult)
        self.assertEqual(result.test, self.kcidb_test)
        self.assertEqual(result.id, 'redhat:2.1')

    def test_full(self, mocked_send_signal):
        """Submit complete result."""
        data = {
            "id": "redhat:2.1",
            "status": "PASS",
            "comment": "sub result for test 2",
            "name": "ignored",
            "output_files": [
                {"url": "http://log.server/output.file", "name": "output.file"},
                {"url": "http://log.server/output.file.2", "name": "output.file.2"},
            ],
        }
        # If the reader is interested in reading all queries, simply modify the assertion number
        # Summary: 5 SAVEPOINT + 5 RELEASE; 4 SELECT; 4 INSERT
        with self.assertNumQueries(18):
            result = models.KCIDBTestResult.create_from_json(self.kcidb_test, data)

        mocked_send_signal.assert_not_called()

        self.assertIsInstance(result, models.KCIDBTestResult)
        self.assertEqual(result.test, self.kcidb_test)
        self.assertEqual(result.id, 'redhat:2.1')
        self.assertEqual(result.status, models.ResultEnum.PASS)
        self.assertEqual(result.name, 'sub result for test 2')
        self.assertEqual(
            list(result.output_files.all().order_by('name').values('url', 'name')),
            data['output_files']
        )

        with self.subTest("Resubmit should be almost a noop"):
            # Summary: 2 SAVEPOINT + 2 RELEASE; 4 SELECT; 1 UPDATE
            with self.assertNumQueries(9):
                models.KCIDBTestResult.create_from_json(self.kcidb_test, data)

            mocked_send_signal.assert_not_called()

    def test_name_fallback(self, mocked_send_signal):
        """Submit result with deprecated name."""
        data = {
            'id': 'redhat:2.1',
            'name': 'sub result for test 2',
        }
        result = models.KCIDBTestResult.create_from_json(self.kcidb_test, data)

        mocked_send_signal.assert_not_called()

        self.assertIsInstance(result, models.KCIDBTestResult)
        self.assertEqual(result.test, self.kcidb_test)
        self.assertEqual(result.id, 'redhat:2.1')
        self.assertEqual(result.name, 'sub result for test 2')

    def test_re_submit_files(self, mocked_send_signal):
        """Submit files multiple times."""
        data = {
            'id': 'redhat:2.1',
            'status': 'PASS',
            'comment': 'sub result for test 2',
            'output_files': [
                {'url': 'http://log.server/output.file', 'name': 'output.file'},
                {'url': 'http://log.server/output.file.2', 'name': 'output.file.2'},
            ],
        }
        result = models.KCIDBTestResult.create_from_json(self.kcidb_test, data)

        mocked_send_signal.assert_not_called()

        self.assertEqual(
            list(result.output_files.all().order_by('name').values('url', 'name')),
            data['output_files']
        )

        # With new files, the current ones are replaced
        data.update({
            'output_files': [
                {'url': 'http://log.server/output.file.3', 'name': 'output.file.3'},
            ],
        })

        result = models.KCIDBTestResult.create_from_json(self.kcidb_test, data)

        mocked_send_signal.assert_not_called()

        self.assertEqual(
            list(result.output_files.all().order_by('name').values('url', 'name')),
            data['output_files']
        )

        # If no files are provided, the files are not removed
        del data['output_files']

        result = models.KCIDBTestResult.create_from_json(self.kcidb_test, data)

        mocked_send_signal.assert_not_called()

        self.assertEqual(
            list(result.output_files.all().order_by('name').values('url', 'name')),
            [{'url': 'http://log.server/output.file.3', 'name': 'output.file.3'}]
        )


class TestProvenanceComponentModel(utils.TestCase):
    """Test ProvenanceComponent objects."""

    def test_create_from_dict(self):
        """Check ProvenanceComponent objects created by create_from_json."""
        provenance = models.ProvenanceComponent.create_from_dict({
            'url': 'https://url',
            'function': 'coordinator',
            'service_name': 'gitlab',
            'misc': {'some': 'data'},
        })

        self.assertEqual(str(provenance), 'coordinator')
        self.assertEqual(provenance.url, 'https://url')
        self.assertEqual(provenance.function, models.ProvenanceComponentFunctionEnum.COORDINATOR)
        self.assertEqual(provenance.service_name, 'gitlab')
        self.assertEqual(provenance.misc, {'some': 'data'})

    def test_string_format(self):
        """Check ProvenanceComponent __str__ method."""
        test_cases = [
            ('https://gitlab.com/project/-/jobs/1234', 'gitlab', 'Gitlab Job'),
            ('https://gitlab.com/project/-/pipelines/1234', 'gitlab', 'Gitlab Pipeline'),
            ('https://beaker.foo.com/recipes/11508560', 'beaker', 'Beaker Recipe'),
            ('https://beaker.foo.com/not-recipes/1', 'beaker', 'coordinator'),
            ('https://foo.bar/foo', None, 'coordinator')
        ]
        for url, service_name, expected in test_cases:
            provenance = models.ProvenanceComponent.create_from_dict({
                'url': url, 'function': 'coordinator', 'service_name': service_name
            })

            self.assertEqual(str(provenance), expected)
