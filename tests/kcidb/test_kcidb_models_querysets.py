"""Test custom QuerySets for KCIDB models."""
import json
from unittest import mock

from django.db.models import F
from django.db.models import Q
from django.db.models.query import QuerySet
from django.utils import timezone

from datawarehouse import models
from datawarehouse.api.kcidb.serializers import DW_KCIDB_SCHEMA_VERSION
from datawarehouse.utils import timestamp_to_datetime
from tests import utils


class TestCheckoutAggregated(utils.TestCase):
    """Test aggregated data on a checkout."""

    fixtures = [
        'tests/fixtures/basic_policies.yaml',
    ]

    def setUp(self):
        data = {
            'version': DW_KCIDB_SCHEMA_VERSION,
            'checkouts': [
                {'origin': 'redhat', 'id': 'redhat:decd6167bf4f6bec1284006d0522381b44660df3', 'valid': False},
            ],
            'builds': [
                {'origin': 'redhat', 'checkout_id': 'redhat:decd6167bf4f6bec1284006d0522381b44660df3',
                 'id': 'redhat:build-1', 'valid': False},
                {'origin': 'redhat', 'checkout_id': 'redhat:decd6167bf4f6bec1284006d0522381b44660df3',
                 'id': 'redhat:build-2', 'valid': False},
                {'origin': 'redhat', 'checkout_id': 'redhat:decd6167bf4f6bec1284006d0522381b44660df3',
                 'id': 'redhat:build-3', 'valid': True},
            ],
            'tests': [
                {'origin': 'redhat', 'build_id': 'redhat:build-1', 'id': 'redhat:test-1', 'status': 'FAIL'},
                {'origin': 'redhat', 'build_id': 'redhat:build-1', 'id': 'redhat:test-2', 'status': 'ERROR'},
                {'origin': 'redhat', 'build_id': 'redhat:build-1', 'id': 'redhat:test-3', 'status': 'PASS'},
            ]
        }
        self.assert_authenticated_post(
            201, 'add_kcidbcheckout', '/api/1/kcidb/submit', json.dumps({'data': data}),
            content_type="application/json"
        )

    def _test_rev(self, cases, only_aggregated=False):
        """Test attrs on both rev and rev_aggregated."""
        rev_id = 'redhat:decd6167bf4f6bec1284006d0522381b44660df3'
        rev = models.KCIDBCheckout.objects.get(id=rev_id)
        rev_aggregated = models.KCIDBCheckout.objects.aggregated().get(id=rev_id)

        for attr, value in cases:
            check = self.assertQuerySetEqual if isinstance(value, QuerySet) else self.assertEqual

            check(getattr(rev_aggregated, attr), value, msg=attr)
            if not only_aggregated:
                check(getattr(rev, attr), value, msg=attr)

    def test_nothing_triaged(self):
        """Test none objects were triaged."""
        rev = models.KCIDBCheckout.objects.get(id='redhat:decd6167bf4f6bec1284006d0522381b44660df3')

        # Without aggregated call these methods are not available.
        self.assertFalse(hasattr(rev, 'stats_checkout_triaged'))
        self.assertFalse(hasattr(rev, 'stats_checkout_untriaged'))
        self.assertFalse(hasattr(rev, 'stats_tests_triaged'))
        self.assertFalse(hasattr(rev, 'stats_tests_untriaged'))
        self.assertFalse(hasattr(rev, 'stats_builds_triaged'))
        self.assertFalse(hasattr(rev, 'stats_builds_untriaged'))
        self.assertIsNone(rev.has_objects_missing_triage)
        self.assertIsNone(rev.has_objects_with_issues)

        # No issues but failed jobs. All untriaged, no triaged.
        cases = [
            ('stats_checkout_untriaged', True),
            ('stats_builds_untriaged', True),
            ('stats_tests_untriaged', True),
            ('stats_checkout_triaged', False),
            ('stats_builds_triaged', False),
            ('stats_tests_triaged', False),
            # No issues, missing triage.
            ('has_objects_missing_triage', True),
            ('has_objects_with_issues', False),
        ]
        self._test_rev(cases, only_aggregated=True)

        cases = [
            # Check list of triaged and untriaged jobs.
            ('builds_triaged', models.KCIDBBuild.objects.none()),
            ('builds_untriaged', models.KCIDBBuild.objects.filter(id__in=('redhat:build-1', 'redhat:build-2'))),
            # Checkout is not triaged.
            ('is_missing_triage', True),
        ]
        self._test_rev(cases)

    def test_partially_triaged(self):
        """Test some objects were triaged and some others not."""
        # Add some issues to some builds and tests.
        issue = models.Issue.objects.create(
            kind=models.IssueKind.objects.create(description="fail 1", tag="1"),
            description='foo bar',
            ticket_url='http://some.url',
        )
        models.KCIDBCheckout.objects.get(id='redhat:decd6167bf4f6bec1284006d0522381b44660df3').issues.add(issue)
        models.KCIDBBuild.objects.get(id='redhat:build-1').issues.add(issue)
        models.KCIDBTest.objects.get(id='redhat:test-1').issues.add(issue)

        # We now have both triaged and untriaged jobs.
        cases = [
            ('stats_checkout_triaged', True),
            ('stats_checkout_untriaged', False),
            ('stats_builds_triaged', True),
            ('stats_builds_untriaged', True),
            ('stats_tests_triaged', True),
            ('stats_tests_untriaged', True),
            # Some issues, missing triage.
            ('has_objects_missing_triage', True),
            ('has_objects_with_issues', True),
        ]
        self._test_rev(cases, only_aggregated=True)

        cases = [
            # Check list of triaged and untriaged jobs.
            ('builds_triaged', models.KCIDBBuild.objects.filter(id='redhat:build-1')),
            ('builds_untriaged', models.KCIDBBuild.objects.filter(id='redhat:build-2')),
            # Checkout is triaged.
            ('is_missing_triage', False),
        ]
        self._test_rev(cases)

    def test_fully_triaged(self):
        """Test all objects were triaged."""
        # Add issue to all failures.
        issue = models.Issue.objects.create(
            kind=models.IssueKind.objects.create(description="fail 1", tag="1"),
            description='foo bar',
            ticket_url='http://some.url',
        )
        models.KCIDBCheckout.objects.get(id='redhat:decd6167bf4f6bec1284006d0522381b44660df3').issues.add(issue)
        models.KCIDBBuild.objects.get(id='redhat:build-1').issues.add(issue)
        models.KCIDBBuild.objects.get(id='redhat:build-2').issues.add(issue)
        models.KCIDBTest.objects.get(id='redhat:test-1').issues.add(issue)
        models.KCIDBTest.objects.get(id='redhat:test-2').issues.add(issue)

        # We now have all issues triaged.
        cases = [
            ('stats_builds_triaged', True),
            ('stats_builds_untriaged', False),
            ('stats_tests_triaged', True),
            ('stats_tests_untriaged', False),
            # No failures missing triage.
            ('has_objects_missing_triage', False),
            ('has_objects_with_issues', True),
        ]
        self._test_rev(cases, only_aggregated=True)

        cases = [
            # Check list of triaged and untriaged jobs.
            ('builds_untriaged', models.KCIDBBuild.objects.none()),
            ('builds_triaged', models.KCIDBBuild.objects.filter(id__in=('redhat:build-1', 'redhat:build-2'))),
        ]
        self._test_rev(cases)


class TestKCIDBCheckoutAggregateCounts(utils.TestCase):
    """Test for KCIDBCheckout aggregated counts."""

    fixtures = [
        'tests/fixtures/issues.yaml',
        'tests/fixtures/policies_for_group_abc.yaml',
        'tests/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_extra_tests.yaml',
    ]

    def test_broken_builds(self):
        checkout_qs = models.KCIDBCheckout.objects.filter(id="redhat:public_checkout").aggregated().first()
        self.assertEqual(checkout_qs.stats_builds_broken_boot_tests_count, 2)

        models.KCIDBTest.objects.filter(id="redhat:public_test_18").update(status=models.ResultEnum.PASS)
        checkout_qs = models.KCIDBCheckout.objects.filter(id="redhat:public_checkout").aggregated().first()
        self.assertEqual(checkout_qs.stats_builds_broken_boot_tests_count, 1)


class TestKCIDBCheckoutQuerySet(utils.TestCase):
    """Test for KCIDBCheckoutQuerySet filters and annotations."""

    fixtures = [
        'tests/fixtures/issues.yaml',
        'tests/fixtures/policies_for_group_abc.yaml',
        'tests/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
    ]

    def _test_untriaged_tests_blocking(self, *, triage_subtest: bool):
        """Test untriaged_tests_blocking method."""
        checkout = models.KCIDBCheckout.objects.get(id='redhat:public_checkout')
        queryset = models.KCIDBTest.objects.filter(build__checkout=checkout)
        empty = models.KCIDBTest.objects.none()

        with self.subTest("aggregated() with stats_tests_untriaged=0 short-circuits"):
            # A checkout without any tests should render a zeroed counter
            aggregated = models.KCIDBCheckout.objects.aggregated().get(id="redhat:public_checkout_valid")
            with self.assertNumQueries(0):
                result = aggregated.untriaged_tests_blocking

            self.assertQuerySetEqual(empty, result)

        tests = models.KCIDBTest.objects.filter(build__checkout__id="redhat:public_checkout")
        results = models.KCIDBTestResult.objects.filter(test__build__checkout__id="redhat:public_checkout")

        assert tests.filter(id="redhat:public_test_R", status=None).exists()
        test_f = tests.get(id="redhat:public_test_F", status="F")
        test_e = tests.get(id="redhat:public_test_E", status="E")
        assert tests.filter(id="redhat:public_test_M", status="M").exists()
        assert tests.filter(id="redhat:public_test_S", status="S").exists()
        assert tests.filter(id="redhat:public_test_P", status="P").exists()

        if triage_subtest:
            test_f_result_f = results.get(id="redhat:public_test_F_result_F", status="F")
            test_f_result_e = results.get(id="redhat:public_test_F_result_E", status="E")
            test_e_result_e = results.get(id="redhat:public_test_E_result_E", status="E")
        else:
            test_f_result_f = mock.Mock()
            test_f_result_e = mock.Mock()
            test_e_result_e = mock.Mock()

        issue_1 = models.Issue.objects.get(id=1)
        issue_2 = models.Issue.objects.get(id=2)
        issue_4 = models.Issue.objects.get(id=4)

        input_domain = utils.product_dict(
            waived=[None, False, True],
            triaged=[False, True],
        )
        for case_kwargs in input_domain:
            with self.subTest(triage_subtest=triage_subtest, **case_kwargs):
                tests.update(waived=case_kwargs["waived"])

                # Reset issue occurrences
                test_f.issues.set([])
                test_e.issues.set([])
                test_f_result_f.issues.set([])
                test_e_result_e.issues.set([])
                # NOTE: Partial subtest triaging must not be enough to count as triaged
                test_f_result_e.issues.set([issue_4])

                if case_kwargs["triaged"]:
                    if triage_subtest:
                        test_f_result_f.issues.set([issue_2])
                        test_e_result_e.issues.set([issue_1])
                    else:
                        test_f.issues.set([issue_2, issue_4])
                        test_e.issues.set([issue_1])

                match case_kwargs:
                    # Waived or triaged are always non blocking, therefore nothing ever should be returned
                    case {"waived": True} | {"triaged": True}:
                        expected = empty
                    # Otherwise, if neither are defined, only FAIL should be returned
                    case _:
                        expected = [queryset.get(status=models.ResultEnum.FAIL)]

                self.assertQuerySetEqual(expected, checkout.untriaged_tests_blocking)

    def test_untriaged_tests_blocking_with_subtests(self):
        """Test untriaged_tests_blocking method with subtests."""
        self._test_untriaged_tests_blocking(triage_subtest=True)

    def test_untriaged_tests_blocking_without_subtests(self):
        """Test untriaged_tests_blocking method without subtests."""
        models.KCIDBTestResult.objects.all().delete()
        self._test_untriaged_tests_blocking(triage_subtest=False)

    def _test_untriaged_tests_non_blocking(self, *, triage_subtest: bool):
        """Test untriaged_tests_non_blocking method."""
        checkout = models.KCIDBCheckout.objects.get(id='redhat:public_checkout')
        queryset = models.KCIDBTest.objects.filter(build__checkout=checkout)
        empty = models.KCIDBTest.objects.none()

        with self.subTest("aggregated() with stats_tests_untriaged=0 short-circuits"):
            aggregated = models.KCIDBCheckout.objects.aggregated().get(id="redhat:public_checkout_valid")
            with self.assertNumQueries(0):
                result = aggregated.untriaged_tests_non_blocking

            self.assertQuerySetEqual(empty, result)

        tests = models.KCIDBTest.objects.filter(build__checkout__id="redhat:public_checkout")
        results = models.KCIDBTestResult.objects.filter(test__build__checkout__id="redhat:public_checkout")

        assert tests.filter(id="redhat:public_test_R", status=None).exists()
        test_f = tests.get(id="redhat:public_test_F", status="F")
        test_e = tests.get(id="redhat:public_test_E", status="E")
        assert tests.filter(id="redhat:public_test_M", status="M").exists()
        assert tests.filter(id="redhat:public_test_S", status="S").exists()
        assert tests.filter(id="redhat:public_test_P", status="P").exists()

        if triage_subtest:
            test_f_result_f = results.get(id="redhat:public_test_F_result_F", status="F")
            test_f_result_e = results.get(id="redhat:public_test_F_result_E", status="E")
            test_e_result_e = results.get(id="redhat:public_test_E_result_E", status="E")
        else:
            test_f_result_f = mock.Mock()
            test_f_result_e = mock.Mock()
            test_e_result_e = mock.Mock()

        issue_1 = models.Issue.objects.get(id=1)
        issue_2 = models.Issue.objects.get(id=2)
        issue_4 = models.Issue.objects.get(id=4)

        input_domain = utils.product_dict(
            waived=[None, False, True],
            triaged=[False, True],
        )
        for case_kwargs in input_domain:
            with self.subTest(triage_subtest=triage_subtest, **case_kwargs):
                tests.update(waived=case_kwargs["waived"])

                # Reset issue occurrences
                test_f.issues.set([])
                test_e.issues.set([])
                test_f_result_f.issues.set([])
                test_e_result_e.issues.set([])
                # NOTE: Partial subtest triaging must not be enough to count as triaged
                test_f_result_e.issues.set([issue_4])

                if case_kwargs["triaged"]:
                    if triage_subtest:
                        test_f_result_f.issues.set([issue_2])
                        test_e_result_e.issues.set([issue_1])
                    else:
                        test_f.issues.set([issue_2, issue_4])
                        test_e.issues.set([issue_1])

                match case_kwargs:
                    # Waived or triaged are always non blocking, but most importantly not considered untriaged
                    case {"waived": True} | {"triaged": True}:
                        expected = []
                    # Otherwise, if neither are defined, only ERROR should be returned
                    case _:
                        expected = [queryset.get(status=models.ResultEnum.ERROR)]

                self.assertQuerySetEqual(expected, checkout.untriaged_tests_non_blocking)

    def test_untriaged_tests_non_blocking_with_subtests(self):
        """Test untriaged_tests_non_blocking method with subtests."""
        self._test_untriaged_tests_non_blocking(triage_subtest=True)

    def test_untriaged_tests_non_blocking_without_subtests(self):
        """Test untriaged_tests_non_blocking method without subtests."""
        models.KCIDBTestResult.objects.all().delete()
        self._test_untriaged_tests_non_blocking(triage_subtest=False)


class TestCheckoutAnnotatedByArchitecture(utils.TestCase):
    """Test annotated_by_architecture data on a checkout."""

    fixtures = [
        'tests/fixtures/policies_for_group_abc.yaml',
        'tests/fixtures/basic.yaml',
        'tests/fixtures/issues.yaml',
        'tests/kcidb/fixtures/base_multiple_architectures.yaml',
    ]

    def test_counters(self):
        """Test annotated_by_architecture counters."""
        rev = (
            models.KCIDBCheckout.objects
            .annotated_by_architecture()
            .get(id='redhat:d1c47b385764aaa488bce182d944fa22bb1325d1')
        )

        test_cases = {
            'aarch64': {
                'builds_ran': 1,
                'builds_failed': 0,
                'builds_failed_untriaged': 0,
                'builds_with_issues': 0,
                'tests_ran': 1,
                'tests_failed': 1,
                'tests_failed_untriaged': 1,
                'tests_failed_waived': 0,
                'tests_with_issues': 0,
            },
            'ppc64': {
                'builds_ran': 1,
                'builds_failed': 0,
                'builds_failed_untriaged': 0,
                'builds_with_issues': 0,
                'tests_ran': 1,
                'tests_failed': 0,
                'tests_failed_untriaged': 0,
                'tests_failed_waived': 0,
                'tests_with_issues': 0,
            },
            'ppc64le': {
                'builds_ran': 1,
                'builds_failed': 1,
                'builds_failed_untriaged': 0,
                'builds_with_issues': 1,
                'tests_ran': 3,
                'tests_failed': 1,
                'tests_failed_untriaged': 0,
                'tests_failed_waived': 1,
                'tests_with_issues': 2,
            },
        }

        for arch, fields in test_cases.items():
            for key, value in fields.items():
                self.assertEqual(getattr(rev, f'stats_{arch}_{key}_count'), value, (arch, key))

        # All architectures not included in test_case should be zero.
        for arch in models.ArchitectureEnum:
            if arch.name in test_cases:
                continue

            for key in test_cases['aarch64']:
                self.assertEqual(getattr(rev, f'stats_{arch.name}_{key}_count'), 0, (arch.name, key))

    def test_annotated_by_architeture(self):
        """Test annotated_by_architecture."""
        checkout = (
            models.KCIDBCheckout.objects
            .annotated_by_architecture()
            .get(id='redhat:d1c47b385764aaa488bce182d944fa22bb1325d1')
        )

        for arch in models.ArchitectureEnum:
            self.assertEqual(
                checkout.annotated_by_architecture[arch.name]['builds']['ran'],
                getattr(checkout, f'stats_{arch.name}_builds_ran_count'))
            self.assertEqual(
                checkout.annotated_by_architecture[arch.name]['builds']['failed'],
                getattr(checkout, f'stats_{arch.name}_builds_failed_count'))
            self.assertEqual(
                checkout.annotated_by_architecture[arch.name]['tests']['ran'],
                getattr(checkout, f'stats_{arch.name}_tests_ran_count'))
            self.assertEqual(
                checkout.annotated_by_architecture[arch.name]['tests']['failed'],
                getattr(checkout, f'stats_{arch.name}_tests_failed_count'))
            self.assertEqual(
                checkout.annotated_by_architecture[arch.name]['tests']['failed_waived'],
                getattr(checkout, f'stats_{arch.name}_tests_failed_waived_count'))
            self.assertEqual(
                checkout.annotated_by_architecture[arch.name]['known_issues'],
                (
                    getattr(checkout, f'stats_{arch.name}_builds_with_issues_count') +
                    getattr(checkout, f'stats_{arch.name}_tests_with_issues_count')
                )
            )


class TestCheckoutFilterReadyToReport(utils.TestCase):
    """Unit tests for KCIDBCheckoutQuerySet.filter_ready_to_report."""

    fixtures = [
        'tests/fixtures/basic_policies.yaml',
        'tests/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
    ]

    def test_already_ready_to_report(self):
        """All checkouts are tagged as ready_to_report."""
        models.KCIDBCheckout.objects.update(ready_to_report=True)

        self.assertQuerySetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_ready_to_report()
        )

    def test_checkouts_not_finished(self):
        """Checkouts have no valid value."""
        models.KCIDBCheckout.objects.update(valid=None)

        self.assertQuerySetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_ready_to_report()
        )

    def test_builds_not_finished(self):
        """Builds have no valid value."""
        models.KCIDBBuild.objects.update(valid=None)

        self.assertQuerySetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_ready_to_report()
        )

    def test_tests_not_finished(self):
        """Tests have no status value."""
        models.KCIDBTest.objects.update(status=None)

        self.assertQuerySetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_ready_to_report()
        )

    def test_not_triaged(self):
        """All objects finished but were not triaged."""
        models.KCIDBCheckout.objects.update(valid=True)
        models.KCIDBBuild.objects.update(valid=True)
        models.KCIDBTest.objects.update(status='P')

        self.assertQuerySetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_ready_to_report()
        )

    def test_ready(self):
        """All objects finished and were triaged."""
        models.KCIDBCheckout.objects.update(valid=True, last_triaged_at=timezone.now())
        models.KCIDBBuild.objects.update(valid=True, last_triaged_at=timezone.now())
        models.KCIDBTest.objects.update(status='P', last_triaged_at=timezone.now())

        self.assertQuerySetEqual(
            models.KCIDBCheckout.objects.all(),
            models.KCIDBCheckout.objects.filter_ready_to_report()
        )

    def test_ready_and_reported(self):
        """All objects finished, were triaged but already reported."""
        models.KCIDBCheckout.objects.update(valid=True, last_triaged_at=timezone.now(), ready_to_report=True)
        models.KCIDBBuild.objects.update(valid=True, last_triaged_at=timezone.now())
        models.KCIDBTest.objects.update(status='P', last_triaged_at=timezone.now())

        self.assertQuerySetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_ready_to_report()
        )

    def test_test_plan_missing(self):
        """All objects finished, but a build still has test plan missing."""
        models.KCIDBCheckout.objects.update(valid=True, last_triaged_at=timezone.now())
        models.KCIDBBuild.objects.update(valid=True, last_triaged_at=timezone.now(), test_plan_missing=True)
        models.KCIDBTest.objects.update(status='P', last_triaged_at=timezone.now())

        checkouts_ready = models.KCIDBCheckout.objects.exclude(
            kcidbbuild__test_plan_missing=True
        )
        self.assertNotEqual(0, checkouts_ready.count())

        self.assertQuerySetEqual(
            checkouts_ready,
            models.KCIDBCheckout.objects.filter_ready_to_report()
        )

    def test_failed_build(self):
        """There are builds with test_plan_missing=True but some builds failed."""
        models.KCIDBCheckout.objects.update(valid=True, last_triaged_at=timezone.now())
        models.KCIDBBuild.objects.update(valid=True, last_triaged_at=timezone.now(), test_plan_missing=True)
        models.KCIDBTest.objects.update(status='P', last_triaged_at=timezone.now())

        failed_build = models.KCIDBBuild.objects.first()

        # test_plan_missing=True and all builds succeeded, this checkout is not ready.
        self.assertFalse(failed_build.checkout in models.KCIDBCheckout.objects.filter_ready_to_report())

        failed_build.valid = False
        failed_build.save()

        # test_plan_missing=True but a build failed, this checkout is ready.
        self.assertTrue(failed_build.checkout in models.KCIDBCheckout.objects.filter_ready_to_report())


class TestCheckoutFilterBuildSetupsFinishedToReport(utils.TestCase):
    """Unit tests for KCIDBCheckoutQuerySet.filter_build_setups_finished."""

    fixtures = [
        'tests/fixtures/basic_policies.yaml',
        'tests/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
    ]

    def test_already_notification_sent_build_steps_finished(self):
        """All checkouts are tagged as notification_sent_build_setups_finished."""
        models.KCIDBCheckout.objects.update(notification_sent_build_setups_finished=True)

        self.assertQuerySetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_build_setups_finished()
        )

    def test_build_setups_not_finished(self):
        """Builds without finished setup stage."""
        models.KCIDBCheckout.objects.update(notification_sent_build_setups_finished=False)
        models.KCIDBBuild.objects.update(kpet_tree_name=None)

        self.assertQuerySetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_build_setups_finished()
        )

    def test_build_setups_finished_and_message_has_not_been_sent(self):
        """Build setups finished but the message has not been sent."""
        models.KCIDBCheckout.objects.update(notification_sent_build_setups_finished=False)
        models.KCIDBBuild.objects.update(kpet_tree_name='kpet_tree_name')

        self.assertQuerySetEqual(
            models.KCIDBCheckout.objects.filter_has_builds(),
            models.KCIDBCheckout.objects.filter_build_setups_finished()
        )

    def test_build_setups_finished_and_message_has_been_sent(self):
        """Build setups finished and the message has been sent."""
        models.KCIDBCheckout.objects.update(notification_sent_build_setups_finished=True)
        models.KCIDBBuild.objects.update(kpet_tree_name='kpet_tree_name')

        self.assertQuerySetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_build_setups_finished()
        )


class TestCheckoutFilterTestsFinishedToReport(utils.TestCase):
    """Unit tests for KCIDBCheckoutQuerySet.filter_tests_finished."""

    fixtures = [
        'tests/fixtures/basic_policies.yaml',
        'tests/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
    ]

    def test_already_notification_sent_tests_finished(self):
        """All checkouts are tagged as notification_sent_tests_finished."""
        models.KCIDBCheckout.objects.update(notification_sent_tests_finished=True)

        self.assertQuerySetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_tests_finished()
        )

    def test_tests_not_finished(self):
        """Checkouts with tests unfinished."""
        models.KCIDBCheckout.objects.update(notification_sent_tests_finished=False)
        models.KCIDBTest.objects.update(status=None)

        self.assertQuerySetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_tests_finished()
        )

    def test_tests_finished_and_message_has_not_been_sent(self):
        """Tests finished but the message has not been sent."""
        models.KCIDBCheckout.objects.update(notification_sent_tests_finished=False)
        models.KCIDBTest.objects.update(status='P')

        self.assertQuerySetEqual(
            models.KCIDBCheckout.objects.filter_has_tests(),
            models.KCIDBCheckout.objects.filter_tests_finished()
        )

    def test_tests_finished_and_message_has_been_sent(self):
        """Tests finished and the message has been sent."""
        models.KCIDBCheckout.objects.update(notification_sent_tests_finished=True)
        models.KCIDBTest.objects.update(status='P')

        self.assertQuerySetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_tests_finished()
        )

    def test_tests_finished_test_plan_missing(self):
        """Tests finished when a build has test_plan_missing=True."""
        models.KCIDBTest.objects.update(status='P')

        self.assertTrue(
            models.KCIDBCheckout.objects.filter_has_tests().count() > 0
        )
        self.assertQuerySetEqual(
            models.KCIDBCheckout.objects.filter_has_tests(),
            models.KCIDBCheckout.objects.filter_tests_finished()
        )

        models.KCIDBBuild.objects.update(test_plan_missing=True)

        self.assertQuerySetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_tests_finished()
        )


class TestKCIDBCheckoutQuerySetFilterHasBuilds(utils.TestCase):
    """Unit tests for KCIDBCheckoutQuerySet.filter_has_builds."""

    def test_checkouts_with_builds_and_without_builds(self):
        """Checkout with any builds."""
        checkout_with_build = models.KCIDBCheckout.objects.create(
            id='redhat-1',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        models.KCIDBCheckout.objects.create(
            id='redhat-2',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        models.KCIDBBuild.objects.create(
            checkout=checkout_with_build,
            id='redhat:887318',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )

        self.assertEqual(1, models.KCIDBCheckout.objects.filter_has_builds().count())
        self.assertEqual(checkout_with_build,
                         models.KCIDBCheckout.objects.filter_has_builds()[0]
                         )

    def test_all_checkouts_without_builds(self):
        """Neither checkouts have build."""
        models.KCIDBCheckout.objects.create(
            id='redhat-2',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )

        self.assertQuerySetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_has_builds()
        )

    def test_all_checkouts_with_builds(self):
        """All checkouts have builds."""
        checkout = models.KCIDBCheckout.objects.create(
            id='redhat-1',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        models.KCIDBBuild.objects.create(
            checkout=checkout,
            id='redhat:887318',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )

        self.assertQuerySetEqual(
            models.KCIDBCheckout.objects.all(),
            models.KCIDBCheckout.objects.filter_has_builds()
        )


class TestKCIDBCheckoutQuerySetFilterHasTests(utils.TestCase):
    """Unit tests for KCIDBCheckoutQuerySet.filter_has_tests."""

    def test_checkouts_with_tests_and_without_tests(self):
        """Checkout with any tests."""
        checkout_with_test = models.KCIDBCheckout.objects.create(
            id='redhat-1',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        models.KCIDBCheckout.objects.create(
            id='redhat-2',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        models.KCIDBBuild.objects.create(
            checkout=checkout_with_test,
            id='redhat:887318',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
        }
        models.KCIDBTest.create_from_json(data)

        self.assertEqual(1, models.KCIDBCheckout.objects.filter_has_builds().count())
        self.assertEqual(checkout_with_test,
                         models.KCIDBCheckout.objects.filter_has_builds()[0]
                         )

    def test_all_checkouts_without_tests(self):
        """Neither checkouts have tests."""
        models.KCIDBCheckout.objects.create(
            id='redhat-2',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )

        self.assertQuerySetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_has_tests()
        )

    def test_all_checkouts_with_tests(self):
        """All checkouts have tests."""
        checkout = models.KCIDBCheckout.objects.create(
            id='redhat-1',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        models.KCIDBBuild.objects.create(
            checkout=checkout,
            id='redhat:887318',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
        }
        models.KCIDBTest.create_from_json(data)

        self.assertQuerySetEqual(
            models.KCIDBCheckout.objects.all(),
            models.KCIDBCheckout.objects.filter_has_tests()
        )


class TestKCIDBBuildQuerySet(utils.TestCase):
    """Test KCIDBBuildQuerySet filters and annotations."""

    fixtures = [
        'tests/fixtures/issues.yaml',
        'tests/fixtures/policies_for_group_abc.yaml',
        'tests/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
    ]

    def test_filter_untriaged(self):
        """Test filter_untriaged method."""
        issue = models.Issue.objects.get(id=1)
        empty = models.KCIDBBuild.objects.none()
        queryset = models.KCIDBBuild.objects.filter(id="redhat:public_build_1")
        build = queryset.get()

        input_domain = utils.product_dict(
            valid=[None, False, True],
            triaged=[False, True],
        )

        for case in input_domain:
            queryset.update(valid=case["valid"])
            build.issues.set([issue] if case["triaged"] else [], clear=True)

            with self.subTest(**case):
                match case:
                    case {"valid": False, "triaged": False}:
                        expected = queryset
                    case _:
                        expected = empty

                self.assertQuerySetEqual(queryset.filter_untriaged(), expected)

    def test_filter_triaged(self):
        """Test filter_triaged method."""
        issue = models.Issue.objects.get(id=1)
        empty = models.KCIDBBuild.objects.none()
        queryset = models.KCIDBBuild.objects.filter(id="redhat:public_build_1")
        build = queryset.get()

        input_domain = utils.product_dict(
            valid=[None, False, True],
            triaged=[False, True],
        )

        for case in input_domain:
            queryset.update(valid=case["valid"])
            build.issues.set([issue] if case["triaged"] else [], clear=True)

            with self.subTest(**case):
                match case:
                    case {"valid": False, "triaged": True}:
                        expected = queryset
                    case _:
                        expected = empty

                self.assertQuerySetEqual(queryset.filter_triaged(), expected)

    def test_has_broken_boot_tests_annotation(self):
        """Test has_broken_boot_tests annotation."""
        test = models.Test.objects.create(name="Boot test")
        test2 = models.Test.objects.create(name="Other test")
        kcidb_build = models.KCIDBBuild.objects.get(id="redhat:public_build_1")
        kcidb_tests = models.KCIDBTest.objects.filter(build=kcidb_build)
        kcidb_tests.update(test=test2)
        kcidb_tests.filter(id="redhat:public_test_R").update(test=test)
        kcidb_tests.filter(id="redhat:public_test_P").update(test=test)

        kcidb_tests.update(status=models.ResultEnum.PASS)
        build = models.KCIDBBuild.objects.annotate_has_broken_boot_tests().get(id="redhat:public_build_1")
        self.assertFalse(build.has_broken_boot_tests)

        kcidb_tests.update(status=models.ResultEnum.FAIL)
        build = models.KCIDBBuild.objects.annotate_has_broken_boot_tests().get(id="redhat:public_build_1")
        self.assertFalse(build.has_broken_boot_tests)

        kcidb_tests.update(status=models.ResultEnum.MISS)
        build = models.KCIDBBuild.objects.annotate_has_broken_boot_tests().get(id="redhat:public_build_1")
        self.assertTrue(build.has_broken_boot_tests)

        kcidb_tests.update(status=models.ResultEnum.ERROR)
        build = models.KCIDBBuild.objects.annotate_has_broken_boot_tests().get(id="redhat:public_build_1")
        self.assertTrue(build.has_broken_boot_tests)

        kcidb_tests.filter(id="redhat:public_test_P").update(status=models.ResultEnum.PASS)
        build = models.KCIDBBuild.objects.annotate_has_broken_boot_tests().get(id="redhat:public_build_1")
        self.assertFalse(build.has_broken_boot_tests)

    def _test_untriaged_tests_blocking(self, *, triage_subtest: bool):
        """Test untriaged_tests_blocking method."""
        build = models.KCIDBBuild.objects.get(id="redhat:public_build_1")
        queryset = models.KCIDBTest.objects.filter(build=build)
        empty = models.KCIDBTest.objects.none()

        with self.subTest("aggregated() with stats_tests_untriaged=0 short-circuits"):
            aggregated = models.KCIDBBuild.objects.aggregated().get(id="redhat:public_build_2")
            with self.assertNumQueries(0):
                result = aggregated.untriaged_tests_blocking

            self.assertQuerySetEqual(empty, result)

        tests = models.KCIDBTest.objects.filter(build=build)
        results = models.KCIDBTestResult.objects.filter(test__build=build)

        assert tests.filter(id="redhat:public_test_R", status=None).exists()
        test_f = tests.get(id="redhat:public_test_F", status="F")
        test_e = tests.get(id="redhat:public_test_E", status="E")
        assert tests.filter(id="redhat:public_test_M", status="M").exists()
        assert tests.filter(id="redhat:public_test_S", status="S").exists()
        assert tests.filter(id="redhat:public_test_P", status="P").exists()

        if triage_subtest:
            test_f_result_f = results.get(id="redhat:public_test_F_result_F", status="F")
            test_f_result_e = results.get(id="redhat:public_test_F_result_E", status="E")
            test_e_result_e = results.get(id="redhat:public_test_E_result_E", status="E")
        else:
            test_f_result_f = mock.Mock()
            test_f_result_e = mock.Mock()
            test_e_result_e = mock.Mock()

        issue_1 = models.Issue.objects.get(id=1)
        issue_2 = models.Issue.objects.get(id=2)
        issue_4 = models.Issue.objects.get(id=4)

        input_domain = utils.product_dict(
            waived=[None, False, True],
            triaged=[False, True],
        )
        for case_kwargs in input_domain:
            with self.subTest(triage_subtest=triage_subtest, **case_kwargs):
                tests.update(waived=case_kwargs["waived"])

                # Reset issue occurrences
                test_f.issues.set([])
                test_e.issues.set([])
                test_f_result_f.issues.set([])
                test_e_result_e.issues.set([])
                # NOTE: Partial subtest triaging must not be enough to count as triaged
                test_f_result_e.issues.set([issue_4])

                if case_kwargs["triaged"]:
                    if triage_subtest:
                        test_f_result_f.issues.set([issue_2])
                        test_e_result_e.issues.set([issue_1])
                    else:
                        test_f.issues.set([issue_2, issue_4])
                        test_e.issues.set([issue_1])

                match case_kwargs:
                    # Waived or triaged are always non blocking, but most importantly not considered untriaged
                    case {"waived": True} | {"triaged": True}:
                        expected = empty
                    # Otherwise, if neither are defined, only FAIL should be returned
                    case _:
                        expected = [queryset.get(status=models.ResultEnum.FAIL)]

                self.assertQuerySetEqual(expected, build.untriaged_tests_blocking)

    def test_untriaged_tests_blocking_with_subtests(self):
        """Test untriaged_tests_blocking method with subtests."""
        self._test_untriaged_tests_blocking(triage_subtest=True)

    def test_untriaged_tests_blocking_without_subtests(self):
        """Test untriaged_tests_blocking method without subtests."""
        models.KCIDBTestResult.objects.all().delete()
        self._test_untriaged_tests_blocking(triage_subtest=False)

    def _test_untriaged_tests_non_blocking(self, *, triage_subtest: bool):
        """Test untriaged_tests_non_blocking method."""
        build = models.KCIDBBuild.objects.get(id="redhat:public_build_1")
        queryset = models.KCIDBTest.objects.filter(build=build)
        empty = models.KCIDBTest.objects.none()

        with self.subTest("aggregated() with stats_tests_untriaged=0 short-circuits"):
            aggregated = models.KCIDBBuild.objects.aggregated().get(id="redhat:public_build_2")
            with self.assertNumQueries(0):
                result = aggregated.untriaged_tests_non_blocking

            self.assertQuerySetEqual(empty, result)

        tests = models.KCIDBTest.objects.filter(build=build)
        results = models.KCIDBTestResult.objects.filter(test__build=build)

        assert tests.filter(id="redhat:public_test_R", status=None).exists()
        test_f = tests.get(id="redhat:public_test_F", status="F")
        test_e = tests.get(id="redhat:public_test_E", status="E")
        assert tests.filter(id="redhat:public_test_M", status="M").exists()
        assert tests.filter(id="redhat:public_test_S", status="S").exists()
        assert tests.filter(id="redhat:public_test_P", status="P").exists()

        if triage_subtest:
            test_f_result_f = results.get(id="redhat:public_test_F_result_F", status="F")
            test_f_result_e = results.get(id="redhat:public_test_F_result_E", status="E")
            test_e_result_e = results.get(id="redhat:public_test_E_result_E", status="E")
        else:
            test_f_result_f = mock.Mock()
            test_f_result_e = mock.Mock()
            test_e_result_e = mock.Mock()

        issue_1 = models.Issue.objects.get(id=1)
        issue_2 = models.Issue.objects.get(id=2)
        issue_4 = models.Issue.objects.get(id=4)

        input_domain = utils.product_dict(
            waived=[None, False, True],
            triaged=[False, True],
        )
        for case_kwargs in input_domain:
            with self.subTest(triage_subtest=triage_subtest, **case_kwargs):
                tests.update(waived=case_kwargs["waived"])

                # Reset issue occurrences
                test_f.issues.set([])
                test_e.issues.set([])
                test_f_result_f.issues.set([])
                test_e_result_e.issues.set([])
                # NOTE: Partial subtest triaging must not be enough to count as triaged
                test_f_result_e.issues.set([issue_4])

                if case_kwargs["triaged"]:
                    if triage_subtest:
                        test_f_result_f.issues.set([issue_2])
                        test_e_result_e.issues.set([issue_1])
                    else:
                        test_f.issues.set([issue_2, issue_4])
                        test_e.issues.set([issue_1])

                match case_kwargs:
                    # Waived or triaged are always non blocking, but most importantly not considered untriaged
                    case {"waived": True} | {"triaged": True}:
                        expected = []
                    # Otherwise, if neither are defined, only ERROR should be returned
                    case _:
                        expected = [queryset.get(status=models.ResultEnum.ERROR)]

                self.assertQuerySetEqual(expected, build.untriaged_tests_non_blocking)

    def test_untriaged_tests_non_blocking_with_subtests(self):
        """Test untriaged_tests_non_blocking method with subtests."""
        self._test_untriaged_tests_non_blocking(triage_subtest=True)

    def test_untriaged_tests_non_blocking_without_subtests(self):
        """Test untriaged_tests_non_blocking method without subtests."""
        models.KCIDBTestResult.objects.all().delete()
        self._test_untriaged_tests_non_blocking(triage_subtest=False)


class TestKCIDBTestQuerySet(utils.TestCase):
    """Test for KCIDBTestQuerySet filters and annotations."""

    fixtures = (
        "tests/fixtures/issues.yaml",
        "tests/fixtures/policies_for_group_abc.yaml",
        "tests/fixtures/basic.yaml",
        "tests/kcidb/fixtures/base_simple.yaml",
    )

    def _test_filter_triaged(self, *, triage_subtest: bool):
        """Test KCIDBTestQuerySet.filter_triaged method."""
        tests = models.KCIDBTest.objects.filter(build__checkout__id="redhat:public_checkout")
        results = models.KCIDBTestResult.objects.filter(test__build__checkout__id="redhat:public_checkout")

        assert tests.filter(id="redhat:public_test_R", status=None).exists()
        test_f = tests.get(id="redhat:public_test_F", status="F")
        test_e = tests.get(id="redhat:public_test_E", status="E")
        assert tests.filter(id="redhat:public_test_M", status="M").exists()
        assert tests.filter(id="redhat:public_test_S", status="S").exists()
        assert tests.filter(id="redhat:public_test_P", status="P").exists()

        if triage_subtest:
            test_f_result_f = results.get(id="redhat:public_test_F_result_F", status="F")
            test_f_result_e = results.get(id="redhat:public_test_F_result_E", status="E")
            test_e_result_e = results.get(id="redhat:public_test_E_result_E", status="E")
        else:
            test_f_result_f = mock.Mock()
            test_f_result_e = mock.Mock()
            test_e_result_e = mock.Mock()

        issue_1 = models.Issue.objects.get(id=1)
        issue_2 = models.Issue.objects.get(id=2)
        issue_4 = models.Issue.objects.get(id=4)

        input_domain = utils.product_dict(
            waived=[None, False, True],
            waived_means_triaged=[False, True],
            triaged=[False, True],
        )
        for case_kwargs in input_domain:
            with self.subTest(triage_subtest=triage_subtest, **case_kwargs):
                tests.update(waived=case_kwargs["waived"])

                # Reset issue occurrences
                test_f.issues.set([])
                test_e.issues.set([])
                test_f_result_f.issues.set([])
                test_e_result_e.issues.set([])
                # NOTE: Partial subtest triaging must not be enough to count as triaged
                test_f_result_e.issues.set([issue_4])

                if case_kwargs["triaged"]:
                    if triage_subtest:
                        test_f_result_f.issues.set([issue_2])
                        test_e_result_e.issues.set([issue_1])
                    else:
                        test_f.issues.set([issue_2, issue_4])
                        test_e.issues.set([issue_1])

                match case_kwargs:
                    # If everything is waived and waived_means_triaged, expects unsuccessful tests (no mather triaged)
                    case {"waived": True, "waived_means_triaged": True}:
                        expected = [test_f, test_e]
                    # Otherwise, if FAIL and ERROR are triaged, expects them (no mather waived)
                    case {"triaged": True}:
                        expected = [test_f, test_e]
                    # Otherwise, expects nothing at all
                    case _:
                        expected = []

                result = tests.filter_triaged(waived_means_triaged=case_kwargs["waived_means_triaged"])
                self.assertQuerySetEqual(result, expected)

    def test_filter_triaged_with_subtests(self):
        """Test KCIDBTestQuerySet.filter_triaged works as expected on tests with subtests."""
        self._test_filter_triaged(triage_subtest=True)

    def test_filter_triaged_without_subtests(self):
        """Test KCIDBTestQuerySet.filter_triaged works as expected on tests without subtests."""
        models.KCIDBTestResult.objects.all().delete()
        self._test_filter_triaged(triage_subtest=False)

    def _test_filter_untriaged_blocking(self, *, triage_subtest: bool):
        """Test KCIDBTestQuerySet.filter_untriaged_blocking method."""
        tests = models.KCIDBTest.objects.filter(build__checkout__id="redhat:public_checkout")
        results = models.KCIDBTestResult.objects.filter(test__build__checkout__id="redhat:public_checkout")

        assert tests.filter(id="redhat:public_test_R", status=None).exists()
        test_f = tests.get(id="redhat:public_test_F", status="F")
        test_e = tests.get(id="redhat:public_test_E", status="E")
        assert tests.filter(id="redhat:public_test_M", status="M").exists()
        assert tests.filter(id="redhat:public_test_S", status="S").exists()
        assert tests.filter(id="redhat:public_test_P", status="P").exists()

        if triage_subtest:
            test_f_result_f = results.get(id="redhat:public_test_F_result_F", status="F")
            test_f_result_e = results.get(id="redhat:public_test_F_result_E", status="E")
            test_e_result_e = results.get(id="redhat:public_test_E_result_E", status="E")
        else:
            test_f_result_f = mock.Mock()
            test_f_result_e = mock.Mock()
            test_e_result_e = mock.Mock()

        issue_1 = models.Issue.objects.get(id=1)
        issue_2 = models.Issue.objects.get(id=2)
        issue_4 = models.Issue.objects.get(id=4)

        input_domain = utils.product_dict(
            waived=[None, False, True],
            waived_means_triaged=[False, True],
            triaged=[False, True],
        )
        for case_kwargs in input_domain:
            with self.subTest(**case_kwargs):
                tests.update(waived=case_kwargs["waived"])

                # Reset issue occurrences
                test_f.issues.set([])
                test_e.issues.set([])
                test_f_result_f.issues.set([])
                test_e_result_e.issues.set([])
                # NOTE: Partial subtest triaging must not be enough to count as triaged
                test_f_result_e.issues.set([issue_4])

                if case_kwargs["triaged"]:
                    if triage_subtest:
                        test_f_result_f.issues.set([issue_2])
                        test_e_result_e.issues.set([issue_1])
                    else:
                        test_f.issues.set([issue_2, issue_4])
                        test_e.issues.set([issue_1])

                match case_kwargs:
                    case {"waived": True, "waived_means_triaged": True} | {"triaged": True}:
                        expected = []
                    case _:
                        expected = [test_f]

                result = tests.filter_untriaged_blocking(waived_means_triaged=case_kwargs["waived_means_triaged"])
                self.assertQuerySetEqual(result, expected)

    def test_filter_untriaged_blocking_with_subtests(self):
        """Test KCIDBTestQuerySet.filter_untriaged_blocking works as expected on tests with subtests."""
        self._test_filter_untriaged_blocking(triage_subtest=True)

    def test_filter_untriaged_blocking_without_subtests(self):
        """Test KCIDBTestQuerySet.filter_untriaged_blocking works as expected on tests without subtests."""
        models.KCIDBTestResult.objects.all().delete()
        self._test_filter_untriaged_blocking(triage_subtest=False)

    def _test_filter_untriaged_blocking_override_issue_occurrences(self, *, triage_subtest: bool):
        """Test overriding issue_occurrences in KCIDBTestQuerySet.filter_untriaged_blocking method."""
        tests = models.KCIDBTest.objects.filter(build__checkout__id="redhat:public_checkout")
        results = models.KCIDBTestResult.objects.filter(test__build__checkout__id="redhat:public_checkout")

        # make every test blocking
        tests.update(status=models.ResultEnum.FAIL, waived=None)
        results.update(status=models.ResultEnum.FAIL)

        # Pick two tests to triage with two different issues
        test_1, test_2, *rest = list(tests)
        issue_1 = models.Issue.objects.only("id").get(id=1)
        issue_2 = models.Issue.objects.only("id").get(id=2)

        if triage_subtest:
            test_1_result = test_1.kcidbtestresult_set.get()
            test_2_result = test_2.kcidbtestresult_set.get()
            test_1_result.issues.set([issue_1])
            test_2_result.issues.set([issue_2])
        else:
            test_1.issues.set([issue_1])
            test_2.issues.set([issue_2])

        with self.subTest("Default issueoccurrence related manager", issue_occurrences=None):
            result = tests.filter_untriaged_blocking()
            expected = [*rest]
            self.assertQuerySetEqual(result, expected)

        issue_occurrences = models.IssueOccurrence.objects.exclude(issue__in=[issue_1])
        with self.subTest("Filtered issueoccurrence related manager", issue_occurrences=issue_occurrences):
            result = tests.filter_untriaged_blocking(issue_occurrences=issue_occurrences)
            expected = [test_1, *rest]
            self.assertQuerySetEqual(result, expected)

    def test_filter_untriaged_blocking_override_issue_occurrences_with_subtest(self):
        """Test overriding issue_occurrences in KCIDBTestQuerySet.filter_untriaged_blocking method with subtest."""
        # We just need one result for each test, so keep only the worst (status matching their parenting test)
        models.KCIDBTestResult.objects.exclude(
            Q(status=None, test__status=None) | Q(status=F("test__status"))
        ).delete()

        self._test_filter_untriaged_blocking_override_issue_occurrences(triage_subtest=True)

    def test_filter_untriaged_blocking_override_issue_occurrences_without_subtest(self):
        """Test overriding issue_occurrences in KCIDBTestQuerySet.filter_untriaged_blocking method with subtest."""
        models.KCIDBTestResult.objects.all().delete()
        self._test_filter_untriaged_blocking_override_issue_occurrences(triage_subtest=False)

    def _test_filter_untriaged_non_blocking(self, *, triage_subtest: bool):
        """Test KCIDBTestQuerySet.filter_untriaged_non_blocking method."""
        tests = models.KCIDBTest.objects.all()
        results = models.KCIDBTestResult.objects.filter(test__build__checkout__id="redhat:public_checkout")

        assert tests.filter(id="redhat:public_test_R", status=None).exists()
        test_f = tests.get(id="redhat:public_test_F", status="F")
        test_e = tests.get(id="redhat:public_test_E", status="E")
        assert tests.filter(id="redhat:public_test_M", status="M").exists()
        assert tests.filter(id="redhat:public_test_S", status="S").exists()
        assert tests.filter(id="redhat:public_test_P", status="P").exists()

        if triage_subtest:
            test_f_result_f = results.get(id="redhat:public_test_F_result_F", status="F")
            test_f_result_e = results.get(id="redhat:public_test_F_result_E", status="E")
            test_e_result_e = results.get(id="redhat:public_test_E_result_E", status="E")
        else:
            test_f_result_f = mock.Mock()
            test_f_result_e = mock.Mock()
            test_e_result_e = mock.Mock()

        issue_1 = models.Issue.objects.get(id=1)
        issue_2 = models.Issue.objects.get(id=2)
        issue_4 = models.Issue.objects.get(id=4)

        input_domain = utils.product_dict(
            waived=[None, False, True],
            waived_means_triaged=[False, True],
            triaged=[False, True],
        )
        for case_kwargs in input_domain:
            with self.subTest(triage_subtest=triage_subtest, **case_kwargs):
                tests.update(waived=case_kwargs["waived"])

                # Reset issue occurrences
                test_f.issues.set([])
                test_e.issues.set([])
                test_f_result_f.issues.set([])
                test_e_result_e.issues.set([])
                # NOTE: Partial subtest triaging must not be enough to count as triaged
                test_f_result_e.issues.set([issue_4])

                if case_kwargs["triaged"]:
                    if triage_subtest:
                        test_f_result_f.issues.set([issue_2])
                        test_e_result_e.issues.set([issue_1])
                    else:
                        test_f.issues.set([issue_2, issue_4])
                        test_e.issues.set([issue_1])

                match case_kwargs:
                    case {"waived": True, "waived_means_triaged": True} | {"triaged": True}:
                        expected = []
                    case _:
                        expected = [test_e]

                result = tests.filter_untriaged_non_blocking(waived_means_triaged=case_kwargs["waived_means_triaged"])
                self.assertQuerySetEqual(result, expected)

    def test_filter_untriaged_non_blocking_with_subtests(self):
        """Test KCIDBTestQuerySet.filter_untriaged_non_blocking works as expected on tests with subtests."""
        self._test_filter_untriaged_non_blocking(triage_subtest=True)

    def test_filter_untriaged_non_blocking_without_subtests(self):
        """Test KCIDBTestQuerySet.filter_untriaged_non_blocking works as expected on tests without subtests."""
        models.KCIDBTestResult.objects.all().delete()
        self._test_filter_untriaged_non_blocking(triage_subtest=False)


class TestKCIDBCheckoutAnnotationForFilter(utils.TestCase):
    """Test annotation_for_filter function."""
    fixtures = [
        'tests/fixtures/basic_policies.yaml',
        'tests/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
    ]

    def test_nvr(self):
        """Test the nvr annotation."""
        checkout_queryset = models.KCIDBCheckout.objects.filter(id="redhat:public_checkout")

        cases = [
            ('kernel', '1.2.3', '123.foo', 'kernel-1.2.3-123.foo'),
            ('kernel', '', '', 'kernel--'),
            ('', '1.2.3', '123.foo', '-1.2.3-123.foo'),
            ('', '', '', '--'),
            ('kernel', None, None, 'kernel--'),
            (None, '1.2.3', '123.foo', '-1.2.3-123.foo'),
            (None, None, None, '--')
        ]

        for spackage_name, spackage_version, spackage_release, expected in cases:
            with self.subTest(source_package_name=spackage_name,
                              source_package_version=spackage_version,
                              source_package_release=spackage_release):
                checkout_queryset.update(source_package_name=spackage_name,
                                         source_package_version=spackage_version,
                                         source_package_release=spackage_release)
                annotated_checkout = checkout_queryset.annotation_for_filter().first()
                self.assertEqual(annotated_checkout.nvr, expected)


class TestKCIDBBuildAnnotationForFilter(utils.TestCase):
    """Test annotation_for_filter function."""
    fixtures = [
        'tests/fixtures/basic_policies.yaml',
        'tests/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
    ]

    def test_nvr(self):
        """Test the nvr annotation."""
        build_queryset = models.KCIDBBuild.objects.filter(id="redhat:public_build_1")

        cases = [
            ('kernel', '1.2.3', '123.foo', 'kernel-1.2.3-123.foo'),
            ('kernel', '', '', 'kernel--'),
            ('', '1.2.3', '123.foo', '-1.2.3-123.foo'),
            ('', '', '', '--'),
            ('kernel', None, None, 'kernel--'),
            (None, '1.2.3', '123.foo', '-1.2.3-123.foo'),
            (None, None, None, '--')
        ]

        for package_name, package_version, package_release, expected in cases:
            with self.subTest(package_name=package_name,
                              package_version=package_version,
                              package_release=package_release):
                build_queryset.update(package_name=package_name,
                                      package_version=package_version,
                                      package_release=package_release)
                annotated_build = build_queryset.annotation_for_filter().first()
                self.assertEqual(annotated_build.nvr, expected)


class TestKCIDBTestAnnotationForFilter(utils.TestCase):
    """Test annotation_for_filter function."""
    fixtures = [
        'tests/fixtures/basic_policies.yaml',
        'tests/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_complete.yaml',
    ]

    def test_inherited_start_time(self):
        """Test the inherited_start_time annotation."""
        tests = models.KCIDBTest.objects.annotation_for_filter()

        cases = [
            ('use test start_time', 'redhat:111218982', '2020-06-03T15:52:25Z'),
            ('use test start_time', 'redhat:111218983', '2020-06-03T16:01:14Z'),
            ('use build start_time', 'redhat:111218984', '2020-06-03T15:14:57.215Z'),
            ('use checkout start_time', 'redhat:111218985', '2020-06-01T06:47:41.108Z'),
            ('missing start_time on every level', 'redhat:111218986', None),
        ]

        for description, test_id, expected in cases:
            with self.subTest(description,
                              test_id=test_id,
                              expected=expected):
                test = tests.get(id=test_id)
                expected_datetime = timestamp_to_datetime(expected)
                self.assertEqual(test.inherited_start_time, expected_datetime)
