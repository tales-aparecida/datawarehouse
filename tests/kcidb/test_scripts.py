"""Test the views module."""
import datetime
from http import HTTPStatus
import json
from unittest import mock

from cki_lib.kcidb import KCIDBFile
import dateutil
from django.conf import settings
from django.urls import reverse
from freezegun import freeze_time
from rest_framework import status

from datawarehouse import models
from datawarehouse import recipients
from datawarehouse.api.kcidb import serializers
from datawarehouse.api.kcidb import views
from datawarehouse.serializers import IssueOccurrenceLightSerializer
from tests import utils

# DataWarehouse should aim to consume the latest cki-lib KCIDB schema
KCIDB_SCHEMA_VERSION = {
    "major": settings.CONSUMER_KCIDB_SCHEMA.major,
    "minor": settings.CONSUMER_KCIDB_SCHEMA.minor,
}


@mock.patch('datawarehouse.signals.kcidb_object.send', mock.Mock())
class TestSubmit(utils.TestCase):
    """Test submit endpoint."""

    fixtures = [
        'tests/fixtures/basic_policies.yaml',
    ]

    FULL_PAYLOAD = "tests/kcidb/test_data/example_submit_payload.json"

    def test_error_invalid_payload(self):
        """Test validation of the payload format."""
        valid_input_data = {'version': KCIDB_SCHEMA_VERSION}
        cases = [
            ("Valid payload", {"data": valid_input_data}, status.HTTP_201_CREATED),
            ("Missing 'data' key", valid_input_data, status.HTTP_400_BAD_REQUEST),
            ("Wrong 'data' content", {"data": json.dumps(valid_input_data)}, status.HTTP_400_BAD_REQUEST),
            ("Empty payload", {}, status.HTTP_400_BAD_REQUEST),
            ("Invalid chars are removed", {"\udc98data": valid_input_data}, status.HTTP_201_CREATED),
            ("Invalid JSON raises a parser error", "not a valid json", status.HTTP_400_BAD_REQUEST),
        ]

        for desc, data, expected_status_code in cases:
            with self.subTest(desc):
                self.assert_authenticated_post(
                    expected_status_code,
                    "add_kcidbcheckout",
                    reverse("kcidb.submit"),
                    json.dumps(data) if not isinstance(data, str) else data,
                    content_type="application/json",
                )

    def test_full_submit(self):
        """Test submitting all the data."""
        with open(self.FULL_PAYLOAD, encoding="utf-8") as fp_payload:
            data = json.load(fp_payload)

        self.assert_authenticated_post(
            201, 'add_kcidbcheckout', '/api/1/kcidb/submit', json.dumps({'data': data}),
            content_type="application/json"
        )

        for checkout in data['checkouts']:
            self.assertTrue(models.KCIDBCheckout.objects.filter(id=checkout['id']).exists())

        for build in data['builds']:
            self.assertTrue(models.KCIDBBuild.objects.filter(id=build['id']).exists())

        for test in data['tests']:
            self.assertTrue(models.KCIDBTest.objects.filter(id=test['id']).exists())

    def test_admit_testresult_status_warn(self):
        """Assert that TestResult.status = "WARN" is admitted and parsed into ERROR."""
        # This value should be allowed until submitters handle validation better
        with open(self.FULL_PAYLOAD, encoding="utf-8") as fp_payload:
            data = json.load(fp_payload)

        # Test Results without cki_lib.kcidb.validate.sanitize_kcidb_status might include "WARN"
        with self.subTest("Payload with TestResult.status=WARN"):
            testresult_dict = data["tests"][1]["misc"]["results"][0]
            testresult_dict["status"] = "WARN/ABORTED"
            testresult_id = testresult_dict['id']

            self.assert_authenticated_post(
                status.HTTP_201_CREATED,
                'add_kcidbcheckout',
                reverse('kcidb.submit'),
                json.dumps({'data': data}),
                content_type="application/json"
            )

            result_status = (
                models.KCIDBTestResult.objects.values_list('status', flat=True)
                .get(id=testresult_id)
            )
            self.assertEqual(result_status, models.ResultEnum.ERROR)

    def test_admit_cki_lib_kcidb_file(self):
        """Test DW supports data produced by cki-lib."""

        self.assert_authenticated_post(
            HTTPStatus.CREATED,
            "add_kcidbcheckout",
            "/api/1/kcidb/submit",
            json.dumps({"data": KCIDBFile.DEFAULT_CONTENT}),
            content_type="application/json",
        )

    def test_error_missing_parent(self):
        """Test submitting an object without it's parent."""
        data = {
            'version': KCIDB_SCHEMA_VERSION,
            'builds': [
                {'origin': 'redhat', 'checkout_id': 'redhat:5678',
                 'id': 'redhat:1234'},
            ]
        }

        self.assertFalse(
            models.KCIDBCheckout.objects.filter(
                id='https://repo.git@decd6167bf4f6bec1284006d0522381b44660df3'
            ).exists()
        )
        response = self.assert_authenticated_post(
            400, 'add_kcidbcheckout', '/api/1/kcidb/submit', json.dumps({'data': data}),
            content_type="application/json"
        )
        self.assertDictEqual(
            {
                'errors': [
                    [
                        'builds',
                        'redhat:1234',
                        'KCIDBCheckout id=redhat:5678 is not present in the DB'
                    ]
                ]
            },
            response.json()
        )

    def test_error_results_in_partial_success(self):
        """Test submitting a payload mixing valid and invalid items results in a mixed response.

        Whatever is valid will be committed to the database,
        but the response will still be a Bad Request
        """

        with open(self.FULL_PAYLOAD, encoding="utf-8") as fp_payload:
            data = json.load(fp_payload)

        checkout_id = data["checkouts"][0]["id"]

        testresult_dict = data["tests"][1]["misc"]["results"][0]
        del testresult_dict["status"]  # Missing required property
        testresult_id = testresult_dict['id']

        self.assertFalse(
            models.KCIDBCheckout.objects.filter(id=checkout_id).exists(),
            "Expected precondition: KCIDBCheckout should not exist"
        )

        response = self.assert_authenticated_post(
            status.HTTP_400_BAD_REQUEST,
            'add_kcidbcheckout',
            reverse('kcidb.submit'),
            json.dumps({'data': data}),
            content_type="application/json"
        )
        self.assertDictEqual(
            {
                'errors': [
                    '<ValidationError: "'
                    "Failed to validate against the CKI's extended KCIDB schema. "
                    "\'status\' is a required property"
                    '">'
                ]
            },
            response.json()
        )

        # Expect invalid objects to not be created
        self.assertFalse(
            models.KCIDBTestResult.objects.filter(id=testresult_id).exists(),
            "Expected KCIDBTestResult with invalid status to NOT be created"
        )
        # In fact, nothing should've been created or updated after the validation fails
        # Checking KCIDBCheckout is enough, given the induction of required FKs
        self.assertFalse(
            models.KCIDBCheckout.objects.filter(id=checkout_id).exists(),
            "Expected KCIDBCheckout to NOT be created"
        )

    def test_error_origin_validation(self):
        """Test submitting an object with different invalid origin and id."""
        exception_match = (
            "<ValidationError: "
            "'checkout id (_:invalid_id) does not match origin (redhat) constraint'>"
        )
        exception_missing_in_id = (
            '<ValidationError: "Failed to validate against the KCIDB schema. '
            '\'invalid_id\' does not match \'^[a-z0-9_]+:.*$\'">'
        )
        exception_missing_origin = (
            '<ValidationError: "Failed to validate against the KCIDB schema. '
            '\'origin\' is a required property">'
        )

        cases = [
            ("Origin in id not matching", {'origin': 'redhat', 'id': '_:invalid_id'}, exception_match),
            ("Origin in id missing", {'origin': 'redhat', 'id': 'invalid_id'}, exception_missing_in_id),
            ("Origin missing", {'id': 'redhat:id'}, exception_missing_origin),
        ]

        for msg, checkout, exception_text in cases:
            with self.subTest(msg):
                data = {'version': KCIDB_SCHEMA_VERSION, 'checkouts': [checkout]}

                self.assertFalse(
                    models.KCIDBCheckout.objects.filter(
                        id='invalid_id'
                    ).exists()
                )

                with self.assertLogs('cki', level='INFO') as logs:
                    response = self.assert_authenticated_post(
                        400, 'add_kcidbcheckout', '/api/1/kcidb/submit', json.dumps({'data': data}),
                        content_type="application/json"
                    )
                    self.assertIn(f"WARNING:{views.LOGGER.name}:{exception_text}", logs.output)

                self.assertDictEqual({'errors': [exception_text]}, response.json())

    @mock.patch('datawarehouse.models.kcidb_models.KCIDBCheckout.create_from_json')
    def test_unexpected_error(self, mocked_create_from_json):
        """Test unexpected errors during "create_from_json" fails graciously."""
        data = {
            "version": KCIDB_SCHEMA_VERSION,
            "checkouts": [{'id': 'redhat:1234', 'origin': 'redhat'}]
        }

        fake_exception = RuntimeError("Something weird happened")
        mocked_create_from_json.side_effect = fake_exception
        expected_error = f"Unexpected error after validation: {fake_exception!r}"
        expected_log = f"ERROR:{views.LOGGER.name}:{expected_error}"

        with self.assertLogs('cki', level='ERROR') as log_ctx:
            response = self.assert_authenticated_post(
                status.HTTP_400_BAD_REQUEST,
                'add_kcidbcheckout',
                reverse('kcidb.submit'),
                json.dumps({'data': data}),
                content_type="application/json"
            )

        mocked_create_from_json.assert_called()

        self.assertEqual(len(log_ctx.output), 1)
        self.assertTrue(log_ctx.output[0].startswith(expected_log),
                        f"{expected_log!r} not in {log_ctx.output[0]!r}")

        self.assertDictEqual(
            {'errors': [['checkouts', 'redhat:1234', expected_error]]},
            response.json()
        )

    def test_retrigger(self):
        """Test submitting retriggered pipeline."""
        data = {
            'version': KCIDB_SCHEMA_VERSION,
            'checkouts': [
                {
                    'origin': 'redhat',
                    'id': 'redhat:repo.git@decd6167bf4f6bec1284006d0522381b44660df3',
                    'misc': {'retrigger': True}
                },
            ]
        }

        self.assert_authenticated_post(
            201, 'add_kcidbcheckout', '/api/1/kcidb/submit', json.dumps({'data': data}),
            content_type="application/json"
        )

        checkout = models.KCIDBCheckout.objects.get(
            id='redhat:repo.git@decd6167bf4f6bec1284006d0522381b44660df3'
        )
        self.assertEqual('retrigger', checkout.policy.name)

    def test_sorted(self):
        """Test sorting the data."""
        checkouts = [
            {'origin': 'redhat', 'id': 'redhat:1'},
        ]
        tests = [
            {'origin': 'redhat', 'build_id': 'redhat:1', 'id': 'redhat:2'},
            {'origin': 'redhat', 'build_id': 'redhat:1', 'id': 'redhat:1',
             'misc': {'rerun_index': 2}},
            {'origin': 'redhat', 'build_id': 'redhat:1', 'id': 'redhat:1',
             'misc': {'rerun_index': 1}},
            {'origin': 'redhat', 'build_id': 'redhat:1', 'id': 'redhat:10'},
        ]

        # Check it doesn't break with something else than tests
        self.assertEqual(
            views.Submit.sorted(checkouts),
            checkouts
        )

        self.assertEqual(
            [
                {'build_id': 'redhat:1', 'id': 'redhat:1', 'origin': 'redhat',
                 'misc': {'rerun_index': 1}},
                {'build_id': 'redhat:1', 'id': 'redhat:1', 'origin': 'redhat',
                 'misc': {'rerun_index': 2}},
                {'build_id': 'redhat:1', 'id': 'redhat:2', 'origin': 'redhat'},
                {'build_id': 'redhat:1', 'id': 'redhat:10', 'origin': 'redhat'}
            ],
            views.Submit.sorted(tests)
        )


@mock.patch('datawarehouse.signals.kcidb_object.send', mock.Mock())
class TestEndpointsAnonymous(utils.KCIDBTestCase):
    """Test kcidb get/list endpoints."""

    fixtures = [
        'tests/fixtures/policies_for_group_abc.yaml',
        'tests/fixtures/basic.yaml',
        'tests/fixtures/issues.yaml',
        'tests/kcidb/fixtures/base_authorization.yaml',
    ]

    anonymous = True
    groups = []

    def test_checkouts_list(self):
        """Test checkouts list endpoint."""
        self._ensure_test_conditions('read')
        authorized_checkouts = models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized['read']
        )
        response = self.client.get('/api/1/kcidb/checkouts')
        self.assertEqual(
            serializers.KCIDBCheckoutSerializer(authorized_checkouts, many=True).data,
            response.json()['results']
        )

    def test_checkouts_get(self):
        """Test checkouts get endpoint. Both iid and id queries."""
        self._ensure_test_conditions('read')
        authorized_checkouts = models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized['read']
        )

        for checkout in models.KCIDBCheckout.objects.all():
            response_iid = self.client.get(f'/api/1/kcidb/checkouts/{checkout.iid}')
            response_id = self.client.get(f'/api/1/kcidb/checkouts/{checkout.id}')

            if checkout not in authorized_checkouts:
                self.assertEqual(404, response_iid.status_code)
                self.assertEqual(404, response_id.status_code)
                continue

            self.assertEqual(
                response_id.json(),
                response_iid.json()
            )
            self.assertEqual(
                serializers.KCIDBCheckoutSerializer(checkout).data,
                response_id.json()
            )

    def test_builds_list(self):
        """Test builds list endpoint."""
        self._ensure_test_conditions('read')
        authorized_checkouts = models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized['read']
        )

        for checkout in models.KCIDBCheckout.objects.all():
            response = self.client.get(f'/api/1/kcidb/checkouts/{checkout.iid}/builds')

            builds = checkout.kcidbbuild_set.all() if checkout in authorized_checkouts else []
            self.assertEqual(
                serializers.KCIDBBuildSerializer(builds, many=True).data,
                response.json()['results']
            )

    def test_builds_get(self):
        """Test builds get endpoint. Both iid and id queries."""
        self._ensure_test_conditions('read')
        authorized_builds = models.KCIDBBuild.objects.filter(
            checkout__id__in=self.checkouts_authorized['read']
        )

        for build in models.KCIDBBuild.objects.all():
            response_iid = self.client.get(f'/api/1/kcidb/builds/{build.iid}')
            response_id = self.client.get(f'/api/1/kcidb/builds/{build.id}')

            if build not in authorized_builds:
                self.assertEqual(404, response_iid.status_code)
                self.assertEqual(404, response_id.status_code)
                continue

            self.assertEqual(
                response_id.json(),
                response_iid.json()
            )
            self.assertEqual(
                serializers.KCIDBBuildSerializer(build).data,
                response_id.json()
            )

    def test_tests_list(self):
        """Test builds list endpoint."""
        self._ensure_test_conditions('read')
        authorized_builds = models.KCIDBBuild.objects.filter(
            checkout__id__in=self.checkouts_authorized['read']
        )

        for build in models.KCIDBBuild.objects.all():
            response = self.client.get(f'/api/1/kcidb/builds/{build.id}/tests')

            tests = build.kcidbtest_set.all() if build in authorized_builds else []
            self.assertEqual(
                serializers.KCIDBTestSerializer(tests, many=True).data,
                response.json()['results']
            )

    def test_tests_get(self):
        """Test tests get endpoint. Both iid and id queries."""
        self._ensure_test_conditions('read')
        authorized_tests = models.KCIDBTest.objects.filter(
            build__checkout__id__in=self.checkouts_authorized['read']
        )

        for test in models.KCIDBTest.objects.all():
            response_iid = self.client.get(f'/api/1/kcidb/tests/{test.iid}')
            response_id = self.client.get(f'/api/1/kcidb/tests/{test.id}')

            if test not in authorized_tests:
                self.assertEqual(404, response_iid.status_code)
                self.assertEqual(404, response_id.status_code)
                continue

            self.assertEqual(
                response_id.json(),
                response_iid.json()
            )
            self.assertEqual(
                serializers.KCIDBTestSerializer(test).data,
                response_id.json()
            )

    def test_testresults_list(self):
        """Test testresults list endpoint."""
        self._ensure_test_conditions('read')
        authorized_tests = models.KCIDBTest.objects.filter(
            build__checkout__id__in=self.checkouts_authorized['read']
        )

        for test in models.KCIDBTest.objects.all():
            response = self.client.get(f'/api/1/kcidb/tests/{test.id}/results')

            testresults = test.kcidbtestresult_set.all() if test in authorized_tests else []
            self.assertEqual(
                serializers.KCIDBTestResultSerializer(testresults, many=True).data,
                response.json()['results']
            )

    def test_testresults_get(self):
        """Test testresults get endpoint. Both iid and id queries."""
        self._ensure_test_conditions('read')
        authorized_testresults = models.KCIDBTestResult.objects.filter(
            test__build__checkout__id__in=self.checkouts_authorized['read']
        )

        for testresult in models.KCIDBTestResult.objects.all():
            response_iid = self.client.get(f'/api/1/kcidb/testresults/{testresult.iid}')
            response_id = self.client.get(f'/api/1/kcidb/testresults/{testresult.id}')

            if testresult not in authorized_testresults:
                self.assertEqual(404, response_iid.status_code)
                self.assertEqual(404, response_id.status_code)
                continue

            self.assertEqual(
                response_id.json(),
                response_iid.json()
            )
            self.assertEqual(
                serializers.KCIDBTestResultSerializer(testresult).data,
                response_id.json()
            )

    @freeze_time("2010-01-02 09:00:00")
    def test_checkout_actions_triaged(self):
        """Test tagging checkouts as triaged."""
        self._ensure_test_conditions('write')
        authorized_checkouts = models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized['write']
        )

        for checkout in models.KCIDBCheckout.objects.all():
            self.assertFalse(checkout.last_triaged_at)
            authorized = checkout in authorized_checkouts and not self.anonymous
            code = 201 if authorized else 404

            self.assert_authenticated_post(
                code, 'add_kcidbcheckout', f'/api/1/kcidb/checkouts/{checkout.iid}/actions/triaged', user=self.user
            )

            checkout.refresh_from_db()

            self.assertEqual(
                checkout.last_triaged_at,
                datetime.datetime(2010, 1, 2, 9, 0, tzinfo=dateutil.tz.UTC) if authorized else None,
            )

    @freeze_time("2010-01-02 09:00:00")
    def test_builds_actions_triaged(self):
        """Test tagging builds as triaged."""
        self._ensure_test_conditions('write')
        authorized_builds = models.KCIDBBuild.objects.filter(
            checkout__id__in=self.checkouts_authorized['write']
        )

        for build in models.KCIDBBuild.objects.all():
            self.assertFalse(build.last_triaged_at)
            authorized = build in authorized_builds and not self.anonymous
            code = 201 if authorized else 404

            self.assert_authenticated_post(
                code, 'add_kcidbbuild', f'/api/1/kcidb/builds/{build.iid}/actions/triaged', user=self.user
            )

            build.refresh_from_db()

            self.assertEqual(
                build.last_triaged_at,
                datetime.datetime(2010, 1, 2, 9, 0, tzinfo=dateutil.tz.UTC) if authorized else None,
            )

    @freeze_time("2010-01-02 09:00:00")
    def test_tests_actions_triaged(self):
        """Test tagging tests as triaged."""
        self._ensure_test_conditions('write')
        authorized_tests = models.KCIDBTest.objects.filter(
            build__checkout__id__in=self.checkouts_authorized['write']
        )

        for test in models.KCIDBTest.objects.all():
            self.assertFalse(test.last_triaged_at)
            authorized = test in authorized_tests and not self.anonymous
            code = 201 if authorized else 404

            self.assert_authenticated_post(
                code, 'add_kcidbtest', f'/api/1/kcidb/tests/{test.iid}/actions/triaged', user=self.user
            )

            test.refresh_from_db()

            self.assertEqual(
                test.last_triaged_at,
                datetime.datetime(2010, 1, 2, 9, 0, tzinfo=dateutil.tz.UTC) if authorized else None,
            )

    def test_checkout_recipients(self):
        """Test retrieving checkout recipients."""
        self._ensure_test_conditions('read')
        authorized_checkouts = models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized['read']
        )

        for checkout in models.KCIDBCheckout.objects.all():
            response_iid = self.client.get(f'/api/1/kcidb/checkouts/{checkout.iid}/recipients')
            response_id = self.client.get(f'/api/1/kcidb/checkouts/{checkout.id}/recipients')

            if checkout not in authorized_checkouts:
                self.assertEqual(404, response_iid.status_code)
                self.assertEqual(404, response_id.status_code)
                continue

            self.assertEqual(
                response_id.json(),
                response_iid.json()
            )

            self.assertEqual(
                # Recipients' set values are encoded as lists
                {k: list(v) for k, v in recipients.Recipients(checkout).render().items()},
                response_id.json()
            )

    @mock.patch('datawarehouse.api.kcidb.views.recipients.Recipients')
    def test_checkout_recipients_aggregated(self, mock_recipients):
        """
        Test retrieving checkout recipients.

        Make sure the checkout passed to recipients.Recipients is aggregated.
        """
        mock_recipients.return_value.render.return_value = {}
        self._ensure_test_conditions('read')

        checkout = models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized['read']
        ).first()
        self.client.get(f'/api/1/kcidb/checkouts/{checkout.id}/recipients')

        # If the checkout is aggregated, it must have stats_builds_passed
        self.assertTrue(hasattr(mock_recipients.call_args[0][0], 'stats_builds_passed'))

    def test_checkout_all(self):
        """Test retrieving checkout child objects."""
        self._ensure_test_conditions('read')
        authorized_checkouts = models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized['read']
        )

        for checkout in models.KCIDBCheckout.objects.all():
            response_iid = self.client.get(f'/api/1/kcidb/checkouts/{checkout.iid}/all')
            response_id = self.client.get(f'/api/1/kcidb/checkouts/{checkout.id}/all')

            if checkout not in authorized_checkouts:
                self.assertEqual(404, response_iid.status_code)
                self.assertEqual(404, response_id.status_code)
                continue

            self.assertEqual(
                response_id.json(),
                response_iid.json()
            )

            builds = models.KCIDBBuild.objects.filter(checkout=checkout)
            tests = models.KCIDBTest.objects.filter(build__checkout=checkout)
            testresults = models.KCIDBTestResult.objects.filter(test__in=tests)
            issueoccurrences = models.IssueOccurrence.objects.filter(related_checkout__iid=checkout.iid)

            self.assertEqual(
                {
                    'checkouts': serializers.KCIDBCheckoutSerializer([checkout], many=True).data,
                    'builds': serializers.KCIDBBuildSerializer(builds, many=True).data,
                    'tests': serializers.KCIDBTestLightSerializer(tests, many=True).data,
                    'testresults': serializers.KCIDBTestResultLightSerializer(testresults, many=True).data,
                    'issueoccurrences': IssueOccurrenceLightSerializer(issueoccurrences, many=True).data,
                },
                response_id.json()
            )


class TestEndpointsNoGroups(TestEndpointsAnonymous):
    """TestEndpoints with no groups."""

    anonymous = False
    groups = []


class TestEndpointsReadGroup(TestEndpointsAnonymous):
    """TestEndpoints with a group with read authorization."""

    anonymous = False
    groups = ['group_a']


class TestEndpointsWriteGroup(TestEndpointsAnonymous):
    """TestEndpoints with a group with write authorization."""

    anonymous = False
    groups = ['group_b']


class TestEndpointsAllGroups(TestEndpointsAnonymous):
    """TestEndpoints with all groups."""

    anonymous = False
    groups = ['group_a', 'group_b']
