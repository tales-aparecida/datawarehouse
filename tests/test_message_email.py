"""Test the email module."""
from unittest import mock

from django import test
from django.conf import settings

from datawarehouse import message_email
from datawarehouse import models


class EmailQueueTest(test.TestCase):
    """Unit tests for the EmailQueue module."""

    def test_add_recipients_string(self):
        """Test add does the right thing. Recipients is a string."""
        message_email.EmailQueue().add(
            'subject_1', 'message_1', 'addr1@mail.com'
        )

        msg = models.MessagePending.objects.get()
        self.assertEqual(msg.kind, models.MessageKindEnum.EMAIL)
        self.assertEqual(msg.content['subject'], 'subject_1')
        self.assertEqual(msg.content['body'], 'message_1')
        self.assertDictEqual(msg.content['recipients'], {'to': ['addr1@mail.com']})
        self.assertEqual(msg.content['headers'], {})

    def test_add_recipients_list(self):
        """Test add does the right thing. Recipients is a list."""
        message_email.EmailQueue().add(
            'subject_1', 'message_1', ['addr1@mail.com', 'addr2@mail.com']
        )

        msg = models.MessagePending.objects.get()
        self.assertEqual(msg.kind, models.MessageKindEnum.EMAIL)
        self.assertEqual(msg.content['subject'], 'subject_1')
        self.assertEqual(msg.content['body'], 'message_1')
        self.assertDictEqual(
            msg.content['recipients'],
            {
                'to': ['addr1@mail.com', 'addr2@mail.com'],
            }
        )
        self.assertEqual(msg.content['headers'], {})

    def test_add_recipients_dict(self):
        """Test add does the right thing. Recipients is a dict."""
        message_email.EmailQueue().add(
            'subject_1', 'message_1',
            {'to': ['addr1@mail.com', 'addr2@mail.com'],
             'cc': ['addr3@mail.com'],
             'bcc': ['addr4@mail.com']}
        )

        msg = models.MessagePending.objects.get()
        self.assertEqual(msg.kind, models.MessageKindEnum.EMAIL)
        self.assertEqual(msg.content['subject'], 'subject_1')
        self.assertEqual(msg.content['body'], 'message_1')
        self.assertEqual(
            msg.content['recipients'],
            {
                'to': ['addr1@mail.com', 'addr2@mail.com'],
                'cc': ['addr3@mail.com'],
                'bcc': ['addr4@mail.com']
            }
        )
        self.assertEqual(msg.content['headers'], {})

    def test_add_headers(self):
        """Test add does the right thing. Include headers."""
        message_email.EmailQueue().add(
            'subject_1', 'message_1', 'addr1@mail.com', headers={'foo': 'bar'}
        )

        msg = models.MessagePending.objects.get()
        self.assertEqual(msg.content['headers'], {'foo': 'bar'})

    @staticmethod
    @mock.patch('datawarehouse.message_email.EmailMessage')
    def test_send(email_mock):
        """Test send method."""
        models.MessagePending.objects.create(
            kind=models.MessageKindEnum.EMAIL,
            content={
                'subject': 'subject_1',
                'body': 'message_1',
                'recipients': {
                    'to': ['addr1@mail.com', 'addr2@mail.com'],
                    'cc': ['addr3@mail.com'],
                    'bcc': ['addr4@mail.com'],
                },
                'headers': {'foo': 'bar'}
            }
        )
        models.MessagePending.objects.create(
            kind=models.MessageKindEnum.EMAIL,
            content={
                'subject': 'subject_2',
                'body': 'message_2',
                'recipients': {
                    'to': ['addr1@mail.com'],
                },
                'headers': {}
            }
        )

        message_email.EmailQueue().send()

        email_mock.assert_has_calls([
            mock.call(
                subject='subject_1',
                body='message_1',
                from_email=settings.DEFAULT_FROM_EMAIL,
                to=['addr1@mail.com', 'addr2@mail.com'],
                cc=['addr3@mail.com'],
                bcc=['addr4@mail.com'],
                headers={'foo': 'bar'}
            ),
            mock.call().send(),
            mock.call(
                subject='subject_2',
                body='message_2',
                from_email=settings.DEFAULT_FROM_EMAIL,
                to=['addr1@mail.com'],
                cc=[],
                bcc=[],
                headers={}
            ),
            mock.call().send(),
        ])
