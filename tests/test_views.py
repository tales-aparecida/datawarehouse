"""Test the views module."""

import datetime
from http import HTTPStatus
import threading
from unittest import mock

import dateutil
from django.db.models import F
from django.db.models import Max
from django.test import override_settings
from django.urls import reverse
from django.utils import timezone
from freezegun import freeze_time
from rest_framework import status

from datawarehouse import models
from datawarehouse.api.kcidb import serializers
from datawarehouse.utils import datetime_bool
from tests import utils


class TestConfidence(utils.TestCase):
    """Test for datawarehouse.views.confidence (without authorization asserts)."""

    fixtures = [
        'tests/fixtures/basic_policies.yaml',
        'tests/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_complete.yaml',
    ]

    DEFAULT_DAYS_AGO = "7"
    DEFAULT_SEARCH = None

    def test_confidence_invalid_group(self):
        """Assert the confidence view fails graciously."""
        group = 'invalid'
        url = reverse('views.confidence', args=[group])
        response = self.client.get(url)
        self.assertEqual(HTTPStatus.NOT_FOUND, response.status_code)

    @freeze_time("2020-06-10 16:00:00")
    def test_confidence_hosts(self):
        """Assert the view for confidence/hosts work as expected."""
        group = 'hosts'
        url = reverse('views.confidence', args=[group])

        with self.subTest("Using default params", days_ago=self.DEFAULT_DAYS_AGO, search=self.DEFAULT_SEARCH):
            response = self.client.get(url)
            expected_context = {
                'map': [{
                    'item': models.BeakerResource.objects.get(fqdn='foobar.redhat.com'),
                    'confidence': 0.0,
                    'results': {
                        'NEW': 0,
                        'FAIL': 0,
                        'ERROR': 1,
                        'MISS': 0,
                        'PASS': 0,
                        'DONE': 0,
                        'SKIP': 0,
                    },
                    'results_percent': {
                        'NEW': 0.0,
                        'FAIL': 0.0,
                        'ERROR': 100.0,
                        'MISS': 0.0,
                        'PASS': 0.0,
                        'DONE': 0.0,
                        'SKIP': 0.0,
                    },
                    'total': 1,
                }],
                'group': group,
                'since': self.DEFAULT_DAYS_AGO,
                'search': self.DEFAULT_SEARCH,
            }
            self.assertContextEqual(response.context, expected_context)

        days_ago = "ever"
        with self.subTest(days_ago=days_ago, search=self.DEFAULT_SEARCH):
            response = self.client.get(url + f'?days_ago={days_ago}')
            expected_context = {
                'map': [{
                    'item': models.BeakerResource.objects.get(fqdn='foobar.redhat.com'),
                    'confidence': 0.0,
                    'results': {
                        'NEW': 0,
                        'FAIL': 0,
                        'ERROR': 1,
                        'MISS': 0,
                        'PASS': 0,
                        'DONE': 0,
                        'SKIP': 0,
                    },
                    'results_percent': {
                        'NEW': 0.0,
                        'FAIL': 0.0,
                        'ERROR': 100.0,
                        'MISS': 0.0,
                        'PASS': 0.0,
                        'DONE': 0.0,
                        'SKIP': 0.0,
                    },
                    'total': 1,
                }, {
                    'item': models.BeakerResource.objects.get(fqdn='hostname.redhat.com'),
                    'confidence': 1.0,
                    'results': {
                        'NEW': 0,
                        'FAIL': 0,
                        'ERROR': 0,
                        'MISS': 0,
                        'PASS': 1,
                        'DONE': 0,
                        'SKIP': 0,
                    },
                    'results_percent': {
                        'NEW': 0.0,
                        'FAIL': 0.0,
                        'ERROR': 0.0,
                        'MISS': 0.0,
                        'PASS': 100.0,
                        'DONE': 0.0,
                        'SKIP': 0.0,
                    },
                    'total': 1,
                }],
                'group': group,
                'since': days_ago,
                'search': self.DEFAULT_SEARCH,
            }
            self.assertContextEqual(response.context, expected_context)

        search = "hostname"
        with self.subTest(days_ago=days_ago, search=search):
            response = self.client.get(url + f'?search={search}&days_ago={days_ago}')
            expected_context = {
                'map': [{
                    'item': models.BeakerResource.objects.get(fqdn='hostname.redhat.com'),
                    'confidence': 1.0,
                    'results': {
                        'NEW': 0,
                        'FAIL': 0,
                        'ERROR': 0,
                        'MISS': 0,
                        'PASS': 1,
                        'DONE': 0,
                        'SKIP': 0,
                    },
                    'results_percent': {
                        'NEW': 0.0,
                        'FAIL': 0.0,
                        'ERROR': 0.0,
                        'MISS': 0.0,
                        'PASS': 100.0,
                        'DONE': 0.0,
                        'SKIP': 0.0,
                    },
                    'total': 1,
                }],
                'group': group,
                'since': days_ago,
                'search': search,
            }
            self.assertContextEqual(response.context, expected_context)

    @freeze_time("2020-06-10 16:00:00")
    def test_confidence_tests(self):
        """Assert the view for confidence/tests work as expected."""
        group = 'tests'
        url = reverse('views.confidence', args=[group])

        with self.subTest("Using default params", days_ago=self.DEFAULT_DAYS_AGO, search=self.DEFAULT_SEARCH):
            response = self.client.get(url)
            expected_context = {
                'map': [{
                    'item': models.Test.objects.get(name='LTP Lite'),
                    'confidence': 0.0,
                    'results': {
                        'NEW': 0,
                        'FAIL': 0,
                        'ERROR': 1,
                        'MISS': 0,
                        'PASS': 0,
                        'DONE': 0,
                        'SKIP': 0,
                    },
                    'results_percent': {
                        'NEW': 0.0,
                        'FAIL': 0.0,
                        'ERROR': 100.0,
                        'MISS': 0.0,
                        'PASS': 0.0,
                        'DONE': 0.0,
                        'SKIP': 0.0,
                    },
                    'total': 1,
                }],
                'group': group,
                'since': self.DEFAULT_DAYS_AGO,
                'search': self.DEFAULT_SEARCH,
            }
            self.assertContextEqual(response.context, expected_context)

        days_ago = "ever"
        with self.subTest(days_ago=days_ago, search=self.DEFAULT_SEARCH):
            response = self.client.get(url + f'?days_ago={days_ago}')
            expected_context = {
                'map': [{
                    'item': models.Test.objects.get(name='LTP Lite'),
                    'confidence': 0.0,
                    'results': {
                        'NEW': 0,
                        'FAIL': 0,
                        'ERROR': 1,
                        'MISS': 0,
                        'PASS': 0,
                        'DONE': 0,
                        'SKIP': 0,
                    },
                    'results_percent': {
                        'NEW': 0.0,
                        'FAIL': 0.0,
                        'ERROR': 100.0,
                        'MISS': 0.0,
                        'PASS': 0.0,
                        'DONE': 0.0,
                        'SKIP': 0.0,
                    },
                    'total': 1,
                }, {
                    'item': models.Test.objects.get(name='Boot Test'),
                    'confidence': 1.0,
                    'results': {
                        'NEW': 0,
                        'FAIL': 0,
                        'ERROR': 0,
                        'MISS': 0,
                        'PASS': 1,
                        'DONE': 0,
                        'SKIP': 0,
                    },
                    'results_percent': {
                        'NEW': 0.0,
                        'FAIL': 0.0,
                        'ERROR': 0.0,
                        'MISS': 0.0,
                        'PASS': 100.0,
                        'DONE': 0.0,
                        'SKIP': 0.0,
                    },
                    'total': 1,
                }],
                'group': group,
                'since': days_ago,
                'search': self.DEFAULT_SEARCH,
            }
            self.assertContextEqual(response.context, expected_context)

        search = "boot"
        with self.subTest(days_ago=days_ago, search=search):
            response = self.client.get(url + f'?search={search}&days_ago={days_ago}')
            expected_context = {
                'map': [{
                    'item': models.Test.objects.get(name='Boot Test'),
                    'confidence': 1.0,
                    'results': {
                        'NEW': 0,
                        'FAIL': 0,
                        'ERROR': 0,
                        'MISS': 0,
                        'PASS': 1,
                        'DONE': 0,
                        'SKIP': 0,
                    },
                    'results_percent': {
                        'NEW': 0.0,
                        'FAIL': 0.0,
                        'ERROR': 0.0,
                        'MISS': 0.0,
                        'PASS': 100.0,
                        'DONE': 0.0,
                        'SKIP': 0.0,
                    },
                    'total': 1,
                }],
                'group': group,
                'since': days_ago,
                'search': search,
            }
            self.assertContextEqual(response.context, expected_context)


class ViewsTestCase(utils.TestCase):
    """Unit tests for the views module."""

    fixtures = (
        "tests/fixtures/basic.yaml",
        "tests/fixtures/policies_for_group_abc.yaml",
        "tests/fixtures/base_many_tests.yaml",
        "tests/fixtures/issues.yaml",
    )

    def setUp(self):
        """Set up tests."""
        self.hosts = models.BeakerResource.objects.all().order_by('id')
        self.tests = models.Test.objects.all().order_by('id')
        self.checkout = models.KCIDBCheckout.objects.get()
        self.test_runs = models.KCIDBTest.objects.filter(build__checkout=self.checkout)

    def test_details_test(self):
        """Test all elements from a certain test details query."""
        response = self.client.get('/details/test/1')
        self.assertContextEqual(response.context, {
            'item': self.tests[0],
            'type': 'test',
            'result_filter': None,
            'runs': [
                {'checkout': self.checkout,
                 'tests': self.test_runs.filter(test=self.tests[0])},
            ],
        })

    def test_details_test_another(self):
        """Test that getting a different test works."""
        response = self.client.get('/details/test/2')
        self.assertContextEqual(response.context, {
            'item': self.tests[1],
        })

    def test_details_tests_result(self):
        """Test filtering by results."""
        response = self.client.get('/details/test/1?result=PASS')
        self.assertContextEqual(response.context, {
            'runs': [
                {'checkout': self.checkout,
                 'tests': self.test_runs.filter(test=self.tests[0], status=models.ResultEnum.PASS)},
            ],
        })

        response = self.client.get('/details/test/1?result=FAIL')
        self.assertContextEqual(response.context, {
            'runs': [
                {'checkout': self.checkout,
                 'tests': self.test_runs.filter(test=self.tests[0], status=models.ResultEnum.FAIL)},
            ],
        })

    def test_details_tests_result_issues_false(self):
        """Test filtering by results and issues tagged."""
        kcidb_test = self.test_runs.filter(test=self.tests[0], status=models.ResultEnum.FAIL).first()

        response = self.client.get('/details/test/1?result=FAIL&issues_tagged=False')
        self.assertContextEqual(response.context, {
            'runs': [
                {'checkout': self.checkout,
                 'tests': [kcidb_test]},
            ],
        })

        models.IssueOccurrence.objects.create(
            issue=models.Issue.objects.last(),
            kcidb_test=kcidb_test
        )

        response = self.client.get('/details/test/1?result=FAIL&issues_tagged=False')
        self.assertContextEqual(response.context, {
            'runs': [],
        })

    def test_details_tests_result_issues_true(self):
        """Test filtering by results and issues tagged."""
        kcidb_test = self.test_runs.filter(test=self.tests[0], status=models.ResultEnum.FAIL).first()

        response = self.client.get('/details/test/1?result=FAIL&issues_tagged=True')
        self.assertContextEqual(response.context, {
            'runs': [],
        })

        models.IssueOccurrence.objects.create(
            issue=models.Issue.objects.last(),
            kcidb_test=kcidb_test
        )

        response = self.client.get('/details/test/1?result=FAIL&issues_tagged=True')
        self.assertContextEqual(response.context, {
            'runs': [
                {'checkout': self.checkout,
                 'tests': [kcidb_test]},
            ],
        })

    def test_details_tests_architecture(self):
        """Test filtering by architecture."""
        response = self.client.get('/details/test/1?arch=aarch64')
        self.assertContextEqual(response.context, {
            'runs': [],
        })

        models.KCIDBBuild.objects.update(architecture=models.ArchitectureEnum.aarch64)

        response = self.client.get('/details/test/1?arch=aarch64')
        self.assertContextEqual(response.context, {
            'runs': [
                {'checkout': self.checkout,
                 'tests': models.KCIDBTest.objects.filter(
                     build__checkout=self.checkout,
                     test__id=1)
                 },
            ],
        })

    def test_details_tests_tree(self):
        """Test filtering by tree."""
        response = self.client.get('/details/test/1?tree=Tree 2')
        self.assertContextEqual(response.context, {
            'runs': [],
        })

        response = self.client.get('/details/test/1?tree=Tree')
        self.assertContextEqual(response.context, {
            'runs': [
                {'checkout': self.checkout,
                 'tests': models.KCIDBTest.objects.filter(
                     build__checkout=self.checkout,
                     test__id=1)
                 },
            ],
        })

    def test_details_tests_table(self):
        """Test returned table."""
        response = self.client.get('/details/test/2')
        self.assertContextEqual(response.context, {
            'table': self.hosts.filter(kcidbtest__test__id=2).distinct()
        })

        self.assertEqual(response.context['table'][0].total_runs, 3)
        self.assertEqual(response.context['table'][0].FAIL, 1)
        self.assertEqual(response.context['table'][0].PASS, 1)
        self.assertEqual(response.context['table'][0].ERROR, 1)

    def test_details_tests_table_result(self):
        """Test returned table when filtering by result."""
        response = self.client.get('/details/test/1?result=PASS')
        self.assertContextEqual(response.context, {
            'table': self.hosts.filter(
                kcidbtest__test__id=1,
                kcidbtest__status=models.ResultEnum.PASS
            ).distinct()
        })

        response = self.client.get('/details/test/1?result=FAIL')
        self.assertContextEqual(response.context, {
            'table': self.hosts.filter(
                kcidbtest__test__id=1,
                kcidbtest__status=models.ResultEnum.FAIL
            ).distinct()
        })

    def test_details_tests_table_arch(self):
        """Test returned table when filtering by architecture."""
        response = self.client.get('/details/test/1?arch=aarch64')
        self.assertContextEqual(response.context, {
            'table': [],
        })

        models.KCIDBBuild.objects.update(architecture=models.ArchitectureEnum['aarch64'])

        response = self.client.get('/details/test/1?arch=aarch64')
        self.assertContextEqual(response.context, {
            'table': self.hosts.filter(
                kcidbtest__test__id=1,
                kcidbtest__build__architecture=models.ArchitectureEnum.aarch64
            ).distinct()
        })

    def test_details_tests_table_tree(self):
        """Test returned table when filtering by tree."""
        response = self.client.get('/details/test/1?tree=Tree 2')
        self.assertContextEqual(response.context, {
            'table': [],
        })

        response = self.client.get('/details/test/1?tree=Tree')
        self.assertContextEqual(response.context, {
            'table': self.hosts.filter(
                kcidbtest__test__id=1,
            ).distinct()
        })

    def test_details_host(self):
        """Test all elements from a certain host details query."""
        response = self.client.get('/details/host/1')
        self.assertContextEqual(response.context, {
            'item': self.hosts[0],
            'type': 'host',
            'result_filter': None,
            'runs': [
                {'checkout': self.checkout,
                 'tests': self.test_runs.filter(environment=self.hosts[0])},
            ],
        })

    def test_details_host_another(self):
        """Test that getting a different host works."""
        response = self.client.get('/details/host/2')
        self.assertContextEqual(response.context, {
            'item': self.hosts[1],
        })

    def test_details_hosts_result(self):
        """Test filtering by results."""
        response = self.client.get('/details/host/1?result=FAIL')
        self.assertContextEqual(response.context, {
            'runs': [
                {'checkout': self.checkout,
                 'tests': self.test_runs.filter(environment=self.hosts[0], status=models.ResultEnum.FAIL)},
            ],
        })

        response = self.client.get('/details/host/1?result=PASS')
        self.assertContextEqual(response.context, {
            'runs': [
                {'checkout': self.checkout,
                 'tests': self.test_runs.filter(environment=self.hosts[0], status=models.ResultEnum.PASS)},
            ],
        })

    def test_details_hosts_table(self):
        """Test returned table."""
        response = self.client.get('/details/host/1')
        self.assertContextEqual(response.context, {
            'table': self.tests.filter(kcidbtest__environment__id=1).distinct()
        })

        self.assertEqual(response.context['table'][0].total_runs, 3)
        self.assertEqual(response.context['table'][0].FAIL, 1)
        self.assertEqual(response.context['table'][0].PASS, 1)
        self.assertEqual(response.context['table'][0].ERROR, 1)

    def test_details_hosts_table_result(self):
        """Test returned table when filtering by result."""
        response = self.client.get('/details/host/1?result=FAIL')
        self.assertContextEqual(response.context, {
            'table': self.tests.filter(
                kcidbtest__environment__id=1,
                kcidbtest__status=models.ResultEnum.FAIL
            ).distinct()
        })

        response = self.client.get('/details/host/1?result=PASS')
        self.assertContextEqual(response.context, {
            'table': self.tests.filter(
                kcidbtest__environment__id=1,
                kcidbtest__status=models.ResultEnum.PASS
            ).distinct()
        })


class TestIssueCreateEditAnonymous(utils.KCIDBTestCase):
    """Issue new and edit test case. Anonymous."""

    fixtures = [
        'tests/fixtures/policies_for_group_abc.yaml',
        'tests/fixtures/basic.yaml',
        'tests/fixtures/issues.yaml',
    ]

    anonymous = True
    groups = []

    def _ensure_test_conditions(self, method):
        """Check that all conditions for testing are optimal."""
        auth_policies = models.Policy.objects.filter(id__in=self.policies_authorized[method])
        no_auth_policies = models.Policy.objects.exclude(id__in=self.policies_authorized[method])

        checks = [
            (auth_policies, 'No authorized policies'),
            (no_auth_policies, 'No unauthorized policies'),
        ]

        for check, message in checks:
            self.assertTrue(check.exists(), f'Test conditions unmet: {message} for "{method}"')

    def test_issue_new(self):
        """Test creating new issues."""
        new_issue = reverse('views.issue.new_or_edit')
        self._ensure_test_conditions('write')
        authorized_policies = models.Policy.objects.filter(
            id__in=self.policies_authorized['write']
        )
        created_at = datetime.datetime(2010, 1, 2, 9, 1, tzinfo=dateutil.tz.UTC)
        issue_kind = models.IssueKind.objects.first()
        public = models.Policy.objects.get(name="public")
        common_args = {"description": "foo bar", "ticket_url": "http://some.url"}

        with self.subTest("Assert that missing kind_id is always invalid"):
            missing_kind = (new_issue, {**common_args, "policy_id": public.id})
            missing_kind_response = self.assert_authenticated_post(
                HTTPStatus.BAD_REQUEST, "add_issue", *missing_kind, user=self.user
            )
            self.assertEqual(missing_kind_response.content, b"Missing IssueKind ID (`kind_id`)")

        with self.subTest("Assert that missing policy_id is always invalid"):
            missing_policy = (new_issue, {**common_args, "kind_id": issue_kind.id})
            missing_policy_response = self.assert_authenticated_post(
                HTTPStatus.BAD_REQUEST, "add_issue", *missing_policy, user=self.user
            )
            self.assertEqual(missing_policy_response.content, b"Missing Policy ID (`policy_id`)")

        with self.subTest("Assert that inexistent policy_id is always invalid"):
            invalid_policy = (new_issue, {**common_args, "kind_id": issue_kind.id, "policy_id": 99})
            invalid_policy_response = self.assert_authenticated_post(
                HTTPStatus.BAD_REQUEST, "add_issue", *invalid_policy, user=self.user
            )
            self.assertEqual(invalid_policy_response.content, b"Couldn't find a matching Policy")

        with self.subTest("Assert that inexistent kind_id is always invalid"):
            invalid_kind = (new_issue, {**common_args, "kind_id": 123, "policy_id": public.id})
            invalid_kind_response = self.assert_authenticated_post(
                HTTPStatus.BAD_REQUEST, "add_issue", *invalid_kind, user=self.user
            )
            self.assertEqual(invalid_kind_response.content, b"Couldn't find a matching IssueKind")

        for policy in models.Policy.objects.all():
            authorized = policy in authorized_policies and not self.anonymous

            post_args = (new_issue, {**common_args, "kind_id": issue_kind.id, "policy_id": policy.id})

            status_code = 302 if authorized else 403

            with freeze_time(created_at):
                response = self.assert_authenticated_post(status_code, 'add_issue', *post_args, user=self.user)

            if not authorized:
                continue

            # Check that it was created ok.
            issue = models.Issue.objects.get(ticket_url='http://some.url')
            self.assertEqual('foo bar', issue.description)
            self.assertEqual(issue_kind, issue.kind)
            self.assertEqual(policy, issue.policy)
            self.assertFalse(issue.policy_auto_public)
            self.assertEqual(self.user, issue.created_by)
            self.assertEqual(created_at, issue.created_at)
            self.assertEqual(self.user, issue.last_edited_by)
            self.assertEqual(created_at, issue.last_edited_at)

            # Same ticket_url. Fail.
            post_args = (new_issue, {'description': 'bar bar', 'ticket_url': 'http://some.url',
                                     'kind_id': issue_kind.id, 'policy_id': policy.id})
            response = self.assert_authenticated_post(400, 'add_issue', *post_args, user=self.user)
            self.assertEqual(b'Issue already exists with ticket URL http://some.url', response.content)

            # Another ticket_url. Ok
            post_args = (new_issue, {'description': 'bar bar', 'ticket_url': 'http://other.url',
                                     'kind_id': issue_kind.id, 'policy_id': policy.id})
            response = self.assert_authenticated_post(302, 'add_issue', *post_args, user=self.user)

            # Cleanup
            models.Issue.objects.all().delete()

        if not self.anonymous:
            with self.subTest("Test redirect_to works as expected by IssueRegex view"):
                new_regex = reverse("views.issue_regex.form")
                post_args = (new_issue, {
                    'description': 'bar bar', 'ticket_url': 'http://barbie.url',
                    'kind_id': issue_kind.id, 'policy_id': public.id, 'redirect_to': new_regex})
                response = self.assert_authenticated_post(HTTPStatus.FOUND, 'add_issue', *post_args, user=self.user)
                issue_id = models.Issue.objects.get(ticket_url='http://barbie.url').id
                self.assertEqual(response.url, new_regex + f'?issue_id={issue_id}')

            with self.subTest("Test redirect_to works as expected by IssueRegex view, even if using querystring"):
                new_regex += "?something=1"
                post_args = (new_issue, {
                    'description': 'bar bar', 'ticket_url': 'http://polly.url',
                    'kind_id': issue_kind.id, 'policy_id': public.id, 'redirect_to': new_regex})
                response = self.assert_authenticated_post(HTTPStatus.FOUND, 'add_issue', *post_args, user=self.user)
                issue_id = models.Issue.objects.get(ticket_url='http://polly.url').id
                self.assertEqual(response.url, new_regex + f'&issue_id={issue_id}')

    def test_issue_whitespace_removal(self):
        """Test issues handle trailing spaces on description and ticket_url."""
        policy = models.Policy.objects.get(name='public')
        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1")
        post_args = ('/issue', {'description': '   foo bar ', 'ticket_url': ' http://some.url ',
                                'kind_id': issue_kind.id, 'policy_id': policy.id})
        self.assert_authenticated_post(302, 'add_issue', *post_args)

        issue = models.Issue.objects.get(ticket_url='http://some.url')
        self.assertEqual(issue.description, 'foo bar')
        self.assertEqual(issue.ticket_url, 'http://some.url')

        # Change the values.
        post_args[1]['issue_id'] = issue.id
        post_args[1]['description'] = ' foo foo    '
        post_args[1]['ticket_url'] = ' http://other.url '
        self.assert_authenticated_post(302, 'change_issue', *post_args)

        # Check new values.
        issue = models.Issue.objects.get(id=issue.id)
        self.assertEqual(issue.description, 'foo foo')
        self.assertEqual(issue.ticket_url, 'http://other.url')

    def test_issue_new_policy_auto_public(self):
        """Test setting policy_auto_public flag on issue creation."""
        policy = models.Policy.objects.get(name='public')
        issue_kind = models.IssueKind.objects.create(description="Kernel bug", tag="kb")
        post_args = ('/issue', {'description': 'foo bar', 'ticket_url': 'http://some.url',
                                'kind_id': issue_kind.id,
                                'policy_id': policy.id,
                                'policy_auto_public': 'on'})
        self.assert_authenticated_post(302, 'add_issue', *post_args)

        # Check that the origin tree is correct
        issue = models.Issue.objects.get(ticket_url='http://some.url')
        self.assertTrue(issue.policy_auto_public)

    def test_issue_edit(self):
        """Test editing issues."""
        self._ensure_test_conditions('write')
        authorized_policies = models.Policy.objects.filter(
            id__in=self.policies_authorized['write']
        )
        edited_at = datetime.datetime(2010, 1, 2, 9, 1, tzinfo=dateutil.tz.UTC)

        for policy in models.Policy.objects.all():
            authorized = policy in authorized_policies and not self.anonymous

            policy_2 = models.Policy.objects.create(
                name='another_ate',
                read_group=policy.read_group,
                write_group=policy.write_group
            )
            issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1")
            issue_kind_2 = models.IssueKind.objects.create(description="fail 2", tag="2")
            post_args = ('/issue', {'description': 'foo bar', 'ticket_url': 'http://some.url',
                                    'kind_id': issue_kind.id, 'policy_id': policy.id})

            status_code = 302 if authorized else 403
            self.assert_authenticated_post(status_code, 'add_issue', *post_args, user=self.user)

            if not authorized:
                policy_2.delete()
                continue

            # Change the values.
            issue = models.Issue.objects.get(ticket_url='http://some.url')
            post_args = ('/issue', {'issue_id': issue.id, 'description': 'var var',
                                    'ticket_url': 'http://other.url', 'kind_id': issue_kind_2.id,
                                    'policy_id': policy_2.id})
            with freeze_time(edited_at):
                self.assert_authenticated_post(302, 'change_issue', *post_args, user=self.user)

            # Check new values.
            issue = models.Issue.objects.get(id=issue.id)
            self.assertEqual(issue.description, 'var var')
            self.assertEqual(issue.ticket_url, 'http://other.url')
            self.assertEqual(issue.kind, issue_kind_2)
            self.assertEqual(issue.policy, policy_2)
            self.assertEqual(issue.last_edited_by, self.user)
            self.assertEqual(issue.last_edited_at, edited_at)

            # Cleanup
            issue.delete()
            policy_2.delete()

    def test_issue_edit_url_check(self):
        """Test when editing issues it's not possible to duplicate the URL."""
        policy = models.Policy.objects.get(name='public')
        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1")
        self.assert_authenticated_post(
            302, 'add_issue', '/issue', {'description': 'foo bar',
                                         'ticket_url': 'http://some.url', 'kind_id': issue_kind.id,
                                         'policy_id': policy.id})

        self.assert_authenticated_post(
            302, 'add_issue', '/issue', {'description': 'foo bar', 'ticket_url': 'http://other.url',
                                         'kind_id': issue_kind.id, 'policy_id': policy.id})

        # Change the values. The url is already used by the second issue.
        issue = models.Issue.objects.get(ticket_url='http://some.url')
        response = self.assert_authenticated_post(
            400, 'change_issue', '/issue', {'issue_id': issue.id, 'description': 'foo bar',
                                            'ticket_url': 'http://other.url', 'kind_id': issue_kind.id,
                                            'policy_id': policy.id})
        self.assertEqual(b'Issue already exists with ticket URL http://other.url', response.content)

    def test_issue_edit_policy_auto_public(self):
        """Test setting policy_auto_public flag on issue edit."""
        policy = models.Policy.objects.get(name='public')
        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1", kernel_code_related=False)
        post_args = ('/issue', {'description': 'foo bar', 'ticket_url': 'http://some.url',
                                'kind_id': issue_kind.id, 'policy_id': policy.id})
        self.assert_authenticated_post(302, 'add_issue', *post_args)

        issue = models.Issue.objects.get(ticket_url='http://some.url')
        self.assertFalse(issue.policy_auto_public)

        # Change the values.
        post_args = ('/issue', {'issue_id': issue.id, 'description': 'foo bar',
                                'ticket_url': 'http://some.url',
                                'kind_id': issue_kind.id,
                                'policy_id': policy.id,
                                'policy_auto_public': 'on'})
        self.assert_authenticated_post(302, 'change_issue', *post_args)

        # Check new values.
        issue = models.Issue.objects.get(id=issue.id)
        self.assertTrue(issue.policy_auto_public)


class TestIssueCreateEditReadGroup(TestIssueCreateEditAnonymous):
    """Issue new and edit test case. Read group."""

    anonymous = False
    groups = ['group_a']


class TestIssueCreateEditWriteGroup(TestIssueCreateEditAnonymous):
    """Issue new and edit test case. Write group."""

    anonymous = False
    groups = ['group_b']


class TestIssueCreateEditAllGroups(TestIssueCreateEditAnonymous):
    """Issue new and edit test case. All groups."""

    anonymous = False
    groups = ['group_a', 'group_b']


class TestIssueRegexAnonymous(utils.KCIDBTestCase):
    """Issue Regex test case. Anonymous."""

    fixtures = [
        'tests/fixtures/policies_for_group_abc.yaml',
        'tests/fixtures/basic.yaml',
        'tests/kcidb/fixtures/multiple_build_package_names.yaml',
        'tests/fixtures/issues.yaml',
    ]

    anonymous = True
    groups = []

    def _ensure_test_conditions(self, method):
        """Check that all conditions for testing are optimal."""
        auth_issues = models.Issue.objects.filter(id__in=self.issues_authorized[method])
        no_auth_issues = models.Issue.objects.exclude(id__in=self.issues_authorized[method])

        checks = [
            (auth_issues, 'No authorized issues'),
            (no_auth_issues, 'No unauthorized issues'),
        ]

        for check, message in checks:
            self.assertTrue(check.exists(), f'Test conditions unmet: {message} for "{method}"')

    def test_new(self):
        """Test issue_regex_new_or_edit GET response."""
        self._ensure_test_conditions('read')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['read']
        )

        response = self.client.get('/issue/-/regex')

        self.assertContextEqual(
            response.context,
            {
                'issues': authorized_issues.filter(resolved_at__isnull=True),
                'package_names': ['kernel', 'kernel-automotive', 'kernel-rt'],
            },
        )

    def test_create(self):
        """Test create IssueRegex."""
        self._ensure_test_conditions('write')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['write']
        )
        created_at = datetime.datetime(2010, 1, 2, 9, 1, tzinfo=dateutil.tz.UTC)

        for issue in models.Issue.objects.all():
            authorized = issue in authorized_issues and not self.anonymous

            post_args = (
                '/issue/-/regex',
                {
                    'action': 'new',
                    'issue_id_select': issue.id,
                    'text_match': ' foo',
                    'file_name_match': ' bar',
                    'test_name_match': ' foobar ',
                    'testresult_name_match': ' club ',
                    'tree_match': ' rhel8 ',
                    'kpet_tree_name_match': ' rhel86-z ',
                    'selected_architectures[]': ['ppc64', 'x86_64'],
                    'selected_package_names[]': ['kernel-baz', 'kernel'],
                }
            )

            status_code = HTTPStatus.FOUND if authorized else HTTPStatus.NOT_FOUND
            with freeze_time(created_at):
                self.assert_authenticated_post(status_code, 'add_issueregex', *post_args, user=self.user)

            if not authorized:
                continue

            issue_regex = models.IssueRegex.objects.get(issue=issue)
            self.assertEqual('foo', issue_regex.text_match)
            self.assertEqual('bar', issue_regex.file_name_match)
            self.assertEqual('foobar', issue_regex.test_name_match)
            self.assertEqual('club', issue_regex.testresult_name_match)
            self.assertEqual('rhel8', issue_regex.tree_match)
            self.assertEqual('rhel86-z', issue_regex.kpet_tree_name_match)
            self.assertEqual('ppc64|x86_64', issue_regex.architecture_match)
            self.assertEqual('kernel|kernel-baz', issue_regex.package_name_match)
            self.assertEqual(issue, issue_regex.issue)

            self.assertEqual(self.user, issue_regex.created_by)
            self.assertEqual(created_at, issue_regex.created_at)
            self.assertEqual(self.user, issue_regex.last_edited_by)
            self.assertEqual(created_at, issue_regex.last_edited_at)

    def test_create_incomplete(self):
        """Test create IssueRegex without the required fields."""
        self._ensure_test_conditions('write')

        for issue in models.Issue.objects.all():
            post_args = (
                '/issue/-/regex',
                {
                    'action': 'new',
                    'issue_id_select': issue.id,
                    'text_match': ' foo',
                }
            )

            status_code = HTTPStatus.BAD_REQUEST
            self.assert_authenticated_post(status_code, 'add_issueregex', *post_args, user=self.user)

            self.assertFalse(models.IssueRegex.objects.filter(issue=issue).exists())

    def test_create_minimal(self):
        """Test create IssueRegex without all the fields."""
        self._ensure_test_conditions('write')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['write']
        )

        for issue in models.Issue.objects.all():
            authorized = issue in authorized_issues and not self.anonymous

            post_args = (
                '/issue/-/regex',
                {
                    'action': 'new',
                    'issue_id_select': issue.id,
                    'text_match': ' foo',
                    'file_name_match': '.*',
                    'test_name_match': '.* ',
                    'testresult_name_match': '.* ',
                }
            )

            status_code = HTTPStatus.FOUND if authorized else HTTPStatus.NOT_FOUND
            self.assert_authenticated_post(status_code, 'add_issueregex', *post_args, user=self.user)

            if not authorized:
                continue

            issue_regex = models.IssueRegex.objects.get(issue=issue)
            self.assertEqual('foo', issue_regex.text_match)
            self.assertEqual('.*', issue_regex.file_name_match)
            self.assertEqual('.*', issue_regex.test_name_match)
            self.assertEqual('.*', issue_regex.testresult_name_match)
            self.assertEqual(None, issue_regex.architecture_match)
            self.assertEqual(None, issue_regex.tree_match)
            self.assertEqual(None, issue_regex.kpet_tree_name_match)
            self.assertEqual(None, issue_regex.package_name_match)
            self.assertEqual(issue, issue_regex.issue)

    def test_edit(self):
        """Test edit IssueRegex."""
        self._ensure_test_conditions('write')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['write']
        )
        edited_at = datetime.datetime(2010, 1, 2, 9, 1, tzinfo=dateutil.tz.UTC)

        for issue in models.Issue.objects.all():
            authorized = issue in authorized_issues and not self.anonymous

            issue_regex = models.IssueRegex.objects.create(
                issue=issue,
                text_match="foo",
                created_by=None,  # this could happen if the user gets deleted
                last_edited_by=None,
            )
            issue_2 = models.Issue.objects.create(
                description='foo foo',
                kind=issue.kind,
                ticket_url=f'http://some.other.url.{issue.id}',
                policy=issue.policy,
            )

            post_args = (
                '/issue/-/regex',
                {
                    'action': 'edit',
                    'issue_regex_id': issue_regex.id,
                    'issue_id_select': issue_2.id,
                    'text_match': 'barbar ',
                    'file_name_match': ' bar',
                    'test_name_match': ' foobar',
                    'testresult_name_match': ' club ',
                    'tree_match': ' rhel8',
                    'kpet_tree_name_match': ' rhel86-z',
                    'selected_architectures[]': ['ppc64', 'x86_64'],
                    'selected_package_names[]': ['kernel-baz', 'kernel'],
                }
            )

            status_code = HTTPStatus.FOUND if authorized else HTTPStatus.NOT_FOUND
            with freeze_time(edited_at):
                self.assert_authenticated_post(status_code, 'change_issueregex', *post_args, user=self.user)

            if not authorized:
                continue

            issue_regex = models.IssueRegex.objects.get(id=issue_regex.id)
            self.assertEqual('barbar', issue_regex.text_match)
            self.assertEqual('bar', issue_regex.file_name_match)
            self.assertEqual('foobar', issue_regex.test_name_match)
            self.assertEqual('club', issue_regex.testresult_name_match)
            self.assertEqual('rhel8', issue_regex.tree_match)
            self.assertEqual('rhel86-z', issue_regex.kpet_tree_name_match)
            self.assertEqual('ppc64|x86_64', issue_regex.architecture_match)
            self.assertEqual('kernel|kernel-baz', issue_regex.package_name_match)
            self.assertEqual(issue_2, issue_regex.issue)

            self.assertEqual(self.user, issue_regex.last_edited_by)
            self.assertEqual(edited_at, issue_regex.last_edited_at)
            self.assertEqual(None, issue_regex.created_by)

    def test_edit_incomplete(self):
        """Test edit IssueRegex without all required fields."""
        self._ensure_test_conditions('write')
        issue = models.Issue.objects.filter(id__in=self.issues_authorized["write"]).first()
        authorized = not self.anonymous

        issue_regex = models.IssueRegex.objects.create(
            issue=issue,
            text_match=".*text_match.*",
            file_name_match=".*file_name_match.*",
            test_name_match=".*test_name_match.*",
            testresult_name_match=".*testresult_name_match.*",
        )
        post_args = (
            "/issue/-/regex",
            {
                "action": "edit",
                "issue_regex_id": issue_regex.id,
                "issue_id_select": issue.id,
                "text_match": "new pattern",
            },
        )

        status_code = HTTPStatus.BAD_REQUEST if authorized else HTTPStatus.FORBIDDEN
        response = self.assert_authenticated_post(status_code, "change_issueregex", *post_args, user=self.user)

        if authorized:
            expected_error = [
                "Please fill out these fields: ['File Name Match', 'Test Name Match', 'Subtest Name Match']"
            ]
            self.assertContextEqual(response.context, {"error_message": expected_error})

        issue_regex.refresh_from_db()
        self.assertEqual(issue_regex.text_match, ".*text_match.*", "Expects text_match to remain unchanged")

    def test_create_invalid_regex(self):
        """Test create IssueRegex with invalid regex patterns."""
        self._ensure_test_conditions("write")

        issue = models.Issue.objects.filter(id__in=self.issues_authorized["write"]).first()
        authorized = not self.anonymous

        invalid_regex_pattern = "*"  # Invalid regex pattern that raises re.error exception
        field_names = [
            "text_match",
            "file_name_match",
            "test_name_match",
            "testresult_name_match",
            "tree_match",
            "kpet_tree_name_match",
        ]
        for field_name in field_names:
            with self.subTest(field_name=field_name):
                post_args = (
                    "/issue/-/regex",
                    {
                        "action": "new",
                        "issue_id_select": issue.id,
                        # Required regex fields
                        "text_match": ".*",
                        "file_name_match": ".*",
                        "test_name_match": ".*",
                        "testresult_name_match": ".*",
                        # Define or overwrite an invalid regex field
                        field_name: invalid_regex_pattern,
                    },
                )
                status_code = HTTPStatus.BAD_REQUEST if authorized else HTTPStatus.FORBIDDEN
                response = self.assert_authenticated_post(status_code, "add_issueregex", *post_args, user=self.user)

                if authorized:
                    expected_error = [f"Invalid regex in field {field_name}: {invalid_regex_pattern!r}"]
                    self.assertContextEqual(response.context, {"error_message": expected_error})

                # Ensure that the record was not created
                self.assertFalse(models.IssueRegex.objects.filter(issue=issue).exists())

    def test_edit_invalid_regex(self):
        """Test edit IssueRegex with invalid regex patterns."""
        self._ensure_test_conditions("write")

        issue = models.Issue.objects.filter(id__in=self.issues_authorized["write"]).first()
        authorized = not self.anonymous

        # Create regex with a regex with valid patterns
        issue_regex = models.IssueRegex.objects.create(
            issue=issue,
            text_match=".*text_match.*",
            file_name_match=".*file_name_match.*",
            test_name_match=".*test_name_match.*",
            testresult_name_match=".*testresult_name_match.*",
            tree_match=".*tree_match.*",
            kpet_tree_name_match=".*kpet_tree_name_match.*",
        )

        invalid_regex_pattern = "*"  # Invalid regex pattern that raises re.error exception
        field_names = [
            "text_match",
            "file_name_match",
            "test_name_match",
            "testresult_name_match",
            "tree_match",
            "kpet_tree_name_match",
        ]
        for field_name in field_names:
            with self.subTest(field_name=field_name):
                post_args = (
                    "/issue/-/regex",
                    {
                        "action": "edit",
                        "issue_regex_id": issue_regex.id,
                        "issue_id_select": issue.id,
                        # Required regex fields
                        "text_match": ".*",
                        "file_name_match": ".*",
                        "test_name_match": ".*",
                        "testresult_name_match": ".*",
                        # Define or overwrite an invalid regex field
                        field_name: invalid_regex_pattern,
                    },
                )
                status_code = HTTPStatus.BAD_REQUEST if authorized else HTTPStatus.FORBIDDEN
                response = self.assert_authenticated_post(status_code, "change_issueregex", *post_args, user=self.user)

                if authorized:
                    expected_error = [f"Invalid regex in field {field_name}: {invalid_regex_pattern!r}"]
                    self.assertContextEqual(response.context, {"error_message": expected_error})

                # Ensure that the regex pattern in the database remains unchanged due to the validation failure
                issue_regex.refresh_from_db()
                self.assertEqual(
                    getattr(issue_regex, field_name), f".*{field_name}.*", f"Expects {field_name} to remain unchanged"
                )

    def test_edit_minimal(self):
        """Test edit IssueRegex without all fields."""
        self._ensure_test_conditions('write')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['write']
        )

        for issue in models.Issue.objects.all():
            authorized = issue in authorized_issues and not self.anonymous

            issue_regex = models.IssueRegex.objects.create(
                issue=issue,
                text_match='foo',
            )

            post_args = (
                '/issue/-/regex',
                {
                    'action': 'edit',
                    'issue_regex_id': issue_regex.id,
                    'issue_id_select': issue.id,
                    'text_match': 'barbar ',
                    'file_name_match': '.*',
                    'test_name_match': '.* ',
                    'testresult_name_match': '.* ',
                }
            )

            status_code = HTTPStatus.FOUND if authorized else HTTPStatus.NOT_FOUND
            self.assert_authenticated_post(status_code, 'change_issueregex', *post_args, user=self.user)

            if not authorized:
                continue

            issue_regex = models.IssueRegex.objects.get(id=issue_regex.id)
            self.assertEqual('barbar', issue_regex.text_match)
            self.assertEqual('.*', issue_regex.file_name_match)
            self.assertEqual('.*', issue_regex.test_name_match)
            self.assertEqual('.*', issue_regex.testresult_name_match)
            self.assertEqual(None, issue_regex.architecture_match)
            self.assertEqual(None, issue_regex.tree_match)
            self.assertEqual(None, issue_regex.kpet_tree_name_match)
            self.assertEqual(None, issue_regex.package_name_match)

    def test_delete(self):
        """Test delete IssueRegex."""
        self._ensure_test_conditions('write')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['write']
        )

        for issue in models.Issue.objects.all():
            authorized = issue in authorized_issues and not self.anonymous

            issue_regex = models.IssueRegex.objects.create(
                issue=issue,
                text_match='foo',
            )
            post_args = (
                '/issue/-/regex',
                {
                    'action': 'delete',
                    'issue_regex_id': issue_regex.id,
                }
            )

            status_code = 302 if authorized else 404
            self.assert_authenticated_post(status_code, 'delete_issueregex', *post_args, user=self.user)

            self.assertEqual(
                not authorized,  # If authorized, the model doesnt exists anymore
                models.IssueRegex.objects.filter(id=issue_regex.id).exists()
            )

    def test_issue_regex_new_or_edit_not_handled_action(self):
        """Test not handled actions are rejected."""
        post_args = (
            '/issue/-/regex',
            {
                'action': 'foobar',
            }
        )

        self.assert_authenticated_post(400, 'delete_issueregex', *post_args, user=self.user)

    def test_issue_regex_new_or_edit_not_handled_methods(self):
        """Test not handled methods are rejected."""
        allowed_methods = ['post', 'get']
        url = '/issue/-/regex'

        for method in utils.ALL_METHODS:
            if method in allowed_methods:
                # Skip allowed ones
                continue

            self.assertEqual(
                getattr(self.client, method)(url).status_code,
                405
            )

    def test_get_single(self):
        """Test get IssueRegex."""
        self._ensure_test_conditions('read')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['read']
        )

        for issue in models.Issue.objects.all():
            issue_regex = models.IssueRegex.objects.create(
                issue=issue,
                text_match='foo',
                package_name_match='kernel|kernel-rt',
                architecture_match='ppc64|x86_64',
            )

            response = self.client.get(f'/issue/-/regex/{issue_regex.id}')

            if issue not in authorized_issues:
                self.assertEqual(404, response.status_code)
                continue

            self.assertContextEqual(
                response.context,
                {
                    'issue_regex': issue_regex,
                    'issues': authorized_issues,
                    'package_names': ['kernel', 'kernel-automotive', 'kernel-rt'],
                    'selected_package_names': ['kernel', 'kernel-rt'],
                    'selected_architectures': ['ppc64', 'x86_64'],
                },
            )

    def test_get_single_resolved(self):
        """Test get IssueRegex for resolved issues."""
        self._ensure_test_conditions('read')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['read']
        )

        for issue in models.Issue.objects.all():
            issue_regex = models.IssueRegex.objects.create(
                issue=issue,
                text_match='foo',
            )

            # Resolve the issue related to the regex.
            issue.resolved_at = datetime_bool(True)
            issue.save()

            response = self.client.get(f'/issue/-/regex/{issue_regex.id}')

            if issue not in authorized_issues:
                self.assertEqual(404, response.status_code)
                continue

            self.assertContextEqual(
                response.context,
                {
                    'issue_regex': issue_regex,
                    # The issue is in authorized_issues, must be included on the context.
                    'issues': authorized_issues,
                },
            )

            # Restore to non resolved
            issue.resolved_at = None
            issue.save()

    def test_get_list(self):
        """Test get all IssueRegex."""
        self._ensure_test_conditions('read')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['read']
        )

        for issue in models.Issue.objects.all():
            models.IssueRegex.objects.create(
                issue=issue,
                text_match='foo',
            )
        response = self.client.get('/issue/-/regex/list')

        self.assertContextEqual(
            response.context,
            {
                'issue_regexes': models.IssueRegex.objects.filter(issue__in=authorized_issues),
                'issues': authorized_issues,
            },
        )

    def test_get_list_not_handled_methods(self):
        """Test not handled methods are rejected."""
        allowed_methods = ['get']
        url = '/issue/-/regex/list'

        for method in utils.ALL_METHODS:
            if method in allowed_methods:
                # Skip allowed ones
                continue

            self.assertEqual(
                getattr(self.client, method)(url).status_code,
                405
            )


class TestIssueRegexReadGroup(TestIssueRegexAnonymous):
    """Issue regex test case. Read groups."""

    anonymous = False
    groups = ['group_a']


class TestIssueRegexWriteGroup(TestIssueRegexAnonymous):
    """Issue regex test case. Write group."""

    anonymous = False
    groups = ['group_b']


class TestIssueRegexTransaction(utils.KCIDBTransactionTestCase):
    """Issue regex tests that depend on transactions behaviors."""
    anonymous = False
    groups = ['group_b']

    fixtures = [
        'tests/fixtures/policies_for_group_abc.yaml',
        'tests/fixtures/basic.yaml',
        'tests/kcidb/fixtures/multiple_build_package_names.yaml',
        'tests/fixtures/issues.yaml',
    ]

    @freeze_time("2010-01-02 09:00:00")
    @override_settings(TIMER_RETRIAGE_PERIOD_S=0)
    def test_regex_form_concurrency(self):
        """Assert POST still works while running the QueuedTask('send_kcidb_object_for_retriage').

        Most of the time spent in 'send_kcidb_object_for_retriage' actually comes from serializing stuff,
        so let's pretend how long it takes by freezing it until the POST is done.

        NOTE: This is a white box test, relying on a lot of known interaction between methods to prevent a regression.
        """
        issue = models.Issue.objects.filter(id__in=self.issues_authorized['write']).first()
        self.assertIsNotNone(issue, "Precondition: There one authorized issue")
        self.assertFalse(models.IssueRegex.objects.filter(issue=issue).exists(),
                         "Precondition: There's no IssueRegex for the given issue")
        self.assertFalse(models.QueuedTask.objects.exists(),
                         "Precondition: There's no QueuedTask")
        authorized_checkouts = models.KCIDBCheckout.objects.filter(id__in=self.checkouts_authorized['read'])
        self.assertTrue(authorized_checkouts.exists(),
                        "Precondition: There should be at least one authorized KCIDBCheckout to retriage")
        # Make sure the checkouts can be sent for retriage
        authorized_checkouts.update(valid=False, start_time=timezone.now())

        with self.subTest("Prepare the QueuedTask('send_kcidb_object_for_retriage') via regex form submission"):
            post_url = reverse("views.issue_regex.form")
            form_data = {
                'action': 'new',
                'issue_id_select': issue.id,
                'text_match': r'barbar ',
                'file_name_match': r'.*',
                'test_name_match': r'.* ',
                'testresult_name_match': r'.* ',
            }
            # POST to create an IssueRegex which should trigger the creation of the QueuedTask
            self.assert_authenticated_post(HTTPStatus.FOUND, 'add_issueregex', post_url, form_data, user=self.user)

            issue_regex = models.IssueRegex.objects.filter(issue=issue).first()
            self.assertIsNotNone(issue_regex, "Precondition: Expected Regex to have been created")
            queued_task = models.QueuedTask.objects.filter(call_id="send_kcidb_object_for_retriage").first()
            self.assertIsNotNone(queued_task, "Precondition: Expected QueuedTask to have been created")

        # prepare the mocked serializer
        freeze_until_posted = threading.Event()
        started_serializing = threading.Event()

        class FakeKCIDBCheckoutSerializer(serializers.KCIDBCheckoutSerializer):
            """The same behavior as the original serializer, but with a threading wait."""
            @property
            def data(self):
                """Wait for threading condition before returning the serialized data."""
                started_serializing.set()

                serialized_data = super().data

                # wait for the lock.acquire until the test is done POSTing
                assert freeze_until_posted.wait(timeout=3), "Timeout while waiting for POST"

                return serialized_data

        def run_queued_task():
            from django.db import connections  # noqa: PLC0415 (Threads need manual connection handling)

            with mock.patch("datawarehouse.signal_receivers.kcidb_serializers.KCIDBCheckoutSerializer",
                            FakeKCIDBCheckoutSerializer):
                queued_task.run()
            connections.close_all()

        try:
            threaded_send = threading.Thread(target=run_queued_task, daemon=True)
            threaded_send.start()
            # Wait a little bit for the thread to start the QueuedTask
            self.assertTrue(started_serializing.wait(timeout=3), "Timeout while waiting for serialization to start")

            queued_task.refresh_from_db()
            self.assertIsNotNone(
                queued_task.start_time,
                "Precondition: task should've started, but not purged, within the thread.",
            )

            form_data.update({"action": "edit", "issue_regex_id": issue_regex.id})
            self.assert_authenticated_post(HTTPStatus.FOUND, "change_issueregex", post_url, form_data, user=self.user)

            freeze_until_posted.set()

        finally:
            # wait for it to conclude to avoid warnings
            threaded_send.join(timeout=3)


class TestIssueRegexAllGroups(TestIssueRegexAnonymous):
    """Issue regex test case. All groups."""

    anonymous = False
    groups = ['group_a', 'group_b']


class TestIssueResolveAnonymous(utils.KCIDBTestCase):
    """Issue resolve test case. Anonymous."""

    fixtures = [
        "tests/fixtures/policies_for_group_abc.yaml",
        "tests/fixtures/basic.yaml",
        "tests/fixtures/issues.yaml",
        "tests/kcidb/fixtures/base_simple.yaml",
    ]

    anonymous = True
    groups = []

    def _ensure_test_conditions(self, method):
        """Check that all conditions for testing are optimal."""
        auth_issues = models.Issue.objects.filter(id__in=self.issues_authorized[method])
        no_auth_issues = models.Issue.objects.exclude(id__in=self.issues_authorized[method])

        checks = [
            (auth_issues, 'No authorized issues'),
            (no_auth_issues, 'No unauthorized issues'),
        ]

        for check, message in checks:
            self.assertTrue(check.exists(), f'Test conditions unmet: {message} for "{method}"')

    def test_issue_resolve_not_handled_methods(self):
        """Test not handled methods are rejected."""
        allowed_methods = ['post']
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['write']
        )
        if issue_id := authorized_issues.values_list('id', flat=True).first():
            url = reverse('views.issue.resolve', args=[issue_id])

            for method in utils.ALL_METHODS:
                if method in allowed_methods:
                    # Skip allowed ones
                    continue

                self.assert_authenticated(
                    method, status.HTTP_405_METHOD_NOT_ALLOWED,
                    'change_issue', url, user=self.user)

    def test_issue_resolve(self):
        """Test resolving issues."""
        self._ensure_test_conditions('write')

        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['write']
        )

        for issue in models.Issue.objects.all():
            authorized = issue in authorized_issues and not self.anonymous
            self.assertIsNone(issue.last_edited_by)

            # Resolve it
            status_code = 302 if authorized else 404
            self.assert_authenticated_post(status_code, 'change_issue', f'/issue/{issue.id}/resolve', user=self.user)
            issue.refresh_from_db()
            resolved = True if authorized else False  # Shouldn't change if not authorized
            self.assertEqual(resolved, issue.resolved)
            self.assertEqual(self.user if authorized else None, issue.last_edited_by)

            # Un-resolve it
            self.assert_authenticated_post(status_code, 'change_issue', f'/issue/{issue.id}/resolve', user=self.user)
            issue.refresh_from_db()
            self.assertEqual(False, issue.resolved)
            self.assertEqual(self.user if authorized else None, issue.last_edited_by)

    @mock.patch("datawarehouse.scripts.notify_checkout_issueoccurrences_changed")
    def test_regression_toggle(self, mocked_notify):
        """Test toggling is_regression flag."""
        self._ensure_test_conditions('write')
        url = reverse("function.issueoccurrence.regression.toggle")
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['write']
        )
        kcidb_test = models.KCIDBTest.objects.get(id="redhat:public_test_R")

        for issue in models.Issue.objects.all():
            issue_occurrence = models.IssueOccurrence.objects.create(
                issue=issue,
                kcidb_test=kcidb_test,
                related_checkout=kcidb_test.checkout,
                is_regression=False,
            )
            authorized = issue in authorized_issues and not self.anonymous

            post_args = (url, {"issueoccurrences_ids": [issue_occurrence.id]})

            # Toggle to True
            self.assert_authenticated_post(HTTPStatus.FOUND, "change_issueoccurrence", *post_args, user=self.user)
            issue_occurrence.refresh_from_db()

            if not authorized:
                mocked_notify.assert_not_called()
                self.assertFalse(issue_occurrence.is_regression)
            else:
                mocked_notify.assert_called_once_with(issue_occurrence.related_checkout, checkout_outcome_changed=None)
                self.assertTrue(issue_occurrence.is_regression)

                mocked_notify.reset_mock()

                # Toggle back to false
                self.assert_authenticated_post(HTTPStatus.FOUND, "change_issueoccurrence", *post_args, user=self.user)
                issue_occurrence.refresh_from_db()

                mocked_notify.assert_called_once_with(issue_occurrence.related_checkout, checkout_outcome_changed=None)
                self.assertFalse(issue_occurrence.is_regression)

                mocked_notify.reset_mock()

    def test_regression_toggle_not_handled_methods(self):
        """Test not handled methods are rejected."""
        allowed_methods = ['post']
        url = reverse("function.issueoccurrence.regression.toggle")

        for method in utils.ALL_METHODS:
            with self.subTest(method=method):
                if method in allowed_methods:
                    # Skip allowed ones
                    continue

                self.assert_authenticated(
                    method, HTTPStatus.METHOD_NOT_ALLOWED, "change_issueoccurrence", url, user=self.user
                )


class TestIssueResolveReadGroup(TestIssueResolveAnonymous):
    """Issue resolve test case. Read group."""

    anonymous = False
    groups = ['group_a']


class TestIssueResolveWriteGroup(TestIssueResolveAnonymous):
    """Issue resolve test case. Write group."""

    anonymous = False
    groups = ['group_b']


class TestIssueResolveAllGroups(TestIssueResolveAnonymous):
    """Issue resolve test case. All groups."""

    anonymous = False
    groups = ['group_a', 'group_b']


class TestIssueViewAnonymous(utils.KCIDBTestCase):
    """Issue views test case. Anonymous."""

    fixtures = [
        'tests/fixtures/policies_for_group_abc.yaml',
        'tests/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_authorization.yaml',
        'tests/fixtures/issues.yaml',
    ]

    anonymous = True
    groups = []

    def _ensure_test_conditions(self, method):
        """Check that all conditions for testing are optimal."""
        auth_checkouts = models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized[method],
        )
        no_auth_checkouts = models.KCIDBCheckout.objects.exclude(
            id__in=self.checkouts_authorized[method],
        )

        auth_issues = models.Issue.objects.filter(id__in=self.issues_authorized[method])
        no_auth_issues = models.Issue.objects.exclude(id__in=self.issues_authorized[method])

        checks = [
            (auth_checkouts, 'No authorized checkouts'),
            (no_auth_checkouts, 'No unauthorized checkouts'),
            (auth_issues, 'No authorized issues'),
            (no_auth_issues, 'No unauthorized issues'),
        ]

        for check, message in checks:
            self.assertTrue(check.exists(), f'Test conditions unmet: {message} for "{method}"')

    def test_get_list_all(self):
        """Test get all Issues."""
        self._ensure_test_conditions('read')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['read']
        )
        ordered_issues = authorized_issues.annotate(
            last_occurrence=Max(
                'issueoccurrence__related_checkout__start_time'
            )
        ).order_by(
            F('last_occurrence').desc(nulls_last=True),
            '-id',
        )

        authorized_issues.update(resolved_at=None)

        response = self.client.get('/issue/-/list')
        self.assertContextEqual(
            response.context,
            {
                'issues': ordered_issues,
                'resolved': None,
                'search': '',
            },
        )

        authorized_issues.update(resolved_at=datetime_bool(True))

        response = self.client.get('/issue/-/list')
        self.assertContextEqual(
            response.context,
            {
                'issues': ordered_issues,
                'resolved': None,
                'search': '',
            },
        )

    def test_get_list_unresolved(self):
        """Test get all Issues."""
        self._ensure_test_conditions('read')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['read']
        ).order_by('id')
        ordered_issues = authorized_issues.annotate(
            last_occurrence=Max(
                'issueoccurrence__related_checkout__start_time'
            )
        ).order_by(
            F('last_occurrence').desc(nulls_last=True),
            '-id',
        )

        authorized_issues.update(resolved_at=None)

        response = self.client.get('/issue/-/list?resolved=false')
        self.assertContextEqual(
            response.context,
            {
                'issues': ordered_issues,
                'resolved': 'false',
                'search': '',
            },
        )

        # All unresolved, resolved should be empty
        response = self.client.get('/issue/-/list?resolved=true')
        self.assertContextEqual(
            response.context,
            {
                'issues': [],
                'resolved': 'true',
                'search': '',
            },
        )

    def test_get_list_resolved(self):
        """Test get all Issues."""
        self._ensure_test_conditions('read')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['read']
        ).order_by('id')
        ordered_issues = authorized_issues.annotate(
            last_occurrence=Max(
                'issueoccurrence__related_checkout__start_time'
            )
        ).order_by(
            F('last_occurrence').desc(nulls_last=True),
            '-id',
        )

        authorized_issues.update(resolved_at=datetime_bool(True))

        response = self.client.get('/issue/-/list?resolved=true')
        self.assertContextEqual(
            response.context,
            {
                'issues': ordered_issues,
                'resolved': 'true',
                'search': '',
            },
        )

        # All resolved, unresolved should be empty
        response = self.client.get('/issue/-/list?resolved=false')
        self.assertContextEqual(
            response.context,
            {
                'issues': [],
                'resolved': 'false',
                'search': '',
            },
        )

    def test_get_list_search(self):
        """Test get all Issues. Search argument."""
        self._ensure_test_conditions('read')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['read']
        )

        authorized_issues.update(resolved_at=datetime_bool(True))

        response = self.client.get('/issue/-/list?resolved=true&search=Issue Public')
        self.assertContextEqual(
            response.context,
            {
                'issues': models.Issue.objects.filter(description__contains='Issue Public').order_by('id'),
                'resolved': 'true',
                'search': 'Issue Public',
            },
        )

        response = self.client.get('/issue/-/list?resolved=true&search=https://issue.public')
        self.assertContextEqual(
            response.context,
            {
                'issues': models.Issue.objects.filter(ticket_url__contains='https://issue.public').order_by('id'),
                'resolved': 'true',
                'search': 'https://issue.public',
            },
        )

    def test_get_list_search_resolved(self):
        """Test search for resolved issues."""
        self._ensure_test_conditions('read')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['read']
        )

        authorized_issues.update(resolved_at=datetime_bool(True))

        response = self.client.get('/issue/-/list?resolved=true&search=Issue Public')
        self.assertContextEqual(
            response.context,
            {
                'issues': models.Issue.objects.filter(description__contains='Issue Public').order_by('id'),
                'resolved': 'true',
                'search': 'Issue Public',
            },
        )

        response = self.client.get('/issue/-/list?resolved=false&search=Issue Public')
        self.assertContextEqual(
            response.context,
            {
                'issues': [],
                'resolved': 'false',
                'search': 'Issue Public',
            },
        )

    def test_get_list_search_unresolved(self):
        """Test search for unresolved issues."""
        self._ensure_test_conditions('read')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['read']
        )

        authorized_issues.update(resolved_at=None)

        response = self.client.get('/issue/-/list?resolved=true&search=Issue Public')
        self.assertContextEqual(
            response.context,
            {
                'issues': [],
                'resolved': 'true',
                'search': 'Issue Public',
            },
        )

        response = self.client.get('/issue/-/list?resolved=false&search=Issue Public')
        self.assertContextEqual(
            response.context,
            {
                'issues': models.Issue.objects.filter(description__contains='Issue Public').order_by('id'),
                'resolved': 'false',
                'search': 'Issue Public',
            },
        )

    def test_get_list_search_all(self):
        """Test search for all issues."""
        self._ensure_test_conditions('read')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['read']
        )

        authorized_issues.update(resolved_at=None)

        response = self.client.get('/issue/-/list?search=Issue Public')
        self.assertContextEqual(
            response.context,
            {
                'issues': models.Issue.objects.filter(description__contains='Issue Public').order_by('id'),
                'resolved': None,
                'search': 'Issue Public',
            },
        )

        authorized_issues.update(resolved_at=datetime_bool(True))

        response = self.client.get('/issue/-/list?search=Issue Public')
        self.assertContextEqual(
            response.context,
            {
                'issues': models.Issue.objects.filter(description__contains='Issue Public').order_by('id'),
                'resolved': None,
                'search': 'Issue Public',
            },
        )

    def test_get_list_search_last_seen_ago(self):
        """Test search for issues with last_seen_ago."""
        self._ensure_test_conditions('read')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['read']
        )

        models.KCIDBCheckout.objects.update(start_time=timezone.now() - datetime.timedelta(days=10))
        for checkout in models.KCIDBCheckout.objects.all():
            checkout.issues.set(authorized_issues)

        response = self.client.get('/issue/-/list?last_seen_ago=2 weeks&search=Issue Public')

        self.assertContextEqual(
            response.context,
            {
                'issues': [],
                'last_seen_ago': '2 weeks',
                'search': 'Issue Public',
            },
        )

        response = self.client.get('/issue/-/list?last_seen_ago=1 week&search=Issue Public')
        self.assertContextEqual(
            response.context,
            {
                'issues': models.Issue.objects.filter(description__contains='Issue Public').order_by('-id'),
                'last_seen_ago': '1 week',
                'search': 'Issue Public',
            },
        )

    def test_get_single(self):
        """Test get a single issue."""
        self._ensure_test_conditions('read')

        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['read']
        ).order_by('-id')

        checkout = models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized["read"],
        ).get(id="redhat:public_checkout")

        build = checkout.kcidbbuild_set.exclude(kcidbtest=None).get(id="redhat:public_build_1")
        build.architecture = 2
        build.save()

        test = build.kcidbtest_set.get(id="redhat:public_test_2")
        test.environment = models.BeakerResource.objects.create(fqdn='host.name')
        test.save()

        other_test = build.kcidbtest_set.get(id="redhat:public_test_1")
        other_test.environment = models.BeakerResource.objects.create(fqdn="pooh.bear")
        other_test.save()
        testresult = other_test.kcidbtestresult_set.get(id="redhat:public_test_1_result_1")

        for issue in models.Issue.objects.all():
            models.IssueOccurrence.objects.all().delete()
            models.IssueOccurrence.objects.create(issue=issue, related_checkout=checkout, kcidb_build=build)
            models.IssueOccurrence.objects.create(issue=issue, related_checkout=checkout, kcidb_test=test)
            models.IssueOccurrence.objects.create(issue=issue, related_checkout=checkout, kcidb_testresult=testresult)

            response = self.client.get(f'/issue/{issue.id}')

            if issue not in authorized_issues:
                self.assertEqual(404, response.status_code)
                continue

            self.assertContextEqual(
                response.context,
                {
                    "issue": issue,
                    "checkouts": [checkout],
                    "affected_hosts": [
                        {"environment__fqdn": "host.name", "total_hits": 1},
                        {"environment__fqdn": "pooh.bear", "total_hits": 1},
                    ],
                    "affected_archs": [{"name": "aarch64", "total_hits": 3}],
                    "affected_tests": [
                        {"test__name": "test_1", "total_hits": 1},
                        {"test__name": "test_2", "total_hits": 1},
                    ],
                    "issues": authorized_issues,
                    "issue_kinds": models.IssueKind.objects.all(),
                    "git_trees": models.GitTree.objects.filter(
                        kcidbcheckout__id__in=self.checkouts_authorized["read"]
                    ).distinct(),
                    "policies": models.Policy.objects.filter(id__in=self.policies_authorized["write"]),
                },
            )


class TestIssueViewReadGroup(TestIssueViewAnonymous):
    """Issue resolve test case. Read group."""

    anonymous = False
    groups = ['group_a']


class TestIssueViewWriteGroup(TestIssueViewAnonymous):
    """Issue resolve test case. Write group."""

    anonymous = False
    groups = ['group_b']


class TestIssueViewAllGroups(TestIssueViewAnonymous):
    """Issue resolve test case. All groups."""

    anonymous = False
    groups = ['group_a', 'group_b']


class TestIssueOccurrenceEditAnonymous(utils.KCIDBTestCase):
    """IssueOccurrence edit test case. Anonymous."""

    fixtures = [
        'tests/fixtures/policies_for_group_abc.yaml',
        'tests/fixtures/basic.yaml',
        'tests/fixtures/issues.yaml',
        'tests/kcidb/fixtures/base_authorization.yaml',
    ]

    anonymous = True
    groups = []

    def _ensure_test_conditions(self, method):
        """Check that all conditions for testing are optimal."""
        auth_policies = models.Policy.objects.filter(id__in=self.policies_authorized[method])
        no_auth_policies = models.Policy.objects.exclude(id__in=self.policies_authorized[method])
        auth_checkouts = models.KCIDBCheckout.objects.filter(id__in=self.checkouts_authorized[method])
        no_auth_checkouts = models.KCIDBCheckout.objects.exclude(id__in=self.checkouts_authorized[method])
        auth_issues = models.Issue.objects.filter(id__in=self.issues_authorized[method])
        no_auth_issues = models.Issue.objects.exclude(id__in=self.issues_authorized[method])

        checks = [
            (auth_policies, 'No authorized policies'),
            (no_auth_policies, 'No unauthorized policies'),
            (auth_checkouts, 'No authorized checkouts'),
            (no_auth_checkouts, 'No unauthorized checkouts'),
            (auth_issues, 'No authorized issues'),
            (no_auth_issues, 'No unauthorized issues'),
        ]

        for check, message in checks:
            self.assertTrue(check.exists(), f'Test conditions unmet: {message} for "{method}"')

        self.assertTrue(auth_issues.count() > 1)
        self.assertTrue(no_auth_issues.count() > 1)

    def test_issueoccurrence_remove(self):
        """Test removing IssueOccurrence from checkout."""
        self._ensure_test_conditions('write')
        authorized_checkouts = models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized['write']
        )

        for checkout in models.KCIDBCheckout.objects.all():
            authorized = checkout in authorized_checkouts and not self.anonymous

            issue = checkout.issues.first()
            # Make sure that for this test the issue's policy is fine
            issue.policy = checkout.policy
            issue.save()
            self.assertIsNotNone(issue)

            post_args = (
                '/issue/-/occurrences/edit',
                {
                    'issue_id': issue.id,
                    'action': 'remove',
                    'checkouts_iids': [checkout.iid],
                }
            )
            self.assert_authenticated_post(302, 'change_issueoccurrence', *post_args, user=self.user)

            self.assertEqual(
                not authorized,  # If authorized, no issueoccurrence matching should exist.
                models.IssueOccurrence.objects.filter(related_checkout=checkout, issue=issue).exists()
            )

    def test_issueoccurrence_move(self):
        """Test moving IssueOccurrence from one issue to another."""
        self._ensure_test_conditions('write')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['write']
        )

        checkout = models.KCIDBCheckout.objects.exclude(kcidbbuild__kcidbtest=None).first()
        build = models.KCIDBBuild.objects.filter(checkout=checkout).first()
        test = models.KCIDBTest.objects.filter(build=build).first()

        issues = models.Issue.objects.all()
        issue_combinations = [
            (issue_orig, issue_dest)
            for issue_orig in issues
            for issue_dest in issues
        ]

        for issue_orig, issue_dest in issue_combinations:
            models.IssueOccurrence.objects.all().delete()

            # Add the issue to multiple child objects of the same checkout
            # to make sure all of them are moved.
            checkout.issues.set([issue_orig])
            build.issues.set([issue_orig])
            test.issues.set([issue_orig])

            post_args = (
                '/issue/-/occurrences/edit',
                {
                    'issue_id': issue_orig.id,
                    'action': 'move',
                    'action_input': issue_dest.id,
                    'checkouts_iids': [checkout.iid],
                }
            )

            # If issue_dest is not authorized, 404 is returned
            orig_authorized = issue_orig in authorized_issues and not self.anonymous
            # If issue_orig is not authorized, nothing is changed but the endpoint does not return error.
            dest_authorized = issue_dest in authorized_issues and not self.anonymous

            status_code = 302 if dest_authorized else 404
            self.assert_authenticated_post(status_code, 'change_issueoccurrence', *post_args, user=self.user)

            count_issue_orig = models.IssueOccurrence.objects.filter(
                issue=issue_orig,
                related_checkout=checkout
            ).count()
            count_issue_dest = models.IssueOccurrence.objects.filter(
                issue=issue_dest,
                related_checkout=checkout
            ).count()

            if issue_orig == issue_dest:
                # If both issues are the same, nothing should change, no matter the authorization.
                self.assertEqual(3, count_issue_orig)
            elif orig_authorized and dest_authorized:
                self.assertEqual(0, count_issue_orig)
                self.assertEqual(3, count_issue_dest)
            else:
                self.assertEqual(3, count_issue_orig)
                self.assertEqual(0, count_issue_dest)

    def test_edit_handled_methods(self):
        """Test not handled methods are rejected."""
        allowed_methods = ['post']
        url = '/issue/-/occurrences/edit'

        for method in utils.ALL_METHODS:
            if method in allowed_methods:
                # Skip allowed ones
                continue

            self.assert_authenticated(method, 405, 'change_issueoccurrence', url, user=self.user)


class TestIssueOccurrenceEditReadGroup(TestIssueOccurrenceEditAnonymous):
    """IssueOccurrence edit test case. Read group."""

    anonymous = False
    groups = ['group_a']


class TestIssueOccurrenceEditWriteGroup(TestIssueOccurrenceEditAnonymous):
    """IssueOccurrence edit test case. Write group."""

    anonymous = False
    groups = ['group_b']


class TestIssueOccurrenceEditAllGroups(TestIssueOccurrenceEditAnonymous):
    """IssueOccurrence edit test case. All groups."""

    anonymous = False
    groups = ['group_a', 'group_b']


class TestIssueDeleteAnonymous(utils.KCIDBTestCase):
    """Issue deletion test case. Anonymous."""

    fixtures = [
        'tests/fixtures/policies_for_group_abc.yaml',
        'tests/fixtures/basic.yaml',
        'tests/fixtures/issues.yaml',
    ]

    anonymous = True
    groups = []

    def _ensure_test_conditions(self, method):
        """Check that all conditions for testing are optimal."""
        auth_issues = models.Issue.objects.filter(id__in=self.issues_authorized[method])
        no_auth_issues = models.Issue.objects.exclude(id__in=self.issues_authorized[method])

        checks = [
            (auth_issues, 'No authorized issues'),
            (no_auth_issues, 'No unauthorized issues'),
        ]

        for check, message in checks:
            self.assertTrue(check.exists(), f'Test conditions unmet: {message} for "{method}"')

    def test_delete(self):
        """Test delete Issue."""
        self._ensure_test_conditions('write')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['write']
        )

        for issue in models.Issue.objects.all():
            authorized = issue in authorized_issues and not self.anonymous

            status_code = 302 if authorized else 404
            self.assert_authenticated_post(status_code, 'delete_issue', f'/issue/{issue.id}/delete', user=self.user)

            self.assertEqual(
                not authorized,  # If not authorized, the issue should still exist.
                models.Issue.objects.filter(id=issue.id).exists()
            )

    def test_delete_handled_methods(self):
        """Test not handled methods are rejected."""
        allowed_methods = ['post']
        url = '/issue/1/delete'

        for method in utils.ALL_METHODS:
            if method in allowed_methods:
                # Skip allowed ones
                continue

            self.assert_authenticated(method, 405, 'delete_issue', url, user=self.user)


class TestIssueDeleteReadGroup(TestIssueDeleteAnonymous):
    """Issue deletion test case. Read groups."""

    anonymous = False
    groups = ['group_a']


class TestIssueDeleteWriteGroup(TestIssueDeleteAnonymous):
    """Issue deletion test case. Write group."""

    anonymous = False
    groups = ['group_b']


class TestIssueDeleteAllGroups(TestIssueDeleteAnonymous):
    """Issue deletion test case. All groups."""

    anonymous = False
    groups = ['group_a', 'group_b']
