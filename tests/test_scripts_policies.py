"""Test scripts/policies.py."""
from unittest import mock

from django.contrib.auth import get_user_model
import django.contrib.auth.models as auth_models
from django.test import override_settings

from datawarehouse import models
from datawarehouse import scripts
from tests import utils

User = get_user_model()


class TestUpdateIssuePolicy(utils.TestCase):
    """
    Test update_issue_policy.

    Ensure Issue.policy is updated after new IssueOccurrence.
    """

    fixtures = [
        'tests/fixtures/basic.yaml',
        'tests/fixtures/basic_policies.yaml',
        'tests/fixtures/issue_policy_auto.yaml',
    ]

    @staticmethod
    def _associate_issue(issue, checkout_id):
        """Associate issue to checkout."""
        checkout = models.KCIDBCheckout.objects.get(id=checkout_id)
        # Setting related_checkout we avoid having to test it for checkout, build and test
        models.IssueOccurrence.objects.create(issue=issue, kcidb_checkout=checkout, related_checkout=checkout)

        scripts.update_issue_policy((issue.id, ))
        issue.refresh_from_db()

    def test_issue_auto_public_internal(self):
        """
        Test issue with policy_auto_public=True is updated.

        When related to internal checkout, keep it internal.
        """
        issue = models.Issue.objects.get(policy_auto_public=True)
        self.assertEqual(issue.policy.name, models.Policy.INTERNAL)

        self._associate_issue(issue, 'internal_checkout')
        self.assertEqual(issue.policy.name, models.Policy.INTERNAL)

    def test_issue_auto_public_public(self):
        """
        Test issue with policy_auto_public=True is updated.

        When related to public checkout, make it public.
        """
        issue = models.Issue.objects.get(policy_auto_public=True)
        self.assertEqual(issue.policy.name, models.Policy.INTERNAL)

        self._associate_issue(issue, "redhat:public_checkout")
        self.assertEqual(issue.policy.name, models.Policy.PUBLIC)

    def test_issue_auto_public_mixed(self):
        """
        Test issue with policy_auto_public=True is updated.

        When related to multiple checkout, make it public if any is public.
        """
        issue = models.Issue.objects.get(policy_auto_public=True)
        self.assertEqual(issue.policy.name, models.Policy.INTERNAL)

        self._associate_issue(issue, 'internal_checkout')
        self.assertEqual(issue.policy.name, models.Policy.INTERNAL)

        self._associate_issue(issue, "redhat:public_checkout")
        self.assertEqual(issue.policy.name, models.Policy.PUBLIC)

    def test_issue_auto_public_mixed_inverse_order(self):
        """
        Test issue with policy_auto_public=True is updated.

        When related to multiple checkout, make it public if any is public.
        """
        issue = models.Issue.objects.get(policy_auto_public=True)
        self.assertEqual(issue.policy.name, models.Policy.INTERNAL)

        self._associate_issue(issue, "redhat:public_checkout")
        self.assertEqual(issue.policy.name, models.Policy.PUBLIC)

        self._associate_issue(issue, 'internal_checkout')
        self.assertEqual(issue.policy.name, models.Policy.PUBLIC)

    def test_issue_no_update(self):
        """Test issue with policy_auto_public=False is not updated."""
        issue = models.Issue.objects.get(policy_auto_public=False)
        self.assertEqual(issue.policy.name, models.Policy.INTERNAL)

        self._associate_issue(issue, "redhat:public_checkout")
        self.assertEqual(issue.policy.name, models.Policy.INTERNAL)

    def test_issue_auto_public_multiple(self):
        """
        Test issue with policy_auto_public=True is updated.

        Test call with multiple issues.
        """
        issues = models.Issue.objects.all()
        # Set all issues as INTERNAL with auto update.
        issues.update(
            policy=models.Policy.objects.get(name=models.Policy.INTERNAL),
            policy_auto_public=True
        )
        self.assertTrue(issues.count() > 1)

        checkout = models.KCIDBCheckout.objects.get(id="redhat:public_checkout")
        for issue in issues:
            models.IssueOccurrence.objects.create(issue=issue, kcidb_checkout=checkout, related_checkout=checkout)

        scripts.update_issue_policy({i.id for i in issues})

        for issue in issues:
            issue.refresh_from_db()
            self.assertEqual(issue.policy.name, models.Policy.PUBLIC)


@override_settings(LDAP_CONFIG={'server_url': 'ldap.url'})
@mock.patch('datawarehouse.scripts.policies.ldap.LDAPConnection.connect', mock.Mock())
class TestUpdateLDAPGroupMembers(utils.TestCase):
    """Test update_ldap_group_members."""

    fixtures = [
        'tests/fixtures/user_ldap_sync.yaml',
    ]

    @override_settings(FF_LDAP_GROUP_SYNC=False)
    @mock.patch('datawarehouse.scripts.policies.ldap.connection')
    def test_disabled(self, mock_conn):
        """Test it does nothing when FF_LDAP_GROUP_SYNC=False."""
        scripts.update_ldap_group_members()
        mock_conn.assert_not_called()

    @override_settings(FF_LDAP_GROUP_SYNC=True)
    @mock.patch('datawarehouse.scripts.policies.User.objects.filter')
    @mock.patch('datawarehouse.scripts.policies.ldap.connection')
    def test_enabled_no_links(self, mock_conn, mock_user):
        """Test it does nothing if FF_LDAP_GROUP_SYNC=True but no links."""
        scripts.update_ldap_group_members()
        mock_conn.assert_called_once()
        mock_user.assert_not_called()

    @override_settings(FF_LDAP_GROUP_SYNC=True)
    @mock.patch("datawarehouse.scripts.policies.ldap.LDAPConnection.get_users", return_value=[])
    def test_ldap_users(self, mock_get_users):
        """Test ldap users are correctly added."""
        group = auth_models.Group.objects.get(name='group_a')
        self.assertFalse(group.user_set.exists())

        link_1 = models.LDAPGroupLink.objects.create(filter_query="some query but it is mocked")
        link_1.groups.set([group])
        link_2 = models.LDAPGroupLink.objects.create(filter_query="some other query")
        link_2.groups.set([group])

        mock_get_users.reset_mock()
        mock_get_users.side_effect = [
            [('test_user_1', 'test_user_1@mail.com')],
            [('test_user_2', 'test_user_2@mail.com')],
        ]

        scripts.update_ldap_group_members()

        mock_get_users.assert_has_calls(
            [mock.call(link_1.filter_query), mock.call(link_2.filter_query)], any_order=True
        )

        self.assertCountEqual(
            group.user_set.all().values_list("username", flat=True),
            ["test_user_1", "test_user_2"],
        )

        mock_get_users.reset_mock(side_effect=True)
        with (
            self.subTest("Call with argument adds the new users without deleting the existing"),
            mock.patch(
                "datawarehouse.scripts.update_ldap_group_members", wraps=scripts.update_ldap_group_members
            ) as wrapped_call,
        ):
            # post_save signal should trigger a call with argument, but no group means it shouldn't fetch LDAP
            link_3 = models.LDAPGroupLink.objects.create(filter_query="yet another query")
            wrapped_call.assert_called_once_with(link_3)
            mock_get_users.assert_not_called()

            wrapped_call.reset_mock()
            mock_get_users.return_value = [("test_user_3", "test_user_3@mail.com")]
            # post_save signal should trigger a call with argument
            link_3.groups.set([group])

            wrapped_call.assert_called_once_with(link_3)
            mock_get_users.assert_called_once_with(link_3.filter_query)

            self.assertCountEqual(
                group.user_set.all().values_list("username", flat=True),
                ["test_user_1", "test_user_2", "test_user_3"],
            )

    @override_settings(FF_LDAP_GROUP_SYNC=True)
    @mock.patch("datawarehouse.scripts.policies.ldap.LDAPConnection.get_users", return_value=[])
    def test_ldap_unknown(self, mock_get_users):
        """Test unknown ldap users do nothing."""
        group = auth_models.Group.objects.get(name='group_a')
        self.assertFalse(group.user_set.exists())

        link = models.LDAPGroupLink.objects.create(filter_query="some query but it is mocked")
        link.groups.set([group])

        mock_get_users.reset_mock()
        mock_get_users.return_value = [
            ('other_user_1', 'other_user_1@mail.com'),
            ('other_user_2', 'other_user_2@mail.com'),
        ]

        scripts.update_ldap_group_members()

        mock_get_users.assert_called_once_with(link.filter_query)

        self.assertFalse(group.user_set.exists())

    @override_settings(FF_LDAP_GROUP_SYNC=True)
    @mock.patch("datawarehouse.scripts.policies.ldap.LDAPConnection.get_users", return_value=[])
    def test_ldap_mismatch_email(self, mock_get_users):
        """Test users with different emails."""
        group = auth_models.Group.objects.get(name='group_a')

        self.assertFalse(group.user_set.exists())

        link = models.LDAPGroupLink.objects.create(filter_query="some query but it is mocked")
        link.groups.set([group])

        mock_get_users.reset_mock()
        mock_get_users.return_value = [
            ('test_user_1', 'wrong_mail_1@mail.com'),
            ('test_user_2', 'wrong_mail_2@mail.com'),
        ]

        scripts.update_ldap_group_members()

        mock_get_users.assert_called_once_with(link.filter_query)

        self.assertFalse(group.user_set.exists())

    @override_settings(FF_LDAP_GROUP_SYNC=True)
    @mock.patch("datawarehouse.scripts.policies.ldap.LDAPConnection.get_users", return_value=[])
    def test_extra_users(self, mock_get_users):
        """Test extra_users are added despite the query result."""
        user = User.objects.get(username='test_user_1')
        group = auth_models.Group.objects.get(name='group_a')
        self.assertFalse(user.groups.exists())

        link = models.LDAPGroupLink.objects.create(filter_query="some query but it is mocked")
        link.groups.set([group])
        link.extra_users.set([user])

        mock_get_users.reset_mock()
        mock_get_users.return_value = []

        scripts.update_ldap_group_members()

        mock_get_users.assert_called_once_with(link.filter_query)

        self.assertQuerySetEqual(group.user_set.all(), [user])

    @override_settings(FF_LDAP_GROUP_SYNC=True)
    @mock.patch("datawarehouse.scripts.policies.ldap.LDAPConnection.get_users", return_value=[])
    def test_delete_users(self, mock_get_users):
        """Test synchronization removes users once they're no longer linked."""
        user_3 = User.objects.get(username='test_user_3')
        group = auth_models.Group.objects.get(name='group_a')

        self.assertFalse(group.user_set.exists())

        link = models.LDAPGroupLink.objects.create(filter_query="some query but it is mocked")
        link.groups.set([group])
        link.extra_users.add(user_3)

        mock_get_users.reset_mock()
        mock_get_users.return_value = [
            ('test_user_1', 'test_user_1@mail.com'),
            ('test_user_2', 'test_user_2@mail.com'),
        ]

        scripts.update_ldap_group_members()

        mock_get_users.assert_called_once_with(link.filter_query)

        self.assertEqual(
            set(User.objects.filter(username__in=('test_user_1', 'test_user_2', 'test_user_3'))),
            set(group.user_set.all())
        )

        # User test_user_2 and test_user_3 are removed
        link.extra_users.set([])
        mock_get_users.return_value = [
            ('test_user_1', 'test_user_1@mail.com'),
        ]
        scripts.update_ldap_group_members()
        self.assertEqual(
            set(User.objects.filter(username__in=('test_user_1', ))),
            set(group.user_set.all())
        )


@override_settings(LDAP_CONFIG={'server_url': 'ldap.url'})
@mock.patch('datawarehouse.scripts.policies.ldap.LDAPConnection.connect', mock.Mock())
class TestUpdateLDAPGroupMembersForUser(utils.TestCase):
    """Test update_ldap_group_members_for_user."""

    fixtures = [
        'tests/fixtures/user_ldap_sync.yaml',
    ]

    @override_settings(FF_LDAP_GROUP_SYNC=False)
    @mock.patch('datawarehouse.scripts.policies.ldap.connection')
    def test_disabled(self, mock_conn):
        """Test it does nothing when FF_LDAP_GROUP_SYNC=False."""
        scripts.update_ldap_group_members_for_user(mock.Mock())
        self.assertFalse(mock_conn.called)

    @override_settings(FF_LDAP_GROUP_SYNC=True)
    @mock.patch("datawarehouse.scripts.policies.ldap.LDAPConnection.get_users", return_value=[])
    def test_add_user_to_groups(self, mock_get_users):
        """Test ldap users are correctly added."""
        group = auth_models.Group.objects.get(name='group_a')
        self.assertFalse(group.user_set.exists())

        link = models.LDAPGroupLink.objects.create(filter_query="some query but it is mocked")
        link.groups.set([group])
        user = User.objects.get(username='test_user_1')
        mock_get_users.return_value = [
            (user.username, user.email),
        ]

        scripts.update_ldap_group_members_for_user(user)

        self.assertTrue(group.user_set.filter(username=user.username).exists())

    @override_settings(FF_LDAP_GROUP_SYNC=True)
    @mock.patch("datawarehouse.scripts.policies.ldap.LDAPConnection.get_users", return_value=[])
    def test_new_user_keeps_existing(self, mock_get_users):
        """Test that adding a new user does not remove the previous ones."""
        group = auth_models.Group.objects.get(name='group_a')
        self.assertFalse(group.user_set.exists())

        link = models.LDAPGroupLink.objects.create(filter_query="some query but it is mocked")
        link.groups.set([group])
        mock_get_users.return_value = [
            ('test_user_1', 'test_user_1@mail.com'),
            ('test_user_2', 'test_user_2@mail.com'),
        ]

        scripts.update_ldap_group_members()

        # Group already has 2 users
        self.assertEqual(2, group.user_set.count())
        self.assertEqual(
            set(User.objects.filter(username__in=('test_user_1', 'test_user_2'))),
            set(group.user_set.all())
        )

        # New user is added
        mock_get_users.return_value.append(
            ('test_user_3', 'test_user_3@mail.com'),
        )
        user_3 = User.objects.get(username='test_user_3')
        scripts.update_ldap_group_members_for_user(user_3)

        # All 3 users are in the group now
        self.assertEqual(3, group.user_set.count())
        self.assertEqual(
            set(User.objects.filter(username__in=('test_user_1', 'test_user_2', 'test_user_3'))),
            set(group.user_set.all())
        )

    @override_settings(FF_LDAP_GROUP_SYNC=True)
    @mock.patch("datawarehouse.scripts.policies.ldap.LDAPConnection.get_users", return_value=[])
    def test_add_user_to_groups_no_username(self, mock_get_users):
        """Test ldap users are correctly added when they still have no username."""
        group = auth_models.Group.objects.get(name='group_a')
        self.assertFalse(group.user_set.exists())

        link = models.LDAPGroupLink.objects.create(filter_query="some query but it is mocked")
        link.groups.set([group])
        user = User.objects.create(email='test_user_99@mail.com')

        mock_get_users.return_value = [
            ('test_user_99', user.email),
        ]

        scripts.update_ldap_group_members_for_user(user)

        self.assertTrue(group.user_set.filter(email=user.email).exists())
