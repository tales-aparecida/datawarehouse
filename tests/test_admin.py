"""Test views defined in admin.py."""

from functools import partial
from unittest import mock

from django.contrib import messages
from django.contrib.admin import site
from django.contrib.auth import get_user_model
from django.contrib.auth import models as auth_models
from django.contrib.sessions.middleware import SessionMiddleware
from django.db.models import Q
from django.test import RequestFactory

from datawarehouse import admin
from datawarehouse import models
from datawarehouse.authorization import RequestAuthorization
from tests import utils


class TestIssueOccurrenceAdmin(utils.TestCase):
    """Test custom actions on TestIssueOccurrenceAdmin."""

    fixtures = (
        "tests/fixtures/basic_policies.yaml",
        "tests/fixtures/basic.yaml",
        "tests/fixtures/issues_all_basic_policies.yaml",
        "tests/fixtures/multiple_issue_occurrences.yaml",
    )

    request_factory = RequestFactory()

    def _prepare_request(self, read_group_name: str, *, method=None, url="", data=None):
        """Prepare a request with a user authorized to the given group."""
        request = self.request_factory.get(url, data) if method is None else method(url, data)
        request.user = get_user_model().objects.get(username="test_user")
        request.user.groups.set(auth_models.Group.objects.filter(name=read_group_name))
        # RequestFactory doesn't use middleware, hence the authorization backend needs to be enabled manually
        middleware = SessionMiddleware(mock.Mock())
        middleware.process_request(request)
        request.session.save()
        RequestAuthorization.fill_user_data(request)

        return request

    def test_authorized_get_queryset(self):
        """Test the inherited AuthorizedModelAdmin.get_queryset filters as expected using models.Issue as reference."""
        modeladmin = admin.AuthorizedModelAdmin(models.Issue, site)

        with self.subTest("User allowed to read public stuff"):
            request = self._prepare_request(None)
            queryset = modeladmin.get_queryset(request)
            descriptions = queryset.values_list("description", flat=True)

            self.assertIn("Issue Public", descriptions)
            self.assertNotIn("Issue Internal", descriptions)
            self.assertNotIn("Issue Retrigger", descriptions)
            self.assertNotIn("Issue Unavailable", descriptions)

        with self.subTest("User allowed to read internal stuff"):
            request = self._prepare_request("policy_internal_read")
            queryset = modeladmin.get_queryset(request)
            descriptions = queryset.values_list("description", flat=True)

            self.assertIn("Issue Public", descriptions)
            self.assertIn("Issue Internal", descriptions)
            self.assertNotIn("Issue Retrigger", descriptions)
            self.assertNotIn("Issue Unavailable", descriptions)

        with self.subTest("User allowed to read retrigger stuff"):
            request = self._prepare_request("policy_retrigger_rw")
            queryset = modeladmin.get_queryset(request)
            descriptions = queryset.values_list("description", flat=True)

            self.assertIn("Issue Public", descriptions)
            self.assertNotIn("Issue Internal", descriptions)
            self.assertIn("Issue Retrigger", descriptions)
            self.assertNotIn("Issue Unavailable", descriptions)

        with self.subTest("SuperUser allowed to read all but unavailable"):
            request.user.is_superuser = True
            request.user.save(update_fields=["is_superuser"])
            request = self._prepare_request(None)
            queryset = modeladmin.get_queryset(request)
            descriptions = queryset.values_list("description", flat=True)

            self.assertIn("Issue Public", descriptions)
            self.assertIn("Issue Internal", descriptions)
            self.assertIn("Issue Retrigger", descriptions)
            self.assertNotIn("Issue Unavailable", descriptions)

    def test_move_to_another_issue(self):
        """Test IssueOccurrenceAdmin.move_to_another_issue filters as expected."""
        modeladmin = admin.IssueOccurrenceAdmin(models.IssueOccurrence, site)
        mocked_message_user = mock.Mock()
        modeladmin.message_user = mocked_message_user
        _prepare_post = partial(self._prepare_request, "policy_public_write", method=self.request_factory.post)
        public_issue_1 = models.Issue.objects.get(description="Issue Public")
        public_issue_2 = models.Issue.objects.get(description="Issue Public 2")
        internal_issue = models.Issue.objects.get(description="Issue Internal")
        kcidb_testresult = models.KCIDBTestResult.objects.get(iid=1)
        kcidb_checkout = models.KCIDBCheckout.objects.get(iid=2)

        with self.subTest("Missing target issue parameter"):
            request = _prepare_post(data={})
            queryset = modeladmin.get_queryset(request).filter(kcidb_testresult=kcidb_testresult, issue=public_issue_1)

            result = modeladmin.move_to_another_issue(request, queryset)

            self.assertTrue(result is mocked_message_user.return_value)
            expected_message = "Missing target issue"
            mocked_message_user.assert_called_once_with(request, expected_message, messages.ERROR)

        mocked_message_user.reset_mock()

        with self.subTest("Trying to move to an unauthorized issue"):
            request = _prepare_post(data={"target_issue": internal_issue.pk})
            queryset = modeladmin.get_queryset(request).filter(kcidb_testresult=kcidb_testresult, issue=public_issue_1)

            result = modeladmin.move_to_another_issue(request, queryset)

            self.assertTrue(result is mocked_message_user.return_value)
            expected_message = f"Target issue {internal_issue.pk} does not exist"
            mocked_message_user.assert_called_once_with(request, expected_message, messages.ERROR)

        mocked_message_user.reset_mock()

        with self.subTest("Trying to move what is already there"):
            request = _prepare_post(data={"target_issue": public_issue_1.pk})
            queryset = modeladmin.get_queryset(request).filter(kcidb_testresult=kcidb_testresult, issue=public_issue_1)

            result = modeladmin.move_to_another_issue(request, queryset)

            self.assertTrue(result is mocked_message_user.return_value)
            expected_message = (
                "0 occurrences were moved to the targeted issue. 1 were already pointing to the targeted issue."
            )
            mocked_message_user.assert_called_once_with(request, expected_message, messages.SUCCESS)

        mocked_message_user.reset_mock()

        with self.subTest("Trying to move something new"):
            request = _prepare_post(data={"target_issue": public_issue_2.pk})
            queryset = modeladmin.get_queryset(request).filter(kcidb_testresult=kcidb_testresult, issue=public_issue_1)

            result = modeladmin.move_to_another_issue(request, queryset)

            self.assertTrue(result is mocked_message_user.return_value)
            expected_message = "1 occurrences were moved to the targeted issue."
            mocked_message_user.assert_called_once_with(request, expected_message, messages.SUCCESS)

        mocked_message_user.reset_mock()

        with self.subTest("Trying to move what already has an equivalent occurrence there"):
            kcidb_testresult.issues.set([public_issue_1, public_issue_2])

            request = _prepare_post(data={"target_issue": public_issue_2.pk})
            queryset = modeladmin.get_queryset(request).filter(kcidb_testresult=kcidb_testresult, issue=public_issue_1)

            result = modeladmin.move_to_another_issue(request, queryset)

            self.assertTrue(result is mocked_message_user.return_value)
            expected_message = (
                "0 occurrences were moved to the targeted issue."
                " 1 had an equivalent occurrence in the target and were deleted"
                " ({'datawarehouse.IssueOccurrence': 1})."
            )
            mocked_message_user.assert_called_once_with(request, expected_message, messages.SUCCESS)

        mocked_message_user.reset_mock()

        with self.subTest("The three together"):
            kcidb_checkout.issues.set([public_issue_1])
            kcidb_testresult.issues.set([public_issue_1, public_issue_2])

            request = _prepare_post(data={"target_issue": public_issue_2.pk})
            queryset = modeladmin.get_queryset(request).filter(
                Q(kcidb_checkout=kcidb_checkout) | Q(kcidb_testresult=kcidb_testresult)
            )

            result = modeladmin.move_to_another_issue(request, queryset)

            self.assertTrue(result is mocked_message_user.return_value)
            expected_message = (
                "1 occurrences were moved to the targeted issue."
                " 1 were already pointing to the targeted issue."
                " 1 had an equivalent occurrence in the target and were deleted"
                " ({'datawarehouse.IssueOccurrence': 1})."
            )
            mocked_message_user.assert_called_once_with(request, expected_message, messages.SUCCESS)

    def test_move_to_another_issue_subtest_triaging(self):
        """Test that tests and subtests are handled as expected."""
        modeladmin = admin.IssueOccurrenceAdmin(models.IssueOccurrence, site)
        mocked_message_user = mock.Mock()
        modeladmin.message_user = mocked_message_user
        _prepare_post = partial(self._prepare_request, "policy_public_write", method=self.request_factory.post)
        kcidb_testresult = models.KCIDBTestResult.objects.get(iid=1)
        kcidb_test = kcidb_testresult.test
        public_issue_1 = models.Issue.objects.get(description="Issue Public")
        public_issue_2 = models.Issue.objects.get(description="Issue Public 2")

        with self.subTest("Test there, subtest here; results in both there"):
            models.IssueOccurrence.objects.all().delete()

            kcidb_test.issues.set([public_issue_2])
            kcidb_testresult.issues.set([public_issue_1])

            request = _prepare_post(data={"target_issue": public_issue_2.pk})
            queryset = modeladmin.get_queryset(request).filter(kcidb_testresult=kcidb_testresult)

            result = modeladmin.move_to_another_issue(request, queryset)

            self.assertTrue(result is mocked_message_user.return_value)
            expected_message = "1 occurrences were moved to the targeted issue."
            mocked_message_user.assert_called_once_with(request, expected_message, messages.SUCCESS)

        mocked_message_user.reset_mock()

        with self.subTest("Subtest there, test here; results in both there"):
            models.IssueOccurrence.objects.all().delete()

            kcidb_test.issues.set([public_issue_1])
            kcidb_testresult.issues.set([public_issue_2])

            request = _prepare_post(data={"target_issue": public_issue_2.pk})
            queryset = modeladmin.get_queryset(request).filter(kcidb_test=kcidb_test, kcidb_testresult=None)

            result = modeladmin.move_to_another_issue(request, queryset)

            self.assertTrue(result is mocked_message_user.return_value)
            expected_message = "1 occurrences were moved to the targeted issue."
            mocked_message_user.assert_called_once_with(request, expected_message, messages.SUCCESS)
