"""Test templatags."""
import datetime
from unittest import mock

from django.conf import settings
from django.http.request import QueryDict
from django.test.utils import override_settings
from django.utils import timezone

from datawarehouse import models
from datawarehouse.templatetags import artifacts
from datawarehouse.templatetags import avatar
from datawarehouse.templatetags import getattribute
from datawarehouse.templatetags import queryset_utils
from datawarehouse.templatetags import serialize
from datawarehouse.templatetags import strutils
from datawarehouse.templatetags import styles
from datawarehouse.templatetags import timetag
from datawarehouse.templatetags import url_replace
from tests import utils


class TestQuerysetUtils(utils.TestCase):
    """Test queryset_utils templatetags."""

    def test_trim_queryset(self):
        """Test trim_queryset templatetag."""
        models.GitTree.objects.bulk_create([
            models.GitTree(name=name)
            for name in range(15)
        ])

        queryset = models.GitTree.objects.all().order_by('id')

        # Test default length: 10
        trimmed = queryset_utils.trim_queryset(queryset)
        self.assertEqual(10, len(trimmed))
        self.assertEqual(5, trimmed.trimmed_elements)
        self.assertListEqual(
            [item.name for item in trimmed],
            [str(item) for item in range(10)]
        )

        # Test length as parameter
        trimmed = queryset_utils.trim_queryset(queryset, trim_at=5)
        self.assertEqual(5, len(trimmed))
        self.assertEqual(10, trimmed.trimmed_elements)
        self.assertListEqual(
            [item.name for item in trimmed],
            [str(item) for item in range(5)]
        )


class TestUrlReplace(utils.TestCase):
    """Test url_replace templatetags."""

    def test_url_replace_none(self):
        """Test url_replace without kwargs."""
        query = QueryDict('foo=foo&bar=bar')

        context = {'request': mock.Mock(GET=query)}

        new_url = url_replace.url_replace(context)
        self.assertEqual('foo=foo&bar=bar', new_url)

    def test_url_replace(self):
        """Test url_replace with kwargs. If the value exists it should replace it."""
        query = QueryDict('foo=foo&bar=bar')

        context = {'request': mock.Mock(GET=query)}

        new_url = url_replace.url_replace(context, bar='barbar')
        self.assertEqual('foo=foo&bar=barbar', new_url)

    def test_url_add(self):
        """Test url_replace with kwargs. If the value doesn't exit it should add it."""
        query = QueryDict('foo=foo')

        context = {'request': mock.Mock(GET=query)}

        new_url = url_replace.url_replace(context, bar='bar')
        self.assertEqual('foo=foo&bar=bar', new_url)

    def test_url_remove_param(self):
        """Test url_remove_param."""
        query = QueryDict('foo=foo&bar=bar')

        context = {'request': mock.Mock(GET=query)}

        new_url = url_replace.url_remove_param(context, 'bar', 'baz')
        self.assertEqual('foo=foo', new_url)


class TestSerialize(utils.TestCase):
    """Test serialize templatetags."""

    fixtures = [
        'tests/fixtures/basic_policies.yaml',
        'tests/fixtures/basic.yaml',
        'tests/fixtures/issue_with_kcidb.yaml',
    ]

    def test_serialize(self):
        """Test serialize templatetag."""
        issue = models.Issue.objects.first()
        serialized = serialize.serialize(issue, 'IssueSerializer')

        self.assertEqual(
            serialized,
            '{'
            '"id": 3, '
            '"kind": {"id": 1, "description": "Test issue kind", "tag": "Issue"}, '
            '"description": "Issue Public 3", '
            '"ticket_url": "https://issue.public.3", '
            '"resolved": false, '
            '"resolved_at": null, '
            '"policy": null, '
            '"first_seen": "2020-06-03T15:14:57.215000+00:00", '
            '"last_seen": "2020-06-03T16:14:57.215000+00:00", '
            '"last_edited_at": "2021-01-01T01:10:20Z", '
            '"regexes": []'
            '}'
        )


class TestAvatar(utils.TestCase):
    """Test avatar templatetags."""

    @override_settings(EXTERNAL_AVATAR_URL='https://www.avatar.com/avatar')
    @override_settings(EXTERNAL_AVATAR_DEFAULT='mp')
    def test_avatar_url(self):
        """Test avatar_url does the correct request."""
        url = avatar.avatar_url('some@mail.com')
        self.assertEqual(
            'https://www.avatar.com/avatar/c23eba0c9d22b28bd441ff30d835512b7724098a6e1d77b8937a6e313dcc51da?d=mp&s=40',
            url
        )

    @override_settings(FF_EXTERNAL_AVATAR_ENABLED=False)
    def test_avatar_url_disabled(self):
        """Test avatar_url returns the default image when disabled."""
        url = avatar.avatar_url('some@mail.com')
        self.assertEqual(
            settings.DEFAULT_USER_IMAGE,
            url
        )


class TestGetAttribute(utils.TestCase):
    """Test templatetags.getattribute."""

    def test_getattribute(self):
        """Test getattribute() works as expected."""
        obj = mock.Mock(an_attribute="a value", spec=True)

        with self.subTest("Returns value from an object"):
            result = getattribute.getattribute(value=obj, arg="an_attribute")
            expected = "a value"

            self.assertEqual(result, expected)

        with self.subTest("Returns empty if object doesn't have the attr"):
            result = getattribute.getattribute(value=obj, arg="missing_attr")
            expected = ""

            self.assertEqual(result, expected)

        mocked_dict = {"a key": "its value"}
        with self.subTest("Returns value from Dict"):
            result = getattribute.getattribute(value=mocked_dict, arg="a key")
            expected = "its value"

            self.assertEqual(result, expected)

        with self.subTest("Raises on Dict's KeyError"), self.assertRaises(KeyError):
            result = getattribute.getattribute(value=mocked_dict, arg="missing key")


class TestArtifacts(utils.TestCase):
    """Test artifacts templatetags."""

    @override_settings(ARTIFACTS_DEFAULT_VALID_FOR_DAYS=10)
    def test_older_than_artifacts_default_expiry(self):
        """Test older_than_artifacts_default_expiry method."""
        test_cases = [
            (1, False),
            (9, False),
            (11, True),
            (20, True)
        ]

        for days_delta, expected in test_cases:
            timestamp = timezone.now() - datetime.timedelta(days=days_delta)
            self.assertEqual(expected, artifacts.older_than_artifacts_default_expiry(timestamp))


class TestStrUtils(utils.TestCase):
    """Test strutils templatetags."""

    def test_addstr(self):
        """Test addstr."""
        cases = [
            ('1', None, ''),
            (1, 2, '12'),
            ('1', 2, '12'),
            ('1', '2', '12')
        ]
        for str1, str2, expected in cases:
            self.assertEqual(strutils.addstr(str1, str2), expected)

    def test_git_url_trim(self):
        """Test git_url_trim."""
        cases = [
            ('https://git.kernel.org/pub/scm/linux/kernel/git/bpf/bpf-next.git',
             'https://git.kernel.org/.../bpf/bpf-next.git'),
            ('https://git.kernel.org/kernel/git/bpf/bpf-next.git',
             'https://git.kernel.org/.../bpf/bpf-next.git'),
            ('https://git.kernel.org/bpf-next.git',
             'https://git.kernel.org/bpf-next.git'),
            ('foo/bar', 'foo/bar'),
            (None, None)
        ]
        for full, expected in cases:
            self.assertEqual(strutils.git_url_trim(full), expected)

    def test_remove_prefix(self):
        """Test remove_prefix."""
        cases = [
            ('foo-bar', 'foo', 'bar'),
            ('foo--bar', 'foo', '-bar'),
            ('foo_bar', 'foo', 'bar'),
            ('foo_bar', 'foo_', 'bar'),
            ('foobar', 'foo', 'bar'),
            ('foo_bar', 'foo-', 'foo_bar'),
            ('baz-bar', 'foo', 'baz-bar'),
        ]
        for full, prefix, expected in cases:
            self.assertEqual(strutils.remove_prefix(full, prefix), expected)


class TestStyles(utils.TestCase):
    """Test styles templatetags."""

    def test_get_status_text(self):
        """Test get_status_text."""
        for status in models.ResultEnum.values:
            self.assertNotEqual(styles.get_status_text(status), "Unknown")

        self.assertEqual(styles.get_status_text(None), "Running")
        self.assertEqual(styles.get_status_text("not-exist"), "Unknown")


class TestTimetag(utils.TestCase):
    """Test templatetags.timetag."""

    def test_timetag(self):
        """Test timetag() works as expected."""
        with self.subTest("Returns '-' if given an invalid float"):
            result = timetag.timetag(timestamp='x')
            expected = "-"

            self.assertEqual(result, expected)

        with self.subTest("Returns HH:MM:SS when given seconds"):
            result = timetag.timetag('3666')
            expected = "01:01:06"

            self.assertEqual(result, expected)

        with self.subTest("Returns HH:MM:SS when given microseconds"):
            result = timetag.timetag('8109.999999')
            expected = "02:15:09"

            self.assertEqual(result, expected)
