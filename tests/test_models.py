"""Test the models module."""
import datetime
from unittest import mock

from cki_lib.kcidb import ValidationError
from cki_lib.kcidb import validate_extended_kcidb_schema
import dateutil
from django.conf import settings
from django.contrib.auth import get_user_model
from django.test.utils import override_settings
from freezegun import freeze_time

from datawarehouse import models
from datawarehouse.utils import clean_dict
from datawarehouse.utils import nested_dict_from_double_underscore
from tests import utils


class TestPatch(utils.TestCase):
    """Unit tests for the models module."""

    fixtures = (
        "tests/fixtures/basic.yaml",
        "tests/fixtures/patches.yaml",
    )

    def test_patch_gui_url(self):
        """Test that the GUI URLs are correctly provided by the model."""
        self.assertEqual(models.Patch.objects.first().gui_url, 'url 1')


class TestIssue(utils.TestCase):
    """Test issue model."""

    fixtures = [
        'tests/fixtures/basic.yaml',
        'tests/fixtures/policies_for_group_abc.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
        'tests/fixtures/issues.yaml',
    ]

    def setUp(self):
        """Set up data."""
        self.checkout = models.KCIDBCheckout.objects.get(iid=1)
        self.build_1 = models.KCIDBBuild.objects.get(iid=1)
        self.build_2 = models.KCIDBBuild.objects.get(iid=2)
        self.test_1 = models.KCIDBTest.objects.get(iid=1)
        self.test_2 = models.KCIDBTest.objects.get(iid=2)

        self.issue = models.Issue.objects.get(id=1)

    def test_checkouts_empty(self):
        """Test checkouts property."""
        self.assertEqual(0, self.issue.get_checkouts(mock.MagicMock()).count())

    def test_checkouts_direct(self):
        """Test checkouts property."""
        self.checkout.issues.set([self.issue])
        self.assertEqual(
            "redhat:public_checkout",
            self.issue.get_checkouts(mock.MagicMock()).get().id,
        )

    def test_checkouts_indirect_build(self):
        """Test checkouts property."""
        self.build_1.issues.set([self.issue])
        self.assertEqual(
            "redhat:public_checkout",
            self.issue.get_checkouts(mock.MagicMock()).get().id,
        )

        # Another build shouldn't change anything, it's already related.
        self.build_2.issues.set([self.issue])
        self.assertEqual(
            "redhat:public_checkout",
            self.issue.get_checkouts(mock.MagicMock()).get().id,
        )

    def test_checkouts_indirect_test(self):
        """Test checkouts property."""
        self.test_1.issues.set([self.issue])
        self.assertEqual(
            "redhat:public_checkout",
            self.issue.get_checkouts(mock.MagicMock()).get().id,
        )

        # Another test shouldn't change anything, it's already related.
        self.test_2.issues.set([self.issue])
        self.assertEqual(
            "redhat:public_checkout",
            self.issue.get_checkouts(mock.MagicMock()).get().id,
        )

    def test_checkouts_mixed(self):
        """Test checkouts property."""
        self.checkout.issues.set([self.issue])
        self.build_1.issues.set([self.issue])
        self.test_1.issues.set([self.issue])
        self.assertEqual(
            "redhat:public_checkout",
            self.issue.get_checkouts(mock.MagicMock()).get().id,
        )

    def test_created_edited_timestamps(self):
        """Test created_at and last_edited_at timestamps are correctly set."""
        created_at = datetime.datetime(2010, 1, 2, 9, 0, tzinfo=dateutil.tz.UTC)
        edited_at = datetime.datetime(2010, 1, 2, 9, 1, tzinfo=dateutil.tz.UTC)

        with freeze_time(created_at):
            # Create new copy of an existing issue
            issue = models.Issue.objects.last()
            issue.id = None
            issue.ticket_url += '2'
            issue.save()

        self.assertEqual(created_at, issue.created_at)
        self.assertEqual(created_at, issue.last_edited_at)

        with freeze_time(edited_at):
            issue.save()

        self.assertEqual(created_at, issue.created_at)
        self.assertEqual(edited_at, issue.last_edited_at)


class TestIssueOccurrence(utils.TestCase):
    """Test model IssueOccurrence."""

    fixtures = (
        "tests/fixtures/basic_policies.yaml",
        "tests/fixtures/basic.yaml",
        "tests/fixtures/issues_all_basic_policies.yaml",
        "tests/fixtures/multiple_issue_occurrences.yaml",
    )

    def test__str__(self):
        """Test IssueOccurrence.__str__()."""
        with self.subTest("KCIDBCheckout"):
            checkout_occurrence = models.IssueOccurrence.objects.filter(kcidb_checkout__isnull=False).first()
            self.assertEqual(
                str(checkout_occurrence),
                "1 - <Issue: Issue Public> - <KCIDBCheckout: redhat:public_checkout_2>",
            )

        with self.subTest("KCIDBBuild"):
            build_occurrence = models.IssueOccurrence.objects.filter(kcidb_build__isnull=False).first()
            self.assertEqual(
                str(build_occurrence), "4 - <Issue: Issue Unavailable> - <KCIDBBuild: redhat:public_build_2>"
            )

        with self.subTest("KCIDBTest"):
            test_occurrence = models.IssueOccurrence.objects.filter(kcidb_test__isnull=False).first()
            self.assertEqual(
                str(test_occurrence), "3 - <Issue: Issue Unavailable> - <KCIDBTest: redhat:public_test_1>"
            )

        with self.subTest("KCIDBTestResult"):
            testresult_occurrence = models.IssueOccurrence.objects.filter(kcidb_testresult__isnull=False).first()
            self.assertEqual(
                str(testresult_occurrence), "5 - <Issue: Issue Public> - <KCIDBTestResult: redhat:public_testresult_1>"
            )

    def test_kcidb_object(self):
        """Test property IssueOccurrence.kcidb_object."""
        with self.subTest("KCIDBCheckout"):
            checkout_occurrence = models.IssueOccurrence.objects.filter(kcidb_checkout__isnull=False).first()
            self.assertEqual(checkout_occurrence.kcidb_object, checkout_occurrence.kcidb_checkout)

        with self.subTest("KCIDBBuild"):
            build_occurrence = models.IssueOccurrence.objects.filter(kcidb_build__isnull=False).first()
            self.assertEqual(build_occurrence.kcidb_object, build_occurrence.kcidb_build)

        with self.subTest("KCIDBTest"):
            test_occurrence = models.IssueOccurrence.objects.filter(kcidb_test__isnull=False).first()
            self.assertEqual(test_occurrence.kcidb_object, test_occurrence.kcidb_test)

        with self.subTest("KCIDBTestResult"):
            testresult_occurrence = models.IssueOccurrence.objects.filter(kcidb_testresult__isnull=False).first()
            self.assertEqual(testresult_occurrence.kcidb_object, testresult_occurrence.kcidb_testresult)

    def test_related_test(self):
        """Test property IssueOccurrence.related_test."""
        with self.subTest("KCIDBTest"):
            test_occurrence = models.IssueOccurrence.objects.filter(kcidb_test__isnull=False).first()
            self.assertEqual(test_occurrence.related_test, test_occurrence.kcidb_test)

        with self.subTest("KCIDBTestResult"):
            testresult_occurrence = models.IssueOccurrence.objects.filter(kcidb_testresult__isnull=False).first()
            self.assertEqual(testresult_occurrence.related_test, testresult_occurrence.kcidb_testresult.test)


class TestIssueRegex(utils.TestCase):
    """Test IssueRegex model."""

    fixtures = [
        'tests/fixtures/policies_for_group_abc.yaml',
        'tests/fixtures/basic.yaml',
        'tests/fixtures/issues.yaml',
        'tests/fixtures/issue_regexes.yaml',
    ]

    def test_created_edited_timestamps(self):
        """Test created_at and last_edited_at timestamps are correctly set."""
        created_at = datetime.datetime(2010, 1, 2, 9, 0, tzinfo=dateutil.tz.UTC)
        edited_at = datetime.datetime(2010, 1, 2, 9, 1, tzinfo=dateutil.tz.UTC)

        with freeze_time(created_at):
            # Create new copy of an existing issue_regex
            issue_regex = models.IssueRegex.objects.last()
            issue_regex.id = None
            issue_regex.save()

        self.assertEqual(created_at, issue_regex.created_at)
        self.assertEqual(created_at, issue_regex.last_edited_at)

        with freeze_time(edited_at):
            issue_regex.save()

        self.assertEqual(created_at, issue_regex.created_at)
        self.assertEqual(edited_at, issue_regex.last_edited_at)


class TestTestMaintainer(utils.TestCase):
    """Test TestMaintainer model."""

    def test_str(self):
        """Test TestMaintainer.__str__()"""
        maintainer = models.TestMaintainer(
            name="Cosme Fulanito",
            email="cosme@fulanito.com",
        )
        self.assertEqual(str(maintainer), "Cosme Fulanito <cosme@fulanito.com>")

    def test_create_from_dict(self):
        """Test create_from_address method."""
        test_cases = [
            {'name': 'Cosme Fulanito', 'email': 'cosme@fulanito.com', 'gitlab': 'cosme'},
            {'name': 'Cosme Fulanito', 'email': 'cosme@fulanito.com', 'gitlab': None},
        ]

        for data in test_cases:
            maintainer = models.TestMaintainer.create_from_dict(data)
            self.assertEqual(maintainer.email, data.get('email'))
            self.assertEqual(maintainer.name, data.get('name'))
            self.assertEqual(maintainer.gitlab_username, data.get('gitlab'))

    def test_create_from_dict_update(self):
        """Test create_from_dict, with changes so it saves."""
        models.TestMaintainer.objects.create(name='foo', email='foo@bar.com')
        with mock.patch('datawarehouse.models.test_models.TestMaintainer.save') as save_mock:
            models.TestMaintainer.create_from_dict({
                'name': 'foo',
                'email': 'foo@bar.com',
                'gitlab': 'username'
            })
            self.assertTrue(save_mock.called)

    def test_create_from_dict_no_update(self):
        """Test create_from_dict, no changes no save."""
        models.TestMaintainer.objects.create(name='foo', email='foo@bar.com')
        with mock.patch('datawarehouse.models.test_models.TestMaintainer.save') as save_mock:
            models.TestMaintainer.create_from_dict({
                'name': 'foo',
                'email': 'foo@bar.com',
                'gitlab': None,
            })
            self.assertFalse(save_mock.called)

    @override_settings(GITLAB_URL='https://gitlab.com')
    def test_gitlab_profile_url(self):
        """Test gitlab_profile_url property."""
        maintainer = models.TestMaintainer.objects.create(
            name='Some Name',
            email='some@email.com',
            gitlab_username='foobar',
        )
        self.assertEqual('https://gitlab.com/foobar', maintainer.gitlab_profile_url)

    @override_settings(GITLAB_URL='https://gitlab.com')
    def test_gitlab_profile_url_no_username(self):
        """Test gitlab_profile_url property when maintainer has no username."""
        maintainer = models.TestMaintainer.objects.create(
            name='Some Name',
            email='some@email.com',
            gitlab_username=None
        )
        self.assertEqual(None, maintainer.gitlab_profile_url)

    @override_settings(GITLAB_URL=None)
    def test_gitlab_profile_url_no_gitlab_url(self):
        """Test gitlab_profile_url property when GITLAB_URL is not set."""
        maintainer = models.TestMaintainer.objects.create(
            name='Some Name',
            email='some@email.com',
            gitlab_username='foobar',
        )
        self.assertEqual(None, maintainer.gitlab_profile_url)


class TestTest(utils.TestCase):
    """Test Test model."""

    def test_set_maintainers(self):
        """Test set_maintainers method."""
        test = models.Test.objects.create(name='some test')
        test.set_maintainers([
            {'name': 'Some One', 'email': 'some@one.com'},
            {'name': 'Some Other', 'email': 'some@other.com'}
        ])

        self.assertEqual(
            sorted([('Some One', 'some@one.com'), ('Some Other', 'some@other.com')]),
            sorted([(m.name, m.email) for m in test.maintainers.all()])
        )

    def test_get_and_update(self):
        """Test get_and_update method creates object."""
        with mock.patch('datawarehouse.models.Test.set_maintainers') as set_maintainers:
            test = models.Test.get_and_update(
                name='test_name', universal_id='test_path', fetch_url='http://test.url'
            )
            self.assertEqual('test_name', test.name)
            self.assertEqual('test_path', test.universal_id)
            self.assertEqual('http://test.url', test.fetch_url)
            self.assertFalse(set_maintainers.called)

    def test_get_and_update_updates(self):
        """Test get_and_update method updates fields."""
        with mock.patch('datawarehouse.models.Test.set_maintainers') as set_maintainers:
            test = models.Test.get_and_update(
                name='test_name', universal_id=None, fetch_url=None
            )
            self.assertEqual('test_name', test.name)
            self.assertEqual(None, test.universal_id)
            self.assertEqual(None, test.fetch_url)
            self.assertFalse(set_maintainers.called)

            # Update it
            test_updated = models.Test.get_and_update(
                name='test_name',
                universal_id='test_path',
                fetch_url='http://test.url',
                maintainers=[
                    {'name': 'Some One', 'email': 'some@one.com'},
                ]
            )
            self.assertEqual(test.id, test_updated.id)
            self.assertEqual('test_name', test_updated.name)
            self.assertEqual('test_path', test_updated.universal_id)
            self.assertEqual('http://test.url', test_updated.fetch_url)

            set_maintainers.assert_called_with(
                [{'name': 'Some One', 'email': 'some@one.com'}]
            )

    def test_get_and_update_with_maintainers(self):
        """Test get_and_update method."""
        with mock.patch('datawarehouse.models.Test.set_maintainers') as set_maintainers:
            test = models.Test.get_and_update(
                name='test_name',
                universal_id='test_path',
                fetch_url='http://test.url',
                maintainers=[
                    {'name': 'Some One', 'email': 'some@one.com'},
                ]
            )
            self.assertEqual('test_name', test.name)
            self.assertEqual('test_path', test.universal_id)

            set_maintainers.assert_called_with(
                [{'name': 'Some One', 'email': 'some@one.com'}]
            )

    def test_get_and_update_doesnt_clear(self):
        """
        Test get_and_update method updates fields.

        When a None value is passed as parameter, it won't override the current values.
        """
        test = models.Test.get_and_update(
            name='test_name', universal_id='test_path', fetch_url='http://test.url'
        )
        self.assertEqual('test_name', test.name)
        self.assertEqual('test_path', test.universal_id)
        self.assertEqual('http://test.url', test.fetch_url)

        # Update it
        test_updated = models.Test.get_and_update(
            name='test_name',
            universal_id=None,
            fetch_url=None,
        )
        self.assertEqual(test.id, test_updated.id)
        self.assertEqual('test_name', test_updated.name)
        self.assertEqual('test_path', test_updated.universal_id)
        self.assertEqual('http://test.url', test_updated.fetch_url)

    def test_get_and_update_changed(self):
        """Test get_and_update method detects changes correctly."""
        cases = [
            # No changes. Some are the same, the others None.
            (None,      None,       False),
            ('path',    None,       False),
            (None,      'url',      False),
            ('path',    'url',      False),

            # One or both changed.
            ('path_2',  'url',      True),
            ('path',    'url_2',    True),
            ('path_2',  None,       True),
            (None,      'url_2',    True),
        ]
        models.Test.get_and_update(
            name='test_name', universal_id='path', fetch_url='url'
        )

        for universal_id, fetch_url, changed in cases:
            with mock.patch('datawarehouse.models.Test.save') as mock_save:
                models.Test.get_and_update(
                    name='test_name', universal_id=universal_id, fetch_url=fetch_url,
                )
                self.assertEqual(mock_save.called, changed)


@override_settings(ARTIFACTS_DEFAULT_VALID_FOR_DAYS=123)
@override_settings(ARTIFACTS_KNOWN_VALID_FOR={'some.s3.host/BUCKET-NAME': 99})
class TestArtifact(utils.TestCase):
    """Test Artifact class."""

    def test_get_valid_for(self):
        # pylint: disable=protected-access
        """Test _get_valid_for method."""
        self.assertEqual(
            99, models.Artifact._get_valid_for('some.s3.host/BUCKET-NAME')
        )
        self.assertEqual(
            123, models.Artifact._get_valid_for('some.s3.host/OTHER-BUCKET-NAME')
        )

    def test_save_calculate_valid_for(self):
        """Test save sets the correct valid_for value."""
        artifact = models.Artifact.objects.create(
            name='test_file',
            url='https://some.s3.host/BUCKET-NAME/some/path/to/the/file.log'
        )
        self.assertEqual(99, artifact.valid_for)

        # Unknown url host
        artifact = models.Artifact.objects.create(
            name='test_file',
            url='https://some.other.s3.host/BUCKET-NAME/some/path/to/the/file.log'
        )
        self.assertEqual(123, artifact.valid_for)

    def test_save_calculate_expiry_date(self):
        """Test save sets the correct expiry date."""
        created_at = datetime.datetime(2010, 1, 1, 10, 0, tzinfo=dateutil.tz.UTC)

        with freeze_time(created_at):
            artifact = models.Artifact.objects.create(
                name='test_file',
                url='https://some.s3.host/BUCKET-NAME/some/path/to/the/file.log'
            )

        self.assertEqual(created_at + datetime.timedelta(days=99), artifact.expiry_date)

    def test_save_set_valid_for(self):
        """Test save sets the correct expiry date when valid_for is set."""
        created_at = datetime.datetime(2010, 1, 1, 10, 0, tzinfo=dateutil.tz.UTC)

        with freeze_time(created_at):
            artifact = models.Artifact.objects.create(
                name='test_file',
                url='https://some.s3.host/BUCKET-NAME/some/path/to/the/file.log',
                valid_for=10,
            )

        self.assertEqual(10, artifact.valid_for)
        self.assertEqual(created_at + datetime.timedelta(days=10), artifact.expiry_date)

    def test_create_from_json(self):
        """Test create_from_json."""
        artifact = models.Artifact.create_from_json({
            'name': 'test_file',
            'url': 'https://some.s3.host/BUCKET-NAME/some/path/to/the/file.log',
        })

        self.assertEqual(artifact.name, 'test_file')
        self.assertEqual(
            artifact.url,
            'https://some.s3.host/BUCKET-NAME/some/path/to/the/file.log',
        )

    def test_create_from_json_names(self):
        """Test create_from_json for different names."""
        data = {
            'name': 'test_file',
            'url': 'https://some.s3.host/BUCKET-NAME/some/path/to/the/file.log',
        }

        artifact = models.Artifact.create_from_json(data)
        self.assertEqual(models.Artifact.create_from_json(data).id, artifact.id)

        data['name'] = 'test_other_file'
        self.assertNotEqual(models.Artifact.create_from_json(data).id, artifact.id)

    def test_create_names(self):
        """Test creating artifacts with automatic names."""
        artifact = models.Artifact(url='https://some.s3.host/BUCKET-NAME/some/path/to/the/file.log')
        artifact.save()
        self.assertEqual(artifact.name, 'file.log')

    def test_create_or_update(self):
        """Test update_or_create method of artifact."""
        url = 'https://some.s3.host/BUCKET-NAME/some/path/to/the/file.log'
        artifact, created = models.Artifact.objects.update_or_create(url=url)
        self.assertTrue(created, "Artifact should be created")
        self.assertEqual(artifact.name, 'file.log')

        artifact, created = models.Artifact.objects.update_or_create(url=url, defaults={'name': 'new.log'})
        self.assertFalse(created, "Artifact should not be created")
        self.assertEqual(artifact.name, 'new.log')

    def test_str_dunder(self):
        """Test __str__ method of Artifact."""
        artifact = models.Artifact(url='https://some.s3.host/BUCKET-NAME/some/path/to/the/file.log')
        artifact.save()

        self.assertEqual(str(artifact), 'file.log')


class TestStampedModel(utils.TestCase):
    """Test StampedModel attributes."""

    def test_created_stamp(self):
        """Test created_at and created_by are correctly populated."""
        created_at = datetime.datetime(2010, 1, 2, 9, 0, tzinfo=dateutil.tz.UTC)
        test_user = get_user_model().objects.create(username='test_1', email='test1@test.com')

        with freeze_time(created_at):
            issue = models.Issue.objects.create(
                ticket_url="http://foo",
                created_by=test_user,
                last_edited_by=test_user,
            )

        self.assertEqual(created_at, issue.created_at)
        self.assertEqual(test_user, issue.created_by)
        self.assertEqual(created_at, issue.last_edited_at)
        self.assertEqual(test_user, issue.last_edited_by)

    def test_edited_stamp(self):
        """Test edited_at and edited_by are correctly populated."""
        test_user_1 = get_user_model().objects.create(username='test_1', email='test1@test.com')
        test_user_2 = get_user_model().objects.create(username='test_2', email='test2@test.com')
        created_at = datetime.datetime(2010, 1, 2, 9, 0, tzinfo=dateutil.tz.UTC)
        edited_at = datetime.datetime(2010, 1, 2, 10, 0, tzinfo=dateutil.tz.UTC)

        with freeze_time(created_at):
            # Create new copy of an existing issue
            issue = models.Issue.objects.create(
                ticket_url="http://foo",
                created_by=test_user_1,
                last_edited_by=test_user_1,
            )

        with self.subTest("Assert was_edited is false after creation"):
            self.assertFalse(issue.was_edited)

        with freeze_time(edited_at):
            issue.last_edited_by = test_user_2
            issue.save()

        with self.subTest("Assert timestamps are kept/updated as expected."):
            self.assertEqual(created_at, issue.created_at)
            self.assertEqual(test_user_1, issue.created_by)
            self.assertEqual(edited_at, issue.last_edited_at)
            self.assertEqual(test_user_2, issue.last_edited_by)

        with self.subTest("Assert was_edited becomes true after update"):
            self.assertTrue(issue.was_edited)


class TestBeakerResource(utils.TestCase):
    """Test BeakerResource attributes and methods."""

    def test_natural_key(self):
        """Test BeakerResource.natural_key works as expected."""
        fqdn = "dummy"
        expected = (fqdn, )

        beaker_resource = models.BeakerResource.objects.create(fqdn=fqdn)
        result = beaker_resource.natural_key()
        self.assertEqual(expected, result)

    def test__str__(self):
        """Test BeakerResource.__str__ works as expected."""
        fqdn = "dummy"
        beaker_resource = models.BeakerResource.objects.create(fqdn=fqdn)

        self.assertEqual(fqdn, str(beaker_resource))

    def test_create_from_string(self):
        """Test BeakerResource.create_from_string works as expected."""
        fqdn = "dummy"

        self.assertFalse(models.BeakerResource.objects.all().exists(), "Precondition: Expected no BeakerResource.")

        with self.subTest("Assert it creates a new record if it doesn't exist"):
            # SELECT, SAVEPOINT, INSERT, RELEASE SAVEPOINT
            with self.assertNumQueries(4):
                result = models.BeakerResource.create_from_string(fqdn=fqdn)

            self.assertEqual(result.fqdn, fqdn)

        expected = result
        with self.subTest("Assert it returns existing record."):
            # Only SELECT
            with self.assertNumQueries(1):
                result = models.BeakerResource.create_from_string(fqdn=fqdn)

            self.assertEqual(expected.id, result.id)
            self.assertEqual(expected.fqdn, result.fqdn)


class TestCompiler(utils.TestCase):
    """Test methods for the Compiler model."""

    def test__str__(self):
        """Test Compiler.__str__ works as expected."""
        expected_name = "GCC v1"

        compiler = models.Compiler.objects.create(name=expected_name)

        self.assertEqual(str(compiler), expected_name)

    def test_create_from_string(self):
        """Test Compiler.create_from_string works as expected."""
        self.assertFalse(models.Compiler.objects.all().exists(), "Precondition: Expected no Compiler.")

        compiler_name = "GCC v1"

        with self.subTest("Assert it creates a new record if it doesn't exist"):
            # SELECT, SAVEPOINT, INSERT, RELEASE SAVEPOINT
            with self.assertNumQueries(4):
                result = models.Compiler.create_from_string(compiler_name)

            self.assertEqual(result.name, compiler_name)

        expected = result
        with self.subTest("Assert it returns existing record."):
            # Only SELECT
            with self.assertNumQueries(1):
                result = models.Compiler.create_from_string(compiler_name)

            self.assertEqual(expected.id, result.id)
            self.assertEqual(expected.name, result.name)


class TestIssueQuerySet(utils.TestCase):
    """Test IssueQuerySet works as expected."""

    fixtures = (
        "tests/fixtures/basic_policies.yaml",
        "tests/fixtures/basic.yaml",
        "tests/fixtures/issues_all_basic_policies.yaml",
        "tests/fixtures/multiple_issue_occurrences.yaml",
    )

    def test_kcidb_values(self):
        """Test IssueQuerySet.kcidb_values()."""
        queryset = models.Issue.objects.kcidb_values()

        with self.subTest("Query performance"), self.assertNumQueries(1):
            result = list(queryset)

        with self.subTest("Expected result"):
            expected = [
                {
                    "id": "redhat:issue_8",
                    "origin": "redhat",
                    "comment": "Issue Retrigger 2",
                    "report_url": "https://issue.retrigger.2",
                    "version": 1609463420,
                    "culprit": {"code": False, "tool": False, "harness": True},
                    "misc__is_public": False,
                    "misc__kcidb__version__major": settings.PRODUCER_KCIDB_SCHEMA.major,
                    "misc__kcidb__version__minor": settings.PRODUCER_KCIDB_SCHEMA.minor,
                },
                {
                    "id": "redhat:issue_7",
                    "origin": "redhat",
                    "comment": "Issue Retrigger",
                    "report_url": "https://issue.retrigger",
                    "version": 1609463420,
                    "culprit": {"code": False, "tool": True, "harness": False},
                    "misc__is_public": False,
                    "misc__kcidb__version__major": settings.PRODUCER_KCIDB_SCHEMA.major,
                    "misc__kcidb__version__minor": settings.PRODUCER_KCIDB_SCHEMA.minor,
                },
                {
                    "id": "redhat:issue_6",
                    "origin": "redhat",
                    "comment": "Issue Unavailable 2",
                    "report_url": "https://issue.unavailable.2",
                    "version": 1609463420,
                    "culprit": {"code": False, "tool": False, "harness": True},
                    "misc__is_public": False,
                    "misc__kcidb__version__major": settings.PRODUCER_KCIDB_SCHEMA.major,
                    "misc__kcidb__version__minor": settings.PRODUCER_KCIDB_SCHEMA.minor,
                },
                {
                    "id": "redhat:issue_5",
                    "origin": "redhat",
                    "comment": "Issue Internal 2",
                    "report_url": "https://issue.internal.2",
                    "version": 1609463420,
                    "culprit": {"code": False, "tool": False, "harness": True},
                    "misc__is_public": False,
                    "misc__kcidb__version__major": settings.PRODUCER_KCIDB_SCHEMA.major,
                    "misc__kcidb__version__minor": settings.PRODUCER_KCIDB_SCHEMA.minor,
                },
                {
                    "id": "redhat:issue_4",
                    "origin": "redhat",
                    "comment": "Issue Public 2",
                    "report_url": "https://issue.public.2",
                    "version": 1609463420,
                    "culprit": None,
                    "misc__is_public": True,
                    "misc__kcidb__version__major": settings.PRODUCER_KCIDB_SCHEMA.major,
                    "misc__kcidb__version__minor": settings.PRODUCER_KCIDB_SCHEMA.minor,
                },
                {
                    "id": "redhat:issue_3",
                    "origin": "redhat",
                    "comment": "Issue Unavailable",
                    "report_url": "https://issue.unavailable",
                    "version": 1609463420,
                    "culprit": {"code": False, "tool": True, "harness": False},
                    "misc__is_public": False,
                    "misc__kcidb__version__major": settings.PRODUCER_KCIDB_SCHEMA.major,
                    "misc__kcidb__version__minor": settings.PRODUCER_KCIDB_SCHEMA.minor,
                },
                {
                    "id": "redhat:issue_2",
                    "origin": "redhat",
                    "comment": "Issue Internal",
                    "report_url": "https://issue.internal",
                    "version": 1609463420,
                    "culprit": {"code": False, "tool": False, "harness": True},
                    "misc__is_public": False,
                    "misc__kcidb__version__major": settings.PRODUCER_KCIDB_SCHEMA.major,
                    "misc__kcidb__version__minor": settings.PRODUCER_KCIDB_SCHEMA.minor,
                },
                {
                    "id": "redhat:issue_1",
                    "origin": "redhat",
                    "comment": "Issue Public",
                    "report_url": "https://issue.public",
                    "version": 1609463420,
                    "culprit": {"code": True, "tool": False, "harness": False},
                    "misc__is_public": True,
                    "misc__kcidb__version__major": settings.PRODUCER_KCIDB_SCHEMA.major,
                    "misc__kcidb__version__minor": settings.PRODUCER_KCIDB_SCHEMA.minor,
                },
            ]
            self.assertQuerySetEqual(queryset, expected)

        with self.subTest("Valid KCIDB format"):
            try:
                data = {
                    "version": {
                        "major": settings.PRODUCER_KCIDB_SCHEMA.major,
                        "minor": settings.PRODUCER_KCIDB_SCHEMA.minor,
                    },
                    # NOTE: pass through helpers to make it valid KCIDB data
                    "issues": [nested_dict_from_double_underscore(clean_dict(kcidb_issue)) for kcidb_issue in result],
                }
                validate_extended_kcidb_schema(data)
            except ValidationError as validation_error:
                raise AssertionError("Failed to validate against the KCIDB schema.", validation_error) from None


class TestIssueOccurrenceQuerySet(utils.TestCase):
    """Test IssueOccurrenceQuerySet works as expected."""

    fixtures = (
        "tests/fixtures/basic_policies.yaml",
        "tests/fixtures/basic.yaml",
        "tests/fixtures/issues_all_basic_policies.yaml",
        "tests/fixtures/multiple_issue_occurrences.yaml",
    )

    def test_kcidb_values(self):
        """Test IssueOccurrenceQuerySet.kcidb_values()."""
        queryset = models.IssueOccurrence.objects.kcidb_values()

        with self.subTest("Query performance"), self.assertNumQueries(1):
            result = list(queryset)

        with self.subTest("Expected result"):
            expected = [
                {
                    "id": "redhat:incident_5",
                    "origin": "redhat",
                    "issue_id": "redhat:issue_1",
                    "issue_version": 1609463420,
                    "build_id": None,
                    "test_id": "redhat:public_test_2",
                    "present": True,
                    "misc__is_public": True,
                    "misc__kcidb__version__major": settings.PRODUCER_KCIDB_SCHEMA.major,
                    "misc__kcidb__version__minor": settings.PRODUCER_KCIDB_SCHEMA.minor,
                },
                {
                    "id": "redhat:incident_4",
                    "origin": "redhat",
                    "issue_id": "redhat:issue_3",
                    "issue_version": 1609463420,
                    "build_id": "redhat:public_build_2",
                    "test_id": None,
                    "present": True,
                    "misc__is_public": False,
                    "misc__kcidb__version__major": settings.PRODUCER_KCIDB_SCHEMA.major,
                    "misc__kcidb__version__minor": settings.PRODUCER_KCIDB_SCHEMA.minor,
                },
                {
                    "id": "redhat:incident_3",
                    "origin": "redhat",
                    "issue_id": "redhat:issue_3",
                    "issue_version": 1609463420,
                    "build_id": None,
                    "test_id": "redhat:public_test_1",
                    "present": True,
                    "misc__is_public": False,
                    "misc__kcidb__version__major": settings.PRODUCER_KCIDB_SCHEMA.major,
                    "misc__kcidb__version__minor": settings.PRODUCER_KCIDB_SCHEMA.minor,
                },
                {
                    "id": "redhat:incident_2",
                    "origin": "redhat",
                    "issue_id": "redhat:issue_4",
                    "issue_version": 1609463420,
                    "build_id": "redhat:public_build_2",
                    "test_id": None,
                    "present": True,
                    "misc__is_public": True,
                    "misc__kcidb__version__major": settings.PRODUCER_KCIDB_SCHEMA.major,
                    "misc__kcidb__version__minor": settings.PRODUCER_KCIDB_SCHEMA.minor,
                },
            ]
            self.assertQuerySetEqual(queryset, expected)

        with self.subTest("Valid KCIDB format"):
            try:
                data = {
                    "version": {
                        "major": settings.PRODUCER_KCIDB_SCHEMA.major,
                        "minor": settings.PRODUCER_KCIDB_SCHEMA.minor,
                    },
                    # NOTE: pass through helpers to make it valid KCIDB data
                    "incidents": [
                        nested_dict_from_double_underscore(clean_dict(kcidb_incident)) for kcidb_incident in result
                    ],
                }
                validate_extended_kcidb_schema(data)
            except ValidationError as validation_error:
                raise AssertionError("Failed to validate against the KCIDB schema.", validation_error) from None
